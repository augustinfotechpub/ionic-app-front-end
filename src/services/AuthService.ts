import { isPlatform } from '@ionic/react';
import { App } from '@capacitor/app';
import { AuthService } from 'ionic-appauth';
import { CapacitorBrowser, CapacitorSecureStorage } from 'ionic-appauth/lib/capacitor';

import { AxiosRequestor } from './AxiosService';


export class Auth  {

  private static authService : AuthService | undefined;

  private static buildAuthInstance() {
    const authService = new AuthService(new CapacitorBrowser(), new CapacitorSecureStorage(), new AxiosRequestor());
    authService.authConfig = {
      client_id: '228530987063-3m94fsab35094hias3l4f3las38n40m9.apps.googleusercontent.com', // AHA! These are the values I need to change
      server_host: 'http://dev-18360996.okta.com',
      redirect_url: isPlatform('capacitor') ? 'com.appauth.demo://callback' : window.location.origin + '/loginredirect',
      end_session_redirect_url: isPlatform('capacitor') ?  'com.appauth.demo://endSession' : window.location.origin + '/endredirect',
      scopes: 'openid offline_access',
      pkce: true
    }

    console.log("SHI==="+isPlatform('capacitor')+"===");

    if (isPlatform('capacitor')) {
      console.log("applistenercreated");
      App.addListener('appUrlOpen', (data: any) => {
        if ((data.url).indexOf(authService.authConfig.redirect_url) === 0) {
          authService.authorizationCallback(data.url);
        }else{
            authService.endSessionCallback();
        }
      });
    }
    
    authService.init();

    console.log(authService);

    return authService;
  }

  public static get Instance() : AuthService {
    if (!this.authService) {
      this.authService = this.buildAuthInstance();
    }

    return this.authService;
  }
}
