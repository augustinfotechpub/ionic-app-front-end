import React, { useState } from 'react';
import { useTranslation } from "react-i18next";

import { 
    IonButton, 
    IonList,
    IonIcon,
    IonPopover,
    IonItem,
} from '@ionic/react';

import { ellipsisVerticalSharp } from "ionicons/icons";

import '../assets/css/Custom.css';
import '../assets/css/Responsive.css';

const ManagePropertyMoreMenu: React.FC<{ prop_id: string; onEditProp: () => void; 
            ontoggleArchived: () => void; ArchivedStr: string;
            onPermentRemove: () => void; RemoveStr: string;
            onFavoriteClick: () => void; FavoriteStr: string; }> = props => {

    const { t } = useTranslation();
    const [popoverState, setShowPopover] = useState<{showPopover: boolean, event: Event | undefined}>({ showPopover: false, event: undefined });

    if (!props.prop_id) {
        return null
    }

    return (
        <div>
            <IonPopover
                cssClass='manage-property-more-menu'
                mode="ios"
                event={popoverState.event}
                isOpen={popoverState.showPopover}
                onDidDismiss={() => setShowPopover({ showPopover: false, event: undefined })}
            >   
                <IonList>
                    <IonItem>
                        <IonButton fill="clear" 
                          onClick={props.onEditProp} >
                            {t('General-Modify')}
                        </IonButton>
                    </IonItem>

                    <IonItem>
                        <IonButton fill="clear" 
                          onClick={props.onFavoriteClick} >
                            {props.FavoriteStr}
                        </IonButton>
                    </IonItem>

                    <IonItem>
                        <IonButton fill="clear"  color="danger"
                          onClick={props.ontoggleArchived} >
                            {props.ArchivedStr}
                        </IonButton>
                    </IonItem>

                    {props.RemoveStr != '' ? (
                        <IonItem>
                            <IonButton fill="clear"  color="danger"
                              onClick={props.onPermentRemove} >
                                {t('Prop-Perment-Remove')}
                            </IonButton>
                        </IonItem>
                    ) : ('')}
                </IonList>
            </IonPopover>

            <IonButton 
            className="manage-property-more-menu-button" 
            fill="clear" 
            onClick={
                (e) => {
                    setShowPopover({ showPopover: true, event: e.nativeEvent })
                }}
            >
                <IonIcon slot="icon-only" icon={ellipsisVerticalSharp} />
            </IonButton>
        </div>
    );
};

export default ManagePropertyMoreMenu;