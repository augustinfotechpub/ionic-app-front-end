import React, {useState, useEffect, useCallback} from "react";
import {IonCol} from '@ionic/react';

import '../assets/css/Custom.css';
import '../assets/css/Responsive.css';

import ManagementMenu from '../components/ManagementMenu';
import TenantMenu from '../components/TenantMenu';

const DashboardSidebar: React.FC<{path: string}> = ({path}) => {

    const [UserTypeData, setUserTypeData] = useState<any>({'id': '', 'name': '', 'lease_code': ''});
    const [FeatureAccessData, setFeatureAccessData] = useState<any>({'manage_posts': '', 'manage_propertys': '', 'manage_rents': ''});

    useEffect(() => {
        if ((localStorage.getItem('user_type_id')!==null)) {
            setUserTypeData({'id': localStorage.getItem('user_type_id'), 'name': localStorage.getItem('user_type_name'), 'lease_code': localStorage.getItem('user_code')});
            setFeatureAccessData({'manage_posts': localStorage.getItem('manage_posts'), 'manage_propertys': localStorage.getItem('manage_propertys'), 'manage_rents': localStorage.getItem('manage_rents')});
        }
    }, []);
    
    return (
        <IonCol className="dashboard-sidebar" size="12" sizeMd="12" sizeLg="3" sizeXl="2">
            <p className="ion-hide-lg-up">What would you like to do today?</p>
            
            {UserTypeData.id == '2' ? (
                <ManagementMenu UserTypeData={UserTypeData} FeatureAccessData={FeatureAccessData} path={path} />
            ) : ('')}

            {UserTypeData.id == '3' ? (
                <ManagementMenu UserTypeData={UserTypeData} FeatureAccessData={FeatureAccessData} path={path} />
            ) : ('')}

            {UserTypeData.id == '1' ? (
                <TenantMenu UserTypeData={UserTypeData} path={path} />
            ) : ('')}
        </IonCol>
    );
};

export default DashboardSidebar;