import React, { useRef, useState } from 'react';
import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";

import { 
    IonRow,
    IonCol,
    IonIcon,
    IonButton
} from '@ionic/react';

import { caretDownOutline, createOutline } from "ionicons/icons";
import { Routes } from "../App";

interface AccordionProps {
    id: React.ReactNode,
    filestoragekey: React.ReactNode,
    title: React.ReactNode,
    content: React.ReactNode,
    date: React.ReactNode,
    attachFile: [{"file_name": ""}],
}

export const Accordion: React.FC<AccordionProps> = ({ id, filestoragekey, title, content, date, attachFile }) => {
    const { t } = useTranslation();
	const [active, setActive] = useState(false)
    const [height, setHeight] = useState('0px')
    const [rotate, setRotate] = useState('transform duration-700 ease')

    let history = useHistory();

    const contentSpace = useRef(null)

    function toggleAccordion() {
        setActive(active === false ? true : false)
        // setActive((prevState) => !prevState)
        // @ts-ignore
        setHeight(active ? '0px' : `${contentSpace.current.scrollHeight}px`)
        setRotate(active ? 'transform duration-700 ease' : 'transform duration-700 ease rotate-180')
    }

    const onEditClick = async (path, postID, file_storagekey) => {
        history.push({
            pathname: path+"/"+postID,
            state: { path: path, postID: postID, filestoragekey: file_storagekey }
        });

        /* history.push({
            pathname: path,
            state: { path: path, postID: postID, filestoragekey: file_storagekey }
        }); */
    };

    return (
        <div className={ active ? 'accordion-item active' : 'accordion-item'}>
            <IonRow className="accordion-button ion-align-items-center">
                <IonCol className="accordion-edit-button">
                    <IonButton fill="clear" onClick={() => onEditClick(Routes.editPost, id, filestoragekey)} >
                        <IonIcon icon={createOutline} />
                    </IonButton>
                </IonCol>
                <IonCol className="accordion-click-button">
                    <IonButton
                        className=""
                        expand="block" 
                        fill="clear"
                        onClick={toggleAccordion}
                    >
                        <div className="accordion-click-button-inner">
                            <p className="inline-block text-footnote light">{title}</p>
                            <IonIcon className="accordion-arrow" />
                        </div>
                    </IonButton>
                    <span className="post-date">{t('POST-Last-Modified')}: {date}</span>
                </IonCol>
            </IonRow>

            <div
                ref={contentSpace}
                style={{ maxHeight: `${height}` }}
                className="panel overflow-auto transition-max-height duration-700 ease-in-out"
            >
                <div className="accordion-content">
                    <p>{content}</p>
                    <div className="attached-files-list">
                        <div className="attached-file">
                            {attachFile.map((itemFile, indexFile) => (
                                <div>{indexFile+1}. {itemFile.file_name}</div>
                            ))}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}