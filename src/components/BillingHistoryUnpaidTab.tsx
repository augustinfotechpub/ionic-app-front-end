import React, {useState, useEffect} from "react";
import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";

import {  
    IonLabel, 
    IonButton, 
    IonIcon, 
    IonInput,
    IonGrid,
    IonRow,
    IonCol,
    IonAlert,
    useIonViewWillEnter
} from '@ionic/react';

import '@ionic/react/css/flex-utils.css';
import '../assets/css/Custom.css';
import '../assets/css/Responsive.css';
import { Routes } from "../App";


import { globalConst } from "../constants";
import axios from "axios";
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';
import { time } from "ionicons/icons";
axios.defaults.baseURL = globalConst.baseURL;
axios.defaults.headers.post['Content-Type'] = globalConst.ContentType;
axios.defaults.headers.post['Access-Control-Allow-Origin'] = globalConst.AllowOrigin;
axios.defaults.headers.post['Accept'] = globalConst.Accept;
axios.defaults.timeout = globalConst.timeout;

const BillingHistoryUnpaidTab: React.FC<{parentCallback}> = (props) => {
    
    const { t } = useTranslation();
	let history = useHistory();

    const [quickPayAlert, setQuickPayAlert] = useState(false);
    const [paymentCompleteAlert, setPaymentCompleteAlert] = useState(false);
    const [customMessageAlertDismiss, setcustomMessageAlertDismiss] = useState(false);

    const [UnpaidData, setUnpaidData] = useState([{"id": "", "billing_cost": "", "billing_date_text": "", "billing_lease_cnt": "", "billing_status": ""}]);

    const [payPrice, setpayPrice] = useState('');
    const [payID, setpayID] = useState('');
    const [QuickText, setQuickText] = useState('');

    const [payText, setpayText] = useState('');
    const [QuickPayFlag, setQuickPayFlag] = useState(false);

    const [count, setCount] = useState(0);

    useIonViewWillEnter(() => {
        loadUnpaidData();
    });

    useEffect(() => {
        loadUserData(localStorage.getItem('user_id'));
    }, []);

    async function loadUserData(userID: any) {
        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {

            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            api.get("/auth/getPayInfoByID/def/"+userID).then((resPData) => {
                const retData = JSON.parse(JSON.stringify(resPData)).data;

                if (retData['itemFlag']){
                    setpayText(t('Billing-History-Text1') + ' <b>' + retData['c_no'] + t('Billing-History-Text2') +retData['c_date']+'</b>.');
                    setQuickPayFlag(true);
                }
            })
            .catch((error) => {
                alert("Error Get User Payment Data!");
            })
        })
        .catch((error) => {
          alert("Token not Get!");
        })
    }

    const loadUnpaidData =async () => {
        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {

            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            const sendData = {
                UserID: localStorage.getItem('user_id')
            };

            api.put("/propertyleases/duePaymentData", sendData).then((resPData) => {
                const retData = JSON.parse(JSON.stringify(resPData)).data;
                setUnpaidData(retData.UnPaidData);
            })
            .catch((error) => {
                alert("Error Get Property Data!");
            })
        })
        .catch((error) => {
            alert("Token not Get!");
        })
    }

    const clickQuickPayment =async (cardNo:any, payPrice:any,  payID:any,  payMonth:any) => {
        setpayPrice(payPrice);
        setpayID(payID);
        setQuickText('<p>' + cardNo + t('Billing-History-Text3') + '<b>$'+payPrice+'</b>' + t('Billing-History-Text4') + '<b>'+payMonth+'</b>?</p>');

        setQuickPayAlert(true)
    }

    const clickPaymentMethod = async (path, billid) => {
        history.push({pathname: path+"/"+billid});
    }

    const clickPaymentDone = async (cust_cvc:any) => {
        const api = axios.create();
        api.get("/auth/token/refresh").then(res => res).then((response) => {
            
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            var now = new Date();

            const editBillData = {
                "amount_pay" : parseFloat(payPrice),
                "sYear" : now.getFullYear(),
                "sMonth" : (now.getMonth() + 1),
                "sDate" : now.getDate(),
                "billing_id" : payID,
                "pay_user_id" : localStorage.getItem('user_id'),
                "quick_pay" : true,
                "cvcNo" : cust_cvc,
            };

            
            api.put("/propertyleases/billpay/"+payID, editBillData).then((retData) => {
                const retDataArr = JSON.parse(JSON.stringify(retData)).data;

                if (retDataArr.payment=='error') {
                    alert(retDataArr.message);
                } else {
                    setPaymentCompleteAlert(true)
                }

            }).catch((error) => {
                alert("Error found in put Data");
                console.log(error);
            });
        })
        .catch((error) => {
            alert("Token not Get!");
        })
    }

    const clickWillDismiss = () => {
        loadUnpaidData(); 
        setCount((count) => count + 1);
        props.parentCallback(count + 1);
    };

    return (
        <div className="tab-content">

            <div className="digi-lease-search-results-box">
                <p>{t('Billing-History-Text5')} <b>{t('Billing-History-Text6')}</b></p>

                {UnpaidData.map((UnpaidItem, index) => (
                    <div className="tenants-lease-info tenants-lease-payment-info ">
                        <IonGrid className="">
                            <IonRow className="">
                                <IonCol size="12"><b>{UnpaidItem.billing_date_text}</b></IonCol>
                            </IonRow>
                            <IonRow className="">
                                <IonCol size="3" sizeMd="3" className="rent-price-block paid-block">
                                    <p><b>{t('Billing-History-Cost')}</b><br /> ${UnpaidItem.billing_cost} CAD</p>
                                </IonCol>
                                <IonCol size="3" sizeMd="3" className="rent-price-block paid-block">
                                    <p><b>{t('Billing-History-MaxLeases')}</b><br /> {UnpaidItem.billing_lease_cnt}</p>
                                </IonCol>
                                {UnpaidItem.billing_status=='Continue'? 
                                    <IonCol size="6" sizeMd="6" className="rent-pay-buttons ion-text-right">
                                        {t('Billing-History-MonthProgress')}
                                    </IonCol>
                                     : 
                                    <IonCol size="6" sizeMd="6" className="rent-pay-buttons ion-text-right">
                                        <IonButton className="secondary-button choose-payment-method-button"  shape="round" fill="outline" onClick={() => clickPaymentMethod(Routes.billingMethod, UnpaidItem.id)}>
                                            {t('Billing-History-PaymentMethod')}
                                        </IonButton>
                                        {QuickPayFlag? <IonButton className="quick-pay-button"  shape="round" fill="outline" onClick={() => clickQuickPayment(payText, UnpaidItem.billing_cost,  UnpaidItem.id, UnpaidItem.billing_date_text)}>
                                            {t('Billing-History-QuickPay')}
                                        </IonButton> : ''}
                                    </IonCol>
                                }
                            </IonRow>
                        </IonGrid> 
                    </div>
                ))}


                <IonAlert
                    isOpen={quickPayAlert}
                    onDidDismiss={() => setQuickPayAlert(false)}
                    cssClass='orange-alert'
                    mode='md'
                    header={t('Billing-History-QuickPay')}
                    message={QuickText}
                    inputs={[
                        {
                          name: 'cust_cvc',
                          type: 'number',
                          placeholder: 'CVC No',
                          cssClass: 'custom-reminder-message-box'
                        },
                    ]}
                    buttons={[
                        {
                            text: 'Yes',
                            cssClass: 'btn-primary',
                            handler: (formData: { cust_cvc: string }) =>   {
                                if(formData.cust_cvc != null && formData.cust_cvc.length == 3) {
                                    clickPaymentDone(formData.cust_cvc);
                                } else {
                                    setcustomMessageAlertDismiss(true)
                                }
                            }
                        },
                        {
                            text: 'No',
                            role: 'cancel',
                            cssClass: 'btn-outline',
                            handler: () => {
                                
                            }
                        }    
                    ]}
                />

                <IonAlert
                    isOpen={customMessageAlertDismiss}
                    onDidDismiss={() => setcustomMessageAlertDismiss(false)}
                    cssClass='orange-alert'
                    mode='md'
                    header={t('Billing-History-Head1')}
                    message={t('Billing-History-Msg1')}
                    buttons={[
                        {
                            text: 'Close',
                            role: 'cancel',
                            cssClass: 'btn-primary',
                            handler: () => {
                            }
                        }
                        
                    ]}
                />
                
                <IonAlert
                    isOpen={paymentCompleteAlert}
                    onDidDismiss={() => setPaymentCompleteAlert(false)}
                    onWillDismiss={() => clickWillDismiss()}
                    cssClass='orange-alert rent-pay-alert'
                    mode='md'
                    header={t('Billing-History-Head2')}
                    message={t('Billing-History-Msg2')}
                    buttons={[
                        {
                            text: 'Return to Rent Page',
                            role: 'cancel',
                            cssClass: 'btn-primary',
                            handler: () => {
                                
                            }
                        }    
                    ]}
                />

            </div>
        </div>
    );
};

export default BillingHistoryUnpaidTab;