import React, {useState} from "react";
import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";

import { 
    IonButton, 
    IonGrid,
    IonRow, 
    IonCol,
  } from '@ionic/react';

import '@ionic/react/css/flex-utils.css';
import '../assets/css/Custom.css';
import '../assets/css/Responsive.css';
import { Routes } from "../App";

const MessagingUserTab: React.FC<{
        LoadMsgData: any; 
        onClickChat?: () => void;
    }> = (props) => {

        const { t } = useTranslation();
        let history = useHistory();

        const ClickChat = async (path, chat_id) => {
            history.push({
                pathname: path,
                state: { path: path, chat_id: chat_id, chat_msg: '' }
            });
        };
    
        return (
        <div className="tab-content">

            <div className="posts-lists message-list">
                {props.LoadMsgData.map((item: any, ind1: any) => (
                    <div className="posts-list-block">
                        <IonButton className="post-details-link-button" fill="clear" expand="full" shape="round" onClick={() => ClickChat(Routes.chat, item.id)}></IonButton>
                        <IonGrid className="">
                            <IonRow className="post-row">
                                {item.aptno ? <IonCol className="post-image">
                                    <span>{t('Msg-Apt')} #</span>
                                    <h3>{item.aptno}</h3>
                                </IonCol> : <IonCol className="post-image">
                                    <h6 className="staff-name">{item.senduser_name}</h6>
                                </IonCol>}
                                <IonCol className="post-content">
                                    <h4 className="post-title">{item.custom_message}</h4>
                                    <p className="post-date">{item.created_date}</p>
                                </IonCol>
                            </IonRow>
                        </IonGrid>                                    
                    </div>
                ))}
            </div>

            {/* <div className="posts-lists message-list">
                <h4>Today</h4>
            </div> */}

            {/* <div className="posts-lists message-list">
                <h4>Yesterday</h4>
            </div> */}
           
        </div>
        
    );
};

export default MessagingUserTab;