import React, {useEffect, useState} from "react";
import { useForm } from "react-hook-form";
import { useTranslation } from "react-i18next";

import { 
    IonGrid,
    IonRow,
    IonCol,
    IonButton,
    IonIcon,
    IonInput,
    IonLabel,
    createAnimation
} from '@ionic/react';

import { close } from "ionicons/icons";

import '../assets/css/Custom.css';
import '../assets/css/Responsive.css';

import { globalConst } from "../constants";
import axios from "axios";
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';
axios.defaults.baseURL = globalConst.baseURL;
axios.defaults.headers.post['Content-Type'] = globalConst.ContentType;
axios.defaults.headers.post['Access-Control-Allow-Origin'] = globalConst.AllowOrigin;
axios.defaults.headers.post['Accept'] = globalConst.Accept;
axios.defaults.timeout = globalConst.timeout;

const BillPayReceiptPopup: React.FC<{PaidID, parentCallback}> = ({PaidID, parentCallback}) => {

    const { t } = useTranslation();
    const methods = useForm();
    const { register, trigger, reset, watch , handleSubmit, control, setValue, getValues, formState: { errors } } = methods;

    const [TotalPaid, setTotalPaid] = useState('');
    const [PaidDate, setPaidDate] = useState('');

    useEffect(() => {
        loadData(PaidID);
    }, []);

    async function loadData(PaidID: any) {
        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {

            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            api.get("/propertyleases/GetPaymentData/"+PaidID).then((resPData) => {
                const retData = JSON.parse(JSON.stringify(resPData)).data;

                if (retData) {
                    setTotalPaid("$" + retData.billing_payment_amount + " CAD");
                    setPaidDate(retData.billing_payment_date);

                    const fields = ['billing_date_text', 'PaymentMethod', 'CardNumber', 'billing_payment_amount'];
                    fields.forEach(field => {
                        if (field=='billing_payment_amount') {
                            setValue(field, "$" + retData[field])
                        } else {
                            setValue(field, retData[field])
                        }
                    });
                }
            })
            .catch((error) => {
                alert("Error Get Data!");
            })
        })
        .catch((error) => {
          alert("Token not Get!");
        })
    }
    
    return (
        <IonGrid className="rent-pay-receipt-popup-wrapper">
            <div className="popup-title">
                <h4>{t('Billing-History-Receipt')}</h4>
            </div> 

            <IonRow className="receipt-details">
                <IonCol size="6">
                    <IonLabel>{t('Billing-History-Period')}</IonLabel>
                </IonCol>
                <IonCol size="6">
                    <IonInput mode="md" type="text" {...register('billing_date_text')} readonly></IonInput>
                </IonCol>

                <IonCol size="6">
                    <IonLabel>{t('Billing-History-Method')}</IonLabel>
                </IonCol>
                <IonCol size="6">
                    <IonInput mode="md" type="text" {...register('PaymentMethod')} readonly></IonInput>
                </IonCol>

                <IonCol size="6">
                    <IonLabel>{t('Billing-History-CardNo')}</IonLabel>
                </IonCol>
                <IonCol size="6">
                    <IonInput mode="md" type="text" {...register('CardNumber')} readonly></IonInput>
                </IonCol>

                <IonCol size="6">
                    <IonLabel>{t('Billing-History-Cost')}</IonLabel>
                </IonCol>
                <IonCol size="6">
                    <IonInput mode="md" type="text" {...register('billing_payment_amount')} readonly></IonInput>
                </IonCol>
            </IonRow>

            <IonRow  className="receipt-amount">
                <IonCol size="12">
                    <IonLabel>{t('Billing-History-TotalPaid')}</IonLabel>
                </IonCol>
                <IonCol size="12">
                    <p className="total-rent-paid"><b>{TotalPaid}</b></p>
                </IonCol>
                <p className="rent-paid-date">{t('Billing-History-Paid')}: <span className="paid-date">{PaidDate}</span></p>
            </IonRow>
        </IonGrid> 
    );
};

export default BillPayReceiptPopup;