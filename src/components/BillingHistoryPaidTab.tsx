import React, {useState, useCallback, useEffect} from "react";
import { useTranslation } from "react-i18next";

import {  
    IonLabel, 
    IonButton, 
    IonIcon, 
    IonInput,
    IonGrid,
    IonRow,
    IonCol,
    createAnimation,
    IonModal,
    useIonViewWillEnter
} from '@ionic/react';

import { searchSharp, close } from "ionicons/icons";

import BillPayReceiptPopup from "./BillPayReceiptPopup";

import '@ionic/react/css/flex-utils.css';
import '../assets/css/Custom.css';
import '../assets/css/Responsive.css';
import { Routes } from "../App";

import { globalConst } from "../constants";
import axios from "axios";
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';
axios.defaults.baseURL = globalConst.baseURL;
axios.defaults.headers.post['Content-Type'] = globalConst.ContentType;
axios.defaults.headers.post['Access-Control-Allow-Origin'] = globalConst.AllowOrigin;
axios.defaults.headers.post['Accept'] = globalConst.Accept;
axios.defaults.timeout = globalConst.timeout;

const BillingHistoryPaidTab: React.FC<{ReloadCount}> = (props) => {

    const { t } = useTranslation();
    const [PaidData, setPaidData] = useState([{"id": "", "billing_cost": "", "billing_date_text": "", "billing_lease_cnt": "", "billing_status": ""}]);

    const [showModal, setShowModal] = useState(false);
    const [PaidID, setPaidID] = useState(0);

    useIonViewWillEnter(() => {
        loadPaidData();
    });

    useEffect(() => {
        loadPaidData();
    }, [props.ReloadCount]);

    const loadPaidData =async () => {
        if ((localStorage.getItem('user_code')!==null)) {
            const api = axios.create();
            api.get("/auth/token/refresh").then((response) => {
    
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
                const api = axios.create();
    
                const leaseData = {
                    UserID: localStorage.getItem('user_id')
                };

                api.put("/propertyleases/paidPaymentData", leaseData).then((resPData) => {
                    const retData = JSON.parse(JSON.stringify(resPData)).data;

                    setPaidData(retData.PaidData);
                })
                .catch((error) => {
                    alert("Error Get Data!");
                })
            })
            .catch((error) => {
              alert("Token not Get!");
            })
        }
    }

    const clickPastData = async (PaidID:any) => {
        setShowModal(false);
        setPaidID(PaidID);
        setShowModal(true);
    }

    const enterAnimation = (baseEl: any) => {
        const backdropAnimation = createAnimation()
          .addElement(baseEl.querySelector('ion-backdrop')!)
          .fromTo('opacity', '0.01', 'var(--backdrop-opacity)');
    
        const wrapperAnimation = createAnimation()
          .addElement(baseEl.querySelector('.modal-wrapper')!)
          .keyframes([
            { offset: 0, opacity: '0', transform: 'scale(0)' },
            { offset: 1, opacity: '0.99', transform: 'scale(1)' }
          ]);
    
        return createAnimation()
          .addElement(baseEl)
          .easing('ease-out')
          .duration(500)
          .addAnimation([backdropAnimation, wrapperAnimation]);
    }
    
    const leaveAnimation = (baseEl: any) => {
        return enterAnimation(baseEl).direction('reverse');
    }

    const callbackPopup = useCallback((PaidID) => {
        alert(PaidID);
    }, []);

    return (
        <div className="tab-content">
            <div className="digi-lease-search-results-box">
                <p>{t('Billing-History-Text5')} <b>{t('Billing-History-Text8')}</b></p>

                {PaidData.map((PaidItem, index) => (
                    <div className="tenants-lease-info tenants-lease-payment-info ">
                        <IonGrid className="">
                            <IonRow className="">
                                <IonCol size="12"><b>{PaidItem.billing_date_text}</b></IonCol>
                            </IonRow>
                            <IonRow className="">
                                <IonCol size="3" sizeMd="3" className="rent-price-block paid-block">
                                    <p><b>{t('Billing-History-Cost')}</b><br /> ${PaidItem.billing_cost} CAD</p>
                                </IonCol>
                                <IonCol size="3" sizeMd="3" className="rent-price-block paid-block">
                                    <p><b>{t('Billing-History-MaxLeases')}</b><br /> {PaidItem.billing_lease_cnt}</p>
                                </IonCol>
                                <IonCol size="6" sizeMd="6" className="rent-period-block ion-text-right">
                                    <IonButton className="secondary-button view-receipt-button"  shape="round" fill="outline" onClick={() => clickPastData(PaidItem.id)}>{t('Billing-History-ViewReceipt')}</IonButton>
                                </IonCol>
                            </IonRow>
                        </IonGrid> 
                    </div>
                ))}

                <IonModal isOpen={showModal} enterAnimation={enterAnimation} leaveAnimation={leaveAnimation} cssClass="rent-pay-receipt-popup">
                    <IonButton className="close-popup-button" fill="clear" onClick={() => setShowModal(false)}> 
                        <IonIcon icon={close} />
                    </IonButton>
                    <BillPayReceiptPopup parentCallback={callbackPopup} PaidID={PaidID} />
                </IonModal>
            </div>
        </div>
    );
};

export default BillingHistoryPaidTab;