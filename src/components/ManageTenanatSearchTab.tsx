import React, {useEffect, useState} from "react";
import { useHistory } from "react-router-dom";
import { Trans, withTranslation, useTranslation } from "react-i18next";

import {  
    IonLabel, 
    IonButton, 
    IonIcon, 
    IonInput,
    IonGrid,
    IonRow,
    IonCol,
    IonAlert,
    useIonViewDidEnter
} from '@ionic/react';

import { searchSharp, addSharp } from "ionicons/icons";

import '@ionic/react/css/flex-utils.css';
import '../assets/css/Custom.css';
import '../assets/css/Responsive.css';
import { Routes } from "../App";

import { globalConst } from "../constants";

import axios from "axios";
axios.defaults.baseURL = globalConst.baseURL;
axios.defaults.headers.post['Content-Type'] = globalConst.ContentType;
axios.defaults.headers.post['Access-Control-Allow-Origin'] = globalConst.AllowOrigin;
axios.defaults.headers.post['Accept'] = globalConst.Accept;
axios.defaults.timeout = globalConst.timeout;

const ManageTenanatSearchTab: React.FC<{SearchPropID, SearchAptNo, SearchData, parentCallback}> = ({SearchPropID, SearchAptNo, SearchData, parentCallback}) => {
    const { t } = useTranslation();
    const [FlagSearchText, setFlagSearchText] = useState(false);

    const [SearchText, setSearchText] = useState("");
    const [CurrLeasesData, setCurrLeasesData] = useState([{"id": "", "property_id": "", "floorsno": "", "aptno": "", "lease_code": "", "file_storagekey": "", "lease_price": "", "lease_start_date": "", "lease_end_date": "", "ls_year": "", "ls_month": "", "ls_day": "", "le_year": "", "le_month": "", "le_day": "", "ls_month_name": "", "le_month_name": "", "TenantsData": [{"tenant_fname": "", "tenant_lname": ""}]}]);
    const [PastLeasesData, setPastLeasesData] = useState([{"id": "", "property_id": "", "floorsno": "", "aptno": "", "lease_code": "", "file_storagekey": "", "lease_price": "", "lease_start_date": "", "lease_end_date": "", "ls_year": "", "ls_month": "", "ls_day": "", "le_year": "", "le_month": "", "le_day": "", "ls_month_name": "", "le_month_name": "", "TenantsData": [{"tenant_fname": "", "tenant_lname": ""}]}]);
    const [UpcomingData, setUpcomingData] = useState([{"id": "", "property_id": "", "floorsno": "", "aptno": "", "lease_code": "", "file_storagekey": "", "lease_price": "", "lease_start_date": "", "lease_end_date": "", "ls_year": "", "ls_month": "", "ls_day": "", "le_year": "", "le_month": "", "le_day": "", "ls_month_name": "", "le_month_name": "", "TenantsData": [{"tenant_fname": "", "tenant_lname": ""}]}]);
    const [MaxDateData, setMaxDateData] = useState<any>(['', '']);
    const [AddNewLease, setAddNewLease] = useState(0);
    const [RenewalNewLease, setRenewalNewLease] = useState(0);

    const [SPID, setSPID] = useState("");
    const [SFloorNo, setSFloorNo] = useState("");
    const [SAptNo, setSAptNo] = useState("");
    const [SID, setSID] = useState("");

    const [showAlert, setShowAlert] = useState(false);
    const [msgAlert, setMsgAlert] = useState('');
    const [headAlert, setHeadAlert] = useState('');

	let history = useHistory();

    useEffect(() => {
        setCurrLeasesData([]);
        setPastLeasesData([]);
        setUpcomingData([]);
    }, []);

    useEffect(() => {
        //alert("Test 1 = " + SearchAptNo);
        //alert(...SearchData);
        
        //alert(SearchData['0']);
        //alert(SearchData['1']);
        //alert(SearchData['2']);
        if ((SearchAptNo!='') || (SearchAptNo!=0)) {
            //alert("Test 2 = " + SearchAptNo);
            loadLeasesData(SearchData['0'], SearchData['1'], SearchData['2']);
            setSearchText(SearchData['2']);

            setSPID(SearchData['0']);
            setSFloorNo(SearchData['1']);
            setSAptNo(SearchData['2']);
            setSID(SearchData['3']);
        }

    }, [SearchAptNo]);

    const clickSearch = async () => {
        //parentCallback(SearchPropID, SearchText);

        //alert(SearchText);
        
        setSPID(SearchPropID);
        //setSFloorNo(SearchData['1']);
        setSAptNo(SearchText);
        //setSID(SearchData['3']);

        setFlagSearchText(true);


        loadLeasesData(SearchPropID, '0', SearchText);
    }
    
    async function loadLeasesData(PropID: any, FNo: any, AptNo: any) {
        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {

            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            const searchInfo = {
                "PropID" : PropID,
                "AptNo" : AptNo,
                "FNo" : FNo,
                "UserID" : localStorage.getItem('user_id'),
            };

            api.put("/propertyfloors/checkExists", searchInfo).then((resPData) => {

                api.put("/propertyleases/alldatabyaptno", searchInfo).then((resUnitData) => {
                    const retData = JSON.parse(JSON.stringify(resUnitData)).data;
                    console.log(retData);

                    setCurrLeasesData(retData.CurrentData);
                    setPastLeasesData(retData.PastData);
                    setUpcomingData(retData.UpcomingData);

                    setAddNewLease(retData.AddNewLease);
                    setRenewalNewLease(retData.RenewalNewLease);

                    setMaxDateData([retData.MaxDate, retData.MaxDateText, retData.MaxID, retData.MaxStorageKey]);

                })
                .catch((error) => {
                    alert("Error Get Data!");
                })

            })
            .catch((error) => {
                setCurrLeasesData([]);
                setPastLeasesData([]);
                setUpcomingData([]);

                setAddNewLease(0);
                setRenewalNewLease(0);
                setMaxDateData(['', '', '', '']);

                setMsgAlert(t('Tenants-Lease-SearchTool-Error-Msg'));
                setHeadAlert(t('Tenants-Lease-SearchTool-Error-Head'));
                setShowAlert(true);
            })
        })
        .catch((error) => {
          alert("Token not Get!");
        })
    }

    const clickAddLease = async (path:any, SearchPID:any, SearchFloorNo:any, SearchAptNo:string, SearchID:any) => {
        //console.log('TEST');
        //console.log(path);
        //SPID, SFloorNo, SAptNo, SID

        history.push({
            pathname: path,
            state: { path: path, SearchPID: SearchPID, SearchFloorNo: SearchFloorNo, SearchAptNo: SearchAptNo, SearchID: SearchID }
        });
    }

    const clickRenewalProposalLease = async (path, leaseid, file_storagekey) => {
        history.push({
            pathname: path,
            state: { path: path, leaseid: leaseid, filestoragekey: file_storagekey }
        });
    }

    const ViewLease = async (path, leaseid) => {
        console.log('TEST');
        console.log(path);
        console.log(leaseid);
        
        history.push({
            pathname: path,
            state: { path: path, leaseid: leaseid }
        });

        //console.log(history);
    };

    const EditLease = async (path, leaseid, file_storagekey) => {
        history.push({
            pathname: path,
            state: { path: path, leaseid: leaseid, filestoragekey: file_storagekey }
        });
    };

    const clickWillDismiss = (FlagDismiss) => {
        //Nothing
    };

    return (
        <div className="tab-content">

            <div className="find-digi-lease-block">

                <IonAlert
                    isOpen={showAlert}
                    onDidDismiss={() => setShowAlert(false)}
                    onWillDismiss={() => clickWillDismiss(true)}
                    cssClass='orange-alert'
                    header={headAlert}
                    message={msgAlert}
                    buttons={[{text: 'Close', cssClass: 'btn-primary'}]}
                />

                <h6><Trans>Tenants-Lease-SearchTool-Text1</Trans></h6>

                <div className="search-box">
                    <IonLabel>{t('Tenants-Lease-Apartment')} #</IonLabel>
                    <IonInput onIonInput={(e: any) => setSearchText(e.target.value)} mode="md" type="text" value={SearchAptNo}></IonInput>
                    <IonButton onClick={() => clickSearch()} expand="full" fill="solid" shape="round">
                        <div className="">    
                            <IonIcon icon={searchSharp}  />
                            <span className="">{t('General-Search')}</span>
                        </div>
                    </IonButton>
                </div>

            </div>

            <div className="digi-lease-search-results-box">
                {FlagSearchText ? (<h6>{t('Tenants-Lease-SearchTool-Text2')} <b><u>{t('Tenants-Lease-Apt')} {SearchText}</u></b></h6>) : ('')}
                
                {RenewalNewLease == 1 ? 
                    <div className="search-result-box">
                        <div className="lease-renewal">
                            <IonButton className="lease-renewal-button" fill="clear" expand="full" shape="round" onClick={() => clickRenewalProposalLease(Routes.leaseRenewalProposal, MaxDateData[2], MaxDateData[3])}></IonButton>
                            <IonGrid className="">
                                <IonRow className="ion-align-items-center">
                                    <IonCol sizeXs="2" sizeMd="1" sizeLg="1" sizeXl="1" className="">
                                        <IonIcon icon="assets/images/exclamation-mark.svg"  />
                                    </IonCol>
                                    <IonCol sizeXs="8" sizeMd="10" sizeLg="10" sizeXl="10" className="">
                                        <div className="renewal-lease-info">
                                            {MaxDateData[1] == ''? null: <h6><u>{t('Tenants-Lease-SearchTool-LeaseRenewal')}</u></h6> }
                                            {MaxDateData[1] == ''? null: <p>{t('Tenants-Lease-SearchTool-expires-on')} <b>{MaxDateData[1]}.</b></p> }
                                            <p>{t('Tenants-Lease-SearchTool-Tap-here')}</p>
                                        </div>
                                    </IonCol>
                                    <IonCol sizeXs="2" sizeMd="1" sizeLg="1" sizeXl="1" className="">
                                        <IonIcon icon="assets/images/plus-icon.svg" />
                                    </IonCol>
                                </IonRow>
                            </IonGrid>
                        </div>
                    </div>
                : null}
                     
                <div className="search-result-box">
                    {UpcomingData.length > 0 ? <h5>{t('Tenants-Lease-SearchTool-Upcoming')}</h5>: null }
                    
                    {UpcomingData.map((item, index) => (
                        <div key={index} className="tenants-lease-info">
                            <IonButton className="edit-lease-info-button" fill="clear" expand="full" shape="round"></IonButton>
                            <IonGrid className="">
                                <IonRow className="">
                                    <IonCol size="7" className="start-end-date-block">
                                        <div className="start-date-block">
                                            <h6>{t('Tenants-Lease-SearchTool-StartDate')}</h6>
                                            <p>{item.ls_month_name}</p>
                                            <p>{item.ls_day}</p>
                                            <p>{item.ls_year}</p>
                                        </div>
                                        <div className="to-block">
                                            <p>{t('Tenants-Lease-SearchTool-To')}</p>
                                            <IonIcon icon="assets/images/arrow-right.svg" />
                                        </div>
                                        <div className="end-date-block">
                                            <h6>{t('Tenants-Lease-SearchTool-EndDate')}</h6>
                                            <p>{item.le_month_name}</p>
                                            <p>{item.le_day}</p>
                                            <p>{item.le_year}</p>
                                        </div>
                                    </IonCol>
                                    <IonCol size="5" className="">
                                        <div className="tenant-lease-info">
                                            <h6>{t('Tenants-Lease-SearchTool-Tenants')}</h6>
                                            <ol>
                                                {item.TenantsData.map((Titem, Tindex) => (
                                                    <li>{Titem.tenant_fname} {Titem.tenant_lname}</li>
                                                ))}
                                            </ol>
                                        </div>
                                    </IonCol>
                                </IonRow>
                            </IonGrid>                                    
                        </div>
                    ))}
                </div>

                <div className="search-result-box">
                    {((CurrLeasesData.length > 0) || (AddNewLease == 1)) ? <h5>{t('Tenants-Lease-SearchTool-Current')}</h5> : null}
                    
                    {AddNewLease == 1 ? 
                        <div className="lease-renewal">
                            <IonButton className="lease-renewal-button" fill="clear" expand="full" shape="round" onClick={() => clickAddLease(Routes.addNewLease, SPID, SFloorNo, SAptNo, SID)}></IonButton>
                            <IonGrid className="">
                                <IonRow className="ion-align-items-center">
                                    <IonCol sizeXs="2" sizeMd="1" sizeLg="1" sizeXl="1" className="">
                                        <IonIcon icon="assets/images/exclamation-mark.svg"  />
                                    </IonCol>
                                    <IonCol sizeXs="8" sizeMd="10" sizeLg="10" sizeXl="10" className="">
                                        <div className="renewal-lease-info">
                                            <h6><u>{t('Tenants-Lease-SearchTool-NoUpcoming')}</u></h6>
                                            <p>{t('Tenants-Lease-SearchTool-Taphere-New')}</p>
                                            <p>{t('Tenants-Lease-SearchTool-Text3')}</p>
                                        </div>
                                    </IonCol>
                                    <IonCol sizeXs="2" sizeMd="1" sizeLg="1" sizeXl="1" className="">
                                        <IonIcon icon="assets/images/plus-icon.svg" />
                                    </IonCol>
                                </IonRow>
                            </IonGrid>
                        </div>
                    : null}

                    <br />

                    {CurrLeasesData.map((item, index) => (
                        <div key={index} className="tenants-lease-info">
                            <IonButton className="edit-lease-info-button" fill="clear" expand="full" shape="round" onClick={() => EditLease(Routes.editLeaseInfo, item.id, item.file_storagekey)}></IonButton>
                            <IonGrid className="">
                                <IonRow className="">
                                    <IonCol size="7" className="start-end-date-block">
                                        <div className="start-date-block">
                                            <h6>{t('Tenants-Lease-SearchTool-StartDate')}</h6>
                                            <p>{item.ls_month_name}</p>
                                            <p>{item.ls_day}</p>
                                            <p>{item.ls_year}</p>
                                        </div>
                                        <div className="to-block">
                                            <p>{t('Tenants-Lease-SearchTool-To')}</p>
                                            <IonIcon icon="assets/images/arrow-right.svg" />
                                        </div>
                                        <div className="end-date-block">
                                            <h6>{t('Tenants-Lease-SearchTool-EndDate')}</h6>
                                            <p>{item.le_month_name}</p>
                                            <p>{item.le_day}</p>
                                            <p>{item.le_year}</p>
                                        </div>
                                    </IonCol>
                                    <IonCol size="5" className="">
                                        <div className="tenant-lease-info">
                                            <h6>{t('Tenants-Lease-SearchTool-Tenants')}</h6>
                                            <ol>
                                                {item.TenantsData.map((Titem, Tindex) => (
                                                    <li>{Titem.tenant_fname} {Titem.tenant_lname}</li>
                                                ))}
                                            </ol>
                                        </div>
                                    </IonCol>
                                </IonRow>
                            </IonGrid>                                    
                        </div>
                    ))}
                </div>

                <div className="search-result-box">
                    {PastLeasesData.length > 0 ? <h5>{t('Tenants-Lease-SearchTool-Past')}</h5>: null }

                    {PastLeasesData.map((item, index) => (
                        <div key={index} className="tenants-lease-info">
                            <IonButton className="view-lease-info-button" fill="clear" expand="full" shape="round" onClick={() => ViewLease(Routes.leaseInfo, item.id)}></IonButton>
                            <IonGrid className="">
                                <IonRow className="">
                                    <IonCol size="7" className="start-end-date-block">
                                        <div className="start-date-block">
                                            <h6>{t('Tenants-Lease-SearchTool-StartDate')}</h6>
                                            <p>{item.ls_month_name}</p>
                                            <p>{item.ls_day}</p>
                                            <p>{item.ls_year}</p>
                                        </div>
                                        <div className="to-block">
                                            <p>{t('Tenants-Lease-SearchTool-To')}</p>
                                            <IonIcon icon="assets/images/arrow-right.svg" />
                                        </div>
                                        <div className="end-date-block">
                                            <h6>{t('Tenants-Lease-SearchTool-EndDate')}</h6>
                                            <p>{item.le_month_name}</p>
                                            <p>{item.le_day}</p>
                                            <p>{item.le_year}</p>
                                        </div>
                                    </IonCol>
                                    <IonCol size="5" className="">
                                        <div className="tenant-lease-info">
                                            <h6>{t('Tenants-Lease-SearchTool-Tenants')}</h6>
                                            <ol>
                                                {item.TenantsData.map((Titem, Tindex) => (
                                                    <li>{Titem.tenant_fname} {Titem.tenant_lname}</li>
                                                ))}
                                            </ol>
                                        </div>
                                    </IonCol>
                                </IonRow>
                            </IonGrid>                                    
                        </div>
                    ))}
                </div>
            </div>
        </div>
    );
};

export default ManageTenanatSearchTab;