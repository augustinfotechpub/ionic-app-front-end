import React from "react";
import { useTranslation } from "react-i18next";

import { 
    IonFooter, 
    IonGrid, 
    IonRow, 
    IonCol, 
    IonList, 
    IonItem,
    IonButton
} from '@ionic/react';

import '../assets/css/Custom.css';
import '../assets/css/Responsive.css';
import { Routes } from '../App';

const Footer: React.FC = () => {
    const { t } = useTranslation();

    return (
        <IonFooter className="ion-hide-lg-down">
            <IonGrid>
                <IonRow>
                    <IonCol className="footer-top-col" size="12">
                        <IonList>
                            <IonItem>
                                <IonButton className="page-link" routerLink={Routes.Home} fill="clear">{t('menu-ContactUs')}</IonButton>
                            </IonItem>

                            <IonItem>
                                <IonButton className="page-link" routerLink={Routes.Home} fill="clear">{t('menu-About')}</IonButton>
                            </IonItem>

                            <IonItem>
                                <IonButton className="page-link" routerLink={Routes.Home} fill="clear">{t('menu-TermsofUse')}</IonButton>
                            </IonItem>
                        </IonList>
                        
                    </IonCol>

                    <IonCol className="footer-bottom-col" size="12">
                        <p className="ion-text-center">{t('menu-Text1')}</p>
                    </IonCol>
                   
                </IonRow>

            </IonGrid>
        </IonFooter>
        
    );
};

export default Footer;