import React, {ReactNode, useState, useEffect} from "react";
import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";

import { 
    IonButton, 
    IonGrid,
    IonRow, 
    IonCol,
    IonCard,
    IonCardHeader,
    IonCardTitle,
    IonCardContent,
    IonAlert 
  } from '@ionic/react';

import '@ionic/react/css/flex-utils.css';
import '../assets/css/Custom.css';
import '../assets/css/Responsive.css';
import { Routes } from "../App";

import { globalConst } from "../constants";

import axios from "axios";
axios.defaults.baseURL = globalConst.baseURL;
axios.defaults.headers.post['Content-Type'] = globalConst.ContentType;
axios.defaults.headers.post['Access-Control-Allow-Origin'] = globalConst.AllowOrigin;
axios.defaults.headers.post['Accept'] = globalConst.Accept;
axios.defaults.timeout = globalConst.timeout;

const ManageRentsGeneralTab: React.FC<{
        LoadRentAptData: any; LoadRentAptDataMsg: any; CntRentAptData: any; clickMoreRentAptData: () => void;
        clickAptNo: () => void;
        children?: ReactNode;
        parentCallback;
    }> = (props) => {

    const { t } = useTranslation();
    const [rentReminderAlert, setRentReminderAlert] = useState(false);
    const [rentReminderSentAlert, setRentReminderSentAlert] = useState(false);
    const [customMessageAlert, setCustomMessageAlert] = useState(false);
    const [customMessageAlertSentAlert, setCustomMessageAlertSentAlert] = useState(false);
    const [customMessageAlertDismiss, setcustomMessageAlertDismiss] = useState(false);

    const [TestData, setTestData] = useState([{msg_type: "",property_id: "",floorsno:"",aptno:"",lease_code:"",custom_message:"",senduser_id:""}]);

    let history = useHistory();

    const clickAptNo = async (SearchPID:any, SearchFloorNo:any, SearchAptNo:string) => {
        props.clickAptNo();
        props.parentCallback(SearchPID, SearchFloorNo, SearchAptNo);
    }

    async function AddMsgData(api:any, AddPID:any, AddFNo:any, AddANo:any, LeaseCode:any, AddMsg:any, SendUID:any) {
        const addMsgData = {
            //"current_user" : localStorage.getItem('user_id'),
            "msg_type" : 'Management',
            "property_id" : AddPID,
            "floorsno" : AddFNo,
            "aptno" : AddANo,
            "lease_code" : LeaseCode,
            "custom_message" : AddMsg,
            "senduser_id" : SendUID
        };

        console.log(addMsgData);

        //TestData.push
        setTestData([...TestData, addMsgData]);
        //setTestData(TestData => [...TestData, addMsgData]);

        console.log('TTT');
        console.log([...TestData, addMsgData]);


        /* api.post("/messaging/", addMsgData).then((retData) => {
            const retDataArr = JSON.parse(JSON.stringify(retData));
            //console.log(retDataArr);

        }).catch((error) => {
            //alert("Error found in put Data");
            console.log(error);
        });  */
    }

    const clickSendCustomMsg = async (CustMessage:any) => {
        console.log(CustMessage);
        console.log(props.LoadRentAptDataMsg);

        //return false;

        const api = axios.create();
        api.get("/auth/token/refresh").then(res => res)
        .then((response) => {
            
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            //{props.LoadRentAptDataMsg.map((item: any) => (
            //    AddMsgData(api, item.pid, item.floorsno, item.aptno, item.lease_code, CustMessage, item.UserID)
            //))}

            const addMsgData = {
                "current_user" : localStorage.getItem('user_id'),
                "current_usertype" : localStorage.getItem('user_type_id'),
                "custom_message" : CustMessage,
                "LoadRentAptDataMsg" : props.LoadRentAptDataMsg
            };

            console.log(addMsgData);

            api.post("/messaging/", addMsgData).then((retData) => {
                const retDataArr = JSON.parse(JSON.stringify(retData));
                //console.log(retDataArr);
                setCustomMessageAlertSentAlert(true);
    
            }).catch((error) => {
                console.log(error);
            });
            
            //console.log('Fial');
            //console.log(TestData);
        })
        .catch((error) => {
          alert("Token not Get!");
        })
    }

    const clickRentPay = async (path:any, SearchPID:any, SearchFloorNo:any, SearchAptNo:string) => {
        history.push({
            pathname: path,
            state: { path: path, SearchPID: SearchPID, SearchFloorNo: SearchFloorNo, SearchAptNo: SearchAptNo }
        });
    }

    const ClickRentReminderSent = async () => {
        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {
    
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            api.get("/propertyleases/sendnotification").then((resUType) => {
                const retData = JSON.parse(JSON.stringify(resUType)).data;
                setRentReminderSentAlert(true);
            })
            .catch((error) => {
                alert("Error Get in User Type!");
            })
        })
        .catch((error) => {
          alert("Token not Get!");
        })
    }

    
    return (
        <div className="tab-content">

            <div className="apartments-list-block requires-renewal-proposal-block">
                <h5 className="title-with-line">{t('Rent-Manage-General-Text1')} ({props.CntRentAptData})</h5>

                <IonCard className="tenants-rent-details-card">
                    <IonCardHeader>
                        <IonCardTitle>{t('General-Actions')}</IonCardTitle>
                    </IonCardHeader>

                    <IonCardContent>

                        <IonButton 
                            className="set-reminder-btn" 
                            routerLink="#" 
                            fill="outline" 
                            shape="round"
                            onClick={() =>
                                setRentReminderAlert(true)
                            }
                        >
                            {t('Rent-Manage-General-SendReminder')}
                        </IonButton>
                        <IonAlert
                            isOpen={rentReminderAlert}
                            onDidDismiss={() => setRentReminderAlert(false)}
                            cssClass='orange-alert'
                            mode='md'
                            header={t('Rent-Manage-General-SendReminder-Head')}
                            message={t('Rent-Manage-General-SendReminder-Msg1')}
                            buttons={[
                                {
                                    text: 'Yes',
                                    cssClass: 'btn-primary',
                                    handler: () => {
                                        //setRentReminderSentAlert(true)
                                        ClickRentReminderSent()
                                    }
                                },
                                {
                                    text: 'No',
                                    role: 'cancel',
                                    cssClass: 'btn-outline',
                                    handler: () => {
                                        
                                    }
                                }
                                
                            ]}
                        />
                        <IonAlert
                            isOpen={rentReminderSentAlert}
                            onDidDismiss={() => setRentReminderSentAlert(false)}
                            cssClass='orange-alert'
                            mode='md'
                            header={t('Rent-Manage-General-SendReminder-Head')}
                            message={t('Rent-Manage-General-SendReminder-Msg2')}
                            buttons={[
                                {
                                    text: 'Close',
                                    role: 'cancel',
                                    cssClass: 'btn-primary',
                                    handler: () => {
                                    }
                                }
                                
                            ]}
                        />

                        <IonButton 
                            className="set-reminder-btn" 
                            routerLink="#" 
                            fill="outline" 
                            shape="round"
                            onClick={() =>
                                setCustomMessageAlert(true)
                            }
                        >
                            {t('Rent-Manage-General-SendCustomMessage')} 
                        </IonButton>
                        <IonAlert
                            isOpen={customMessageAlert}
                            onDidDismiss={() => setCustomMessageAlert(false)}
                            cssClass='orange-alert'
                            mode='md'
                            header={t('Rent-Manage-General-SendCustomMessage-Head')}
                            message={t('Rent-Manage-General-SendCustomMessage-Msg1')}
                            inputs={[
                                {
                                  name: 'customRemiderMessageBox',
                                  type: 'textarea',
                                  placeholder: 'Message content',
                                  cssClass: 'custom-reminder-message-box'
                                },
                            ]}
                            buttons={[
                                {
                                    text: 'Send',
                                    cssClass: 'btn-outline secondary-button',
                                    handler: (formData: { customRemiderMessageBox: string }) =>   {
                                        if(formData.customRemiderMessageBox != null && formData.customRemiderMessageBox.length > 0) {
                                            clickSendCustomMsg(formData.customRemiderMessageBox);
                                        } else {
                                            setcustomMessageAlertDismiss(true)
                                        }
                                    }
                                },
                                {
                                    text: 'Cancel',
                                    role: 'cancel',
                                    cssClass: 'btn-outline',
                                    handler: () => {
                                        
                                    }
                                }
                                
                            ]}
                        />
                        <IonAlert
                            isOpen={customMessageAlertSentAlert}
                            onDidDismiss={() => setCustomMessageAlertSentAlert(false)}
                            cssClass='orange-alert'
                            mode='md'
                            header={t('Rent-Manage-General-SendCustomMessage-Head')}
                            message={t('Rent-Manage-General-SendCustomMessage-Msg2')}
                            buttons={[
                                {
                                    text: 'Close',
                                    role: 'cancel',
                                    cssClass: 'btn-primary',
                                    handler: () => {
                                    }
                                }
                                
                            ]}
                        />

                        <IonAlert
                            isOpen={customMessageAlertDismiss}
                            onDidDismiss={() => setcustomMessageAlertDismiss(false)}
                            cssClass='orange-alert'
                            mode='md'
                            header={t('Rent-Manage-General-CustomMessage-Head')}
                            message={t('Rent-Manage-General-CustomMessage-Msg1')}
                            buttons={[
                                {
                                    text: 'Close',
                                    role: 'cancel',
                                    cssClass: 'btn-primary',
                                    handler: () => {
                                    }
                                }
                                
                            ]}
                        />

                    </IonCardContent>
                </IonCard>

                <IonGrid className="">
                    <IonRow className="apartments-list">
                        {props.LoadRentAptData.map((item: any, ind1: any) => (
                            <IonCol key={item.id} size="6" sizeMd="6" sizeLg="6" sizeXl="3" className="">
                                {/* onClick={() => clickAptNo(item.pid, item.floorsno, item.aptno)} */}
                                <IonButton onClick={() => clickRentPay(Routes.rentPayDetails, item.pid, item.floorsno, item.aptno)} expand="full" fill="solid" shape="round">
                                    <div>
                                        <span>{t('Rent-Manage-Apt')}#</span>
                                        <h2>{item.aptno}</h2>
                                    </div>
                                </IonButton>
                            </IonCol>
                        ))}
                    </IonRow>
                    {/* <IonRow className="ion-justify-content-center">
                        <IonCol size="6" sizeMd="4" sizeLg="4" sizeXl="3" className="">
                            <IonButton className="show-more-button" expand="full" shape="round" fill="solid">More</IonButton>
                        </IonCol>
                    </IonRow> */}
                </IonGrid>
            </div>
        </div>
    );
};

export default ManageRentsGeneralTab;