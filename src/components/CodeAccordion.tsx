import React, { useRef, useState } from 'react';

import { 
    IonRow,
    IonCol,
    IonIcon,
    IonButton
} from '@ionic/react';

import { caretDownOutline, createOutline, checkmarkCircle, closeCircle, eyeOff, eye, checkmarkSharp, close } from "ionicons/icons";

import { globalConst } from "../constants";

import axios from "axios";
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';
axios.defaults.baseURL = globalConst.baseURL;
axios.defaults.headers.post['Content-Type'] = globalConst.ContentType;
axios.defaults.headers.post['Access-Control-Allow-Origin'] = globalConst.AllowOrigin;
axios.defaults.headers.post['Accept'] = globalConst.Accept;
axios.defaults.timeout = globalConst.timeout;

interface AccordionProps {
    id: React.ReactNode,
    access_code: React.ReactNode,
    assign_user_active_code: React.ReactNode,
    assign_username: React.ReactNode,
    assign_useremail: React.ReactNode,
    assign_userphoneno: React.ReactNode,
    FeatureAccess: [{"manage_posts": React.ReactNode, "manage_propertys": React.ReactNode, "manage_rents": React.ReactNode}],
    parentCallback: any,
}

export const CodeAccordion: React.FC<AccordionProps> = ({ id, access_code, assign_user_active_code, assign_username, assign_useremail, assign_userphoneno, FeatureAccess, parentCallback }) => {
    
    const [count, setCount] = useState(0);

    const onToggleActiveClick = async (ID, access_code) => {
        
        const api = axios.create();
        api.get("/auth/token/refresh").then(res => res)
        .then((response) => {
            
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            const editData = {
                "access_code" : access_code,
            };

            api.put("/auth/toggleactivecode", editData).then((retData) => {
                const retDataArr = JSON.parse(JSON.stringify(retData)).data;
                
                setCount((count) => count + 1);
                parentCallback(count + 1);

                //setMsgAlert("Your property Lease data added successfully.");
                //setHeadAlert("Property Lease Data Add");
                //setShowAlert(true);
            
                //return false;
            }).catch((error) => {
                alert("Error found in put Data");
                console.log(error);
            });
        })
        .catch((error) => {
          alert("Token not Get!");
        })


    };

    return (
        <div className='accordion-item active'>
            <IonRow className="accordion-button ion-align-items-center">
                <IonCol className="accordion-edit-button">
                    <IonButton fill="clear" onClick={() => onToggleActiveClick(id, access_code)} >
                        {assign_user_active_code == '0' ? <IonIcon icon={close} /> : <IonIcon icon={checkmarkSharp} /> }
                    </IonButton>
                </IonCol>
                <IonCol>
                    <div>
                        <p>{access_code}</p>
                    </div>
                </IonCol>

                <IonCol>
                    <div>
                        <p>{assign_username}</p>
                        <p>{assign_useremail}</p>
                        <p>{assign_userphoneno}</p>
                    </div>
                </IonCol>

                <IonCol>
                    {FeatureAccess.map((itemName, ind) => (
                        <div>
                            {itemName.manage_posts == '1' ? <p>Manage Posts</p> : ''}
                            {itemName.manage_propertys == '1' ? <p>Manage Propertys</p> : ''}
                            {itemName.manage_rents == '1' ? <p>Manage Rents</p> : ''}
                        </div>
                    ))}
                </IonCol>
            </IonRow>
        </div>
    )
}