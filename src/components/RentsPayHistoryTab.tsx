import ReactDOM from "react-dom";
import React, {useState, useCallback, useEffect, forwardRef, useRef} from "react";
import { useForm } from "react-hook-form";
import ReactToPrint, { PrintContextConsumer, useReactToPrint } from "react-to-print";
import { useTranslation } from "react-i18next";

import {  
    IonLabel, 
    IonButton, 
    IonIcon, 
    IonInput,
    IonGrid,
    IonRow,
    IonCol,
    createAnimation,
    IonModal,
    useIonViewWillEnter
} from '@ionic/react';

import { searchSharp, close } from "ionicons/icons";

import RentPayReceiptPopup from "./RentPayReceiptPopup";

import '@ionic/react/css/flex-utils.css';
import '../assets/css/Custom.css';
import '../assets/css/Responsive.css';
import { Routes } from "../App";

//import ComponentToPrint from "../components/ComponentToPrint";

import { globalConst } from "../constants";
import axios from "axios";
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';
axios.defaults.baseURL = globalConst.baseURL;
axios.defaults.headers.post['Content-Type'] = globalConst.ContentType;
axios.defaults.headers.post['Access-Control-Allow-Origin'] = globalConst.AllowOrigin;
axios.defaults.headers.post['Accept'] = globalConst.Accept;
axios.defaults.timeout = globalConst.timeout;

const RentsPayHistoryTab: React.FC<{ReloadCount}> = (props) => {

    const { t } = useTranslation();
    //const ref = useRef<HTMLDivElement>(null);
    //const exampleRef = useRef<null | string>(null) // MutableRefObject<null | string>
    //exampleRef.current = "test"

    const [DisableLeaseCode, setDisableLeaseCode] = useState([]);

    const [YearTextData, setYearTextData] = useState([{"YearText": "", "YearData": [{"id": "", "leaseStartMonthName": "", "leaseStartYear": "", "lease_code": "", "lease_end_date": "", "lease_price": "", "lease_start_date": "", "lease_start_date_text": ""}]}]);
    const [DisableData, setDisableData] = useState([{"YearText": "", "YearData": [{"id": "", "leaseStartMonthName": "", "leaseStartYear": "", "lease_code": "", "lease_end_date": "", "lease_price": "", "lease_start_date": "", "lease_start_date_text": ""}]}]);

    const [showModal, setShowModal] = useState(false);
    const [PopID, setPopID] = useState(0);

    //const componentRef = useRef(null)
    //const componentRef = React.useRef() as React.MutableRefObject<HTMLInputElement>;
    //const componentRef = useRef<HTMLDivElement>(null);

    //const componentRef = useRef<HTMLInputElement>(null);
    //const componentRef = useR React.RefObject<HTMLDivElement>;
    
    //const handlePrintTest =async () => {
        //console.log('TEST');
        //console.log(componentRef.current);
        //const handlePrintDone = useReactToPrint({content: () => componentRef.current, });
    //}

    //const handlePrint = useReactToPrint({content: () => componentRef.current, });
    
    //const handlePrint = useReactToPrint({
    //    content: () => componentRef.current,
    //});

    const handlePrint =async (PopID:any) => {
        window.open(`${Routes.printRentReceipt}` + "/" + PopID, '_system', 'location=yes');
        //history.push(`${Routes.Home}`);
        //window.print();
        //console.log('TEST');
        //console.log(componentRef.current);
        //const handlePrintDone = useReactToPrint({content: () => componentRef.current, });
    }

    //const paraRef = useRef<null | HTMLParagraphElement>(null)

    //useEffect(() => {
    //    console.log(paraRef.current.innerHTML) // "object is possibly 'null'"
    //})

    //useEffect(() => {
    //    const divElement = componentRef.current;
    //    console.log(divElement); // logs <div>I'm an element</div>
    //}, []);

    useIonViewWillEnter(() => {
        loadPaidData();
    });

    useEffect(() => {
        //alert(props.ReloadCount);
        loadPaidData();
    }, [props.ReloadCount]);

    const loadPaidData =async () => {
        if ((localStorage.getItem('user_code')!==null)) {
            const api = axios.create();
            api.get("/auth/token/refresh").then((response) => {
    
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
                const api = axios.create();
    
                const leaseData = {
                    LeaseCode: localStorage.getItem('user_code'), 
                    UserID: localStorage.getItem('user_id')
                };

                api.put("/propertyleases/paidbyLeaseCode", leaseData).then((resPData) => {
                    const retData = JSON.parse(JSON.stringify(resPData)).data;
                    console.log('Get Prop by Lease code');
                    console.log(retData);
                    
                    setDisableLeaseCode(retData.DisableLeaseCode);
                    
                    setYearTextData(retData.YearTextArr);
                    setDisableData(retData.DisableData);
                })
                .catch((error) => {
                    alert("Error Get Data!");
                })
            })
            .catch((error) => {
              alert("Token not Get!");
            })
        }
    }

    const clickPastData = async (PopID:any) => {
        setShowModal(false);
        setPopID(PopID);
        setShowModal(true);
    }

    const enterAnimation = (baseEl: any) => {
        const backdropAnimation = createAnimation()
          .addElement(baseEl.querySelector('ion-backdrop')!)
          .fromTo('opacity', '0.01', 'var(--backdrop-opacity)');
    
        const wrapperAnimation = createAnimation()
          .addElement(baseEl.querySelector('.modal-wrapper')!)
          .keyframes([
            { offset: 0, opacity: '0', transform: 'scale(0)' },
            { offset: 1, opacity: '0.99', transform: 'scale(1)' }
          ]);
    
        return createAnimation()
          .addElement(baseEl)
          .easing('ease-out')
          .duration(500)
          .addAnimation([backdropAnimation, wrapperAnimation]);
    }
    
    const leaveAnimation = (baseEl: any) => {
        return enterAnimation(baseEl).direction('reverse');
    }

    const callbackPopup = useCallback((PopID) => {
        alert(PopID);
    }, []);

    return (
        <div className="tab-content">
            <div className="digi-lease-search-results-box">
                <p>{t('Billing-History-Text5')} <b>{t('Billing-History-Text8')}</b></p>

                {/* <p ref={paraRef}>Current renders: {countRenders.current}</p> */}
                
                {/* <div className="bg-gray-200 p-6">
                    <button
                        type="button"
                        className="bg-gray-500 border border-gray-500 p-2 mb-4"
                        onClick={handlePrint}
                    >
                        {" "}
                        Print Resume{" "}
                    </button>
                    <ReactToPrint
                                trigger={() => <a href="#">Print this out!</a>}
                                content={() => componentRef}
                            />

                    <ReactToPrint trigger={() => (<div>PRINT</div>)} content={() => componentRef.current} />
                    <button onClick={printView}>Print calendar</button>
                    <ComponentToPrint ref={setInst} modules={MY_MODULES} />
                    <ComponentToPrint PopID="3" ref={componentRef} />
                </div> */}

                {YearTextData.map((yearitem, index) => (
                    <div key={index} className="search-result-box">
                        <h3>{yearitem.YearText}</h3>
                        <IonGrid className="">
                            <IonRow className="paid-rents-list">
                                {(yearitem.YearData).map((item, index1) => (
                                    <IonCol key={index1} size="12" sizeXl="6">
                                        <div className="tenants-lease-info tenants-lease-payment-info ">
                                            <IonGrid className="">
                                                <IonRow className="ion-align-items-center">
                                                    <IonCol size="5" sizeMd="4" className="rent-price-block paid-block">
                                                        <p><span>{t('Billing-History-Paid')}</span>: ${item.lease_price} CAD</p>
                                                        <h4>{item.leaseStartMonthName}</h4>
                                                        <p>{item.lease_start_date_text}</p>
                                                    </IonCol>
                                                    <IonCol size="7" sizeMd="8" className="rent-period-block ion-text-right">
                                                        <IonButton className="secondary-button view-receipt-button"  shape="round" fill="outline" onClick={() => clickPastData(item.id)}>{t('Billing-History-ViewReceipt')}</IonButton>
                                                        <IonButton className="secondary-button view-receipt-button"  shape="round" fill="outline" onClick={() => handlePrint(item.id)}>{t('Billing-History-PrintReceipt')}</IonButton>

                                                        {/* <ReactToPrint
                                                            trigger={() => <a href="#">Print this out!</a>}
                                                            content={() => componentRef}
                                                        /> */}
                                                    </IonCol>
                                                </IonRow>
                                            </IonGrid> 
                                        </div>                                   
                                    </IonCol>
                                ))}
                            </IonRow>

                            {/* <IonRow className="ion-justify-content-center">
                                <IonCol size="6" sizeMd="4" sizeLg="4" sizeXl="3" className="">
                                    <IonButton className="show-more-button" expand="full" shape="round" fill="solid">More</IonButton>
                                </IonCol>
                            </IonRow> */}
                        </IonGrid>
                    </div>
                ))}


                <h6>{t('Rent-Text1')} <b><u>{t('Tenants-Lease-Text3')} {DisableLeaseCode.join(", ")}</u></b></h6>

                {DisableData.map((yearitem, index) => (
                    <div key={index} className="search-result-box">
                        {(yearitem.YearData).map((item, index1) => (
                            <IonCol key={index1} size="12" sizeXl="6">
                                <div className="tenants-lease-info tenants-lease-payment-info ">
                                    <IonGrid className="">
                                        <IonRow className="ion-align-items-center">
                                            <IonCol size="4" sizeMd="4" className="rent-price-block paid-block">
                                                <p><span>{t('Tenants-Lease-Lease-Code')}</span>: {item.lease_code}</p>
                                                <p><span>{t('Billing-History-Paid')}</span>: ${item.lease_price} CAD</p>
                                            </IonCol>
                                            <IonCol size="4" sizeMd="4" className="rent-price-block paid-block">
                                                <h4>{item.leaseStartMonthName}</h4>
                                                <p>{item.lease_start_date_text}</p>
                                            </IonCol>
                                            <IonCol size="4" sizeMd="4" className="rent-period-block ion-text-right">
                                                <IonButton className="secondary-button view-receipt-button"  shape="round" fill="outline" onClick={() => clickPastData(item.id)}>{t('Billing-History-ViewReceipt')}</IonButton>
                                            </IonCol>
                                        </IonRow>
                                    </IonGrid> 
                                </div>                                   
                            </IonCol>
                        ))}
                    </div>
                ))}

                <IonModal isOpen={showModal} enterAnimation={enterAnimation} leaveAnimation={leaveAnimation} cssClass="rent-pay-receipt-popup">
                    <IonButton className="close-popup-button" fill="clear" onClick={() => setShowModal(false)}> 
                        <IonIcon icon={close} />
                    </IonButton>
                    <RentPayReceiptPopup parentCallback={callbackPopup} PopID={PopID} />
                </IonModal>
                {/* <ComponentToPrint ref={el => (componentRef = el)} PopID={PopID} /> */}
                {/* <ComponentToPrint PopID="4" ref={componentRef} /> */}
            </div>
        </div>
    );
};

export default RentsPayHistoryTab;