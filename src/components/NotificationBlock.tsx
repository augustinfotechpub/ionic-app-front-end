import React, {useEffect, useState} from "react";
import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";

import {  
    IonLabel, 
    IonButton, 
    IonIcon, 
    IonInput,
    IonGrid,
    IonRow,
    IonCol,
} from '@ionic/react';

import { searchSharp, addSharp } from "ionicons/icons";
import { alertCircleSharp } from "ionicons/icons";

import '@ionic/react/css/flex-utils.css';
import '../assets/css/Custom.css';
import '../assets/css/Responsive.css';
import { Routes } from "../App";

const NotificationBlock: React.FC<{SIndex: any; SText: any; SData: any; SCntData: any;}> = (props) => {
    const { t } = useTranslation();

    return (
        <div key={props.SIndex} >
            {props.SCntData > 0 ? <h6>{props.SText}</h6> : ""}
            <IonRow className="">
                {props.SData.map((item, index) => (
                    <IonCol size="12" sizeMd="6" sizeLg="12" sizeXl="6">
                        <div className={item.n_type=='Normal' ? "notification-box yellow-notification" : "notification-box red-notification"}>
                            <IonButton className="notification-detail-button" fill="clear" expand="full" shape="round" routerLink={item.n_link}></IonButton>
                            <div className="notofication-content">
                                <div className="notification-box-header">
                                    <h6>{item.n_title} <span>{item.n_subtitle}</span></h6>
                                    {item.n_type=='Normal' ? <IonIcon slot="icon-only" src="/assets/images/Pay-Rent-icon-only.svg" /> : <IonIcon icon={alertCircleSharp} />}
                                </div>
                                <p dangerouslySetInnerHTML={{ __html: item.n_desc }} />
                                <span className="notification-date">{item.n_date_text}</span>                                  
                            </div>
                        </div>
                    </IonCol>
                ))}
            </IonRow>
        </div>
    );
};

export default NotificationBlock;