import React, { useState, useEffect } from 'react';

import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";

import { 
    IonHeader, 
    IonGrid,
    IonRow,
    IonCol,
    IonImg,
    IonButton,
    IonPopover,
    IonIcon,
    useIonPopover,
    IonList,
    IonRadioGroup,
    IonItem,
    IonLabel,
    IonRadio,
    IonLoading,
    IonSelect,
    IonSelectOption,
    useIonViewWillEnter,
    useIonViewDidLeave,
  } from '@ionic/react';

import ChangePropertyPopover from './Change-Property-Popup';

import { createOutline, addOutline, close, alertCircleSharp, settingsSharp } from "ionicons/icons";

import HeadNotificationBlock from '../components/HeadNotificationBlock';

import '../assets/css/Custom.css';
import '../assets/css/Responsive.css';
import { Routes } from '../App';

import { globalConst } from "../constants";

import "../i18n";
import { TranslatorProvider, useTranslate } from "react-translate"

import axios from "axios";
axios.defaults.baseURL = globalConst.baseURL;
axios.defaults.headers.post['Content-Type'] = globalConst.ContentType;
axios.defaults.headers.post['Access-Control-Allow-Origin'] = globalConst.AllowOrigin;
axios.defaults.headers.post['Accept'] = globalConst.Accept;
axios.defaults.timeout = globalConst.timeout;

const HeaderMain: React.FC<{ pageTitle: string; logoHide: string; onBack?: () => void }> = props => {
    
    const [popaccountState, setShowPopaccount] = useState<{showPopaccount: boolean, event: Event | undefined}>({ showPopaccount: false, event: undefined });
    useEffect(() => {
        console.log(popaccountState); // do something after state has updated
    }, [popaccountState]);
    
    let history = useHistory();

    const [UserTypeData, setUserTypeData] = useState<any>({'id': '', 'name': '', 'lease_code': ''});
    const [FeatureAccessData, setFeatureAccessData] = useState<any>({'manage_posts': '', 'manage_propertys': '', 'manage_rents': ''});

/*     const [selected, setSelected] = useState(2);
    const [ArrTest, setArrTest] = useState([{id: 1, text: "Test 1"}, {id: 2, text: "Test 2"}, {id: 3, text: "Test 3"}, { id: 4, text: "Test 4"}, { id: 5, text: "Test 5"}, { id: 6, text: "Test 6"}, { id: 7, text: "Test 7"}]);

 */
    const [SelPropID, setSelPropID] = useState<number>();
    const [SelPropName, setSelPropName] = useState<any>();
    const [SelPropSuffix, setSelPropSuffix] = useState<any>();
    const [UnreadMsg, setUnreadMsg] = useState<any>('0');
    const [UnreadNotification, setUnreadNotification] = useState<any>('0')
    const [SendData, setSendData] = useState([{"Text": "", "CntData": 0, "Data": [{"id": ""}]}]);

    const [LogoLink, setLogoLink] = useState('');

    const [popoverState, setShowPopover] = useState<{showPopover: boolean, event: Event | undefined}>({ showPopover: false, event: undefined });
    const [showLoading, setShowLoading] = useState(true);
    const [PropData,setPropData]=useState([{"id": '', "name": "", "suffix": "", "address": "", "postalcode": "", "Country": "", "State": "", "City": "", "Unitformat": ""}]);
    const [accountPopoverState, setAccountShowPopover] = useState<{showAccountPopover: boolean, event: Event | undefined}>({ showAccountPopover: false, event: undefined });

    useEffect(() => {
        if ((localStorage.getItem('user_type_id')!==null)) {
            setUserTypeData({'id': localStorage.getItem('user_type_id'), 'name': localStorage.getItem('user_type_name'), 'lease_code': localStorage.getItem('user_code')});
            setFeatureAccessData({'manage_posts': localStorage.getItem('manage_posts'), 'manage_propertys': localStorage.getItem('manage_propertys'), 'manage_rents': localStorage.getItem('manage_rents')});
        }

        if ((localStorage.getItem('sel_prop_name')!='') && (localStorage.getItem('sel_prop_name')!=='null')) {
            setSelPropName(localStorage.getItem('sel_prop_name'));
        }

        if ((localStorage.getItem('sel_prop_suffix')!='') && (localStorage.getItem('sel_prop_suffix')!=='null')) {
            setSelPropSuffix(localStorage.getItem('sel_prop_suffix'));
        }

        if ((localStorage.getItem('sel_prop_id')!='') && (localStorage.getItem('sel_prop_id')!='0') && (localStorage.getItem('sel_prop_id')!=='null')) {
            setSelPropID(Number(localStorage.getItem('sel_prop_id')));
        }
        
    }, []);

    useEffect(() => {
        console.log(popoverState); // do something after state has updated
    }, [popoverState]);

    useIonViewWillEnter(() => {
        if (localStorage.getItem('loginState')!='1') {
            setLogoLink(`${Routes.Home}`);
        } else {
            setLogoLink(`${Routes.UserDashboard}`);
        }

        if ((localStorage.getItem('sel_prop_name')!='') && (localStorage.getItem('sel_prop_name')!=='null')) {
            setSelPropName(localStorage.getItem('sel_prop_name'));
        }

        if ((localStorage.getItem('sel_prop_suffix')!='') && (localStorage.getItem('sel_prop_suffix')!=='null')) {
            setSelPropSuffix(localStorage.getItem('sel_prop_suffix'));
        }

        if ((localStorage.getItem('sel_prop_id')!='') && (localStorage.getItem('sel_prop_id')!='0') && (localStorage.getItem('sel_prop_id')!=='null')) {
            setSelPropID(Number(localStorage.getItem('sel_prop_id')));
        }
      
        //console.log('useIonViewWillEnter Header');
        //console.log(props);
        if ((localStorage.getItem('user_id')!='') && (localStorage.getItem('user_id')!=='null')) {
            loadUnreadData();
        }

        if ((localStorage.getItem('user_type_id')!==null) && (localStorage.getItem('user_type_id')!='1')) {
            loadHeaderData();
        } else {
            setShowLoading(false);
        }
    });

    async function loadUnreadData() {
        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {
    
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            api.get("/messaging/unreadcnt/"+localStorage.getItem('user_id')).then((resUType) => {
                const retData = JSON.parse(JSON.stringify(resUType)).data;
                setUnreadMsg(retData.cntData);
            })
            .catch((error) => {
                //alert("Error Get in User Type!");
            })

            api.get("/notification/unreadcnt/"+localStorage.getItem('user_id')).then((resUType) => {
                const retData = JSON.parse(JSON.stringify(resUType)).data;
                setUnreadNotification(retData.cntData);
                setSendData(retData.SendData);
            })
            .catch((error) => {
                //alert("Error Get in User Type!");
            })

            /*
            api.get("/propertyleases/sendnotification").then((resUType) => {
                const retData = JSON.parse(JSON.stringify(resUType)).data;
                console.log('Send Notification');
                console.log(retData);
            })
            .catch((error) => {
                alert("Error Get in User Type!");
            })
            */
        })
        .catch((error) => {
          alert("Token not Get!");
        })
    }
  
    const clearNotifications = () => {
        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {
    
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            api.get("/notification/cleardata/"+localStorage.getItem('user_id')).then((resUType) => {
                const retData = JSON.parse(JSON.stringify(resUType)).data;
                closeNotificationPopup();
                loadUnreadData();
            })
            .catch((error) => {
                alert("Error Get in User Type!");
            })
        })
        .catch((error) => {
          alert("Token not Get!");
        })
    }

    async function loadHeaderData() {
        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {
    
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            api.get("/propertys/?page=1&per_page=4&current_user="+localStorage.getItem('user_management_id')).then((resUType) => {
                const retData = JSON.parse(JSON.stringify(resUType)).data;
                setPropData(retData.data);
                setShowLoading(false);
                console.log(retData)
            })
            .catch((error) => {
                alert("Error Get in User Type!");
            })
        })
        .catch((error) => {
          alert("Token not Get!");
        })
    }

    const [changeProperty, setChangeProperty] = useState({ showPopover: false, event: undefined });

    const [present, dismiss] = useIonPopover(ChangePropertyPopover, { onHide: () => dismiss() });

    const pageBack = () => {
        history.goBack();
    };
  

    const clickProperty = (event: CustomEvent, pathN) => {
        if ((event.detail.value!='') && (event.detail.value!==undefined)) {
            setShowLoading(true);

            console.log(event.detail.value);

            const api = axios.create();
            api.get("/auth/token/refresh").then((response) => {
        
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
                const api = axios.create();

                const propData = {
                    propID: event.detail.value,
                    user_id: localStorage.getItem('user_id')
                };

                api.put("/auth/updateSelProp", propData).then((resData) => {
                    const resReturnData = JSON.parse(JSON.stringify(resData)).data;
                    console.log(resReturnData);

                    localStorage.setItem('sel_prop_id', resReturnData.user_prop_id);
                    localStorage.setItem('sel_prop_name', resReturnData.user_prop_name);
                    localStorage.setItem('sel_prop_suffix', resReturnData.user_prop_suffix);

                    setSelPropID(Number(resReturnData.user_prop_id));
                    setSelPropName(resReturnData.user_prop_name);
                    setSelPropSuffix(resReturnData.user_prop_suffix);

                    setShowLoading(false);

                    closePopup();

                    //history.push(`${Routes.UserDashboard}`);
                    history.push({
                        pathname: pathN,
                        state: { path: pathN, prop_id: resReturnData.user_prop_id }
                    });
                })
                .catch((error) => {
                    alert("Error Update User Lease Code!");
                })
            })
            .catch((error) => {
                alert("Token not Get!");
            })

            //setSelPropName(event.detail.value.name);
            //setSelPropSuffix(event.detail.value.suffix);
        }
    };

    const closePopup = () => {
        setChangeProperty({ showPopover: false, event: undefined })
    };

    const closeNotificationPopup = () => {
        setShowPopover({ showPopover: false, event: undefined })
    };

    const closeAccountPopup = () => {
        setAccountShowPopover({ showAccountPopover: false, event: undefined })
        //setShowPopaccount({ showPopaccount: false, event: undefined })
    };


    /* const handleRadio = (e) => {
        setSelected(e.detail.value);
        console.log(selected);
    }; */

    const { t, i18n } = useTranslation();
    const [selLang, setselLang] = useState(i18n.language);

    function changeLanguageHandler(selLang) {
        setselLang(selLang);
        i18n.changeLanguage(selLang);
    }
        
    return (
        
        <IonHeader className="inner-main-header">
            <IonLoading
              isOpen={showLoading}
              onDidDismiss={() => setShowLoading(false)}
              message={'Loading...'}
            />
            <IonGrid>
                <IonRow className="">

                    <IonButton className="back-buton" fill="clear"  onClick={props.onBack}>
                        <IonIcon slot="icon-only" src="/assets/images/back-arrow.svg" />
                    </IonButton>

                    <div className="apart-logo">
                        <IonButton className={props.logoHide} routerLink={LogoLink} fill="clear">
                            <IonImg className="logo" src="assets/images/aparto-logo.svg" />
                        </IonButton>
                    </div>

                    <h2 className="header-page-title">{props.pageTitle}</h2>

                    <IonItem className="no-border">
                        <IonSelect value={selLang} onIonChange={e => changeLanguageHandler(e.detail.value)}>
                            <IonSelectOption value="en">English</IonSelectOption>
                            <IonSelectOption value="es">Spanish</IonSelectOption>
                            <IonSelectOption value="fr">French</IonSelectOption>
                        </IonSelect>
                    </IonItem>

                    {/* notification popup and button start  */}
                    <IonPopover
                        cssClass='notification-popup'
                        mode="ios"
                        event={popoverState.event}
                        isOpen={popoverState.showPopover}
                        onDidDismiss={() => setShowPopover({ showPopover: false, event: undefined })}
                    >
                        <IonButton className="close-popup-button ion-hide-lg-up" fill="clear" onClick={closeNotificationPopup}>
                                <IonIcon icon={close} />
                        </IonButton>
                        <div className="notification-list">
                            {SendData.map((item, index) => (
                                <HeadNotificationBlock SIndex={index} SText={item.Text} SData={item.Data} SCntData={item.CntData} closePopup={closeNotificationPopup} />
                            ))}
                        </div>
                        <div className="notification-footer">
                            <IonButton className="notification-history-button" fill="clear" routerLink={Routes.notificationHistory} onClick={closeNotificationPopup}>{t('Head-History')}</IonButton>
                            <IonButton className="notification-clear-button" fill="clear" onClick={clearNotifications}>{t('Head-ClearNotifications')}</IonButton>
                        </div>
                    </IonPopover>
                    
                    {UserTypeData.id != '' ? (
                        <IonButton className="notification-button ion-hide-lg-down" fill="clear" onClick={
                            (e) => {
                            // e.persist();
                            setShowPopover({ showPopover: true, event: e.nativeEvent })
                            }}
                        >
                            <IonIcon slot="icon-only" src="/assets/images/bell-icon.svg" />
                            {UnreadNotification=='0' ? '' : <span className="notification-count">{UnreadNotification}</span>}
                        </IonButton>
                    ) : (
                        <IonButton className="notification-button ion-hide-lg-down" fill="clear"></IonButton>
                    )}
                    {/* notification popup and button end  */}


                    {UserTypeData.id != '' ? (<IonButton className="message-button ion-hide-lg-down" fill="clear" routerLink={Routes.messaging}>
                        <IonIcon slot="icon-only" src="/assets/images/mail_outline_black_24dp.svg" />
                        {UnreadMsg=='0' ? '' : <span className="notification-count">{UnreadMsg}</span>}
                    </IonButton>) : ('')}


		    {/* account popup and button start  */}  
                    <IonPopover
                        cssClass='footer-menu'
                        mode="ios"
                        event={accountPopoverState.event}
                        isOpen={accountPopoverState.showAccountPopover}
                        onDidDismiss={() => setAccountShowPopover({ showAccountPopover: false, event: undefined })}
                    >
                        <IonList className="footer-mobile-menu">
                            {UserTypeData.id == '2' ? (
                                <IonItem>
                                    <IonButton className="" fill="clear" routerLink={Routes.tenantaccount} onClick={closeAccountPopup}>
                                        <div className="">
                                            <IonIcon icon="/assets/images/swap-horizontal.svg"  />
                                            <span className="">{t('menu-tenant-account')}</span>
                                        </div>
                                    </IonButton>
                                </IonItem>
                            ) : ('')}

                            <IonItem>
                                <IonButton className="" fill="clear" routerLink={Routes.myAccount} onClick={closeAccountPopup}>
                                    <div className="">    
                                        <IonIcon icon="/assets/images/ios-person.svg"  />
                                        <span className="">{t('menu-account-settings')}</span>
                                    </div>
                                </IonButton>
                            </IonItem>

                            {UserTypeData.id == '1' ? (
                                <IonItem>
                                    <IonButton className="" fill="clear" routerLink={Routes.postBillingLising} onClick={closeAccountPopup}>
                                        <div className="">    
                                            <IonIcon icon="/assets/images/bill-icon.svg"  />
                                            <span className="">{t('menu-bonus-post-list')}</span>
                                        </div>
                                    </IonButton>
                                </IonItem>
                            ) : ('')}

                            {UserTypeData.id == '2' ? (
                                <IonItem>
                                    <IonButton className="" fill="clear" routerLink={Routes.billingHistory} onClick={closeAccountPopup}>
                                        <div className="">    
                                            <IonIcon icon="/assets/images/bill-icon.svg"  />
                                            <span className="">{t('menu-billing-history')}</span>
                                        </div>
                                    </IonButton>
                                </IonItem>
                            ) : ('')}

                            {/* <IonItem>
                                <IonButton className="" fill="clear" routerLink={Routes.serviceSettings} onClick={closeAccountPopup}>
                                    <div className="">    
                                        <IonIcon icon={settingsSharp}  />
                                        <span className="">Service Settings</span>
                                    </div>
                                </IonButton>
                            </IonItem>
 */}
                            <IonItem>
                                <IonButton className="" fill="clear" routerLink={Routes.faq} onClick={closeAccountPopup}>
                                    <div className="">    
                                        <IonIcon />
                                        <span className="">{t('menu-faq')}</span>   
                                    </div>
                                </IonButton>
                            </IonItem>

                            <IonItem>
                                <IonButton className="" fill="clear" onClick={closeAccountPopup} routerLink={Routes.Logout}>
                                    <div className="">    
                                        <IonIcon />
                                        <span className="">{t('menu-logout')}</span>
                                    </div>
                                </IonButton>
                            </IonItem>

                        </IonList>
                    </IonPopover>
                    <IonButton 
                        className="account-button ion-hide-lg-down" 
                        fill="clear" 
                        routerLink="#"onClick={
                            (e) => {
                            // e.persist();
                            setAccountShowPopover({ showAccountPopover: true, event: e.nativeEvent })
                        }}
                    >
                        <IonIcon slot="icon-only" src="/assets/images/ios-person.svg"/>
                    </IonButton>
                    {/* account popup and button end  */} 
                    
                    {/* change property popup and button start  */} 
                    <IonPopover
                        cssClass='change-property-popup'
                        event={changeProperty.event}
                        isOpen={changeProperty.showPopover}
                        onDidDismiss={() => setChangeProperty({ showPopover: false, event: undefined })}
                    >
                        <IonGrid className="change-property-popup-wrapper">
                            <IonButton className="close-popup-button" fill="clear" onClick={closePopup}>
                                <IonIcon icon={close} />
                            </IonButton>   


                            {/* <Link to={{pathname: "/courses", search: "?sort=name", hash: "#the-hash", state: { fromDashboard: true }}} >Test</Link> */}

                            <IonList>
                                <p>{t('Head-Selectpropeprty')}</p>
                                <IonRadioGroup value={SelPropID} onIonChange={(e: any) => clickProperty(e, Routes.manageTenants)}>

                                {PropData.map((item, index) => (
                                    <IonItem key={item.id} className="user-select-item active">
                                        <IonLabel>
                                            {item.suffix} - {item.name}
                                            <p>{item.address}, {item.City}-{item.postalcode}, {item.State} {item.Country}</p>
                                        </IonLabel>
                                        <IonRadio mode="md" value={item.id} />
                                    </IonItem>
                                ))}

                                </IonRadioGroup>
                            </IonList>

                            {/* {ArrTest.map((res, index) => (
                                <IonList key={index}>
                                    <IonRadioGroup
                                    allowEmptySelection
                                    color="danger"
                                    value={SelPropID}
                                    onIonChange={handleRadio}
                                    >
                                    <IonItem>
                                        <IonLabel>{res.text}</IonLabel>
                                        <IonRadio slot="start" value={res.id}></IonRadio>
                                    </IonItem>
                                    </IonRadioGroup>
                                </IonList>
                            ))} */}

                            <IonGrid>
                                <IonRow>
                                    <IonCol size="6" className="login-btn">
                                        <IonButton className="manage-property-btn secondary-button" routerLink={Routes.manageProperties} expand="block" shape="round" fill="outline" onClick={closePopup}>
                                            <div className="button-inner">
                                                <IonIcon icon={createOutline} />
                                                <span>{t('Head-ManagePropeprty')}</span>
                                            </div>
                                        </IonButton>
                                    </IonCol>
                                    <IonCol size="6" className="login-btn">
                                        <IonButton className="add-property-btn" routerLink={Routes.addNewProperty} expand="block" shape="round" fill="outline" onClick={closePopup}>
                                            <div className="button-inner">
                                                <IonIcon icon={addOutline} />
                                                <span>{t('Head-AddPropeprty')}</span>
                                            </div>
                                        </IonButton>
                                    </IonCol>
                                </IonRow>
                            </IonGrid>
                        </IonGrid>
                    </IonPopover>

                    <IonButton 
                        className="change-property" 
                        fill="clear" 
                        // onClick={(e) =>
                        //   present({
                        //     event: e.nativeEvent,
                        //     cssClass: 'change-property-popup'
                        //   })
                        // }
                        onClick={
                            (e: any) => {
                                if ((`${UserTypeData.id}` == '2') || ((`${UserTypeData.id}` == '3') && (`${FeatureAccessData.manage_propertys}` == '1'))) {
                                    //alert(`${UserTypeData.id}`);
                                    e.persist();
                                    setChangeProperty({ showPopover: true, event: e })
                                }
                        }}
                    >
                        <IonIcon slot="icon-only" src="/assets/images/home-icon.svg" />
                        <div className="proerty-name">{SelPropSuffix} <span className="ion-hide-lg-down">- {SelPropName}</span></div>
                    </IonButton>
                    {/* change property popup and button end  */} 
                </IonRow>
            </IonGrid>
        </IonHeader>
        
    );
};

export default HeaderMain;