import React, {useState, useCallback, useEffect} from "react";
import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";

import {  
    IonLabel, 
    IonButton, 
    IonIcon, 
    IonInput,
    IonGrid,
    IonRow,
    IonCol,
    useIonPopover,
    createAnimation,
    IonModal,
    IonAlert
} from '@ionic/react';

import { searchSharp, close } from "ionicons/icons";

import RentPayReceiptPopup from "./RentPayReceiptPopup";

import '@ionic/react/css/flex-utils.css';
import '../assets/css/Custom.css';
import '../assets/css/Responsive.css';
import { Routes } from "../App";

import { globalConst } from "../constants";

import axios from "axios";
axios.defaults.baseURL = globalConst.baseURL;
axios.defaults.headers.post['Content-Type'] = globalConst.ContentType;
axios.defaults.headers.post['Access-Control-Allow-Origin'] = globalConst.AllowOrigin;
axios.defaults.headers.post['Accept'] = globalConst.Accept;
axios.defaults.timeout = globalConst.timeout;

const ManageRentsSearchTab: React.FC<{SearchPropID, SearchAptNo, SearchData, parentCallback}> = ({SearchPropID, SearchAptNo, SearchData, parentCallback}) => {

    const { t } = useTranslation();
    let history = useHistory();

    // const [present, dismiss] = useIonPopover(RentPayReceiptPopup, { onHide: () => dismiss() });
    const [showModal, setShowModal] = useState(false);

    const [FlagSearchText, setFlagSearchText] = useState(false);

    const [OverdueData, setOverdueData] = useState([{"id": "", "property_id": "", "floorsno": "", "aptno": "", "lease_code": "", "lease_price": "", "lease_price_pay": "", "lease_start_date": "", "lease_end_date": "", "end_date": "", "start_date": "", "lease_payment_flag": "", "lease_payment_date": "", "pay_date": ""}]);
    const [UpcomingData, setUpcomingData] = useState([{"id": "", "property_id": "", "floorsno": "", "aptno": "", "lease_code": "", "lease_price": "", "lease_price_pay": "", "lease_start_date": "", "lease_end_date": "", "end_date": "", "start_date": "", "lease_payment_flag": "", "lease_payment_date": "", "pay_date": ""}]);
    const [PastLeasesData, setPastLeasesData] = useState([{"id": "", "property_id": "", "floorsno": "", "aptno": "", "lease_code": "", "lease_price": "", "lease_price_pay": "", "lease_start_date": "", "lease_end_date": "", "end_date": "", "start_date": "", "lease_payment_flag": "", "lease_payment_date": "", "pay_date": ""}]);

    const [SearchText, setSearchText] = useState("");
    const [SPID, setSPID] = useState("");
    const [PopID, setPopID] = useState(0);

    const [showAlert, setShowAlert] = useState(false);
    const [msgAlert, setMsgAlert] = useState('');
    const [headAlert, setHeadAlert] = useState('');

    const enterAnimation = (baseEl: any) => {
        const backdropAnimation = createAnimation()
          .addElement(baseEl.querySelector('ion-backdrop')!)
          .fromTo('opacity', '0.01', 'var(--backdrop-opacity)');
    
        const wrapperAnimation = createAnimation()
          .addElement(baseEl.querySelector('.modal-wrapper')!)
          .keyframes([
            { offset: 0, opacity: '0', transform: 'scale(0)' },
            { offset: 1, opacity: '0.99', transform: 'scale(1)' }
          ]);
    
        return createAnimation()
          .addElement(baseEl)
          .easing('ease-out')
          .duration(500)
          .addAnimation([backdropAnimation, wrapperAnimation]);
    }
    
    const leaveAnimation = (baseEl: any) => {
        return enterAnimation(baseEl).direction('reverse');
    }

    const clickSearch = async () => {
        //parentCallback('111', '111');
        
        setSPID(SearchPropID);
        //setSAptNo(SearchText);

        setFlagSearchText(true);

        loadRentData(SearchPropID, SearchText);
    }

    useEffect(() => {
        setOverdueData([]);
        setPastLeasesData([]);
        setUpcomingData([]);
    }, []);


    async function loadRentData(PropID: any, AptNo: any) {
        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {

            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            const searchInfo = {
                "PropID" : PropID,
                "AptNo" : AptNo,
                "UserID" : localStorage.getItem('user_id'),
            };

            api.put("/propertyfloors/checkExists", searchInfo).then((resPData) => {

                api.put("/propertyleases/allrentdatabyaptno", searchInfo).then((resUnitData) => {
                    const retData = JSON.parse(JSON.stringify(resUnitData)).data;
                    console.log(retData);

                    setOverdueData(retData.OverdueData);
                    setPastLeasesData(retData.PastData);
                    setUpcomingData(retData.UpcomingData);

                    //setAddNewLease(retData.AddNewLease);
                    //setRenewalNewLease(retData.RenewalNewLease);

                    //setMaxDateData([retData.MaxDate, retData.MaxDateText, retData.MaxID]);

                })
                .catch((error) => {
                    alert("Error Get Data!");
                })
            })
            .catch((error) => {
                setOverdueData([]);
                setPastLeasesData([]);
                setUpcomingData([]);
        
                setMsgAlert(t('Rent-Manage-Property-Error-Msg'));
                setHeadAlert(t('Rent-Manage-Property-Error-Head'));
                setShowAlert(true);
            })
        })
        .catch((error) => {
          alert("Token not Get!");
        })
    }

    const clickPastData = async (PopID:any) => {
        setShowModal(false);
        setPopID(PopID);
        setShowModal(true);
    }

    const callbackPopup = useCallback((PopID) => {
        alert(PopID);
    }, []);

    const clickRentPay = async (path:any, RentID:any) => {
        history.push({
            pathname: path,
            state: { path: path, RentID: RentID }
        });
    }

    const clickWillDismiss = (FlagDismiss) => {
        //Nothing
    };

    return (
        <div className="tab-content">
            <div className="find-digi-lease-block">
                <IonAlert
                    isOpen={showAlert}
                    onDidDismiss={() => setShowAlert(false)}
                    onWillDismiss={() => clickWillDismiss(true)}
                    cssClass='orange-alert'
                    header={headAlert}
                    message={msgAlert}
                    buttons={[{text: 'Close', cssClass: 'btn-primary'}]}
                />

                <h6>{t('Rent-Manage-SearchTool-Text1')}</h6>
                <div className="search-box">
                    <IonLabel>{t('Rent-Apartment')} #</IonLabel>
                    <IonInput onIonInput={(e: any) => setSearchText(e.target.value)} mode="md" type="text" value={SearchText}></IonInput>
                    <IonButton onClick={() => clickSearch()}  expand="full" fill="solid" shape="round">
                        <div className="">    
                            <IonIcon icon={searchSharp}  />
                            <span className="">{t('General-Search')}</span>
                        </div>
                    </IonButton>
                </div>
            </div>

            <div className="digi-lease-search-results-box">
                {FlagSearchText ? (<h6><b>{t('Rent-Manage-SearchTool-Text2')}</b> {t('Rent-Manage-SearchTool-Text3')} <b><u>{t('Rent-Manage-SearchTool-Text4')} {SearchText}</u></b></h6>) : ('')}

                {/* <div className="search-result-box">
                    <IonRow className="ion-justify-content-center">
                        <IonCol size="6" sizeMd="4" sizeLg="4" sizeXl="3" className="">
                            <IonButton className="show-more-button" expand="full" shape="round" fill="solid">More</IonButton>
                        </IonCol>
                    </IonRow>
                </div> */}

                <div className="search-result-box">
                    {UpcomingData.length > 0 ? <h5>{t('Rent-Manage-SearchTool-Upcoming')}</h5>: null }

                    {UpcomingData.map((item, index) => (
                        <div key={index} className="tenants-lease-info tenants-lease-payment-info ">
                            <IonButton 
                                className="edit-lease-info-button" 
                                fill="clear" 
                                expand="full" 
                                shape="round" 
                                onClick={() => clickRentPay(Routes.rentPayDetails, item.id)}
                            >
                            </IonButton>

                            <IonGrid className="">
                                <IonRow className="">
                                    <IonCol size="3" sizeMd="3" sizeLg="2" sizeXl="2" className={(function() { if (item.lease_price_pay=='0') { return 'rent-price-block due-block'; } else { return 'rent-price-block paid-block'; }})()}>
                                        {(function() {
                                            if (item.lease_price_pay=='0') {
                                                return <h6>{t('Rent-Manage-SearchTool-Due')}:</h6>;
                                            } else {
                                                return <h6>{t('Rent-Manage-SearchTool-Paid')}:</h6>;
                                            }
                                        })()}
                                        {(function() {
                                            if (item.lease_price_pay=='0') {
                                                return <p>${item.lease_price}</p>;
                                            } else {
                                                return <p>${item.lease_price_pay}</p>;
                                            }
                                        })()}
                                    </IonCol>
                                    <IonCol size="4" sizeMd="3" sizeLg="2" sizeXl="2" className="payment-date-block">
                                        {(function() {
                                            if (item.lease_price_pay=='0') {
                                                return <h6>{t('Rent-Manage-SearchTool-DueDate')}:</h6>;
                                            } else {
                                                return <h6>{t('Rent-Manage-SearchTool-PayDate')}:</h6>;
                                            }
                                        })()}
                                        <p>{item.pay_date}</p>
                                    </IonCol>
                                    <IonCol size="5" sizeMd="6" sizeLg="8" sizeXl="8" className="rent-period-block">
                                        <h6>{t('Rent-Manage-SearchTool-RentPeriod')}:</h6>
                                        <p>{item.start_date} <IonIcon icon="assets/images/arrow-right.svg" />{item.end_date}</p>
                                    </IonCol>
                                </IonRow>
                            </IonGrid>                                    
                        </div>
                    ))}
		    {/* <IonRow className="ion-justify-content-center">
                        <IonCol size="6" sizeMd="4" sizeLg="4" sizeXl="3" className="">
                            <IonButton className="show-more-button" expand="full" shape="round" fill="solid">More</IonButton>
                        </IonCol>
                    </IonRow>*/}
                </div>

                <div className="search-result-box">
                    {OverdueData.length > 0 ? <h5>{t('Rent-Manage-SearchTool-Overdue')}</h5> : null}

                    {OverdueData.map((item, index) => (
                        <div key={index} className="tenants-lease-info tenants-lease-payment-info ">
                            <IonButton 
                                className="view-lease-info-button" 
                                fill="clear" 
                                expand="full" 
                                shape="round" 
                                onClick={() => clickRentPay(Routes.rentPayDetails, item.id)}
                            >
                            </IonButton>

                            <IonGrid className="">
                                <IonRow className="">
                                    <IonCol size="3" sizeMd="3" sizeLg="2" sizeXl="2" className={(function() { if (item.lease_price_pay=='0') { return 'rent-price-block due-block'; } else { return 'rent-price-block paid-block'; }})()}>
                                        {(function() {
                                            if (item.lease_price_pay=='0') {
                                                return <h6>{t('Rent-Manage-SearchTool-Due')}:</h6>;
                                            } else {
                                                return <h6>{t('Rent-Manage-SearchTool-Paid')}:</h6>;
                                            }
                                        })()}
                                        {(function() {
                                            if (item.lease_price_pay=='0') {
                                                return <p>${item.lease_price}</p>;
                                            } else {
                                                return <p>${item.lease_price_pay}</p>;
                                            }
                                        })()}
                                    </IonCol>
                                    <IonCol size="4" sizeMd="3" sizeLg="2" sizeXl="2" className="payment-date-block">
                                        {(function() {
                                            if (item.lease_price_pay=='0') {
                                                return <h6>{t('Rent-Manage-SearchTool-DueDate')}:</h6>;
                                            } else {
                                                return <h6>{t('Rent-Manage-SearchTool-PayDate')}:</h6>;
                                            }
                                        })()}
                                        <p>{item.pay_date}</p>
                                    </IonCol>
                                    <IonCol size="5" sizeMd="6" sizeLg="8" sizeXl="8" className="rent-period-block">
                                        <h6>{t('Rent-Manage-SearchTool-RentPeriod')}:</h6>
                                        <p>{item.start_date} <IonIcon icon="assets/images/arrow-right.svg" />{item.end_date}</p>
                                    </IonCol>
                                </IonRow>
                            </IonGrid>                                    
                        </div>
                    ))}
                </div>

                <div className="search-result-box">
                    {PastLeasesData.length > 0 ? <h5>{t('Rent-Manage-SearchTool-Past')}</h5>: null }

                    {PastLeasesData.map((item, index) => (
                        <div key={index} className="tenants-lease-info tenants-lease-payment-info ">
                            <IonButton 
                                className="view-lease-info-button" 
                                fill="clear" 
                                expand="full" 
                                shape="round" 
                                onClick={() => clickPastData(item.id)}
                            >
                            </IonButton>

                            <IonGrid className="">
                                <IonRow className="">
                                    <IonCol size="3" sizeMd="3" sizeLg="2" sizeXl="2" className={(function() { if (item.lease_price_pay=='0') { return 'rent-price-block due-block'; } else { return 'rent-price-block paid-block'; }})()}>
                                        {(function() {
                                            if (item.lease_price_pay=='0') {
                                                return <h6>{t('Rent-Manage-SearchTool-Due')}:</h6>;
                                            } else {
                                                return <h6>{t('Rent-Manage-SearchTool-Paid')}:</h6>;
                                            }
                                        })()}
                                        {(function() {
                                            if (item.lease_price_pay=='0') {
                                                return <p>${item.lease_price}</p>;
                                            } else {
                                                return <p>${item.lease_price_pay}</p>;
                                            }
                                        })()}
                                    </IonCol>
                                    <IonCol size="4" sizeMd="3" sizeLg="2" sizeXl="2" className="payment-date-block">
                                        {(function() {
                                            if (item.lease_price_pay=='0') {
                                                return <h6>{t('Rent-Manage-SearchTool-DueDate')}:</h6>;
                                            } else {
                                                return <h6>{t('Rent-Manage-SearchTool-PayDate')}:</h6>;
                                            }
                                        })()}
                                        <p>{item.pay_date}</p>
                                    </IonCol>
                                    <IonCol size="5" sizeMd="6" sizeLg="8" sizeXl="8" className="rent-period-block">
                                        <h6>{t('Rent-Manage-SearchTool-RentPeriod')}:</h6>
                                        <p>{item.start_date} <IonIcon icon="assets/images/arrow-right.svg" />{item.end_date}</p>
                                    </IonCol>
                                </IonRow>
                            </IonGrid>                                    
                        </div>
                    ))}
		    {/* <IonRow className="ion-justify-content-center">
                        <IonCol size="6" sizeMd="4" sizeLg="4" sizeXl="3" className="">
                            <IonButton className="show-more-button" expand="full" shape="round" fill="solid">More</IonButton>
                        </IonCol>
                    </IonRow> */}

                    <IonModal isOpen={showModal} enterAnimation={enterAnimation} leaveAnimation={leaveAnimation} cssClass="rent-pay-receipt-popup">
                        <IonButton className="close-popup-button" fill="clear" onClick={() => setShowModal(false)}> 
                            <IonIcon icon={close} />
                        </IonButton>
                        <RentPayReceiptPopup parentCallback={callbackPopup} PopID={PopID} />
                    </IonModal>
                </div>
            </div>
        </div>
    );
};

export default ManageRentsSearchTab;