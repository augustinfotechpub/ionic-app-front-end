import React, {useEffect, useState} from "react";
import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";

import {  
    IonLabel, 
    IonButton, 
    IonIcon, 
    IonInput,
    IonGrid,
    IonRow,
    IonCol,
} from '@ionic/react';

import { searchSharp, addSharp } from "ionicons/icons";

import '@ionic/react/css/flex-utils.css';
import '../assets/css/Custom.css';
import '../assets/css/Responsive.css';
import { Routes } from "../App";

const ManageLeaseList: React.FC<{LeaseIndex: any; LeaseData: any; onclickButton?: () => void;}> = (props) => {
    const { t } = useTranslation();

    return (
        <div key={props.LeaseIndex} className="tenants-lease-info">
        <IonButton className="edit-lease-info-button" fill="clear" expand="full" shape="round" onClick={props.onclickButton}></IonButton>
        <IonGrid className="">
            <IonRow className="">
                <IonCol size="2" className="">
                    <div className="start-date-block">
                        <h6>{t('Tenants-Lease-Apt-No')}</h6>
                        <p>{props.LeaseData.aptno}</p>
                    </div>
                </IonCol>
                <IonCol size="3" className="">
                    <div className="start-date-block">
                        <h6>{t('Prop-Address')}</h6>
                        <p>
                            {props.LeaseData.address}<br />{props.LeaseData.cityname}<br />
                            {props.LeaseData.statename}<br />{props.LeaseData.countryname}
                        </p>
                    </div>
                </IonCol>
                <IonCol size="4" className="start-end-date-block">
                    <div className="end-date-block">
                        <h6>{t('Tenants-Lease-Details')}</h6>
                        {/* Lease Start and End date: */}
                        <p>{props.LeaseData.RentPeriod}</p>

                        {/* Number of Tenant in the Apartment:  */}

                        {/* Attached Lease documents  */}
                        <ol>
                            {props.LeaseData.FileData.map((Titem, Tindex) => (
                                <li>{Titem.file_name}</li>
                            ))}
                        </ol>
                    </div>
                </IonCol>
                <IonCol size="3" className="">
                    <div className="tenant-lease-info">
                        <h6>{t('Tenants-Lease-details')} <b>{props.LeaseData.TenantsData.length}</b></h6>
                        <ol>
                            {props.LeaseData.TenantsData.map((Titem, Tindex) => (
                                <li>{Titem.tenant_fname} {Titem.tenant_lname}</li>
                            ))}
                        </ol>
                    </div>
                </IonCol>
            </IonRow>
        </IonGrid>                                    
    </div>
);
};

export default ManageLeaseList;