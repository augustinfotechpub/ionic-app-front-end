import React, {ReactNode, useState, useEffect} from "react";
import { useTranslation } from "react-i18next";
import { useHistory } from "react-router-dom";

import { 
    IonButton, 
    IonGrid,
    IonRow, 
    IonCol,
    useIonViewWillEnter,
} from '@ionic/react';

import '@ionic/react/css/flex-utils.css';
import '../assets/css/Custom.css';
import '../assets/css/Responsive.css';
import { Routes } from '../App';

const ManageTenantGeneralTab: React.FC<{
        LoadUAptData: any; cntUAptData: any; clickMoreUAptData: () => void; clsMoreUApt: any; 
        LoadRAptData: any; cntRAptData: any; clickMoreRAptData: () => void;
        clickAptNo: () => void;
        children?: ReactNode;
        parentCallback;
    }> = (props) => {

    const { t } = useTranslation();
	let history = useHistory();

    useIonViewWillEnter(() => {
        //console.log('Tenants SSHIHIHIIH useIonViewWillEnter  event fired');
        //props.clickMoreUAptData();
    });

    const clickAptNo = async (SearchPID:any, SearchFloorNo:any, SearchAptNo:string, SearchID:any) => {
        //alert(SearchAptNo);
        props.clickAptNo();
        props.parentCallback(SearchPID, SearchFloorNo, SearchAptNo, SearchID);
    }

    const clickAddLease = async (path:any, SearchPID:any, SearchFloorNo:any, SearchAptNo:string, SearchID:any) => {
        //console.log('TEST');
        //console.log(path);

        history.push({
            pathname: path,
            state: { path: path, SearchPID: SearchPID, SearchFloorNo: SearchFloorNo, SearchAptNo: SearchAptNo, SearchID: SearchID }
        });
    }

    return (
        <div className="tab-content">
            <div className="apartments-list-block requires-renewal-proposal-block">
                <h5 className="title-with-line">{t('Tenants-Lease-General-Renewal')} ({props.cntRAptData})</h5>
                <IonGrid className="">
                    <IonRow className="apartments-list">
                        {props.LoadRAptData.map((item: any, ind1: any) => (
                            <IonCol key={item.id} size="6" sizeMd="6" sizeLg="6" sizeXl="3" className="">
                                <IonButton onClick={() => clickAptNo(item.pid, item.floorsno, item.aptno, item.id)} expand="full" fill="solid" shape="round">
                                <div>
                                        <span>{t('Tenants-Lease-Apt')}#</span>
                                        <h2>{item.aptno}</h2>
                                    </div>
                                </IonButton>
                                {/* <IonButton onClick={() => clickAddLease(Routes.addNewLease, item.pid, item.floorsno, item.aptno, item.id)} expand="full" fill="solid" shape="round">
                                    <div>
                                        <span>Apt#</span>
                                        <h2>{item.aptno}</h2>
                                    </div>
                                </IonButton> */}
                            </IonCol>
                        ))}
                    </IonRow>

                    {/* <IonRow className="ion-justify-content-center">
                        <IonCol size="6" sizeMd="6" sizeLg="6" sizeXl="3" className="">
                            <IonButton onClick={props.clickMoreRAptData} expand="full" className="show-more-button" shape="round" fill="solid">More</IonButton>
                        </IonCol>
                    </IonRow> */}
                </IonGrid>
            </div>

            <div className="apartments-list-block unoccupied-apartments-block">
                <h5 className="title-with-line">{t('Tenants-Lease-General-Unoccupied')} ({props.cntUAptData})</h5>
                <IonGrid className="">
                    <IonRow className="apartments-list">
                        {props.LoadUAptData.map((item: any, ind1: any) => (
                            <IonCol key={item.id} size="6" sizeMd="6" sizeLg="6" sizeXl="3" className="">
                                <IonButton onClick={() => clickAptNo(item.pid, item.floorsno, item.aptno, item.id)} expand="full" fill="solid" shape="round">
                                    <div>
                                        <span>{t('Tenants-Lease-Apt')}#</span>
                                        <h2>{item.aptno}</h2>
                                    </div>
                                </IonButton>
                            </IonCol>
                        ))}
                    </IonRow>

                    <IonRow className="ion-justify-content-center">
                        <IonCol size="6" sizeMd="6" sizeLg="6" sizeXl="3" className="">
                            <IonButton onClick={props.clickMoreUAptData} expand="full" className={props.clsMoreUApt} shape="round" fill="solid">{t('Tenants-Lease-More')}</IonButton>
                        </IonCol>
                    </IonRow>
                </IonGrid>
            </div>
        </div>
    );
};

export default ManageTenantGeneralTab;