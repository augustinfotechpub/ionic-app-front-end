import React, {useRef, useEffect, useState} from "react";
import { useTranslation } from "react-i18next";

import { 
    IonButton, 
    IonCol, 
    IonList,
    IonIcon,
    useIonViewWillEnter,
  } from '@ionic/react';

import '../assets/css/Custom.css';
import '../assets/css/Responsive.css';
import { Routes } from '../App';

import { globalConst } from "../constants";

import axios from "axios";
axios.defaults.baseURL = globalConst.baseURL;
axios.defaults.headers.post['Content-Type'] = globalConst.ContentType;
axios.defaults.headers.post['Access-Control-Allow-Origin'] = globalConst.AllowOrigin;
axios.defaults.headers.post['Accept'] = globalConst.Accept;
axios.defaults.timeout = globalConst.timeout;

const ManagementDashboard: React.FC<{ path: string; UserTypeData: any;}> = props => {

    const { t } = useTranslation();
    const [showLoading, setShowLoading] = useState(true);
    const [PropManageData,setPropManageData]=useState([{"id": '', "name": "", "suffix": "", "address": "", "postalcode": "", "Country": "", "State": "", "City": "", "Unitformat": "", "owner": ""}]);

    useIonViewWillEnter(() => {
        loadDeskData();
    });

    async function loadDeskData() {
        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {
    
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            api.get("/propertys/manage/"+localStorage.getItem('user_id')).then((resUType) => {
                const retData = JSON.parse(JSON.stringify(resUType)).data;
                setPropManageData(retData.data);
                setShowLoading(false);
                console.log(retData)
            })
            .catch((error) => {
                alert("Error Get in User Type!");
            })
        })
        .catch((error) => {
          alert("Token not Get!");
        })
    }

    const openChangePropertyPopup = () => {
        let element: HTMLElement = document.getElementsByClassName('change-property')[0] as HTMLElement;
        element.click();
    };

    return (
        <IonCol className="dashboard-content" size="12" sizeMd="12" sizeLg="6" sizeXl="8">
            <h4>{t('Tenants-Hi')} {localStorage.getItem('user_firstname')} {localStorage.getItem('user_lastname')},</h4>
            <p>{t('Tenants-Text9')}</p>

            {PropManageData.length == 0 ? (
                <IonList className="property-list no-property-selected">
                    <div  className="property-list-item">
                        <div  className="property-list-item-content">
                            <div className="property-item-footer">
                                <IonButton 
                                    className="change-property-button" 
                                    fill="clear"
                                    expand="block" 
                                    routerLink={Routes.addNewProperty}
                                >
                                    <span>{t('Prop-App')}</span>
                                    <IonIcon src="/assets/images/add-property-con.svg" />
                                </IonButton>
                            </div>
                        </div>
                    </div>
                </IonList>
            ) : ('')}

            {PropManageData.map((item, index) => (
                <IonList className="property-list" key={item.id}>
                    <div  className="property-list-item">
                        <div  className="property-list-item-content">
                            <h4 className="property-name">{item.suffix} - {item.name}</h4>
                            <p className="property-address">{item.address}, {item.City}-{item.postalcode}, {item.State} {item.Country}</p>
                            <div className="property-item-footer">
                                <p className="property-owner">{item.owner}</p>
                                <IonButton 
                                    className="change-property-button" 
                                    fill="clear" 
                                    onClick={openChangePropertyPopup}
                                >
                                    {/* <span>Change</span> */}
                                    <IonIcon src="/assets/images/Switch-Builindings-Icon.svg" />
                                </IonButton>
                            </div>
                        </div>
                    </div>
                </IonList>
            ))}
        </IonCol>
    );
};

export default ManagementDashboard;