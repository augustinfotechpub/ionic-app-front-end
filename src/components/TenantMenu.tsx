import React, { useState } from 'react';
import { useTranslation } from "react-i18next";

import {  
    IonLabel, 
    IonButton,  
    IonList,
    IonItem,
    IonAvatar
  } from '@ionic/react';

import '../assets/css/Custom.css';
import '../assets/css/Responsive.css';
import { Routes } from "../App";

const TenantMenu: React.FC<{ path: string; UserTypeData: any; }> = props => {
	
	const { t } = useTranslation();

    const getClassName = (routePath: string[], className: string): string => {
        return routePath.indexOf(props.path) > -1 ? (className + " active") : className
    }

    return (
        <IonList className="sidebar-menu-list">
            <IonItem>
                <IonButton className={getClassName([Routes.createPosts, Routes.postDetails, Routes.payRents, Routes.rentPayDetails, Routes.paymentMethod], "dashboard-button")} fill="clear" routerLink={Routes.payRents}>
                    <div className="dashboard-button-inner">    
                        <IonAvatar>
                            <img src="assets/images/Pay-Rent-home.svg" />
                        </IonAvatar>
                        <IonLabel  className="dashboard-button-label"><b>{t('sidebar-Pay-your-Rent')}</b></IonLabel>
                    </div>
                </IonButton>
            </IonItem>

            <IonItem>
                <IonButton className={getClassName([Routes.manageLeases, Routes.leaseInfo, Routes.editLeaseInfo, Routes.leaseRenewalProposal], "dashboard-button")} fill="clear" routerLink={Routes.manageLeases}>
                    <div className="dashboard-button-inner">
                        <IonAvatar>
                            <img src="assets/images/Manage-Lease-home.svg" />
                        </IonAvatar>
                        <IonLabel  className="dashboard-button-label">{t('sidebar-Manage-Lease')}</IonLabel>
                    </div>
                </IonButton>
            </IonItem>

        </IonList>
    );
};

export default TenantMenu;