import React from 'react';
import { useTranslation } from "react-i18next";

import { 
    IonButton,  
    IonItem, 
    IonList, 
    IonIcon
} from '@ionic/react';

import { settingsSharp } from "ionicons/icons";

import '../assets/css/Custom.css';
import '../assets/css/Responsive.css';
import { Routes } from '../App';

const FooterMenuPoup: React.FC<{
  onHide: () => void;
}> = ({ onHide }) => {

    const { t } = useTranslation();

    return (
        <div className="footer-menu">
        <IonList className="footer-mobile-menu">
            <IonItem>
                <IonButton className="" fill="clear" routerLink={Routes.tenantaccount}>
                    <div className="">
                        <IonIcon icon="/assets/images/swap-horizontal.svg"  />
                        <span className="">{t('menu-tenant-account')}</span>
                    </div>
                </IonButton>
            </IonItem>

            <IonItem>
                <IonButton className="" fill="clear" routerLink={Routes.myAccount}>
                    <div className="">    
                        <IonIcon icon="/assets/images/ios-person.svg"  />
                        <span className="">{t('menu-account-settings')}</span>
                    </div>
                </IonButton>
            </IonItem>

            <IonItem>
                <IonButton className="" fill="clear" routerLink={Routes.billingHistory}>
                    <div className="">    
                        <IonIcon icon="/assets/images/bill-icon.svg"  />
                        <span className="">{t('menu-billing-history')}</span>
                    </div>
                </IonButton>
            </IonItem>

            <IonItem>
                <IonButton className="" fill="clear" routerLink="#">
                    <div className="">    
                        <IonIcon icon={settingsSharp}  />
                        <span className="">{t('menu-ServiceSettings')}</span>
                    </div>
                </IonButton>
            </IonItem>

            <IonItem>
                <IonButton className="" fill="clear" routerLink={Routes.faq}>
                    <div className="">    
                        <IonIcon />
                        <span className="">{t('menu-faq')}</span>
                    </div>
                </IonButton>
            </IonItem>

            <IonItem>
                <IonButton className="" fill="clear" routerLink={Routes.Logout}>
                    <div className="">    
                        <IonIcon />
                        <span className="">{t('menu-logout')}</span>
                    </div>
                </IonButton>
            </IonItem>

        </IonList>
        </div>
    );
} 

export default FooterMenuPoup;