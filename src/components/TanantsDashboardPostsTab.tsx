import React, {useState, useEffect} from "react";
import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";

import { 
    IonButton, 
    IonGrid,
    IonRow, 
    IonCol,
    IonIcon,
    IonImg,
    useIonViewWillEnter
  } from '@ionic/react';

import '@ionic/react/css/flex-utils.css';
import '../assets/css/Custom.css';
import '../assets/css/Responsive.css';

import { caretDownOutline, createOutline, eyeOutline, addOutline, arrowBackOutline } from "ionicons/icons";
import { Routes } from "../App";

import { usePhotoGallery, UserPhoto  } from '../hooks/usePhotoGallery';

const TanatDahsboadPostsTab: React.FC<{PostData: any; onclickButton?: () => void;}> = (props) => {

    const { t } = useTranslation();
	let history = useHistory();

    const { deletePhoto, photos, prephotos, takePhoto } = usePhotoGallery('');

    const [UID, setUID] = useState<any>(localStorage.getItem('user_id'));

    const ViewPost = async (path, postid, filestoragekey) => {
        //history.push({pathname: path+"/"+leaseid});

        let searchText = '?post_id='+postid;
        history.push({
            pathname: path+"/"+postid,
			//search: searchText,
            state: { path: path, post_id: postid, filestoragekey: filestoragekey }
        });
    };

    const onEditClick = async (path, postID, file_storagekey) => {
        history.push({
            pathname: path+"/"+postID,
            state: { path: path, postID: postID, filestoragekey: file_storagekey }
        });
    };

    return (
        <div className="tab-content tenant-tab-content">
            <div className="posts-lists">
                {props.PostData.length == '0' ? (
                    <div>
                        <div className="posts-list-block">
                            {t('General-No-Data-Found')}
                        </div>
                        <div className="posts-list-block"><br /></div>
                        <div className="posts-list-block"><br /></div>
                        <div className="posts-list-block"><br /></div>
                    </div>
                ) : ''}
                
                {props.PostData.map((Pitem, Pindex) => (
                    <div key={Pitem.id} className="posts-list-block">
                        {/* <IonButton className="post-details-link-button" fill="clear" expand="full" shape="round" onClick={() => ViewPost(Routes.postDetails, Pitem.id, Pitem.filestoragekey)}></IonButton> */}
                        <IonGrid className="">
                            <IonRow className="post-row">
                                {UID == Pitem.user_id ? 
                                <IonCol className="accordion-edit-button">
                                        <IonButton fill="clear" shape="round" onClick={() => onEditClick(Routes.editPost, Pitem.id, Pitem.filestoragekey)} >
                                            <IonIcon icon={createOutline} />
                                        </IonButton>
                                </IonCol> : 
                                    <IonCol className="accordion-edit-button">
                                        <IonButton fill="clear" shape="round" onClick={() => ViewPost(Routes.postDetails, Pitem.id, Pitem.filestoragekey)} >
                                            <IonIcon icon={eyeOutline} />
                                        </IonButton>
                                    </IonCol>
                                }
                                {/* <IonCol className="post-image">
                                    <div className="image-wrapper">
                                        {/* {Pitem.attachFile.map((FileData, Fileindex) => (
                                            <span>
                                            {prephotos.map((photo, index) => (
                                                <span>
                                                    {photo.filepath == FileData.file_name ? (<IonImg className="logo" src={photo.webviewPath} />) : ('')}
                                                </span>
                                            ))}
                                            </span>
                                        ))} * /}
                                    </div>
                                </IonCol> */}
                                <IonCol className="post-content">
                                    <h4 className="post-title">{Pitem.title}</h4>
                                    <h2 className="post-price">{Pitem.price}</h2>
                                    {/* <h2 className="post-price">{Pitem.filestoragekey} - {Pitem.id}</h2> */}
                                    <p className="post-date">{t('Tenants-Posted')}: {Pitem.date}</p>
                                </IonCol>
                            </IonRow>
                        </IonGrid>                                    
                    </div>
                ))}
            </div>
        </div>
    );
};

export default TanatDahsboadPostsTab;