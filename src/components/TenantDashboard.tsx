import React, {useRef, useState, useEffect} from "react";
import { useHistory } from "react-router-dom";
import { Trans, withTranslation, useTranslation } from "react-i18next";

import { 
    IonGrid, 
    IonRow, 
    IonCol, 
    IonSegment,
    IonSegmentButton,
    IonSlides,
    IonSlide,
    IonButton,
    IonIcon,
    IonCard,
    IonCardHeader,
    IonCardTitle,
    IonCardContent,
    IonInput,
    IonLabel,
    IonAvatar,
    IonAlert,
    IonLoading,
    useIonViewWillEnter
} from '@ionic/react';

import { useForm, FieldErrors } from "react-hook-form";
import { ErrorMessage } from '@hookform/error-message';
  
import TanatDahsboadPostsTab from "../components/TanantsDashboardPostsTab";
import { Routes } from '../App';

import '../assets/css/Custom.css';
import '../assets/css/Responsive.css';

import { globalConst } from "../constants";
import axios from "axios";
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';
axios.defaults.baseURL = globalConst.baseURL;
axios.defaults.headers.post['Content-Type'] = globalConst.ContentType;
axios.defaults.headers.post['Access-Control-Allow-Origin'] = globalConst.AllowOrigin;
axios.defaults.headers.post['Accept'] = globalConst.Accept;
axios.defaults.timeout = globalConst.timeout;

const TenantDashboard: React.FC<{ path: string; UserTypeData: any;}> = props => {

    const { t } = useTranslation();
    let history = useHistory();

    const [showLoading, setShowLoading] = useState(true);

    const [GeneralPostsData, setGeneralPostsData] = useState<Array<any>>([{"id": '', "user_id": '', "filestoragekey": '', "title": "", "price": "", "content": "", "date": "", "attachFile": [{"file_name": ""}]}]);
    const [TenantPostsData, setTenantPostsData] = useState<Array<any>>([{"id": '', "user_id": '', "filestoragekey": '', "title": "", "price": "", "content": "", "date": "", "attachFile": [{"file_name": ""}]}]);
    const [ManagementPostsData, setManagementPostsData] = useState<Array<any>>([{"id": '', "user_id": '', "filestoragekey": '', "title": "", "price": "", "content": "", "date": "", "attachFile": [{"file_name": ""}]}]);

    const [GeneralPostsCnt, setGeneralPostsCnt] = useState(0);
    const [TenantPostsCnt, setTenantPostsCnt] = useState(0);
    const [ManagementPostsCnt, setManagementPostsCnt] = useState(0);

    const slider = useRef<any>(null);
    const [value, setValue] = useState("");
    const [defValue, setdefValue] = useState("0");
    const btnref = useRef<HTMLIonButtonElement>(null);
    const [addedLeaseCodeAlert, setAddedLeaseCodeAlert] = useState(false);
    const [leasCode, setLeasecode] = useState(false);
    const [showMessage, setShowMessage] = useState(false);

    const [showResults, setShowResults] = React.useState(false)
    const buttonHandler = () => {
        //setShowResults(true)
        history.push(`${Routes.PostList}`);
    }


    useIonViewWillEnter(() => {
        setGeneralPostsData([]);
        setTenantPostsData([]);
        setManagementPostsData([]);
        loadPostData();
    });

    useEffect(() => {
        //slider.current.lockSwipes(true);
        
        //slider.current!.slideTo(1);
        //slider.current.lockSwipes(true);


        //console.log(props.UserTypeData.lease_code);
        if ((props.UserTypeData.lease_code!='null') && (props.UserTypeData.lease_code!='')) {
            setLeasecode(true);
            setShowMessage(true);
        } else {
            setLeasecode(false);
            setShowMessage(false);
        }
    }, []);

    /* const slideOpts = {
      initialSlide: 0,
      speed: 400,
      loop: false,
      pagination: {
        el: null
      },
    
    }; */

    const slideOpts = {
        initialSlide: 0,
        speed: 400,
        loop: false,
        pagination: {
          el: null
        },
        slidesPerView: 1,
        spaceBetween: 10
    }
      

    // a function to handle the segment changes
    const handleSegmentChange = async (e: any) => {
        //console.log(e.detail.value);

        //alert(e.detail.value);
        
        //history.push({
        //    pathname: `${Routes.PostList}`+"/"+ e.detail.value,
        //    state: { sel_val: e.detail.value }
        //});

        //await slider.current.lockSwipes(false);
        setValue(e.detail.value);
        setdefValue(e.detail.value);
        //slider.current!.slideTo(e.detail.value);
        //await slider.current.lockSwipes(true);
    };


    useEffect(() => {
        if ((value=='0') || (value=='1')) {
            history.push({
                pathname: `${Routes.PostList}`,
                state: { sel_val: value }
            });
        }
    }, [value]);

    // a function to handle the slider changes
    const handleSlideChange = async (event: any) => {
      let index: number = 0;
      await event.target.getActiveIndex().then((value: any) => (index=value));
      //alert(index);
      setValue(''+index)
      setdefValue(''+index);

        //history.push({
        //    pathname: `${Routes.PostList}`,
        //    state: { sel_val: index }
        //});
    }

    const methods = useForm();
    const { register, handleSubmit, formState: { errors } } = methods;

    const onSubmit = (data: any) => {
        if (data.confirmLeaseCode!='') {
            const api = axios.create();
            api.get("/auth/token/refresh").then((response) => {
        
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
                const api = axios.create();

                const leaseData = {
                    LeaseCode: data.confirmLeaseCode,
                    user_id: localStorage.getItem('user_id')
                };

                api.put("/auth/updateLeaseCode", leaseData).then((resData) => {
                    const resReturnData = JSON.parse(JSON.stringify(resData)).data;
                    //console.log(resReturnData);

                    localStorage.setItem('user_code', resReturnData.user_code);
                    localStorage.setItem('sel_prop_id', resReturnData.user_prop_id);
                    localStorage.setItem('sel_prop_name', resReturnData.user_prop_name);
                    localStorage.setItem('sel_prop_suffix', resReturnData.user_prop_suffix);
        
                    setAddedLeaseCodeAlert(true);
                })
                .catch((error) => {
                    alert("Error Update User Lease Code!");
                })
            })
            .catch((error) => {
            alert("Token not Get!");
            })
        }
    };

    type ErrorSummaryProps<T> = {
        errors: FieldErrors<T>;
    };

    function ErrorSummary<T>({ errors }: ErrorSummaryProps<T>) {
        if (Object.keys(errors).length === 0) {
            return null;
        }
        return (
            <div className="error-summary">
                {Object.keys(errors).map((fieldName) => (
                <ErrorMessage errors={errors} name={fieldName as any} as="div" key={fieldName} />
                ))}
            </div>
        );
    }

    async function loadPostData() {
        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {

            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();
            
            if ((props.UserTypeData.lease_code!='null') && (props.UserTypeData.lease_code!='')) {
                api.get("/postdata/datafortenant/"+localStorage.getItem('user_id')+"/4/"+localStorage.getItem('sel_prop_id')).then((resUnitData) => {
                    const retData = JSON.parse(JSON.stringify(resUnitData)).data;
                    //console.log(retData);

                    setGeneralPostsData(retData.GeneralPostsData);
                    setTenantPostsData(retData.TenantPostsData);
                    setManagementPostsData(retData.ManagementPostsData);

                    setGeneralPostsCnt(retData.GeneralPostsCnt);
                    setTenantPostsCnt(retData.TenantPostsCnt);
                    setManagementPostsCnt(retData.ManagementPostsCnt);
                
                    setShowLoading(false);
                })
                .catch((error) => {
                    alert("Error Get Data!");
                })
            } else {
                setShowLoading(false);
            }
        })
        .catch((error) => {
          alert("Token not Get!");
        })
    }

    return (
        <IonCol className="dashboard-content" size="12" sizeMd="12" sizeLg="6" sizeXl="8">

            <IonLoading
                isOpen={showLoading}
                onDidDismiss={() => setShowLoading(false)}
                message={'Loading...'}
            />
            
            <div className="dashboard-content-inner">
                
                <div className="tab-button-wrap">
                    <IonButton className="post-add-button-besides-tab" fill="clear" routerLink={Routes.createPosts} >
                        {t('POST-Create-info')}
                    </IonButton>
                    <IonSegment scrollable  mode="md" value={value} onIonChange={(e) => handleSegmentChange(e)} /* onIonChange={(e) => handleSegmentChange(e)} */ >
                        {/* <IonSegmentButton className="posts-tab-button" value="0">
                            Posts
                            {GeneralPostsCnt>3 ? <span className="notification-count">{GeneralPostsCnt}</span> : ''}
                        </IonSegmentButton> */}
                        <IonSegmentButton value="0">
                            {t('POST-Tenants')}
                            {TenantPostsCnt>3 ? <span className="notification-count">{TenantPostsCnt}</span> : ''}
                        </IonSegmentButton>
                        <IonSegmentButton value="1">
                            {t('POST-Management')}
                            {ManagementPostsCnt>3 ? <span className="notification-count">{ManagementPostsCnt}</span> : ''}
                        </IonSegmentButton>
                    </IonSegment>
                </div>

                <IonSlides pager={true} options={slideOpts} /* onIonSlideDidChange={(e) => handleSlideChange(e)} */ ref={slider}>
{/*                     <IonSlide>
                        <TanatDahsboadPostsTab PostData={GeneralPostsData} />
                    </IonSlide> */}
                    <IonSlide>
                        <TanatDahsboadPostsTab PostData={TenantPostsData} />
                    </IonSlide>
                    <IonSlide>
                        <TanatDahsboadPostsTab PostData={ManagementPostsData} />
                    </IonSlide>
                </IonSlides>
                

                <IonButton className="post-add-button" fill="clear" routerLink={Routes.createPosts} >
                    <IonIcon src="assets/images/post-icon.svg" />
                </IonButton>

                <IonCard className={`enter-lease-code-card ${showResults ? 'hide-div' : ''}`}>
                    <IonCardHeader>
                        <IonCardTitle>{t('Tenants-Hi')} {localStorage.getItem('user_firstname')} {localStorage.getItem('user_lastname')},</IonCardTitle>
                    </IonCardHeader>

                    <IonCardContent>
                        <div className={`before-lease-code-content ${showMessage ? 'hide-div' : ''}`}>
                            <p>{t('Tenants-Text1')}</p> 
                            <p>{t('Tenants-Text2')}</p>

                            <form className="cofirm-lease-code-form" onSubmit={handleSubmit(onSubmit)} autoComplete="off">
                            <IonGrid>
                                <IonRow className="ion-align-items-center">
                                    <IonCol size="12">
                                        <ErrorSummary errors={errors} />
                                    </IonCol>

                                    <IonCol size="12">
                                        <IonLabel className="form-lable">{t('Tenants-Lease-Lease-Code')}</IonLabel>
                                    </IonCol>

                                    <IonCol size="12" sizeMd="12" sizeLg="12" sizeXl="4">
                                        <IonInput
                                            mode="md"
                                            type="text"
                                            className={`form-control ${errors.confirmLeaseCode ? 'is-invalid' : ''}`}
                                            placeholder=""
                                            {...register('confirmLeaseCode', {
                                                required: t('Tenants-Lease-Lease-Code-Error')
                                            })}
                                        />
                                    </IonCol>

                                    <IonCol className="lease-code-confirm-button" size="12" sizeMd="12" sizeLg="12" sizeXl="2">
                                        <IonButton type="submit" shape="round" fill="outline">
                                            {t('General-Confirm')}
                                        </IonButton>
                                    </IonCol>
                                </IonRow>
                            </IonGrid>
                            </form>

                            <IonAlert
                                isOpen={addedLeaseCodeAlert}
                                onDidDismiss={() => setAddedLeaseCodeAlert(false)}
                                cssClass='orange-alert'
                                mode='md'
                                header={t('General-Success')}
                                message={t('Tenants-Text3')}
                                buttons={[
                                    {
                                        text: 'Close',
                                        role: 'cancel',
                                        cssClass: 'btn-primary',
                                        handler: () => {
                                            //history.push(`${Routes.UserDashboard}`);
                                            window.location.assign(`${Routes.UserDashboard}`);
                                            //setLeasecode(true);
                                            //setShowMessage(true);
                                        }
                                    }
                                    
                                ]}
                            />
                        </div>

                        <div className={`after-lease-code-content ${!showMessage ? 'hide-div' : ''}`}>
                            <p>{t('Tenants-Text4')}</p>
                        </div>

                        <IonGrid className="tenant-dhaboard-button-list">
                        <IonRow>
                            <IonCol size="12" sizeMd="6" sizeLg="12" sizeXl="6">
                                <IonButton  className="" expand="block" shape="round" fill="outline" routerLink={Routes.payRents} disabled={!leasCode}>
                                    <div className="tenant-dhaboard-button">
                                        <IonAvatar>
                                            <img src="assets/images/Pay-Rent-home.svg" />
                                        </IonAvatar>
                                        <div className="tenant-dhaboard-button-content">
                                            <h4><Trans>Tenants-Text5</Trans></h4>
                                            {/* <h4>{t('Tenants-Text5')}</h4> */}
                                            {/* <p>{t('welcome', { username: 'SHILPA' })}</p> */}
                                            {/* <Trans i18nKey='Tenants-Text5'></Trans> */}
                                            {/* <p><Trans>Tenants-Text5</Trans></p> */}
                                            <p>{t('Tenants-Text6')}</p>
                                        </div>
                                    </div>
                                </IonButton>
                            </IonCol>

                            {/* <IonCol size="12" sizeMd="6" sizeLg="12" sizeXl="6">
                                <IonButton  className="" expand="block" shape="round" fill="outline" routerLink={Routes.messaging} disabled={!leasCode}>
                                    <div className="tenant-dhaboard-button">
                                        <IonAvatar>
                                            <img src="assets/images/Pay-Rent-home.svg" />
                                        </IonAvatar>
                                        <div className="tenant-dhaboard-button-content">
                                            <h4>Messaging</h4>
                                            <p>Tap here to chat with Management.</p>
                                        </div>
                                    </div>
                                </IonButton>
                            </IonCol> */}

                            {/* <IonCol size="12" sizeMd="6" sizeLg="12" sizeXl="6">
                                <IonButton  className="" expand="block" shape="round" fill="outline" routerLink={Routes.manageTenants} disabled={!leasCode}>
                                    <div className="tenant-dhaboard-button">
                                        <IonAvatar>
                                            <img src="assets/images/Repair-home.svg" />
                                        </IonAvatar>
                                        <div className="tenant-dhaboard-button-content">
                                            <h4><b>Repairs</b> and <b>Request</b></h4>
                                            <p>Tap here to pay your monthly rent using various methods of payments.</p>
                                        </div>
                                    </div>
                                </IonButton>
                            </IonCol> */}

                            <IonCol size="12" sizeMd="6" sizeLg="12" sizeXl="6">
                                <IonButton  className="" expand="block" shape="round" fill="outline" routerLink={Routes.manageLeases} disabled={!leasCode}>
                                    <div className="tenant-dhaboard-button">
                                        <IonAvatar>
                                            <img src="assets/images/Manage-Lease-home.svg" />
                                        </IonAvatar>
                                        <div className="tenant-dhaboard-button-content">
                                            <h4><Trans>Tenants-Text7</Trans></h4>
                                            <p>{t('Tenants-Text8')}</p>
                                        </div>
                                    </div>
                                </IonButton>
                            </IonCol>
                        </IonRow>
                        </IonGrid>

                    </IonCardContent>
                </IonCard>

            </div>
        </IonCol>        
    );
};

export default TenantDashboard;