import React, {useRef, useEffect, useState} from "react";
import { useForm } from "react-hook-form";
import { useTranslation } from "react-i18next";

import { 
    IonGrid,
    IonRow,
    IonCol,
    IonButton,
    IonIcon,
    IonInput,
    IonLabel,
    createAnimation
} from '@ionic/react';

import { close } from "ionicons/icons";

import '../assets/css/Custom.css';
import '../assets/css/Responsive.css';
import { Routes } from "../App";

import { globalConst } from "../constants";
import axios from "axios";
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';
axios.defaults.baseURL = globalConst.baseURL;
axios.defaults.headers.post['Content-Type'] = globalConst.ContentType;
axios.defaults.headers.post['Access-Control-Allow-Origin'] = globalConst.AllowOrigin;
axios.defaults.headers.post['Accept'] = globalConst.Accept;
axios.defaults.timeout = globalConst.timeout;

const RentPayReceiptPopup: React.FC<{PopID, parentCallback}> = ({PopID, parentCallback}) => {

    const { t } = useTranslation();
    const methods = useForm();
    const { register, trigger, reset, watch , handleSubmit, control, setValue, getValues, formState: { errors } } = methods;

    const [TotalPaid, setTotalPaid] = useState('');
    const [PaidDate, setPaidDate] = useState('');

    useEffect(() => {
        loadData(PopID);
    }, []);

    async function loadData(PopID: any) {
        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {

            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            api.get("/propertyleases/"+PopID+"/"+localStorage.getItem('user_id')).then((resPData) => {
                const retData = JSON.parse(JSON.stringify(resPData)).data;

                if (retData) {
                    setTotalPaid("$" + retData.RentCost + " CAD");
                    setPaidDate(retData.PaidDate);

                    const fields = ['floorAptNo', 'RentPeriod', 'PaymentMethod', 'CardNumber', 'RentCost', 'receiptTPS', 'receiptTVQ'];
                    fields.forEach(field => {
                        if (field=='RentCost') {
                            setValue(field, "$" + retData[field])
                        } else if (field=='receiptTPS') {
                            setValue(field, "N/A")
                        } else if (field=='receiptTVQ') {
                            setValue(field, "N/A")
                        } else {
                            setValue(field, retData[field])
                        }
                    });
                }
            })
            .catch((error) => {
                alert("Error Get Data!");
            })
        })
        .catch((error) => {
          alert("Token not Get!");
        })
    }

    const handlePrint =async (PopID:any) => {
        window.open(`${Routes.printRentReceipt}` + "/" + PopID, '_system', 'location=yes');
    }

    return (
        <IonGrid className="rent-pay-receipt-popup-wrapper">
            <div className="popup-title">
                <h4>{t('Billing-History-Receipt')} <IonButton shape="round" fill="outline" onClick={() => handlePrint(PopID)}>{t('Billing-History-Print')}</IonButton></h4>
            </div> 
            <IonRow className="receipt-details">
                <IonCol size="6">
                    <IonLabel>{t('Rent-Apartment')}</IonLabel>
                </IonCol>
                <IonCol size="6">
                    <IonInput mode="md" type="text" {...register('floorAptNo')} readonly></IonInput>
                </IonCol>

                <IonCol size="6">
                    <IonLabel>{t('Rent-Manage-SearchTool-RentPeriod')}</IonLabel>
                </IonCol>
                <IonCol size="6">
                    <IonInput mode="md" type="text" {...register('RentPeriod')} readonly></IonInput>
                </IonCol>

                <IonCol size="6">
                    <IonLabel>{t('Billing-History-Method')}</IonLabel>
                </IonCol>
                <IonCol size="6">
                    <IonInput mode="md" type="text" {...register('PaymentMethod')} readonly></IonInput>
                </IonCol>

                <IonCol size="6">
                    <IonLabel>{t('Billing-History-CardNo')}</IonLabel>
                </IonCol>
                <IonCol size="6">
                    <IonInput mode="md" type="text" {...register('CardNumber')} readonly></IonInput>
                </IonCol>

                <IonCol size="6">
                    <IonLabel>{t('Rent-Cost')}</IonLabel>
                </IonCol>
                <IonCol size="6">
                    <IonInput mode="md" type="text" {...register('RentCost')} readonly></IonInput>
                </IonCol>

                <IonCol size="6">
                    <IonLabel>{t('TPS')}</IonLabel>
                </IonCol>
                <IonCol size="6">
                    <IonInput mode="md" type="text" {...register('receiptTPS')} readonly></IonInput>
                </IonCol>

                <IonCol size="6">
                    <IonLabel>{t('Rent-TVQ')}</IonLabel>
                </IonCol>
                <IonCol size="6">
                    <IonInput mode="md" type="text" {...register('receiptTVQ')} readonly></IonInput>
                </IonCol>
            </IonRow>

            <IonRow  className="receipt-amount">
                <IonCol size="12">
                    <IonLabel>{t('Billing-History-TotalPaid')}</IonLabel>
                </IonCol>
                <IonCol size="12">
                    <p className="total-rent-paid"><b>{TotalPaid}</b></p>
                </IonCol>
                <p className="rent-paid-date">{t('Billing-History-Paid')}: <span className="paid-date">{PaidDate}</span></p>
            </IonRow>
        </IonGrid> 
    );
};

export default RentPayReceiptPopup;