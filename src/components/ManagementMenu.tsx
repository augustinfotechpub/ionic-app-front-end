import React, { useState } from 'react';
import { useTranslation } from "react-i18next";

import {  
    IonLabel, 
    IonButton,  
    IonList,
    IonItem,
    IonAvatar
  } from '@ionic/react';

import '../assets/css/Custom.css';
import '../assets/css/Responsive.css';
import { Routes } from "../App";

const ManagementMenu: React.FC<{ path: string; UserTypeData: any; FeatureAccessData: any; }> = props => {

    const { t } = useTranslation();

    const getClassName = (routePath: string[], className: string): string => {
        return routePath.indexOf(props.path) > -1 ? (className + " active") : className
    }

    return (
        <IonList className="sidebar-menu-list">
            {((props.UserTypeData.id == '3' && props.FeatureAccessData.manage_propertys == '1') || (props.UserTypeData.id == '2')) ? (
                <IonItem>
                    <IonButton className={getClassName([Routes.manageTenants, Routes.leaseInfo, Routes.editLeaseInfo, Routes.addNewLease, Routes.leaseRenewalProposal], "dashboard-button")} fill="clear" routerLink={Routes.manageTenants}>
                        <div className="dashboard-button-inner">
                            <IonAvatar>
                                <img src="assets/images/Manage-Lease-home.svg" />
                            </IonAvatar>
                            <IonLabel  className="dashboard-button-label"><b>{t('sidebar-Tenants')}</b> & <b>{t('sidebar-Leases')}</b></IonLabel>
                        </div>
                    </IonButton>
                </IonItem>
            ) : ('')}

            {((props.UserTypeData.id == '3' && props.FeatureAccessData.manage_rents == '1') || (props.UserTypeData.id == '2')) ? (
                <IonItem>
                    <IonButton className={getClassName([Routes.manageRents, Routes.rentPayDetails], "dashboard-button")} fill="clear" routerLink={Routes.manageRents}>
                        <div className="dashboard-button-inner">    
                            <IonAvatar>
                                <img src="assets/images/Pay-Rent-home.svg" />
                            </IonAvatar>
                            <IonLabel  className="dashboard-button-label"><b>{t('sidebar-Rents')}</b></IonLabel>
                        </div>
                    </IonButton>
                </IonItem>
            ) : ('')}

            {/* <IonItem>
                <IonButton className={getClassName([Routes.manageRequests], "dashboard-button")} fill="clear" routerLink={Routes.manageRequests}>
                    <div className="dashboard-button-inner">
                        <IonAvatar>
                            <img src="assets/images/Repair-home.svg" />
                        </IonAvatar>
                        <IonLabel  className="dashboard-button-label"><b>Repairs</b> & <b>Requests</b></IonLabel>
                    </div>
                </IonButton>
            </IonItem> */}

            {((props.UserTypeData.id == '3' && props.FeatureAccessData.manage_posts == '1') || (props.UserTypeData.id == '2')) ? (
                <IonItem>
                    <IonButton className={getClassName([Routes.managePosts, Routes.createPosts, Routes.editPost], "dashboard-button")} fill="clear" routerLink={Routes.managePosts}>
                        <div className="dashboard-button-inner">
                            <IonAvatar>
                                <img src="assets/images/Posts-Home-Icon.svg" />
                            </IonAvatar>
                            <IonLabel  className="dashboard-button-label"><b>{t('sidebar-Posts')}</b> & <b>{t('sidebar-Marketplace')}</b></IonLabel>
                        </div>
                    </IonButton>
                </IonItem>
            ) : ('')}

            {props.UserTypeData.id == '2' ? (
                <IonItem>
                    <IonButton className={getClassName([Routes.manageAccesscode, Routes.createAccesscode], "dashboard-button")} slot="start" fill="clear" routerLink={Routes.manageAccesscode}>
                        <div className="dashboard-button-inner">
                            <IonAvatar>
                                <img src="assets/images/Staff-home.svg" />
                            </IonAvatar>
                            <IonLabel  className="dashboard-button-label"><b>{t('sidebar-Access')}</b> {t('sidebar-manager')}</IonLabel>
                        </div>
                    </IonButton>
                </IonItem>
            ) : ('')}
        </IonList>
    );
};

export default ManagementMenu;