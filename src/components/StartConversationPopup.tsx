import React, {useState, useEffect} from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import { useTranslation } from "react-i18next";

import { 
    IonGrid,
    IonRow,
    IonCol,
    IonButton,
    IonSearchbar,
    IonCheckbox,
    useIonViewWillEnter
} from '@ionic/react';

import '../assets/css/Custom.css';
import '../assets/css/Responsive.css';
import { Routes } from "../App";

import { globalConst } from "../constants";

import axios from "axios";
axios.defaults.baseURL = globalConst.baseURL;
axios.defaults.headers.post['Content-Type'] = globalConst.ContentType;
axios.defaults.headers.post['Access-Control-Allow-Origin'] = globalConst.AllowOrigin;
axios.defaults.headers.post['Accept'] = globalConst.Accept;
axios.defaults.timeout = globalConst.timeout;

interface filterSearchType {
    property_id: string,
    floorsno: string,
    aptno: string,
    user_code: string,
    id: string,
    name: string,
    role: string,
    val: string,
    isChecked: boolean
}

const StartConversationPopup: React.FC<{parentCallback?: any}> = (props) => {

    const { t } = useTranslation();
    let history = useHistory();

    const [ClassSelectAll, setClassSelectAll] = useState('select-all-result-button');

    const [checked, setChecked] = useState(false);
    const [isCheckAll, setIsCheckAll] = useState(false);
    const [searchQuery, setSearchQuery] = useState('');
    const [filteredSearch, setFilteredSearch] = useState([
    {
        property_id: "",
        floorsno: "",
        aptno: "",
        user_code:"",
        id:"",
        name:"",
        role:"",
        val: "",
        isChecked: false
    }])

    //useIonViewWillEnter(() => {
    //});

    useEffect(() => {
        //console.log('filteredSearch Update');
        //console.log(filteredSearch);
    },[filteredSearch])

    useEffect(() => {
        loadSearchResult(searchQuery);

        if ((localStorage.getItem('user_type_id')=='1') || (localStorage.getItem('user_type_id')=='3')){
            setClassSelectAll('hide-input');
            //console.log('Pop Show '+ localStorage.getItem('user_type_id'));
        }
    },[searchQuery])

    async function loadSearchResult(searchQuery) {
        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {

            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            const searchInfo = {
                "searchQuery" : searchQuery,
                "UserID" : localStorage.getItem('user_id'),
                "UserTypeID" : localStorage.getItem('user_type_id'),
            };

            api.put("/auth/UserSearchResult", searchInfo).then((returnData) => {
                const retData = JSON.parse(JSON.stringify(returnData)).data;

                //console.log(retData);
                setFilteredSearch(retData.retData);

            })
            .catch((error) => {
                alert("Error Get Data!");
            })
        })
        .catch((error) => {
          alert("Token not Get!");
        })
    }

    const clickExit = () => {
        props.parentCallback();
    };

    const handleSelectAll = () => {
        setIsCheckAll(!isCheckAll);
        let selectedAllFilters: filterSearchType[]
        selectedAllFilters = filteredSearch.map(s => {
            if (!isCheckAll) { 
                s.isChecked = true; 
            } else { 
                s.isChecked = false;
            }
            return s;
        });
        setFilteredSearch([...selectedAllFilters]);
         //console.log(selectedAllFilters);
    };

    const ClickConfirm = async () => {
        const api = axios.create();
        api.get("/auth/token/refresh").then(res => res)
        .then((response) => {
            
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();
            
            const ConversationData = {
                "current_user" : localStorage.getItem('user_id'),
                "UserID" : localStorage.getItem('user_id'),
                "UserTypeID" : localStorage.getItem('user_type_id'),
                "SearchSelected" : filteredSearch,
            };
    
            console.log(ConversationData);
    
            api.post("/messaging/startconversion", ConversationData).then((retData) => {
                const retDataArr = JSON.parse(JSON.stringify(retData)).data;
                console.log(retDataArr);

                props.parentCallback();
                history.push({
                    pathname: `${Routes.chat}`,
                    state: { path: `${Routes.chat}`, chat_id: retDataArr.InsertID, chat_msg: ''}
                });
               
            }).catch((error) => {
                //alert("Error found in put Data");
                console.log(error);
            }); 
        })
        .catch((error) => {
            alert("Token not Get!");
        })
    };
    
    const ClickSendMsg = async (propertyid, floorsno, aptno, leasecode, msgtype, senduserid) => {
        let selectedAllFilters: filterSearchType[]

        selectedAllFilters = filteredSearch.map(s => {
            if ((s.property_id==propertyid) && (s.floorsno==floorsno) && (s.aptno==aptno) && (s.user_code==leasecode) && (s.id==senduserid) && (s.role==msgtype)){
                if (!s.isChecked) { 
                    s.isChecked = true; 
                } else { 
                    s.isChecked = false;
                }
            } else {
                if ((localStorage.getItem('user_type_id')=='1') || (localStorage.getItem('user_type_id')=='3')){
                    s.isChecked = false;
                }
            }
            return s;
        });

        setFilteredSearch([...selectedAllFilters]);
    };

    return (
        <IonGrid className="start-conversation-popup-wrapper">

            <div className="popup-title">
                <h3>{t('Msg-Text1')} <span>{t('Msg-Text2')}</span></h3>
            </div> 

            <div className="popup-content">
                <div className="search-box-wrapper">
                    <IonSearchbar mode="ios" placeholder={t('Msg-Text3')} value={searchQuery} onIonChange={e => setSearchQuery(e.detail.value!)}></IonSearchbar>
                    <div className={`search-results ${!searchQuery ? 'hide-div' : ''}`}>
                        <p>{t('Msg-Searchresults')}</p>
                        <IonRow className="search-results-header">
                            <IonCol className="search-results-header-name" size="4">{t('Msg-Name')}</IonCol>
                            <IonCol className="search-results-header-apt" size="4">{t('Msg-Apt')} #</IonCol>
                            <IonCol className="search-results-role" size="4">{t('Msg-Role')}</IonCol>
                        </IonRow>
                        <div className="serach-results-inner">
                            {filteredSearch.map(( search, i) => (
                                <IonRow key={i} className={`search-results-rows ${ search.isChecked ? 'row-checked' : ''}`} onClick={() => ClickSendMsg(search.property_id, search.floorsno, search.aptno, search.user_code, search.role, search.id)}>
                                    <IonCol className="search-results-col-name" size="4">{search.name}</IonCol> 
                                    <IonCol className="search-results-col-apt" size="4">{search.aptno}</IonCol> 
                                    <IonCol className="search-results-col-role" size="4">{search.role=='Staff' ? 'Janitor' : search.role}</IonCol> 
                                    <IonCheckbox value={search.val} checked={search.isChecked} />
                                </IonRow>
                            ))}
                        </div> 
                        <div className="ion-text-end">
                            <IonButton onClick={handleSelectAll} className={ClassSelectAll} fill="outline" shape="round">
                                {t('Msg-SelectAll')}
                            </IonButton>
                        </div>
                    </div>
                </div>
                <IonRow  className="popup-buttons">
                    <IonCol size="6">
                        <IonButton className="exit-button" fill="outline" shape="round" onClick={clickExit}>
                            {t('General-Exit')}
                        </IonButton>
                    </IonCol>
                    <IonCol size="6">
                        <IonButton className="secondary-button" fill="outline" shape="round" onClick={() => ClickConfirm()}>
                            {t('General-Confirm')}
                        </IonButton>
                    </IonCol>
                </IonRow>
            </div>

        </IonGrid> 
    );
};

export default StartConversationPopup;