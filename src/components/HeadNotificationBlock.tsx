import React from "react";

import {  
    IonButton, 
    IonIcon, 
} from '@ionic/react';

import { alertCircleSharp } from "ionicons/icons";

import '@ionic/react/css/flex-utils.css';
import '../assets/css/Custom.css';
import '../assets/css/Responsive.css';

const HeadNotificationBlock: React.FC<{SIndex: any; SText: any; SData: any; SCntData: any; closePopup: any;}> = (props) => {

    return (
        <div key={props.SIndex} >
            {props.SCntData > 0 ? <h6>{props.SText}</h6> : ""}
            {props.SData.map((item, index) => (
                <div className={item.n_type=='Normal' ? "notification-box yellow-notification" : "notification-box red-notification"}>
                    <IonButton className="notification-detail-button" fill="clear" expand="full" shape="round" routerLink={item.n_link} onClick={props.closePopup}></IonButton>
                    <div className="notofication-content">
                        <div className="notification-box-header">
                            <h6>{item.n_title} <span>{item.n_subtitle}</span></h6>
                            {item.n_type=='Normal' ? <IonIcon slot="icon-only" src="/assets/images/Pay-Rent-icon-only.svg" /> : <IonIcon icon={alertCircleSharp} />}
                        </div>
                        <p dangerouslySetInnerHTML={{ __html: item.n_desc }} />
                        <span className="notification-date">{item.n_date_text}</span>                                  
                    </div>
                </div>
            ))}
        </div>
    );
};

export default HeadNotificationBlock;