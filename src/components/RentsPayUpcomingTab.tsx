import React, {useState, useEffect} from "react";
import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";

import {  
    IonLabel, 
    IonButton, 
    IonIcon, 
    IonInput,
    IonGrid,
    IonRow,
    IonCol,
    IonAlert,
    useIonViewWillEnter
} from '@ionic/react';

import '@ionic/react/css/flex-utils.css';
import '../assets/css/Custom.css';
import '../assets/css/Responsive.css';
import { Routes } from "../App";


import { globalConst } from "../constants";
import axios from "axios";
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';
import { time } from "ionicons/icons";
axios.defaults.baseURL = globalConst.baseURL;
axios.defaults.headers.post['Content-Type'] = globalConst.ContentType;
axios.defaults.headers.post['Access-Control-Allow-Origin'] = globalConst.AllowOrigin;
axios.defaults.headers.post['Accept'] = globalConst.Accept;
axios.defaults.timeout = globalConst.timeout;

const RentsPayUpcomingTab: React.FC<{parentCallback}> = (props) => {
    
    const { t } = useTranslation();
	let history = useHistory();

    const [quickPayAlert, setQuickPayAlert] = useState(false);
    const [paymentCompleteAlert, setPaymentCompleteAlert] = useState(false);
    const [customMessageAlertDismiss, setcustomMessageAlertDismiss] = useState(false);

    const [DisableLeaseCode, setDisableLeaseCode] = useState([]);

    const [YearTextData, setYearTextData] = useState([{"YearText": "", "YearData": [{"id": "", "leaseStartMonthName": "", "leaseStartYear": "", "lease_code": "", "lease_end_date": "", "diff_months": "", "lease_price": "", "lease_start_date": "", "lease_start_date_text": ""}]}]);
    const [DisableData, setDisableData] = useState([{"YearText": "", "YearData": [{"id": "", "leaseStartMonthName": "", "leaseStartYear": "", "lease_code": "", "lease_end_date": "", "diff_months": "", "lease_price": "", "lease_start_date": "", "lease_start_date_text": ""}]}]);

    const [payPrice, setpayPrice] = useState('');
    const [payID, setpayID] = useState('');
    const [QuickText, setQuickText] = useState('');

    const [payText, setpayText] = useState('');
    const [QuickPayFlag, setQuickPayFlag] = useState(false);

    const [count, setCount] = useState(0);

    useIonViewWillEnter(() => {
        loadUpcomingData();
    });

    useEffect(() => {
        //loadUpcomingData();
        loadUserData(localStorage.getItem('user_id'));
    }, []);

    async function loadUserData(userID: any) {
        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {

            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            api.get("/auth/getPayInfoByID/def/"+userID).then((resPData) => {
                const retData = JSON.parse(JSON.stringify(resPData)).data;

                if (retData['itemFlag']){
                    setpayText(t('Billing-History-Text1') + ' <b>'+retData['c_no'] + t('Billing-History-Text2')  +retData['c_date']+'</b>.');
                    setQuickPayFlag(true);
                    //console.log(retData);
                }
            })
            .catch((error) => {
                alert("Error Get User Payment Data!");
            })
        })
        .catch((error) => {
          alert("Token not Get!");
        })
    }

    const loadUpcomingData =async () => {
        if ((localStorage.getItem('user_code')!==null)) {
            const api = axios.create();
            api.get("/auth/token/refresh").then((response) => {
    
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
                const api = axios.create();
    
                const leaseData = {
                    LeaseCode: localStorage.getItem('user_code'), 
                    UserID: localStorage.getItem('user_id')
                };

                api.put("/propertyleases/duePaymentbyLeaseCode", leaseData).then((resPData) => {
                    const retData = JSON.parse(JSON.stringify(resPData)).data;
                    console.log('Get Prop by Lease code');
                    console.log(retData);
                    
                    setDisableLeaseCode(retData.DisableLeaseCode);

                    setYearTextData(retData.YearTextArr);
                    setDisableData(retData.DisableData);
                })
                .catch((error) => {
                    alert("Error Get Data!");
                })
            })
            .catch((error) => {
              alert("Token not Get!");
            })
        }
    }

    const clickQuickPayment =async (cardNo:any, payPrice:any,  payID:any,  payMonth:any,  payYear:any) => {
        setpayPrice(payPrice);
        setpayID(payID);
        setQuickText('<p>'+cardNo + t('Billing-History-Text3') + '<b>$'+payPrice+'</b>' + t('Billing-History-Text4') + '<b>'+payMonth+'</b> '+payYear+'?</p>');

        setQuickPayAlert(true)
    }

    const clickPaymentMethod = async (path, leaseid) => {
        //console.log('TEST');
        //console.log(path);
        //console.log(leaseid);

        //window.history.pushState({ path: path, leaseid: leaseid}, 'New Page Title', path);
        
        //window.location.reload(false);
        //window.location.assign(path);
        //history.push(`${Routes.UserDashboard}`);
        /* history.push({
            pathname: path,//+"/"+leaseid
            //key: time,
            state: { path: path, leaseid: leaseid }
        }); */

        history.push({pathname: path+"/"+leaseid});
    }

    const clickPaymentDone = async (cust_cvc:any) => {
        const api = axios.create();
        api.get("/auth/token/refresh").then(res => res).then((response) => {
            
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            var now = new Date();

            const editLeaseData = {
                "lease_price_pay" : payPrice,
                "sYear" : now.getFullYear(),
                "sMonth" : (now.getMonth() + 1),
                "sDate" : now.getDate(),
                "lease_id" : payID,
                "pay_user_id" : localStorage.getItem('user_id'),
                "quick_pay" : true,
                "cvcNo" : cust_cvc,
                //"pay_email" : localStorage.getItem('user_email'),
                //"pay_description" : data.RentPeriod,
            };

            
            api.put("/propertyleases/quickpay/"+payID, editLeaseData).then((retData) => {
                const retDataArr = JSON.parse(JSON.stringify(retData)).data;

                if (retDataArr.payment=='error') {
                    alert(retDataArr.message);
                } else {
                    setPaymentCompleteAlert(true)
                }

            }).catch((error) => {
                alert("Error found in put Data");
                console.log(error);
            });
        })
        .catch((error) => {
            alert("Token not Get!");
        })
    }

    const clickWillDismiss = () => {
        loadUpcomingData(); 
        setCount((count) => count + 1);
        props.parentCallback(count + 1);
    };

    return (
        <div className="tab-content">

            <div className="digi-lease-search-results-box">
                <p>{t('Billing-History-Text5')} <b>{t('Rent-Text2')}</b></p>

                {YearTextData.map((yearitem, index) => (
                    <div key={index} className="search-result-box">
                        <h3>{yearitem.YearText}</h3>
                        <IonGrid className="">
                            <IonRow className="due-rents-list">
                                {(yearitem.YearData).map((item, index1) => (
                                    <IonCol key={index1} size="12" sizeMd="12">
                                        <div className="tenants-lease-info tenants-lease-payment-info ">
                                            <IonGrid className="">
                                                <IonRow className="">
                                                    <IonCol size="5" sizeMd="4" className="rent-price-block paid-block">
                                                        <p><span>{Number(item.diff_months) < 0 ? t('Rent-Manage-SearchTool-overdue') : t('Rent-Manage-SearchTool-pending')} </span>: ${item.lease_price} CAD</p>
                                                        <h4>{item.leaseStartMonthName}</h4>
                                                        <p>{item.lease_start_date_text}</p>
                                                    </IonCol>
                                                    <IonCol size="7" sizeMd="8" className="rent-pay-buttons ion-text-right">
                                                        <IonButton className="secondary-button choose-payment-method-button"  shape="round" fill="outline" onClick={() => clickPaymentMethod(Routes.paymentMethod, item.id)}>
                                                            {t('Billing-History-PaymentMethod')}
                                                        </IonButton>
                                                        {QuickPayFlag? <IonButton className="quick-pay-button"  shape="round" fill="outline" onClick={() => clickQuickPayment(payText, item.lease_price,  item.id, item.leaseStartMonthName, yearitem.YearText)}>
                                                            {t('Billing-History-QuickPay')}
                                                        </IonButton> : ''}
                                                    </IonCol>
                                                </IonRow>
                                            </IonGrid> 
                                        </div>                                   
                                    </IonCol>
                                ))}
                            </IonRow>


                            {/* <IonRow className="ion-justify-content-center">
                                <IonCol size="6" sizeMd="4" sizeLg="4" sizeXl="3" className="">
                                    <IonButton className="show-more-button" expand="full" shape="round" fill="solid">More</IonButton>
                                </IonCol>
                            </IonRow> */}
                        </IonGrid>
                    </div>
                ))}


                <h6>{t('Rent-Text1')} <b><u>{t('Tenants-Lease-Text3')} {DisableLeaseCode.join(", ")}</u></b></h6>
                
                {DisableData.map((yearitem, index) => (
                    <div key={index} className="search-result-box">
                        {(yearitem.YearData).map((item, index1) => (
                            <IonCol key={index1} size="12" sizeMd="12">
                                <div className="tenants-lease-info tenants-lease-payment-info ">
                                    <IonGrid className="">
                                        <IonRow className="">
                                            <IonCol size="6" sizeMd="6" className="rent-price-block paid-block">
                                                <p><span>{t('Tenants-Lease-Lease-Code')}</span>: {item.lease_code}</p>
                                                <p><span>{Number(item.diff_months) < 0 ? t('Rent-Manage-SearchTool-overdue') : t('Rent-Manage-SearchTool-pending')} </span>: ${item.lease_price} CAD</p>
                                            </IonCol>
                                            <IonCol size="6" sizeMd="6" className="rent-price-block paid-block">
                                                <h4>{item.leaseStartMonthName}</h4>
                                                <p>{item.lease_start_date_text}</p>
                                            </IonCol>
                                        </IonRow>
                                    </IonGrid> 
                                </div>                                   
                            </IonCol>
                        ))}
                    </div>
                ))}


                <IonAlert
                    isOpen={quickPayAlert}
                    onDidDismiss={() => setQuickPayAlert(false)}
                    cssClass='orange-alert'
                    mode='md'
                    header={t('Billing-History-QuickPay')}
                    message={QuickText}
                    inputs={[
                        {
                          name: 'cust_cvc',
                          type: 'number',
                          placeholder: 'CVC No',
                          cssClass: 'custom-reminder-message-box'
                        },
                    ]}
                    buttons={[
                        {
                            text: 'Yes',
                            cssClass: 'btn-primary',
                            handler: (formData: { cust_cvc: string }) =>   {
                                if(formData.cust_cvc != null && formData.cust_cvc.length == 3) {
                                    clickPaymentDone(formData.cust_cvc);
                                } else {
                                    setcustomMessageAlertDismiss(true)
                                }
                            }
                        },
                        {
                            text: 'No',
                            role: 'cancel',
                            cssClass: 'btn-outline',
                            handler: () => {
                                
                            }
                        }    
                    ]}
                />

                <IonAlert
                    isOpen={customMessageAlertDismiss}
                    onDidDismiss={() => setcustomMessageAlertDismiss(false)}
                    cssClass='orange-alert'
                    mode='md'
                    header={t('Billing-History-Head1')}
                    message={t('Billing-History-Msg1')}
                    buttons={[
                        {
                            text: 'Close',
                            role: 'cancel',
                            cssClass: 'btn-primary',
                            handler: () => {
                            }
                        }
                        
                    ]}
                />
                
                <IonAlert
                    isOpen={paymentCompleteAlert}
                    onDidDismiss={() => setPaymentCompleteAlert(false)}
                    onWillDismiss={() => clickWillDismiss()}
                    cssClass='orange-alert rent-pay-alert'
                    mode='md'
                    header={t('Billing-History-Head2')}
                    message={t('Billing-History-Msg2')}
                    buttons={[
                        {
                            text: t('Rent-Pay-Done-Btn'),
                            role: 'cancel',
                            cssClass: 'btn-primary',
                            handler: () => {
                                
                            }
                        }    
                    ]}
                />

            </div>
        </div>
    );
};

export default RentsPayUpcomingTab;