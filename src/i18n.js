import i18n from "i18next";
import { initReactI18next } from "react-i18next";


// Importing translation files
import translationEN from "./assets/i18n/en.json";
import translationES from "./assets/i18n/es.json";
import translationFR from "./assets/i18n/fr.json";


//Creating object with the variables of imported translation files
const resources = {
  en: {
    translation: translationEN,
  },
  es: {
    translation: translationES,
  },
  fr: {
    translation: translationFR,
  },
};

//i18N Initialization

i18n
  .use(initReactI18next)
  .init({
    resources,
    lng:"en", //default language
    keySeparator: false,
    interpolation: {
      escapeValue: false,
    },
  });

export default i18n;