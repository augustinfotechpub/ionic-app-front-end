import { Switch } from "react-router";
import { Redirect, Route } from 'react-router-dom';

import { IonApp, IonRouterOutlet } from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';

import Landing from './pages/Landing';
import LoginRedirect from './pages/Redirect';
import EndRedirect from './pages/EndRedirect';
import { PrivateRoute } from './routes/PrivateRoute';

import Home from './pages/Index';
import Logout from './pages/Logout';
import ForgetPassword from './pages/Forget-Password';
import ResetPassword from './pages/Reset-Password';
import Signup from './pages/Signup';
import UserDashboard from './pages/User-dashboard';
import SignupData from './pages/signup-data';
import ManageProperties from './pages/Manage-properties';
import AddNewProperty from './pages/Add-new-property';
import EditProperty from './pages/Edit-property';
import ManageTenants from './pages/Manage-tenants';
import ManageLeases from './pages/Manage-leases';
import LeaseInfo from './pages/Lease-info';
import EditLeaseInfo from './pages/Edit-lease-info';
import AddNewLease from './pages/Add-new-lease';
import LeaseRenewalProposal from './pages/lease-renewal-proposal';
import ManageRents from './pages/Manage-rents';
import RentPayDetails from './pages/Rent-pay-details';
import PayRents from './pages/Pay-rent';
import BillingHistory from './pages/Billing-history';
import PostBillingListing from './pages/Post-Billing-Listing';
import CreatePostPay from './pages/Create-post-payment';
import PaymentMethod from './pages/Payment-method';
import BillingMethod from './pages/Billing-method';
import PrintRentReceipt from './pages/Print-Rent-Receipt';
import ManagePosts from './pages/Manage-posts';
import CreatePosts from './pages/Create-post';
import EditPosts from './pages/Edit-post';
import ManageAccesscode from './pages/Manage-accesscode';
import CreateAccesscode from './pages/Create-accesscode';
import NotificationHistory from './pages/Notification-history';
import PostDetails from './pages/Post-details';
import Messaging from './pages/Messaging';
import FAQ from './pages/FAQ';
import MyAccount from './pages/My-account';
import PersonalInfo from './pages/Personal-info';
import BillingInfo from './pages/Billing-Info';
import TenantAccount from './pages/Tenant-Account';
import PasswordChange from './pages/Password-change';
import Notifications from './pages/Notifications';
import ServiceSettings from './pages/Service-settings';
import ManageRequests from './pages/Manage-requests';
import ManageLoadmore from './pages/Manage-loadmore';
import Chat from './pages/chat';
import PostList from './pages/post-listing';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';

export const Routes = {
  indexURL: '/',
  Home: '/index',
  Logout: '/logout',
  forgetPassword: '/forget-password',
  resetPassword: '/reset-password',
  signup: '/signup',
  SignupData: '/signup-data',
  UserDashboard: '/user-dashboard',
  manageProperties: '/manage-properties',
  addNewProperty: '/add-new-property',
  editProperty: '/edit-property',
  manageTenants: '/manage-tenants',
  manageLeases: '/manage-leases',
  leaseInfo: '/lease-info',
  editLeaseInfo: '/edit-lease-info',
  addNewLease: '/add-new-lease',
  leaseRenewalProposal: '/lease-renewal-proposal',
  manageRents: '/manage-rents',
  rentPayDetails: '/rent-pay-details',
  payRents: '/pay-rents',
  billingHistory: '/billing-history',
  postBillingLising: '/post-billing-listing',
  createPostPay: '/create-post-payment',
  paymentMethod: '/payment-method',
  billingMethod: '/billing-method',
  printRentReceipt: '/print-rent-receipt',
  managePosts: '/manage-posts',
  createPosts: '/create-posts',
  editPost: '/edit-post',
  manageAccesscode: '/manage-accesscode',
  createAccesscode: '/create-accesscode',
  notificationHistory: '/notification-history',
  postDetails: '/post-details',
  messaging: '/messaging',
  faq: '/faq',
  myAccount: '/my-account',
  personalInfo: '/personal-info',
  billingInfo: '/billing-info',
  tenantaccount: '/tenant-account',
  passwordChange: '/password-change',
  notifications: '/notifications',
  serviceSettings: '/servicesettings',
  manageLoadmore: '/manage-loadmore',
  manageRequests: '/manage-requests',
  PostList: '/post-listing',
  chat: '/chat'
}

const App: React.FC = () => {

  return (
    <IonApp>
      <IonReactRouter>
        <IonRouterOutlet>

  {/*         <Route path="/landing" component={Landing} exact />
          <Route path="/authcallback" component={LoginRedirect} exact />
          <Route path="/endsession" component={EndRedirect} exact /> */}

          {/* <PrivateRoute path="/home" component={Home} exact />
          <Route exact path="/" render={() => <Redirect to="/home" />} /> */}

          <Route exact path={Routes.Home} component={Home} />
          <Route exact path={Routes.indexURL} component={Home} />

          <Route exact path={Routes.Logout}>
            <Logout />
          </Route>
          <Route exact path={Routes.forgetPassword}>
            <ForgetPassword />
          </Route>
          <Route exact path={Routes.signup}>
            <Signup />
          </Route>
          
          <Switch>
            {/* <Route exact={true} path="/signup-data/:utypeid" component={SignupData} />
            <Redirect from="/signup-data" to="/signup" exact /> */}

            <Route exact={true} path={Routes.SignupData} component={SignupData} />

  {/*           
            <Route exact={true} path={Routes.editProperty} >
              <EditProperty path={Routes.editProperty} prop_id="0" />
            </Route>
  */}

            <Route exact={true} path={Routes.editProperty} component={EditProperty} />

            <Route exact={true} path={Routes.manageTenants} component={ManageTenants} />
            <Route exact={true} path={Routes.manageRents} component={ManageRents} />
            <Route exact={true} path={Routes.manageLeases} component={ManageLeases} />

            <Route exact={true} path='/payment-method/:id' component={PaymentMethod} />
            <Redirect from="/payment-method" to={Routes.payRents} exact />

            <Route exact={true} path='/billing-method/:id' component={BillingMethod} />
            <Redirect from="/billing-method" to={Routes.billingHistory} exact />

            <Route exact={true} path='/print-rent-receipt/:id' component={PrintRentReceipt} />
            <Redirect from="/print-rent-receipt" to={Routes.payRents} exact />

            <Route exact={true} path='/post-details/:id' component={PostDetails} />
            <Redirect from={Routes.postDetails} to={Routes.UserDashboard} exact />

            <Route exact={true} path='/edit-post/:id' component={EditPosts} />
            <Redirect from={Routes.editPost} to={Routes.managePosts} exact />

            <Route exact={true} path='/reset-password/:code' component={ResetPassword} />
            <Redirect from="/reset-password" to={Routes.indexURL} exact />
          </Switch>

          <Route exact={true} path={Routes.addNewLease} component={AddNewLease} />
          <Route exact={true} path={Routes.leaseInfo} component={LeaseInfo} />
          <Route exact={true} path={Routes.editLeaseInfo} component={EditLeaseInfo} />
          <Route exact={true} path={Routes.leaseRenewalProposal} component={LeaseRenewalProposal} />
          <Route exact={true} path={Routes.rentPayDetails} component={RentPayDetails} />
          
          {/* <Route exact={true} path={Routes.editPost} component={EditPosts} /> */}
          {/* <Route exact={true} path={Routes.paymentMethod} component={PaymentMethod} /> */}

          <Route exact path={Routes.UserDashboard}>
            <UserDashboard path={Routes.UserDashboard} />
          </Route>
          <Route exact path={Routes.manageProperties}>
            <ManageProperties path={Routes.manageProperties} />
          </Route>

          <Route exact path={Routes.addNewProperty}>
            <AddNewProperty path={Routes.addNewProperty} />
          </Route>


          {/* <Route path="/stats" render={() => <Stats/>} exact={true} />
          <Route path="/" render={() => <Redirect to="/stats" />} exact={true} /> */}
    
          <Route exact path={Routes.billingHistory}>
            <BillingHistory path={Routes.billingHistory} />
          </Route>

          <Route exact path={Routes.postBillingLising}>
            <PostBillingListing path={Routes.postBillingLising} />
          </Route>

          <Route exact path={Routes.createPostPay}>
            <CreatePostPay path={Routes.createPostPay} />
          </Route>

          <Route exact path={Routes.payRents}>
            <PayRents path={Routes.payRents} />
          </Route>

          <Route exact path={Routes.managePosts}>
            <ManagePosts path={Routes.managePosts} />
          </Route>
          <Route exact path={Routes.createPosts}>
            <CreatePosts path={Routes.createPosts} />
          </Route>

          <Route exact path={Routes.manageAccesscode}>
            <ManageAccesscode path={Routes.manageAccesscode} />
          </Route>
          <Route exact path={Routes.createAccesscode}>
            <CreateAccesscode path={Routes.createAccesscode} />
          </Route>

          <Route exact path={Routes.notificationHistory}>
            <NotificationHistory path={Routes.notificationHistory} />
          </Route>

          {/* <Route exact={true} path={Routes.postDetails} component={PostDetails} /> */}

          <Route exact path={Routes.messaging}>
            <Messaging path={Routes.messaging} />
          </Route>
          <Route exact path={Routes.faq}>
            <FAQ path={Routes.faq} />
          </Route>
          <Route exact path={Routes.myAccount}>
            <MyAccount path={Routes.myAccount} />
          </Route>
          <Route exact path={Routes.personalInfo}>
            <PersonalInfo path={Routes.personalInfo} />
          </Route>
          <Route exact path={Routes.billingInfo}>
            <BillingInfo path={Routes.billingInfo} />
          </Route>
          <Route exact path={Routes.tenantaccount}>
            <TenantAccount path={Routes.tenantaccount} />
          </Route>
          <Route exact path={Routes.passwordChange}>
            <PasswordChange path={Routes.passwordChange} />
          </Route>
          <Route exact path={Routes.notifications}>
            <Notifications path={Routes.notifications} />
          </Route>
          <Route exact path={Routes.serviceSettings}>
            <ServiceSettings path={Routes.serviceSettings} />
          </Route>
          <Route exact path={Routes.manageRequests}>
            <ManageRequests path={Routes.manageRequests} />
          </Route>
          
          <Route exact={true} path={Routes.chat} component={Chat} />
          <Route exact={true} path={Routes.PostList} component={PostList} />

          {/* <Route exact path={Routes.chat}>
            <Chat path={Routes.chat} />
          </Route> */}

          <Route exact path={Routes.manageLoadmore}>
            <ManageLoadmore path={Routes.manageLoadmore} />
          </Route>

        </IonRouterOutlet>
      </IonReactRouter>
    </IonApp>
  );
};

export default App;
