export const globalConst = {
    baseURL: 'http://127.0.0.1:5000/api/v1',
    ContentType: 'application/json',
    AllowOrigin: '*',
    Accept: 'application/json',
    timeout: 30000
};
