import React, {useRef, useState, useEffect, useCallback} from "react";
import { RouteComponentProps, Switch, useLocation } from "react-router";
import { useTranslation } from "react-i18next";

import { 
    IonContent, 
    IonPage, 
    IonGrid, 
    IonRow, 
    IonCol, 
    IonLoading,
    IonAlert,
    IonButton,
    IonIcon,
    useIonViewWillEnter
} from '@ionic/react';
  
import HeaderMain from '../components/Header-main';
import Footer from '../components/Footer';
import FooterMobile from '../components/Footer-mobile';
import DashboardSidebar from '../components/Dahsboard-sidebar';
import ManageLeaseList from '../components/ManageLeaseList';

import '../assets/css/Custom.css';
import '../assets/css/Custom-Dev.css';
import '../assets/css/Responsive.css';
import { Routes } from '../App';

import { globalConst } from "../constants";

import axios from "axios";
axios.defaults.baseURL = globalConst.baseURL;
axios.defaults.headers.post['Content-Type'] = globalConst.ContentType;
axios.defaults.headers.post['Access-Control-Allow-Origin'] = globalConst.AllowOrigin;
axios.defaults.headers.post['Accept'] = globalConst.Accept;
axios.defaults.timeout = globalConst.timeout;

interface PageProps extends RouteComponentProps {path: string; history: any; match: any; location: any;}

const ManageLeases: React.FC<PageProps> = ({ path, history, match, location }) => {

    const { t } = useTranslation();
    const [showLoading, setShowLoading] = useState(true);
    const [locPath, setlocPath] = useState('');

    const [UserLeaseCode, setUserLeaseCode] = useState<any>();
    const [CurrLeasesData, setCurrLeasesData] = useState([{"id": "", "property_id": "", "floorsno": "", "aptno": "", "address": "", "cityname": "", "statename": "", "countryname": "", "lease_code": "", "file_storagekey": "", "lease_price": "", "lease_start_date": "", "lease_end_date": "", "RentPeriod": "", "FileData": [{"file_name": ""}], "TenantsData": [{"tenant_fname": "", "tenant_lname": ""}]}]);
    const [PastLeasesData, setPastLeasesData] = useState([{"id": "", "property_id": "", "floorsno": "", "aptno": "", "address": "", "cityname": "", "statename": "", "countryname": "", "lease_code": "", "file_storagekey": "", "lease_price": "", "lease_start_date": "", "lease_end_date": "", "RentPeriod": "", "FileData": [{"file_name": ""}], "TenantsData": [{"tenant_fname": "", "tenant_lname": ""}]}]);
    const [UpcomingData, setUpcomingData] = useState([{"id": "", "property_id": "", "floorsno": "", "aptno": "", "address": "", "cityname": "", "statename": "", "countryname": "", "lease_code": "", "file_storagekey": "", "lease_price": "", "lease_start_date": "", "lease_end_date": "", "RentPeriod": "", "FileData": [{"file_name": ""}], "TenantsData": [{"tenant_fname": "", "tenant_lname": ""}]}]);
    const [MaxDateData, setMaxDateData] = useState<any>(['', '']);
    const [AddNewLease, setAddNewLease] = useState(0);
    const [RenewalNewLease, setRenewalNewLease] = useState(0);

    const [DisableLeaseCode, setDisableLeaseCode] = useState([]);
    const [DisableData, setDisableData] = useState([{"id": "", "property_id": "", "floorsno": "", "aptno": "", "address": "", "cityname": "", "statename": "", "countryname": "", "lease_code": "", "file_storagekey": "", "lease_price": "", "lease_start_date": "", "lease_end_date": "", "RentPeriod": "", "FileData": [{"file_name": ""}], "TenantsData": [{"tenant_fname": "", "tenant_lname": ""}]}]);

    useIonViewWillEnter(() => {
        if (localStorage.getItem('loginState')!='1') {
            history.push(`${Routes.Home}`);
        } else {
            setCurrLeasesData([]);
            setPastLeasesData([]);
            setUpcomingData([]);
            setDisableData([]);
            setlocPath(`${Routes.manageLeases}`);

            if ((localStorage.getItem('user_type_id')=='2') || (localStorage.getItem('user_type_id')=='3')) {
                history.push(`${Routes.UserDashboard}`);
            } else if ((localStorage.getItem('user_code')=='null') || (localStorage.getItem('user_code')=='')) {
                history.push(`${Routes.UserDashboard}`);
            } else {
                if ((localStorage.getItem('user_code')!==null)) {
                    var LCode = localStorage.getItem('user_code');
                    
                    setUserLeaseCode(LCode);
                    loadLeasesData(LCode);
                }
            }
        }
    });

    async function loadLeasesData(LeaseCode: any) {
        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {

            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            const leaseData = {
                LeaseCode: LeaseCode, 
                UserID: localStorage.getItem('user_id')
            };

            api.put("/propertyleases/alldatabyleasecode", leaseData).then((resUnitData) => {
                const retData = JSON.parse(JSON.stringify(resUnitData)).data;
                console.log(retData);

                setDisableLeaseCode(retData.DisableLeaseCode);
                setDisableData(retData.DisableData);


                setCurrLeasesData(retData.CurrentData);
                setPastLeasesData(retData.PastData);
                setUpcomingData(retData.UpcomingData);

                setAddNewLease(retData.AddNewLease);
                setRenewalNewLease(retData.RenewalNewLease);

                setMaxDateData([retData.MaxDate, retData.MaxDateText, retData.MaxID, retData.MaxStorageKey]);

                setShowLoading(false);

            })
            .catch((error) => {
                alert("Error Get Data!");
            })
        })
        .catch((error) => {
          alert("Token not Get!");
        })
    }

    const doNothing = () => {
		history.goBack();
	}

    const clickRenewalProposalLease = async (path, leaseid, file_storagekey) => {
        history.push({
            pathname: path,
            state: { path: path, leaseid: leaseid, filestoragekey: file_storagekey }
        });
    }

    const ViewLease = async (path, leaseid) => {
        history.push({
            pathname: path,
            state: { path: path, leaseid: leaseid }
        });
    };

    const EditLease = async (path, leaseid, file_storagekey) => {
        history.push({
            pathname: path,
            state: { path: path, leaseid: leaseid, filestoragekey: file_storagekey }
        });
    };

    return (
        <IonPage>
  
           <HeaderMain pageTitle={t('Tenants-Lease-Title')} logoHide="hide-logo" onBack={doNothing} />
  
           <IonContent className="dashboard-wrapper manage-tenants-wrapper" fullscreen>
                <IonLoading
                    isOpen={showLoading}
                    onDidDismiss={() => setShowLoading(false)}
                    message={'Loading...'}
                />

                <IonGrid className="dashboard-main-grid">
                    <IonRow className="dashboard-main-row">
                        
                        {/* sidebar start  */}
                        <DashboardSidebar path={locPath} />
                        {/* sidebar end  */}

                        {/* dashboar content start */}
                        <IonCol className="dashboard-content" size="12" sizeMd="12" sizeLg="6" sizeXl="8">
                            <div className="dashboard-content-inner">
                                <div className="digi-lease-search-results-box">
                                    <h6>{t('Tenants-Lease-Text1')} <b><u>{t('Tenants-Lease-Text2')} {UserLeaseCode}</u></b></h6>
                                    
                                    <div className="search-result-box">
                                        <div className="lease-renewal">
                                            <IonButton className="lease-renewal-button" fill="clear" expand="full" shape="round" onClick={() => clickRenewalProposalLease(Routes.leaseRenewalProposal, MaxDateData[2], MaxDateData[3])}></IonButton>
                                            <IonGrid className="">
                                                <IonRow className="ion-align-items-center">
                                                    <IonCol sizeXs="2" sizeMd="1" sizeLg="1" sizeXl="1" className="">
                                                        <IonIcon icon="assets/images/exclamation-mark.svg"  />
                                                    </IonCol>
                                                    <IonCol sizeXs="8" sizeMd="10" sizeLg="10" sizeXl="10" className="">
                                                        <div className="renewal-lease-info">
                                                            {MaxDateData[1] == ''? null: <h6><u>{t('Tenants-Lease-SearchTool-LeaseRenewal')}</u></h6> }
                                                            {MaxDateData[1] == ''? null: <p>{t('Tenants-Lease-SearchTool-expires-on')} <b>{MaxDateData[1]}.</b></p> }
                                                            <p>{t('Tenants-Lease-SearchTool-Tap-here')}</p>
                                                        </div>
                                                    </IonCol>
                                                    <IonCol sizeXs="2" sizeMd="1" sizeLg="1" sizeXl="1" className="">
                                                        <IonIcon icon="assets/images/plus-icon.svg" />
                                                    </IonCol>
                                                </IonRow>
                                            </IonGrid>
                                        </div>
                                    </div>
                                        
                                    <div className="search-result-box">
                                        {UpcomingData.length > 0 ? <h5>{t('Tenants-Lease-SearchTool-Upcoming-Text')}</h5>: null }
                                        
                                        {UpcomingData.map((item, index) => (
                                            <ManageLeaseList LeaseIndex={index} LeaseData={item} />
                                        ))}
                                    </div>

                                    <div className="search-result-box">
                                        {((CurrLeasesData.length > 0) || (AddNewLease == 1)) ? <h5>{t('Tenants-Lease-SearchTool-Current-Text')}</h5> : null}
                                        
                                        {CurrLeasesData.map((item, index) => (
                                            <ManageLeaseList LeaseIndex={index} LeaseData={item} onclickButton={() => EditLease(Routes.editLeaseInfo, item.id, item.file_storagekey)} />
                                        ))}
                                    </div>

                                    <div className="search-result-box">
                                        {PastLeasesData.length > 0 ? <h5>{t('Tenants-Lease-SearchTool-Past-Text')}</h5>: null }

                                        {PastLeasesData.map((item, index) => (
                                            <ManageLeaseList LeaseIndex={index} LeaseData={item} onclickButton={() => ViewLease(Routes.leaseInfo, item.id)} />
                                        ))}
                                    </div>

                                    {DisableData.length > 0 ? (
                                        <div>
                                            <h6>{t('Tenants-Lease-Text1')} <b><u>{t('Tenants-Lease-Text3')} {DisableLeaseCode.join(", ")}</u></b></h6>

                                            <div className="search-result-box">
                                                <h5>{t('Tenants-Lease-Text4')}</h5>
                                                
                                                {DisableData.map((item, index) => (
                                                    <ManageLeaseList LeaseIndex={index} LeaseData={item} />
                                                ))}
                                            </div>
                                        </div>
                                    ) : '' }
                                    
                                </div>
                            </div>
                        </IonCol>
                        {/* dashboar content end */}

                        {/* dashboar posts start */}
                        <IonCol  className="dashboard-posts" size="12" sizeMd="12" sizeLg="3" sizeXl="2">
                            
                        </IonCol>
                        {/* dashboar post end */}
    
                    </IonRow>
                </IonGrid>
              
                <Footer />

            </IonContent>

            <FooterMobile />
      </IonPage>
    );
  };
  
  export default ManageLeases;