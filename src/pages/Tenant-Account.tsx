import React, {useRef, useState, useEffect} from "react";
import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";

import { 
    IonContent, 
    IonPage, 
    IonGrid, 
    IonRow, 
    IonCol, 
    IonButton,
    IonLoading,
    useIonViewWillEnter
} from '@ionic/react';

import HeaderMain from '../components/Header-main';
import Footer from '../components/Footer';
import FooterMobile from '../components/Footer-mobile';
import DashboardSidebar from '../components/Dahsboard-sidebar';

import '../assets/css/Custom.css';
import '../assets/css/Responsive.css';
import { Routes } from '../App';

import { globalConst } from "../constants";
import axios from "axios";
axios.defaults.baseURL = globalConst.baseURL;
axios.defaults.headers.post['Content-Type'] = globalConst.ContentType;
axios.defaults.headers.post['Access-Control-Allow-Origin'] = globalConst.AllowOrigin;
axios.defaults.headers.post['Accept'] = globalConst.Accept;
axios.defaults.timeout = globalConst.timeout;

const TenantAccount: React.FC<{ path: string }> = ({path}) => {

    const { t } = useTranslation();
    let history = useHistory();

    const [RetData, setRetData] = useState<string[]>([]);
    const [showLoading, setShowLoading] = useState(false);

    useIonViewWillEnter(() => {
        if (localStorage.getItem('loginState')!='1') {
            history.push(`${Routes.Home}`);
        }

        if (localStorage.getItem('user_type_id')!='2') {
            history.push(`${Routes.UserDashboard}`);
        } else {
            setShowLoading(false);
            loadUserData(localStorage.getItem('user_id'));
        }
    });

    async function loadUserData(userID: any) {
        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {

            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            const passData = {
                "UserTypeID" : '1',
                "UserMainID" : userID
            };

            api.post("/auth/alluser/", passData).then((resPData) => {
                const retData = JSON.parse(JSON.stringify(resPData)).data;
                setRetData(retData);
                //console.log(retData);
            })
            .catch((error) => {
                alert("Error Get Data!");
            })
        })
        .catch((error) => {
          alert("Token not Get!");
        })
    }

    const clickLoginBtn = async (LoginID:any) => {
        const api = axios.create();
        api.get("/auth/token/refresh").then(res => res).then((response) => {

            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            const loginData = {
                LoginID: LoginID,
                DirectLogin: true
            };

            api.post("/auth/login", loginData).then((resLogin) => {
                const resLoginData = JSON.parse(JSON.stringify(resLogin));

                if (resLoginData.data.error!=''){
                    alert(resLoginData.data.error);
                } else {
                    localStorage.setItem('accessCode', resLoginData.data.user.access_token);
                    localStorage.setItem('loginState', '1');
                    localStorage.setItem('user_id', resLoginData.data.user.id);
                    localStorage.setItem('user_firstname', resLoginData.data.user.firstname);
                    localStorage.setItem('user_lastname', resLoginData.data.user.lastname);
                    localStorage.setItem('user_phoneno', resLoginData.data.user.phoneno);
                    localStorage.setItem('user_email', resLoginData.data.user.email);
                    localStorage.setItem('user_type_id', resLoginData.data.user.usertype_id);
                    localStorage.setItem('user_type_name', resLoginData.data.user.usertype_name);
                    localStorage.setItem('user_code', resLoginData.data.user.user_code);
                    localStorage.setItem('user_management_id', resLoginData.data.user.user_management_id);
                    localStorage.setItem('sel_prop_id', resLoginData.data.user.user_prop_id);
                    localStorage.setItem('sel_prop_name', resLoginData.data.user.user_prop_name);
                    localStorage.setItem('sel_prop_suffix', resLoginData.data.user.user_prop_suffix);

                    if (resLoginData.data.user.FeatureAccess.manage_posts=='1') {
                    localStorage.setItem('manage_posts', '1');
                    } else {
                    localStorage.setItem('manage_posts', '0');
                    }

                    if (resLoginData.data.user.FeatureAccess.manage_propertys=='1') {
                    localStorage.setItem('manage_propertys', '1');
                    } else {
                    localStorage.setItem('manage_propertys', '0');
                    }

                    if (resLoginData.data.user.FeatureAccess.manage_rents=='1') {
                    localStorage.setItem('manage_rents', '1');
                    } else {
                    localStorage.setItem('manage_rents', '0');
                    }

                    window.location.assign(`${Routes.UserDashboard}`);
                }
            }).catch((error) => {
                alert("Auth failure! Please create an account");
                console.log(error);
            });
        })
        .catch((error) => {
            alert("Token not Get!");
        })
    }

    const doNothing = () => {
		history.goBack();
	}
   
    return (
        <IonPage>
            <HeaderMain pageTitle={t('menu-tenant-account')} logoHide="hide-logo" onBack={doNothing} />
  
            <IonContent className="dashboard-wrapper manage-tenants-wrapper manage-rents-wrapper" fullscreen>
                <IonLoading
                    isOpen={showLoading}
                    onDidDismiss={() => setShowLoading(false)}
                    message={'Loading...'}
                />

                <IonGrid className="dashboard-main-grid">
                    <IonRow className="dashboard-main-row">
                        {/* sidebar start  */}
                        <DashboardSidebar path={path} />
                        {/* sidebar end  */}

                        {/* dashboar content start */}
                        <IonCol className="dashboard-content my-account-content" size="12" sizeMd="12" sizeLg="6" sizeXl="8">
                            <div className="dashboard-content-inner">
                                <IonGrid className="">
                                    <IonRow className="paid-rents-list">
                                        {RetData.map((PData: any) => (
                                            <IonCol size="12" sizeXl="6">
                                                <div className="tenants-lease-info tenants-lease-payment-info ">
                                                    <p><b>{t('Act-Tenant')}: </b>{PData.firstname} {PData.lastname}</p>
                                                    <p>{PData.email}</p>
                                                    <IonButton className="secondary-button view-receipt-button"  shape="round" fill="outline" onClick={() => clickLoginBtn(PData.user_id)} >{t('Login-Login')}</IonButton>
                                                </div>                                   
                                            </IonCol>
                                        ))}
                                    </IonRow>
                                </IonGrid>
                            </div>
                        </IonCol>
                        {/* dashboar content end */}

                        {/* dashboar posts start */}
                        <IonCol  className="dashboard-posts" size="12" sizeMd="12" sizeLg="3" sizeXl="2">
                            
                        </IonCol>
                        {/* dashboar post end */}
                    </IonRow>
                </IonGrid>
              
                <Footer />
            </IonContent>

            <FooterMobile />
        </IonPage>
    );
  };
  
  export default TenantAccount;