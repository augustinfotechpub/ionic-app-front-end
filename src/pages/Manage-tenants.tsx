import React, {useRef, useState, useEffect, useCallback} from "react";
import { RouteComponentProps, Switch, useLocation } from "react-router";
import { useTranslation } from "react-i18next";

import { 
    IonContent, 
    IonPage, 
    IonGrid, 
    IonRow, 
    IonCol, 
    IonSegment,
    IonSegmentButton,
    IonSlides,
    IonSlide,
    IonLoading,
    IonAlert,
    useIonViewDidEnter,
    useIonViewDidLeave,
    useIonViewWillEnter,
    useIonViewWillLeave,
} from '@ionic/react';
  
import HeaderMain from '../components/Header-main';
import Footer from '../components/Footer';
import FooterMobile from '../components/Footer-mobile';
import DashboardSidebar from '../components/Dahsboard-sidebar';
import ManageTenantGeneralTab from '../components/ManageTenanatGeneralTab';
import ManageTenanatSearchTab from '../components/ManageTenanatSearchTab';

import '../assets/css/Custom.css';
import '../assets/css/Custom-Dev.css';
import '../assets/css/Responsive.css';
import { Routes } from '../App';

import { globalConst } from "../constants";

import axios from "axios";
axios.defaults.baseURL = globalConst.baseURL;
axios.defaults.headers.post['Content-Type'] = globalConst.ContentType;
axios.defaults.headers.post['Access-Control-Allow-Origin'] = globalConst.AllowOrigin;
axios.defaults.headers.post['Accept'] = globalConst.Accept;
axios.defaults.timeout = globalConst.timeout;

interface PageProps extends RouteComponentProps {path: string; history: any; match: any; location: any;}

const ManageTenants: React.FC<PageProps> = ({ path, history, match, location }) => {

    const { t } = useTranslation();
    const locationSHI = useLocation();

    //const location = useLocation();
    // console.log(location.pathname);
    
    // a ref variable to handle the current slider
    // const slider = useRef<HTMLIonSlidesElement>(null);
    const slider = useRef<any>(null);
    // a state value to bind segment value
    const [value, setValue] = useState("0");

    const [showLoading, setShowLoading] = useState(true);
    const [PropID, setPropID] = useState("0");
    const [locPath, setlocPath] = useState('');

    const [showAlert, setShowAlert] = useState(false);
    const [msgAlert, setMsgAlert] = useState('');
    const [headAlert, setHeadAlert] = useState('');
    const [handleDismiss, sethandleDismiss] = useState(false);

    const [UAptData,setUAptData]=useState([]);
    const [cntUAptData, setcntUAptData] = useState<number>();
    const [LoadUAptData, setLoadUAptData] = useState<string[]>([]);
    const [SendUAptData, setSendUAptData] = useState<string[]>([]);
    const [SUAptNo, setSUAptNo] = useState<number>(0);
    const [clsMoreUApt, setclsMoreUApt] = useState('show-more-button');

    const [RAptData,setRAptData]=useState([]);
    const [cntRAptData, setcntRAptData] = useState<number>();
    const [LoadRAptData, setLoadRAptData] = useState<string[]>([]);
    const [SRAptNo, setSRAptNo] = useState<number>(0);

    const [LimitApt, setLimitApt] = useState<number>(4);

    const [SearchData, setSearchData] = useState<any>();
    const [SearchAptNo, setSearchAptNo] = useState<number>(0);


    useIonViewWillEnter(() => {
        if (localStorage.getItem('loginState')!='1') {
            history.push(`${Routes.Home}`);
        } else {
            if ((localStorage.getItem('user_type_id')=='2') || ((localStorage.getItem('user_type_id')=='3') && (localStorage.getItem('manage_propertys')=='1'))) {
                //loadConfigData();
            } else {
                history.push(`${Routes.UserDashboard}`);
            }
        }
    });

    useEffect(() => {
        if (SearchAptNo > 0) {
            //alert(SearchData);
            //loadPropByIDData(location.state.prop_id);
        }
    }, [SearchAptNo]);

    useEffect(() => {
        //console.log('tena shi');
        //console.log(locationSHI.state);
        setShowLoading(true);
        var SPI = localStorage.getItem('sel_prop_id');

        if ((SPI!='') && (SPI!==null)) {
            setlocPath(`${Routes.manageTenants}`);

            loadPropByIDData(SPI);
            setPropID(SPI);
            //setShowLoading(false);
        } else {
            showError()
        }
    }, [localStorage.getItem('sel_prop_id')]);

    useEffect(() => {
        slider.current.lockSwipes(true);
        setShowLoading(true);
        if (locationSHI.state) {
            setlocPath(location.state.path);
            
            if (location.state.prop_id>0) {
                setPropID(location.state.prop_id);
                loadPropByIDData(location.state.prop_id);
            }
        }
        setShowLoading(false);
    }, []);

    async function loadPropByIDData(EPropID: any) {
        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {

            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            api.get("/propertyfloors/aptdataRenewalUnoccupied/"+EPropID+"/"+localStorage.getItem('user_id')).then((resUnitData) => {
                const retData = JSON.parse(JSON.stringify(resUnitData)).data;
                //setUAptData(retData.UnoccupiedAptData);
                setcntUAptData(retData.UnoccupiedAptData.length);
                setLoadUAptData(retData.UnoccupiedAptData);
                console.log(retData);

                
                const newData: string[] = [];
                var addNo = 0;
                for(var i = SUAptNo; i < retData.UnoccupiedAptData.length; i++) {
                    if (addNo < LimitApt) {
                        newData.push(retData.UnoccupiedAptData[i]);
                        addNo = addNo + 1;
                    }
                }
                
                setSendUAptData([
                    ...SendUAptData,
                    ...newData
                ]);
        
                setSUAptNo(LimitApt);
        


                //setRAptData(retData.RenewalAptData);
                setcntRAptData(retData.RenewalAptData.length);
                setLoadRAptData(retData.RenewalAptData);

                setShowLoading(false);
            })
            .catch((error) => {
                alert("Error Get Data!");
            })
        })
        .catch((error) => {
          alert("Token not Get!");
        })
    }

    const clickMoreUAptData = () => {
        const newData: string[] = [];
        var addNo = 0;
        for(var i = SUAptNo; i < LoadUAptData.length; i++) {
            if (addNo < LimitApt) {
                newData.push(LoadUAptData[i]);
                addNo = addNo + 1;
            }
        }
        
        setSendUAptData([
            ...SendUAptData,
            ...newData
        ]);

        //console.log('HIHSISISHISHISHI');
        //console.log(SUAptNo + LimitApt);
        //console.log(LoadUAptData.length);
        if ((SUAptNo + LimitApt) >= LoadUAptData.length) {
            setclsMoreUApt('show-more-button hide-input')
        }

        setSUAptNo(SUAptNo + LimitApt);
    }

    const clickMoreRAptData = () => {
        const newData: string[] = [];
        var addNo = 0;
        for(var i = SRAptNo; i < RAptData
            .length; i++)
        {
            if (addNo < LimitApt) {
                newData.push(RAptData[i]);
                addNo = addNo + 1;
            }
        }
        
        setLoadRAptData([
          ...LoadRAptData,
          ...newData
        ]);

        setSRAptNo(SRAptNo + LimitApt);
    }

    const showError = (): void => {
        setShowLoading(false);
        
        setMsgAlert(t('Tenants-Error-Msg'));
        setHeadAlert(t('Tenants-Error-Head'));
        sethandleDismiss(true);
        setShowAlert(true);
    }

    const slideOpts = {
      initialSlide: 0,
      speed: 400,
      loop: false,
      pagination: {
        el: null
      },
    
    };
	
    // a function to handle the segment changes
    const handleSegmentChange = async (e: any) => {
        await slider.current.lockSwipes(false);
        setValue(e.detail.value);
        slider.current!.slideTo(e.detail.value);
        await slider.current.lockSwipes(true);
    };

    // a function to handle the slider changes
    const handleSlideChange = async (event: any) => {
      let index: number = 0;
      await event.target.getActiveIndex().then((value: any) => (index=value));
      setValue(''+index)
    }

	const doNothing = () => {
		//history.push(`/signup-data/${userTypeSelID}`);
		history.goBack();
	}

    const clickWillDismiss = () => {
        if (handleDismiss) {
            let element: HTMLElement = document.getElementsByClassName('change-property')[0] as HTMLElement;
            element.click();

            history.push(`${Routes.manageProperties}`);
        }
    };

    const clickAptNo = async () => {
        await slider.current.lockSwipes(false);
        setValue('1');
        slider.current!.slideTo(1);
        await slider.current.lockSwipes(true);
    };

    const callback = useCallback((SearchPID, SearchFloorNo, AptNo, SearchID) => {
        //alert(SearchPID);
        //alert(SearchFloorNo);
        //alert(AptNo);
        //alert(SearchID);
        
        setSearchAptNo(AptNo);
        setSearchData([SearchPID, SearchFloorNo, AptNo, SearchID]);
      }, []);
    
    const callbackSearch = useCallback((SearchPID, AptNo) => {
        alert(SearchPID);
        alert(AptNo);
    }, []);

    return (
        <IonPage>
  
           <HeaderMain pageTitle={t('Tenants-Lease-Title')} logoHide="hide-logo" onBack={doNothing} />
  
           <IonContent className="dashboard-wrapper manage-tenants-wrapper" fullscreen>
                <IonLoading
                    isOpen={showLoading}
                    onDidDismiss={() => setShowLoading(false)}
                    message={'Loading...'}
                />

                <IonAlert
                    isOpen={showAlert}
                    onDidDismiss={() => setShowAlert(false)}
                    onWillDismiss={() => clickWillDismiss()}
                    cssClass='orange-alert'
                    header={headAlert}
                    message={msgAlert}
                    buttons={['Close']}
                />

                <IonGrid className="dashboard-main-grid">
                    <IonRow className="dashboard-main-row">
                        
                        {/* sidebar start  */}
                        <DashboardSidebar path={locPath} />
                        {/* sidebar end  */}

                        {/* dashboar content start */}
                        <IonCol className="dashboard-content" size="12" sizeMd="12" sizeLg="6" sizeXl="8">
                            <div className="dashboard-content-inner">
                                <IonSegment scrollable  mode="md" value={value} onIonChange={(e) => handleSegmentChange(e)} >
                                    <IonSegmentButton value="0">
                                        {t('Tenants-Lease-General')}
                                    </IonSegmentButton>
                                    <IonSegmentButton value="1">
                                        {t('Tenants-Lease-SearchTool')}
                                    </IonSegmentButton>
                                </IonSegment>
                                <IonSlides pager={true} options={slideOpts} onIonSlideDidChange={(e) => handleSlideChange(e)} ref={slider}>
                                    <IonSlide>
                                        <ManageTenantGeneralTab 
                                            LoadUAptData={SendUAptData} 
                                            cntUAptData={cntUAptData} 
                                            clickMoreUAptData={() => clickMoreUAptData()} 
                                            clsMoreUApt={clsMoreUApt} 

                                            LoadRAptData={LoadRAptData} 
                                            cntRAptData={cntRAptData} 
                                            clickMoreRAptData={() => clickMoreRAptData()} 

                                            clickAptNo={() => clickAptNo()} 
                                            
                                            parentCallback={callback}
                                        />
                                    </IonSlide>
                                    {/*-- Package Segment --*/}
                                    <IonSlide>
                                        <ManageTenanatSearchTab SearchPropID={PropID} SearchAptNo={SearchAptNo} SearchData={SearchData} parentCallback={callbackSearch} />
                                    </IonSlide>
                                </IonSlides>
                            </div>
                        </IonCol>
                        {/* dashboar content end */}

                        {/* dashboar posts start */}
                        <IonCol  className="dashboard-posts" size="12" sizeMd="12" sizeLg="3" sizeXl="2">
                            
                        </IonCol>
                        {/* dashboar post end */}
    
                    </IonRow>
                </IonGrid>
              
                <Footer />

            </IonContent>

            <FooterMobile />
      </IonPage>
    );
  };
  
  export default ManageTenants;