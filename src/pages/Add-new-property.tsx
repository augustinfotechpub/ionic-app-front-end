import React, { useEffect, useRef, useState } from 'react';
import { useTranslation } from "react-i18next";

import { 
    IonContent, 
    IonPage, 
    IonGrid, 
    IonRow,
    IonCol, 
    IonLabel, 
    IonInput, 
    IonButton, 
    IonSlides, 
    IonSlide, 
    IonSelect,
    IonSelectOption,
    IonNote,
    IonItem,
    IonIcon,
    IonLoading,
    IonModal,
    IonAlert,
    useIonViewWillEnter
} from '@ionic/react';

import { FormProvider, useForm, Controller, useFieldArray } from "react-hook-form";
import { ErrorMessage } from '@hookform/error-message';
import { useHistory } from "react-router-dom";

import { close } from "ionicons/icons";

import HeaderMain from '../components/Header-main';
import Footer from '../components/Footer';
import FooterMobile from '../components/Footer-mobile';
import DashboardSidebar from '../components/Dahsboard-sidebar';

import { globalConst } from "../constants";

import '@ionic/react/css/ionic-swiper.css';
import '@ionic/react/css/flex-utils.css';
import '../assets/css/Custom.css';
import '../assets/css/Custom-Dev.css';
import '../assets/css/Responsive.css';
import { Routes } from '../App';

import axios from "axios";
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';
axios.defaults.baseURL = globalConst.baseURL;
axios.defaults.headers.post['Content-Type'] = globalConst.ContentType;
axios.defaults.headers.post['Access-Control-Allow-Origin'] = globalConst.AllowOrigin;
axios.defaults.headers.post['Accept'] = globalConst.Accept;
axios.defaults.timeout = globalConst.timeout;

const slideOpts = {
    initialSlide: 0,
    speed: 400
};

const AddNewProperty: React.FC<{ path: string }> = ({path}) => {
    const { t } = useTranslation();
    let history = useHistory();

    const [showAlert, setShowAlert] = useState(false);
    const [msgAlert, setMsgAlert] = useState('');
    const [headAlert, setHeadAlert] = useState('');
    const [handleDismiss, sethandleDismiss] = useState(false);
    
    const [removeAptID, setremoveAptID] = useState('');

    const [showAlert3, setShowAlert3] = useState(false);
    const [msgAlert3, setMsgAlert3] = useState('');
    const [headAlert3, setHeadAlert3] = useState('');


    const mySlides = useRef<any>(null);
    const [counter, updateCounter] = useState(0);
    const [floorSelected, setFloor] = useState<number>(1); 

    const [showLoading, setShowLoading] = useState(true);
    const [CityData,setCityData]=useState([{"city_id": '', "city_name": ""}]);
    const [StateData,setStateData]=useState([{"state_id": '', "state_name": ""}]);
    const [CountryData,setCountryData]=useState([{"country_id": '', "country_name": ""}]);
    const [UnitData,setUnitData]=useState([{"unit_id": '', "unit_name": ""}]);

    const [arrSelApt, setarrSelApt] = useState([]);
    const [showModal, setShowModal] = useState(false);

    const [FloorsData,setFloorsData]=useState([]);
    const [AllData,setAllData]=useState([] as any);

    const [NoofFloor, setNoofFloor] = useState<number>();
    const [NoofAptFloor, setNoofAptFloor] = useState<number>();
    const [UnitID, setUnitID] = useState<number>();

    const [isSubmitSuccessful, setisSubmitSuccessful] = useState(false);

    const contentRef = useRef<HTMLIonContentElement | null>(null);
    const scrollToTop= () => {
        contentRef.current && contentRef.current.scrollToTop();
    };

    const methods = useForm();const { register, trigger, handleSubmit, control, setValue, getValues, formState: { errors } } = methods;

    const defaultList = [
        { id: "", value: "" },
    ];

    const [listSub, setListSub] = useState(defaultList);

    useIonViewWillEnter(() => {
        if (localStorage.getItem('loginState')!='1') {
            history.push(`${Routes.Home}`);
        } else {
            if ((localStorage.getItem('user_type_id')=='2') || ((localStorage.getItem('user_type_id')=='3') && (localStorage.getItem('manage_propertys')=='1'))) {
                //loadConfigData();
            } else {
                history.push(`${Routes.UserDashboard}`);
            }
        }
    });

    useEffect(() => {
        if (isSubmitSuccessful) {
            const fields = ['propertyName', 'propertyOwner', 'propertyNameSuffix', 'propertyAddress', 'postalCode', 'sorting_id', 'unitesNoFormatText', 'numberOfFloors', 'numberOfApartments'];
            fields.forEach(field => {
                setValue(field, '')
            });

            setisSubmitSuccessful(false);
        }
    }, [isSubmitSuccessful])

    useEffect(() => {
        mySlides.current.lockSwipes(true);
        loadPropData();
    }, []);

    useEffect(() => {
        if (NoofFloor!=null && NoofAptFloor!=null && UnitID!=null){

            if ((NoofFloor>0) && (NoofAptFloor>0) && (UnitID>0)) {
                //alert(NoofFloor + ' ' + NoofAptFloor + ' ' + UnitID);
                loadAptData(NoofFloor, NoofAptFloor, UnitID);

                setValue('unitesNumberFormat', UnitID);
            }
        }

    }, [NoofFloor, NoofAptFloor, UnitID]);

    useEffect(() => {
        if (floorSelected!=null){
            console.log("Use Effect floorSelected ");
            console.log(AllData);

            AllData.forEach((AllDataInfo:[],index:any)=>{
                let message: string[] = AllDataInfo;
                let output_floorno: any;
                let output_floorarr: any;

                let addList: any = []

                if(message.length >= 2) {
                    // Destructuring follows
                    [output_floorno, output_floorarr] = message;

                    if (floorSelected == output_floorno) {
                        console.log(output_floorarr);
                        //setFloorAptData(output_floorarr);
                        //replace({})
                        output_floorarr.forEach((apartNo: any,ind: any)=>{
                            addList.push({id: ind + '_' + output_floorno, value: apartNo});
                        })

                        setListSub(addList);
                    }
                }
            })

            setValue('chooseFloor', floorSelected);
        }
            
    }, [floorSelected]);

    async function loadAptData(Floors: any, Apts: any, UID: any) {
        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {
    
          axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
          const api = axios.create();
    
          api.get("/propertys/aptdatawithoutstore/"+UnitID+"/"+Floors+"/"+NoofAptFloor).then((resListData) => {
            const retData = JSON.parse(JSON.stringify(resListData)).data;

            console.log('Get Data');
            console.log(retData);

            setFloorsData(retData.FloorsData);
            setAllData(retData.AllData);
          })
          .catch((error) => {
            alert("Error Get in City!");
          })

        })
        .catch((error) => {
          alert("Token not Get!");
        })
    }

    async function loadPropData() {
        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {
    
          axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
          const api = axios.create();
    
          api.get("/propertys/allcity").then((resListData) => {
            const retData = JSON.parse(JSON.stringify(resListData)).data;
            setCityData(retData);
          })
          .catch((error) => {
            alert("Error Get in City!");
          })

          api.get("/propertys/allstate").then((resListData) => {
            const retData = JSON.parse(JSON.stringify(resListData)).data;
            setStateData(retData);
          })
          .catch((error) => {
            alert("Error Get in State!");
          })
          
          api.get("/propertys/allcountry").then((resListData) => {
            const retData = JSON.parse(JSON.stringify(resListData)).data;
            setCountryData(retData);
          })
          .catch((error) => {
            alert("Error Get in Country!");
          })

          api.get("/propertys/allunit").then((resListData) => {
            const retData = JSON.parse(JSON.stringify(resListData)).data;
            setUnitData(retData);
            setShowLoading(false);
          })
          .catch((error) => {
            alert("Error Get in Unit Number Format!");
          })

        })
        .catch((error) => {
          alert("Token not Get!");
        })
    }
    
    const next = async (fieldsbunch: any) => {
        const result = await trigger(fieldsbunch);
        if (!result) return;
        await mySlides.current.lockSwipes(false);
        await mySlides.current.slideNext();
        await mySlides.current.lockSwipes(true);
        scrollToTop();
    };

    // mySlides.current.slideTo(0);
    
    useIonViewWillEnter(() => {
        console.log('useIonViewWillEnter event fired');
        mySlides.current.lockSwipes(false);
        mySlides.current.slideTo(0);
    });

    const slideToFirst = () => {
        setTimeout(() => {
            // mySlides.slides.slideTo(0, 500);
            mySlides.current.slideTo(0);
        }, 500);
    }
    

    const prev = async (fieldsbunch: any) => {
        await mySlides.current.lockSwipes(false);
        await mySlides.current.slidePrev();
        await mySlides.current.lockSwipes(true);
    };

    const onSubmit = (data: any) => {
        console.log(data);
        console.log('Submit All Data');
        console.log(AllData);
        //return false;

        var loopFloorNo = 0;

        const api = axios.create();
        api.get("/auth/token/refresh").then(res => res)
        .then((response) => {
            
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            const addPropData = {
                "name" : data.propertyName,
                "owner" : data.propertyOwner,
                "suffix" : data.propertyNameSuffix,
                "address" : data.propertyAddress,
                "city_id" : data.city,
                "state_id" : data.state,
                "country_id" : data.country,
                "postalcode" : data.postalCode,
                "sorting_id" : data.sorting_id,
                "nooffloors" : data.numberOfFloors,
                "noofaptperfloors" : data.numberOfApartments,
                "unitformat_id" : data.unitesNumberFormat,
                "current_user" : localStorage.getItem('user_management_id')
            };

            api.post("/propertys/", addPropData).then((retPropData) => {
                const retPropDataArr = JSON.parse(JSON.stringify(retPropData));
                const PID = retPropDataArr.data.id;
                console.log(PID);

                
                //console.log('Submit All Data');
                AllData.forEach((AllDataInfo:[],index:any)=>{
                    AllDataInfo.map((FloorInfo:any, keyNew) =>  {
                        if (keyNew == 0) {
                            loopFloorNo = FloorInfo;
                        } else {
                            console.log("Floor No : " + loopFloorNo + "Floor Name Array : ");
                            //console.log(FloorInfo);
                            FloorInfo.map((FloorName:string, key22:number) =>  {
                                console.log(FloorName);

                                const addPFloorData = {
                                    "property_id" : PID,
                                    "floorsno" : loopFloorNo,
                                    "aptno" : FloorName,
                                    "current_user" : localStorage.getItem('user_id')
                                };

                                console.log(addPFloorData);
                                const apisub = axios.create();
                                apisub.post("/propertyfloors/", addPFloorData).then((retPFloorData) => {
                                    const retPFloorArr = JSON.parse(JSON.stringify(retPFloorData));
                                    console.log(retPFloorArr);   
                                }).catch((error) => {
                                    alert("Error found in post Data");
                                    console.log(error);
                                });
                            });
                        }
                    });
                })

                setMsgAlert(t('Prop-Add-Msg-Success'));
                setHeadAlert(t('Prop-Add-Head'));
                sethandleDismiss(true);
                setShowAlert(true);

                setisSubmitSuccessful(true);

                //return false;
                history.push("/manage-properties");
            }).catch((error) => {
                alert("Error found in post Data");
                console.log(error);
            });
      
       
        })
        .catch((error) => {
          alert("Token not Get!");
        })
    };

    function handleRemove(id: any) {///, itemid: any, itemData: any
        if (!listSub) return;

        var eleIDArr: any[] = id.split('_');

        let UpdateData: any[];
        UpdateData  = AllData

        UpdateData.forEach((AllDataInfo:[],index)=>{
            let message: string[] = AllDataInfo;
            let output_floorno: any;
            let output_floorarr: any;

            if(message.length >= 2) {
                [output_floorno, output_floorarr] = message;

                let add_floorno: any;
                let add_floorarr: any = [];

                if (eleIDArr[1]==output_floorno) {
                    output_floorarr.forEach((apartNo: any,ind: any)=>{
                        let eleName = ind + '_' + output_floorno;
                        add_floorno = output_floorno;

                        if (eleName == id) {
                            //add_floorarr.push(evaltext)
                        } else {
                            add_floorarr.push(apartNo);
                        }
                    })
                    
                    UpdateData[index] = [add_floorno, add_floorarr as any[]]
                }
            }
        })

        setAllData(UpdateData);

        let newList = listSub;
        // const elementToSearch = {id: id};
        if (newList.length === 1) {
            newList = defaultList;
        } else {
            newList = listSub.filter(el => el.id !== id);
        }

        console.log(newList);

        setListSub(newList);

        
        /* fields.forEach((tDataAdd: any,ind: any)=>{
                const SelAptIndex = arrSelApt.indexOf(tDataAdd.apartcode);
                arrSelApt.splice(SelAptIndex, 1);
        });  */

    };

    function handleRemoveShow(id: any) {///, itemid: any, itemData: any
        var eleIDArr: any[] = id.split('_');
        //alert(AllData);
        console.log(AllData);

        var output_floorno: any;
        var output_floorarr: any;
        var output_apartNo: any;

        AllData.forEach((AllDataInfo:[],index)=>{
            let message: string[] = AllDataInfo;

            if(message.length >= 2) {
                [output_floorno, output_floorarr] = message;

                if (eleIDArr[1]==output_floorno) {
                    output_floorarr.forEach((apartNo: any,ind: any)=>{
                        let eleName = ind + '_' + output_floorno;

                        if (eleName == id) {
                            output_apartNo = apartNo;
                        }
                    })
                    
                }
            }
        })

        setremoveAptID(id);
        
        setMsgAlert3(t('Prop-Remove-Apt-Msg1') + output_apartNo + t('Prop-Remove-Apt-Msg2'));
        setHeadAlert3(t('Prop-Remove-Apt-Head'));
        setShowAlert3(true);
    };

    function clickOkRemove() {
        handleRemove(removeAptID);
	}

    function addItems(id: any) {
        const lastElement = listSub[listSub.length - 1];
        const lastElementID = lastElement.id;
        let newList = listSub;
        // console.log(lastElementID, counter);
        var eleID: any[] = lastElementID.split('_');
        console.log(eleID[0]);
        console.log(eleID[1]);

        if (newList.length === 0) {
            newList = defaultList;
        } else {
            const newElement = (Number(eleID[0]) + 1) + "_" + eleID[1];
            newList.push({id: newElement, value: ""});
            // newList.splice(lastElementID, 0, {id: newElement, value: ""});
            // setList(newList);
        }
        updateCounter(counter + 1);
        
        console.log(newList);
        setListSub(newList);
    };

    function onChangeList(evaltext: any,eleID: any) {
        console.log("Chnage List ");
        console.log(evaltext);
        console.log(eleID);
        console.log(AllData);
        //return false;

        var eleIDArr: any[] = eleID.split('_');

        let UpdateData: any[];
        UpdateData  = AllData

        UpdateData.forEach((AllDataInfo:[],index)=>{
            let message: string[] = AllDataInfo;
            let output_floorno: any;
            let output_floorarr: any;
            let addFlag: any;

            addFlag = false;
            if(message.length >= 2) {
                [output_floorno, output_floorarr] = message;

                let add_floorno: any;
                let add_floorarr: any = [];

                if (eleIDArr[1]==output_floorno) {
                    output_floorarr.forEach((apartNo: any,ind: any)=>{
                        let eleName = ind + '_' + output_floorno;
                        add_floorno = output_floorno;

                        if (eleName == eleID) {
                            //console.log("SHIALPA");
                            //console.log(evaltext);
                            //console.log(eleID);

                            add_floorarr.push(evaltext)
                            addFlag = true;
                            //console.log(add_floorarr);
                        } else {
                            add_floorarr.push(apartNo);
                        }
                    })
                    
                    if (!addFlag) {
                        add_floorarr.push(evaltext)
                    }
                
                    UpdateData[index] = [add_floorno, add_floorarr as any[]]
                    //console.log(UpdateData[index]);
                }
            }
        })

        //console.log(UpdateData);
        setAllData(UpdateData);
        //console.log(AllData);
    };

    const renderList = () => {
        return listSub.map(item => {
            return (
                // <>
                    <IonCol key={item.id} size="6" sizeMd="4" sizeLg="4" sizeXl="4" id={item.id.toString()}>
                        <div className="apartment-info">
                            <IonButton onClick={() => handleRemoveShow(item.id)} fill="clear">
                                <IonIcon icon={close} />
                            </IonButton>
                            <IonInput name={item.id} onIonInput={(e: any) => onChangeList(e.target.value, item.id)} mode="md" type="text" value={item.value}></IonInput>
                        </div>
                    </IonCol>
                // </>
            );
        })
    }

	  const doNothing = () => {
		//history.push(`/signup-data/${userTypeSelID}`);
		history.goBack();
	  }
    
    const changeCity = (e) =>{
        setValue('city', e.detail.value);
        //setValue('state', '');
        //setValue('country', '');

        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {
    
          axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
          const api = axios.create();
    
          api.get("/setting/city/"+e.detail.value).then((resListData) => {
            const retData = JSON.parse(JSON.stringify(resListData)).data;

            //alert(e.detail.value);

            //setStateData(StateData);
            //setValue('state', null);
            setValue('state', retData.stateID);
            //setValue('country', null);
            setValue('country', retData.countryID);
          })
          .catch((error) => {
            alert("Error Get in City!");
          })
        })
        .catch((error) => {
          alert("Token not Get!");
        })

    }

    return (
    <IonPage>

         <HeaderMain pageTitle={t('Prop-Add-Title')}  logoHide="hide-logo" onBack={doNothing} />

         <IonContent className="dashboard-wrapper" ref={contentRef} scrollEvents={true} fullscreen>
            <IonLoading
              isOpen={showLoading}
              onDidDismiss={() => setShowLoading(false)}
              message={'Loading...'}
            />

            <IonAlert
                isOpen={showAlert}
                onDidDismiss={() => setShowAlert(false)}
                cssClass='my-custom-class'
                header={headAlert}
                message={msgAlert}
                buttons={['Close']}
            />

            <IonAlert
                isOpen={showAlert3}
                onDidDismiss={() => setShowAlert3(false)}
                cssClass='my-custom-class'
                header={headAlert3}
                message={msgAlert3}
                buttons={[
                    {
                      text: 'Cancel',
                      role: 'cancel',
                      cssClass: 'secondary',
                      handler: () => {
                        console.log('Confirm Cancel');
                      }
                    },
                    {
                      text: 'Ok',
                      handler: () => {
                          clickOkRemove();
                        //handleRemove(id)
                        console.log('Confirm Ok');
                      }
                    }
                  ]}
            />

            <IonGrid className="dashboard-main-grid add-proepry-wrapper">
                <IonRow className="ion-justify-content-between dashboard-main-row">

                    {/* sidebar start  */}
                    <DashboardSidebar path={path} />
                    {/* sidebar end  */}
                        
                    <IonCol className="dashboard-content" size="12" sizeMd="12" sizeLg="6" sizeXl="4">
                        <FormProvider {...methods}>
                            <form onSubmit={handleSubmit(onSubmit)}>
                                <IonSlides pager={true} options={slideOpts} ref={mySlides} >
                                    <IonSlide>
                                        <IonGrid>
                                            <IonRow className="ion-justify-content-center">
                                                <IonCol size="12" className="form-field">
                                                    <div className="label-with-tooltip">
                                                        <IonLabel className="form-lable">{t('Prop-Name')}</IonLabel>
                                                        <div className="tooltip" title={t('Prop-Name-Tooltip')}>!</div>
                                                    </div>
                                                    <IonInput
                                                        mode="md"
                                                        type="text"
                                                        {...register('propertyName', {
                                                            required: t('Prop-Name-Error')
                                                        })}
                                                    />
                                                    <ErrorMessage
                                                        errors={errors}
                                                        name="propertyName"
                                                        as={<div className="error-message" style={{ color: 'red' }} />}
                                                    />
                                                </IonCol>

                                                <IonCol size="12" className="form-field">
                                                    <IonLabel className="form-lable">{t('Prop-Owner')}</IonLabel>
                                                    <IonInput
                                                        mode="md"
                                                        type="text"
                                                        {...register('propertyOwner')}
                                                    />
                                                    <ErrorMessage
                                                        errors={errors}
                                                        name="propertyOwner"
                                                        as={<div className="error-message" style={{ color: 'red' }} />}
                                                    />
                                                </IonCol>

                                                <IonCol size="12" className="form-field">
						<div className="label-with-tooltip">
                                                    <IonLabel className="form-lable">{t('Prop-Suffix')}</IonLabel>
						    <div className="tooltip" title={t('Prop-Suffix-Tooltip')}>!</div>
                                                </div>
                                                    <IonInput
                                                        mode="md"
                                                        type="text"
                                                        {...register('propertyNameSuffix', {
                                                            required: t('Prop-Suffix-Error1'),
                                                            pattern: {
                                                                value: /^[A-Z]{3}$/i,
                                                                message: t('Prop-Suffix-Error2')
                                                            }
                                                        })}
                                                    />
                                                    <ErrorMessage
                                                        errors={errors}
                                                        name="propertyNameSuffix"
                                                        as={<div className="error-message" style={{ color: 'red' }} />}
                                                    />
                                                </IonCol>

                                                <IonCol size="12" className="form-field">
                                                    <IonLabel className="form-lable">{t('Prop-Address')}</IonLabel>
                                                    <IonInput
                                                        mode="md"
                                                        type="text"
                                                        {...register('propertyAddress', {
                                                            required: t('Prop-Address-Error')
                                                        })}
                                                    />
                                                    <ErrorMessage
                                                        errors={errors}
                                                        name="propertyAddress"
                                                        as={<div className="error-message" style={{ color: 'red' }} />}
                                                    />
                                                </IonCol>

                                                <IonCol size="12" className="form-field">
                                                    <IonLabel className="form-lable">{t('Prop-City')}</IonLabel>

                                                    <Controller
                                                        render={({ field }) => (
                                                        <IonSelect
                                                            mode="md"
                                                            placeholder=""
                                                            value={field.value}
                                                            onIonChange={changeCity}
                                                        >
                                                            {CityData.map((item, index) => (
                                                                <IonSelectOption key={item.city_id} value={item.city_id}>{item.city_name}</IonSelectOption>
                                                            ))}
                                                        </IonSelect>
                                                        )}
                                                        control={control}   
                                                        name="city"
                                                        rules={{ required: t('Prop-City-Error') }}
                                                    />
                                                    <ErrorMessage
                                                        errors={errors}
                                                        name="city"
                                                        as={<div className="error-message" style={{ color: 'red' }} />}
                                                    />
                                                </IonCol>

                                                <IonCol size="12" className="form-field">
                                                    <IonLabel className="form-lable">{t('Prop-State')}</IonLabel>
                                                    <Controller
                                                        render={({ field }) => (
                                                        <IonSelect
                                                            mode="md"
                                                            placeholder=""
                                                            value={field.value}
                                                            onIonChange={e => setValue('state', e.detail.value)}
                                                        >
                                                            {StateData.map((item, index) => (
                                                                <IonSelectOption key={item.state_id} value={item.state_id}>{item.state_name}</IonSelectOption>
                                                            ))}
                                                        </IonSelect>
                                                        )}
                                                        control={control}   
                                                        name="state"
                                                        rules={{ required: t('Prop-State-Error') }}
                                                    />
                                                    <ErrorMessage
                                                        errors={errors}
                                                        name="state"
                                                        as={<div className="error-message" style={{ color: 'red' }} />}
                                                    />
                                                </IonCol>

                                                <IonCol size="12" className="form-field">
						                            <div className="label-with-tooltip">
                                                        <IonLabel className="form-lable">{t('Prop-Country')}</IonLabel>
                                                        <div className="tooltip" title={t('Prop-Country-Tooltip')}>!</div>
                                                    </div>
                                                    <Controller
                                                        render={({ field }) => (
                                                        <IonSelect
                                                            mode="md"
                                                            placeholder=""
                                                            value={field.value}
                                                            onIonChange={e => setValue('country', e.detail.value)}
                                                        >
                                                            {CountryData.map((item, index) => (
                                                                <IonSelectOption key={item.country_id} value={item.country_id}>{item.country_name}</IonSelectOption>
                                                            ))}
                                                        </IonSelect>
                                                        )}
                                                        control={control}   
                                                        name="country"
                                                        rules={{ required: t('Prop-Country-Error') }}
                                                    />
                                                    <ErrorMessage
                                                        errors={errors}
                                                        name="country"
                                                        as={<div className="error-message" style={{ color: 'red' }} />}
                                                    />
                                                </IonCol>

                                                <IonCol size="12" className="form-field">
                                                    <div className="label-with-tooltip">
                                                        <IonLabel className="form-lable">{t('Prop-Postal-code')}</IonLabel>
                                                        <div className="tooltip" title={t('Prop-Postal-code-Tooltip')}>!</div>
                                                    </div>
                                                    <IonInput
                                                        mode="md"
                                                        type="text"
                                                        {...register('postalCode', {
                                                            required: t('Prop-Postal-code-Error1'),
                                                            minLength: { value: 6, message: "length is 6. " }, 
                                                            pattern: {
                                                                value: /^[A-Z0-9]{6}$/i,
                                                                message: t('Prop-Postal-code-Error2'),
                                                            }
                                                        })}
                                                    />
                                                    <ErrorMessage
                                                        errors={errors}
                                                        name="postalCode"
                                                        as={<div className="error-message" style={{ color: 'red' }} />}
                                                    />
                                                </IonCol>

                                                <IonCol size="12" className="form-field">
                                                    <IonLabel className="form-lable">{t('Prop-Order')}</IonLabel>
                                                    <IonInput
                                                        mode="md"
                                                        type="text"
                                                        {...register('sorting_id', {
                                                            required: t('Prop-Order-Error1'),
                                                            pattern: {
                                                                value: /\d{1}$/i,
                                                                message: t('Prop-Order-Error2')
                                                            }
                                                        })}
                                                    />
                                                    <ErrorMessage
                                                        errors={errors}
                                                        name="sorting_id"
                                                        as={<div className="error-message" style={{ color: 'red' }} />}
                                                    />
                                                </IonCol>

                                                <IonCol  size="6" sizeMd="12" sizeLg="6" sizeXl="6" className="form-field sign-up-btn">
                                                    <IonButton className="secondary-button" expand="block" shape="round" fill="outline" onClick={() => next(['propertyName', 'propertyOwner','propertyNameSuffix', 'propertyAddress', 'city', 'state', 'country', 'postalCode', 'sorting_id'])}>
                                                        {t('General-Continue')}
                                                    </IonButton>
                                                </IonCol>

                                            </IonRow>
                                        </IonGrid>
                                    </IonSlide>

                                    <IonSlide>
                                        <IonGrid>
                                            <IonRow className="ion-justify-content-center">

                                                <IonNote>{t('Prop-Text1')}</IonNote>

                                                <IonCol size="6" sizeMd="4" sizeLg="4" sizeXl="4" className="margin-right-auto form-filed">
                                                    <IonLabel className="form-lable">{t('Prop-No-Floors')}</IonLabel>
                                                    <IonInput
                                                        mode="md"
                                                        type="number" 
                                                        onIonChange={e => setNoofFloor(parseInt(e.detail.value!, 10))}  
                                                        {...register('numberOfFloors', {
                                                            required: t('Prop-No-Floors-Error1'),
                                                            pattern: {
                                                                value: /\d{1}$/i,
                                                                message: t('Prop-No-Floors-Error2')
                                                             }
                                                        })}
                                                    />
                                                    <ErrorMessage
                                                        errors={errors}
                                                        name="numberOfFloors"
                                                        as={<div className="error-message" style={{ color: 'red' }} />}
                                                    />
                                                </IonCol>

                                                <IonCol size="12" className="form-filed">
                                                    <div className="label-with-tooltip">
                                                        <IonLabel className="form-lable">{t('Prop-No-Apt')}</IonLabel>
                                                        <div className="tooltip" title={t('Prop-No-Apt-Tooltip')}>!</div>
                                                    </div>
                                                    <IonInput
                                                        mode="md"
                                                        type="number"   
                                                        onIonChange={e => setNoofAptFloor(parseInt(e.detail.value!, 10))}  
                                                        {...register('numberOfApartments', {
                                                            required: t('Prop-No-Apt-Error1'),
                                                            pattern: {
                                                                value: /^[0-9]{1}$/i,
                                                                message: t('Prop-No-Apt-Error2')
                                                             }
                                                        })}
                                                    />
                                                    <ErrorMessage
                                                        errors={errors}
                                                        name="numberOfApartments"
                                                        as={<div className="error-message" style={{ color: 'red' }} />}
                                                    />
                                                </IonCol>

                                                <IonCol size="12" className="form-field">
                                                    <IonLabel className="form-lable">{t('Prop-Units-No')}</IonLabel>
                                                    <Controller
                                                        render={({ field }) => (
                                                        <IonSelect
                                                            mode="md"
                                                            placeholder=""
                                                            value={UnitID}
                                                            onIonChange={(e: any) => {
                                                                //setValue('unitesNumberFormat', e.detail.value); 
                                                                setUnitID(parseInt(e.detail.value!, 10));
                                                              }}
                                                        >
                                                            {UnitData.map((item, index) => (
                                                                <IonSelectOption key={item.unit_id} value={item.unit_id}>{item.unit_name}</IonSelectOption>
                                                            ))}
                                                        </IonSelect>
                                                        )}
                                                        control={control}   
                                                        name="unitesNumberFormat"
                                                        rules={{ required: t('Prop-Units-No-Error') }}
                                                    />
                                                    <ErrorMessage
                                                        errors={errors}
                                                        name="unitesNumberFormat"
                                                        as={<div className="error-message" style={{ color: 'red' }} />}
                                                    />
                                                </IonCol>

                                                <IonCol  size="6" sizeMd="12" sizeLg="6" sizeXl="6" className="form-field sign-up-btn">
                                                    <IonButton className="secondary-button" expand="block" shape="round" fill="outline" onClick={() => prev([])}>
                                                        {t('General-Previous')}
                                                    </IonButton>
                                                    <IonButton className="secondary-button" expand="block" shape="round" fill="outline" onClick={() => next(['numberOfFloors', 'numberOfApartments', 'unitesNumberFormat'])}>
                                                        {t('General-Continue')}
                                                    </IonButton>
                                                </IonCol>

                                            </IonRow>
                                        </IonGrid>
                                    </IonSlide>

                                    <IonSlide>

                                        <div>
                                            <IonNote className="generated-floor-note">{t('Prop-Text2')}</IonNote>

                                            <div className="generated-floor-box">
                                                <div className="generated-floor-box-title-wrap label-with-tooltip">
                                                    <h4 className="generated-floor-box-title">{t('Prop-Generated-Floors')}</h4>
						    <div className="tooltip" title={t('Prop-Generated-Floors')}>!</div>
                                                </div>
                                                <IonGrid>
                                                    <IonRow className="ion-justify-content-center">
                                                        <IonCol size="6" sizeMd="4" sizeLg="4" sizeXl="4" className="form-field margin-right-auto">
                                                            <IonLabel className="form-lable">{t('Prop-Choose-Floors')}</IonLabel>
                                                            <Controller
                                                                render={({ field }) => (
                                                                <IonSelect
                                                                    className="floor-select"
                                                                    mode="md"
                                                                    placeholder=""
                                                                    value={floorSelected}
                                                                    onIonChange={e => setFloor(e.detail.value)}  
                                                                    >
                                                                    {FloorsData.map((FloorsInfo) => (
                                                                        <IonSelectOption key={FloorsInfo} value={FloorsInfo}>{FloorsInfo}</IonSelectOption>
                                                                    ))}
                                                                </IonSelect>
                                                                )}
                                                                control={control}   
                                                                name="chooseFloor"
                                                            />
                                                            <ErrorMessage
                                                                errors={errors}
                                                                name="chooseFloor"
                                                                as={<div className="error-message" style={{ color: 'red' }} />}
                                                            />
                                                        </IonCol>

                                                        <IonCol size="12" className="form-field">
                                                            <IonLabel className="form-lable">{t('Prop-Apt-Text1')}</IonLabel>
                                                            <IonGrid>
                                                                <IonRow className="ion-justify-content-start">
                                                                    {renderList()}

                                                                    <IonCol size="6" sizeMd="4" sizeLg="4" sizeXl="4" className="" onClick={addItems}>
                                                                        <div className="apartment-info add-unit-button">
                                                                            <IonButton className="" fill="clear">
                                                                                <IonLabel>{t('Prop-Apt-Text2')}</IonLabel>
                                                                            </IonButton>
                                                                            
                                                                        </div>
                                                                    </IonCol> 
                                                                </IonRow>
                                                            </IonGrid>
                                                        </IonCol>
                                                    </IonRow>
                                                </IonGrid>   
                                            </div>

                                            <IonGrid>
                                                <IonRow className="ion-justify-content-center">
                                                    <IonCol size="12" sizeMd="12" sizeLg="6" sizeXl="6"     className="form-field sign-up-btn">
                                                        <IonButton expand="block" shape="round" fill="outline" onClick={() => prev([])}>
                                                            {t('General-Previous')}
                                                        </IonButton>
                                                        <IonButton className="secondary-button" type="submit" expand="block" shape="round" fill="outline">
                                                            {t('General-Submit')}
                                                        </IonButton>
                                                    </IonCol>

                                                </IonRow>
                                            </IonGrid>
                                        </div>

                                    </IonSlide>

                                </IonSlides>
                            </form>
                        </FormProvider>
                    </IonCol>

                    {/* dashboar posts start */}
                    <IonCol  className="dashboard-posts" size="12" sizeMd="12" sizeLg="3" sizeXl="2">
                            
                    </IonCol>
                    {/* dashboar post end */}

                </IonRow>
            </IonGrid>

            <Footer />

        </IonContent>

        
        <FooterMobile />

    </IonPage>
    );
};

export default AddNewProperty;