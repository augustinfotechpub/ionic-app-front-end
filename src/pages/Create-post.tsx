import React, {useEffect, useRef, useState, Component} from "react";
import { BrowserRouter, Route, useHistory, Redirect } from "react-router-dom";
import { IonReactRouter } from '@ionic/react-router';
import { useTranslation } from "react-i18next";

import { 
    IonContent, 
    IonPage,
    IonGrid,
    IonRow,
    IonCol, 
    IonButton, 
    IonIcon,
    IonLabel,
    IonInput,
    IonDatetime,
    IonTextarea,
    IonCard,
    IonCardHeader,
    IonCardTitle,
    IonCardContent,
    IonAlert,
    IonSelectOption,
    IonSelect,
    IonCheckbox,
    useIonViewWillEnter,
    IonApp,
    IonRouterOutlet,
} from '@ionic/react';

import { attachOutline, close } from "ionicons/icons";

import { FormProvider, useForm } from "react-hook-form";
import { ErrorMessage } from '@hookform/error-message';

import HeaderMain from '../components/Header-main';
import Footer from '../components/Footer';
import FooterMobile from '../components/Footer-mobile';
import DashboardSidebar from '../components/Dahsboard-sidebar';

import { usePhotoGallery, UserPhoto  } from '../hooks/usePhotoGallery';

import '@ionic/react/css/flex-utils.css';
import '../assets/css/Custom.css';
import '../assets/css/Responsive.css';
import { Routes } from "../App";

import { globalConst } from "../constants";
import axios from "axios";
axios.defaults.baseURL = globalConst.baseURL;
axios.defaults.headers.post['Content-Type'] = globalConst.ContentType;
axios.defaults.headers.post['Access-Control-Allow-Origin'] = globalConst.AllowOrigin;
axios.defaults.headers.post['Accept'] = globalConst.Accept;
axios.defaults.timeout = globalConst.timeout;

const CreatePosts: React.FC<{ path: string }> = ({path}) => {

    const { t } = useTranslation();

    const [StorageKey, setStorageKey] = useState(getRandomString(12));
    const { deletePhoto, photos, prephotos, takePhoto } = usePhotoGallery(StorageKey);
    const [photoToDelete, setPhotoToDelete] = useState<UserPhoto >();
    const [filepath,setFilepath] = useState();

    const [UserTypeData, setUserTypeData] = useState<any>({'id': '', 'name': '', 'lease_code': ''});
    const [FeatureAccessData, setFeatureAccessData] = useState<any>({'manage_posts': '', 'manage_propertys': '', 'manage_rents': ''});

    const [TenantsMaxImg, setTenantsMaxImg] = useState<number>(0);
    const [ManagementMaxImg, setManagementMaxImg] = useState<number>(0);
    const [maxYear, setmaxYear] = useState('2020')

    let history = useHistory();

    const [showAlert, setShowAlert] = useState(false);
    const [msgAlert, setMsgAlert] = useState('');
    const [headAlert, setHeadAlert] = useState('');

    const [bonusAlert, setBonusAlert] = useState(false);
    const [msgBonusAlert, setMsgBonusAlert] = useState('');
    const [headBonusAlert, setHeadBonusAlert] = useState('');

    const [isSubmitSuccessful, setisSubmitSuccessful] = useState(false);

    const methods = useForm();
    const { register, reset , handleSubmit, setValue, formState: { errors } } = methods;

    const contentRef = useRef<HTMLIonContentElement | null>(null);

    const scrollToBottom= () => {
        contentRef.current && contentRef.current.scrollToBottom(500);
    };

    const [selectedPostDate, setSelectedPostDate] = useState<string>('');
    const [selectedActivationDate, setSelectedActivationDate] = useState<string>('');
    const [selectedExpirationDate, setSelectedExpirationDate] = useState<string>('');

    const [PinPost, setPinPost] = useState(false);

    useEffect(() => {
        if ((localStorage.getItem('user_type_id')!==null)) {
            setUserTypeData({'id': localStorage.getItem('user_type_id'), 'name': localStorage.getItem('user_type_name'), 'lease_code': localStorage.getItem('user_code')});
            setFeatureAccessData({'manage_posts': localStorage.getItem('manage_posts'), 'manage_propertys': localStorage.getItem('manage_propertys'), 'manage_rents': localStorage.getItem('manage_rents')});
        }
    }, []);
    
    useIonViewWillEnter(() => {
        if (localStorage.getItem('loginState')!='1') {
            history.push(`${Routes.Home}`);
        } else {
            if ((localStorage.getItem('user_type_id')=='1') || (localStorage.getItem('user_type_id')=='2') || ((localStorage.getItem('user_type_id')=='3') && (localStorage.getItem('manage_posts')=='1'))) {
                loadConfigData();
            } else {
                history.push(`${Routes.UserDashboard}`);
            }
        }

        var ye = new Date().getFullYear();
        ye = ye + 5;
        setmaxYear(ye.toString());
    });

    async function loadConfigData() {
        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {

            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            if (localStorage.getItem('user_type_id')=='1'){
                var current_user = localStorage.getItem('user_id');
            } else {
                var current_user = localStorage.getItem('user_management_id');
            }

            api.get("/postdata/configdata/"+current_user).then((resPData) => {
                const retData = JSON.parse(JSON.stringify(resPData)).data;
                console.log('Get Config Data --------');
                console.log(retData);

                if (retData.PostsCnt>=retData.tenants_max_post) {
                    setMsgBonusAlert(retData.tenants_max_post+" = "+retData.PostsCnt + t('POST-Bonus-Text1') + retData.tenants_max_post + t('POST-Bonus-Text2') + retData.tenants_post_cost + t('POST-Bonus-Text3') + retData.tenants_post_no + t('POST-Bonus-Text4'));
                    setHeadBonusAlert(t('POST-Bonus-Title'));
                    setBonusAlert(true);
                    //history.push(`${Routes.createPostPay}`);
                }
                
                setTenantsMaxImg(Number(retData.tenants_post_image));
                setManagementMaxImg(Number(retData.management_post_image))
            })
            .catch((error) => {
                //error
            })
        })
        .catch((error) => {
          alert("Token not Get!");
        })
    }

    useEffect(() => {
        console.log('isSubmitSuccessful useEffect ');
        
        setValue('post_title', '');
        setValue('post_message', '');
        setValue('post_price', '');
        setSelectedPostDate('');
        setSelectedActivationDate('');
        setSelectedExpirationDate('');
        setStorageKey(getRandomString(12));
        setisSubmitSuccessful(false);
        
    }, [isSubmitSuccessful])

    function getRandomString(length) {
        var randomChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        var result = '';
        for ( var i = 0; i < length; i++ ) {
            result += randomChars.charAt(Math.floor(Math.random() * randomChars.length));
        }
        return result;
    }

    const onSubmit = (data: any) => {
        console.log(data);
        
        const api = axios.create();
        api.get("/auth/token/refresh").then(res => res)
        .then((response) => {
            
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();
  
            var poYear, poMonth, poDate;
            var postDate = data.postDate.substring(0, 10);
            [ poYear, poMonth, poDate ] = postDate.split('-');

            var aYear, aMonth, aDate;
            var activationDate = data.activationDate.substring(0, 10);
            [ aYear, aMonth, aDate ] = activationDate.split('-');
  
            var eYear, eMonth, eDate;
            var expirationDate = data.expirationDate.substring(0, 10);
            [ eYear, eMonth, eDate ] = expirationDate.split('-');

            if (localStorage.getItem('user_type_id')=='1'){
                var current_user = localStorage.getItem('user_id');
            } else {
                var current_user = localStorage.getItem('user_management_id');
            }
  
            const addPostData = {
                "user_type_id" : localStorage.getItem('user_type_id'),
                "current_user" : current_user,
                "post_prop_id" : localStorage.getItem('sel_prop_id'),
                "post_title" : data.post_title,
                "post_message" : data.post_message,
                "post_price" : data.post_price,
                "post_storagekey" : StorageKey,
                "post_pinned_flag" : PinPost,
                "poYear" : poYear,
                "poMonth" : poMonth,
                "poDate" : poDate,
                "aYear" : aYear,
                "aMonth" : aMonth,
                "aDate" : aDate,
                "eYear" : eYear,
                "eMonth" : eMonth,
                "eDate" : eDate,
                "fileData" : photos,
            };
  
            console.log(addPostData);
            api.post("/postdata/", addPostData).then((retData) => {
                const retDataArr = JSON.parse(JSON.stringify(retData)).data;

                console.log(retDataArr);
                if (retDataArr.InsertFlag){
                    setMsgAlert(t('POST-Create-Alert-Msg'));
                    setHeadAlert(t('POST-Create-Alert-Head'));
                    setShowAlert(true);
                } else {
                    setMsgBonusAlert(t('POST-Bonus-Text1') + retDataArr.tenants_max_post + t('POST-Bonus-Text2') + retDataArr.tenants_post_cost + t('POST-Bonus-Text3') + retDataArr.tenants_post_no + t('POST-Bonus-Text4'));
                    setHeadBonusAlert(t('POST-Bonus-Title'));
                    setBonusAlert(true);
                }
                
                setisSubmitSuccessful(true);
            
                //return false;
            }).catch((error) => {
                alert("Error found in put Data");
                console.log(error);
            });
        })
        .catch((error) => {
          alert("Token not Get!");
        })
  
    };
  
    const doNothing = () => {
        history.goBack();
    }

    const clickWillDismiss = (FlagDismiss) => {
        if (FlagDismiss) {
            if (localStorage.getItem('user_type_id')=='1') {
                history.push(`${Routes.UserDashboard}`);
            } else {
                history.push({pathname: `${Routes.managePosts}`});
            }
        }
    };

    const onClickPhotoData=(photo:any)=>{
        setPhotoToDelete(photo) 
        setFilepath(photo.filepath)
    }

    return (
        <IonPage>
  
           <HeaderMain pageTitle={t('POST-Create-Title')}  logoHide="hide-logo" onBack={doNothing} />
  
           <IonContent className="dashboard-wrapper lease-info-wrapper" ref={contentRef} scrollEvents={true} fullscreen>
                <IonAlert
                    isOpen={showAlert}
                    onDidDismiss={() => setShowAlert(false)}
                    onWillDismiss={() => clickWillDismiss(true)}
                    cssClass='orange-alert'
                    header={headAlert}
                    message={msgAlert}
                    buttons={[{text: 'Close', cssClass: 'btn-primary'}]}
                />

                <IonAlert
                    isOpen={bonusAlert}
                    onDidDismiss={() => setBonusAlert(false)}
                    cssClass='orange-alert'
                    header={headBonusAlert}
                    message={msgBonusAlert}
                    buttons={[{
                                text: 'Yes',
                                cssClass: 'btn-secondary',
                                handler: () => {
                                    //onClickBonusPointBuy();
                                    history.push(`${Routes.createPostPay}`);

                                    //history.push({
                                    //    pathname: `${Routes.chat}`,
                                    //    state: { path: `${Routes.chat}`, chat_id: retDataArr.InsertID}
                                    //});
                                }
                            },
                            {
                                text: 'No',
                                cssClass: 'btn-primary',//btn-outline
                                handler: () => {
                                    clickWillDismiss(true);
                                }
                            }]}
                />

                <IonAlert
                    isOpen={!!photoToDelete}
                    onDidDismiss={() => setPhotoToDelete(undefined)}
                    cssClass='red-alert'
                    mode='md'
                    header={t('POST-Remove-Img-Alert-Head')}
                    message={t('POST-Remove-Img-Alert-Msg')}
                    buttons={[{
                                text: 'Remove',
                                cssClass: 'btn-secondary',
                                handler: () => {
                                if (photoToDelete) {
                                    deletePhoto(photoToDelete);
                                    setPhotoToDelete(undefined);
                                }
                                }
                            }, {
                                text: 'Cancel',
                                cssClass: 'btn-primary',//btn-outline
                            }]}
                />

                <IonGrid className="dashboard-main-grid">
                    <IonRow className="dashboard-main-row">
                        
                        {/* sidebar start  */}
                        <DashboardSidebar path={path} />
                        {/* sidebar end  */}

                        {/* dashboar content start */}
                        <IonCol className="dashboard-content" size="12" sizeMd="12" sizeLg="6" sizeXl="8">
                            <FormProvider {...methods}>
                            <form onSubmit={handleSubmit(onSubmit)}>
                                <div className="dashboard-content-inner create-posts-wrapper">
                                    <IonCard>
                                        <IonCardHeader>
                                            <IonCardTitle>{t('POST-Info')}</IonCardTitle>
                                        </IonCardHeader>

                                        <IonCardContent>
                                        <IonRow>
                                            <IonCol size="12" sizeMd="6" className="form-field">
                                                <IonLabel className="form-lable">{t('POST-Title')}*</IonLabel>
                                                <IonInput
                                                    mode="md"
                                                    type="text"
                                                    className={`form-control ${errors.post_title ? 'is-invalid' : ''}`}   
                                                    {...register('post_title', {
                                                        required: t('POST-Title-msg')
                                                    })}
                                                />
                                                <ErrorMessage
                                                    errors={errors}
                                                    name="post_title"
                                                    as={<div className="error-message" style={{ color: 'red' }} />}
                                                />
                                            </IonCol>
                                            <IonCol size="12" sizeMd="6" className="form-field">
                                                <IonLabel className="form-lable">{t('POST-Description')}*</IonLabel>
                                                <IonTextarea
                                                    mode="md"
                                                    className={`form-control ${errors.post_message ? 'is-invalid' : ''}`}   
                                                    {...register('post_message', {
                                                        required: t('POST-Description-msg')
                                                    })}
                                                />
                                                <ErrorMessage
                                                    errors={errors}
                                                    name="post_message"
                                                    as={<div className="error-message" style={{ color: 'red' }} />}
                                                />
                                            </IonCol>
                                            <IonCol size="12" sizeMd="6" className="form-field">
                                                <IonLabel className="form-lable">{t('POST-Price')}</IonLabel>
                                                <IonInput className="width-20" mode="md" type="number" {...register('post_price')} />
                                                <ErrorMessage
                                                    errors={errors}
                                                    name="post_price"
                                                    as={<div className="error-message" style={{ color: 'red' }} />}
                                                />
                                            </IonCol>

                                            {UserTypeData.id == '1' ? '' : 
                                                <IonCol size="12" sizeMd="6" className="form-field">
                                                    <IonLabel className="form-lable">{t('POST-Pinned')}</IonLabel>
                                                    <IonCheckbox color="primary" checked={PinPost} onIonChange={e => setPinPost(e.detail.checked)} />
                                                </IonCol>
                                            }
                                            

                                            <IonCol size="12" sizeMd="6">
                                                <div className="label-with-tooltip">
                                                    <IonLabel className="form-lable">{t('POST-Date')}</IonLabel>
                                                </div>
                                                <div className="date-picker">
                                                    <IonDatetime 
                                                        displayFormat="MMM DD, YYYY"
                                                        placeholder="Select Date" 
                                                        value={selectedPostDate} 
                                                        className={`form-control ${errors.postDate ? 'is-invalid' : ''}`}   
                                                        onIonChange={e => setSelectedPostDate(e.detail.value!)}
                                                        mode="md"  
                                                        {...register('postDate', {
                                                            required: t('POST-Date-msg')
                                                        })}
                                                    >
                                                    </IonDatetime>
                                                    <IonIcon icon="assets/images/calendar-icon.svg" />
                                                </div>
                                                <ErrorMessage
                                                    errors={errors}
                                                    name="postDate"
                                                    as={<div className="error-message" style={{ color: 'red' }} />}
                                                />
                                            </IonCol>

                                            <IonCol size="12" sizeMd="6" className="form-field">
                                                <IonLabel className="form-lable">{t('POST-Attachments')}</IonLabel>
                                            </IonCol>
                                            
                                            <IonCol size="12" sizeMd="6" className="uploaded-images-list">   
                                                {photos.map((photo, index) => (
                                                    <div className="uploaded-file">
                                                        <p className="uploaded-file-name">{photo.filepath}</p>
                                                        <IonButton fill="clear" onClick={() => onClickPhotoData(photo)} >
                                                            <IonIcon icon={close} />
                                                        </IonButton>
                                                    </div>
                                                ))}
                                            </IonCol>

                                            {(UserTypeData.id == '1' && photos.length == TenantsMaxImg) || (UserTypeData.id == '2' && photos.length == ManagementMaxImg) ? '' : 
                                                <IonCol size="12" sizeMd="6" className="upload-photo-block ion-text-center">
                                                    <div className="upload-photo-block">
                                                        <IonButton className="take-photo-btn" fill="solid" shape="round" onClick={() => takePhoto()}>
                                                            <IonIcon icon="assets/images/add-photo-white.svg" />
                                                        </IonButton>
                                                    </div>
                                                </IonCol>
                                            }
                                        </IonRow>
                                        </IonCardContent>
                                    </IonCard>

                                    <IonCard>
                                        <IonCardHeader>
                                            <IonCardTitle>{t('POST-Settings')}</IonCardTitle>
                                        </IonCardHeader>

                                        <IonCardContent>
                                        <IonRow>
                                            <IonCol size="12" sizeMd="6">
                                                <div className="label-with-tooltip">
                                                    <IonLabel className="form-lable">{t('POST-Activation-Date')}</IonLabel>
                                                    <div className="tooltip" title={t('POST-Activation-Date-tooltip')}>!</div>
                                                </div>
                                                <div className="date-picker">
                                                    <IonDatetime 
                                                        displayFormat="MMM DD, YYYY"
                                                        placeholder="Select Date" 
                                                        value={selectedActivationDate} 
                                                        className={`form-control ${errors.activationDate ? 'is-invalid' : ''}`}   
                                                        onIonChange={e => setSelectedActivationDate(e.detail.value!)}
                                                        mode="md"  
                                                        {...register('activationDate', {
                                                            required: t('POST-Activation-Date-msg')
                                                        })}
                                                    >
                                                    </IonDatetime>
                                                    <IonIcon icon="assets/images/calendar-icon.svg" />
                                                </div>
                                                <ErrorMessage
                                                    errors={errors}
                                                    name="activationDate"
                                                    as={<div className="error-message" style={{ color: 'red' }} />}
                                                />
                                            </IonCol>

                                            <IonCol size="12" sizeMd="6">
                                                <div className="label-with-tooltip">
                                                    <IonLabel className="form-lable">{t('POST-Expiration-Date')}</IonLabel>
                                                    <div className="tooltip" title={t('POST-Expiration-Date-tooltip')}>!</div>
                                                </div>
                                                <div className="date-picker">
                                                    <IonDatetime 
                                                        displayFormat="MMM DD, YYYY"
                                                        max={maxYear}
                                                        placeholder="Select Date"
                                                        className={`form-control ${errors.expirationDate ? 'is-invalid' : ''}`}    
                                                        value={selectedExpirationDate} 
                                                        onIonChange={e => setSelectedExpirationDate(e.detail.value!)}
                                                        mode="md"  
                                                        {...register('expirationDate', {
                                                            required: t('POST-Expiration-Date-msg')
                                                        })}
                                                    >
                                                    </IonDatetime>
                                                    <IonIcon icon="assets/images/calendar-icon.svg" />
                                                </div>
                                                <ErrorMessage
                                                    errors={errors}
                                                    name="expirationDate"
                                                    as={<div className="error-message" style={{ color: 'red' }} />}
                                                />
                                            </IonCol>
                                        </IonRow>
                                        </IonCardContent>
                                    </IonCard>

                                    <IonRow className="ion-justify-content-center">
                                        <IonCol className="ion-text-center">
                                            <IonButton type="submit"  className="secondary-button" fill="outline" shape="round">Submit</IonButton>
                                            <IonButton className="exit-file-btn" onClick={() => clickWillDismiss(true)} fill="solid" shape="round">Exit</IonButton>
                                        </IonCol>
                                    </IonRow>
                                </div>
                            </form>
                            </FormProvider>
                        </IonCol>
                        {/* dashboar content end */}

                        {/* dashboar posts start */}
                        <IonCol  className="dashboard-posts" size="12" sizeMd="12" sizeLg="3" sizeXl="2">
                            
                        </IonCol>
                        {/* dashboar post end */}
    
                    </IonRow>
                </IonGrid>

                <IonButton className="scroll-to-bottom-btn" onClick={scrollToBottom} fill="clear">
                    <IonIcon icon="assets/images/double-arrow-down.svg" />
                </IonButton>
              
                <Footer />
            </IonContent>
            <FooterMobile />
      </IonPage>
    );
};

export default CreatePosts;