import React, {useEffect, useRef, useState, useCallback} from "react";
import { useHistory, useLocation } from 'react-router-dom';
import { useTranslation } from "react-i18next";

import { 
    IonContent, 
    IonPage,
    IonGrid,
    IonRow,
    IonCol, 
    IonButton, 
    IonIcon,
    IonItem,
    IonPopover,
    IonList,
    IonLoading,
    useIonViewWillEnter
} from '@ionic/react';

import { add } from "ionicons/icons";

import HeaderMain from '../components/Header-main';
import Footer from '../components/Footer';
import FooterMobile from '../components/Footer-mobile';
import DashboardSidebar from '../components/Dahsboard-sidebar';
import { CodeAccordion } from "../components/CodeAccordion";


import '@ionic/react/css/flex-utils.css';
import '../assets/css/Custom.css';
import '../assets/css/Responsive.css';
import { Routes } from "../App";

import { globalConst } from "../constants";
import axios from "axios";
axios.defaults.baseURL = globalConst.baseURL;
axios.defaults.headers.post['Content-Type'] = globalConst.ContentType;
axios.defaults.headers.post['Access-Control-Allow-Origin'] = globalConst.AllowOrigin;
axios.defaults.headers.post['Accept'] = globalConst.Accept;
axios.defaults.timeout = globalConst.timeout;

const ManageAccesscode: React.FC<{ path: string }> = ({path}) => {

    const { t } = useTranslation();
    let history = useHistory();
    const contentRef = useRef<HTMLIonContentElement | null>(null);

    const [showLoading, setShowLoading] = useState(true);

    const [CodeData, setCodeData] = useState<Array<any>>([{"id": "", "assign_user_active_code" : '', "access_code": '', "assign_username": "", "assign_useremail": '', "assign_userphoneno": '', "FeatureAccess": [{"manage_posts": "", "manage_propertys": "", "manage_rents": ""}]}]);
    
    const [popoverState, setShowPopover] = useState<{showPopover: boolean, event: Event | undefined}>({ showPopover: false, event: undefined });
    const [count, setCount] = useState(0);

    const scrollToBottom= () => {
        contentRef.current && contentRef.current.scrollToBottom(500);
    };

    useIonViewWillEnter(() => {
        setShowLoading(true);

        if (localStorage.getItem('loginState')!='1') {
            history.push(`${Routes.Home}`);
        }

        if ((localStorage.getItem('user_type_id')!='2')) {
            history.push(`${Routes.UserDashboard}`);
        } else {
            setCodeData([]);
            loadAccessData();
        }
    });

    async function loadAccessData() {
        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {

            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            api.get("/auth/accesscode/?UserID="+localStorage.getItem('user_id')).then((resGetData) => {
                const retData = JSON.parse(JSON.stringify(resGetData)).data;
                //console.log(retData);

                setCodeData(retData);

                setShowLoading(false);
            })
            .catch((error) => {
                alert("Error Get Data!");
            })
        })
        .catch((error) => {
          alert("Token not Get!");
        })
    }

    const historyTabReload = useCallback((count) => {
        setCount(count);
        //alert(count);
        loadAccessData();
    }, []);


    const renderAccessCodeData = () => {
        if (CodeData.length > 0) {
            return CodeData.map(item => {
                return (
                    <div key={item.id}>
                        <CodeAccordion
                            id={item.id}
                            access_code={item.access_code}
                            assign_user_active_code={item.assign_user_active_code}
                            assign_username={item.assign_username}
                            assign_useremail={item.assign_useremail}
                            assign_userphoneno={item.assign_userphoneno}
                            FeatureAccess={item.FeatureAccess}
                            parentCallback={historyTabReload}
                        />
                    </div>
                );
            })
        } else {
            return (
                <div><br />{t('AccessCode-No-Data-Found')}<br /><br /></div>
            );
        }
    }

    const doNothing = () => {
        history.goBack();
    }

    return (
        <IonPage>
  
           <HeaderMain pageTitle={t('AccessCode-Title')}  logoHide="hide-logo" onBack={doNothing} />
  
           <IonContent className="dashboard-wrapper lease-info-wrapper" ref={contentRef} scrollEvents={true} fullscreen>
                <IonLoading
                    isOpen={showLoading}
                    onDidDismiss={() => setShowLoading(false)}
                    message={'Loading...'}
                />

                <IonGrid className="dashboard-main-grid">
                    <IonRow className="dashboard-main-row">
                        
                        {/* sidebar start  */}
                        <DashboardSidebar path={path} />
                        {/* sidebar end  */}

                        {/* dashboar content start */}
                        <IonCol className="dashboard-content" size="12" sizeMd="12" sizeLg="6" sizeXl="8">
                            <div className="dashboard-content-inner">
                                <div className="manage-posts-wrapper">
                                    <div className="posts-list pinned-posts">
                                        <h3 className="title-with-line">Access code Data:</h3>

                                        <div className='accordion-item active'>
                                            <IonRow className="accordion-button ion-align-items-center">
                                                <IonCol><div><b>{t('AccessCode-Active')}</b></div></IonCol>
                                                <IonCol><div><b>{t('AccessCode-Code')}</b></div></IonCol>
                                                <IonCol><div><b>{t('AccessCode-Assign-User-Info')}</b></div></IonCol>
                                                <IonCol><div><b>{t('AccessCode-Feature-Access')}</b></div></IonCol>
                                            </IonRow>
                                        </div>

                                        {renderAccessCodeData()}
                                    </div>

                                    <IonButton 
                                        className="property-change-button" 
                                        fill="clear"
                                        onClick={
                                                (e) => {
                                                setShowPopover({ showPopover: true, event: e.nativeEvent })
                                            }}
                                    >
                                        <IonIcon src="/assets/images/format_list_bulleted.svg"  />
                                    </IonButton>

                                    <IonPopover
                                        cssClass='property-change-menu-popup' 
                                        mode="ios"
                                        event={popoverState.event}
                                        isOpen={popoverState.showPopover}
                                        onDidDismiss={() => setShowPopover({ showPopover: false, event: undefined })}
                                    >
                                        <IonList>
                                            <IonItem>
                                                <IonButton 
                                                    fill="clear" 
                                                    routerLink={Routes.createAccesscode}
                                                    onClick={
                                                        () => {
                                                        setShowPopover({ showPopover: false, event: undefined })
                                                    }}
                                                >
                                                    <IonIcon icon={add} />  
                                                    <span>{t('AccessCode-Create')}</span>
                                                </IonButton>
                                            </IonItem>
                                        </IonList>
                                    </IonPopover>
                                </div>
                            </div>
                        </IonCol>
                        {/* dashboar content end */}

                        {/* dashboar posts start */}
                        <IonCol  className="dashboard-posts" size="12" sizeMd="12" sizeLg="3" sizeXl="2">
                            
                        </IonCol>
                        {/* dashboar post end */}
    
                    </IonRow>
                </IonGrid>

                <IonButton className="scroll-to-bottom-btn" onClick={scrollToBottom} fill="clear">
                    <IonIcon icon="assets/images/double-arrow-down.svg" />
                </IonButton>
              
                <Footer />

            </IonContent>

            <FooterMobile />
      </IonPage>
    );
};

export default ManageAccesscode;