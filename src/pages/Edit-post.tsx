import React, {useEffect, useRef, useState} from "react";
import { RouteComponentProps, Switch, useLocation } from "react-router";
import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";

import { 
    IonContent, 
    IonPage,
    IonGrid,
    IonRow,
    IonCol, 
    IonButton, 
    IonIcon,
    IonLabel,
    IonInput,
    IonDatetime,
    IonTextarea,
    IonCard,
    IonCardHeader,
    IonCardTitle,
    IonCardContent,
    IonAlert,
    IonLoading,
    IonCheckbox,
    IonItem,
    useIonViewWillEnter
} from '@ionic/react';

import { attachOutline, close } from "ionicons/icons";

import { FormProvider, useForm } from "react-hook-form";
import { ErrorMessage } from '@hookform/error-message';

import HeaderMain from '../components/Header-main';
import Footer from '../components/Footer';
import FooterMobile from '../components/Footer-mobile';
import DashboardSidebar from '../components/Dahsboard-sidebar';

import { usePhotoGallery, UserPhoto  } from '../hooks/usePhotoGallery';

import '@ionic/react/css/flex-utils.css';
import '../assets/css/Custom.css';
import '../assets/css/Responsive.css';
import { Routes } from "../App";

import { globalConst } from "../constants";
import axios from "axios";
axios.defaults.baseURL = globalConst.baseURL;
axios.defaults.headers.post['Content-Type'] = globalConst.ContentType;
axios.defaults.headers.post['Access-Control-Allow-Origin'] = globalConst.AllowOrigin;
axios.defaults.headers.post['Accept'] = globalConst.Accept;
axios.defaults.timeout = globalConst.timeout;

interface PageProps extends RouteComponentProps {path: string; history: any; match: any; location: any;}

const EditPosts: React.FC<PageProps> = ({ path, history, match, location }) => {

    const { t } = useTranslation();
    const locationSHI = useLocation();

    let StoKey = '';
    if (locationSHI.state) {
        if (location.state.filestoragekey !== null) {
            StoKey = location.state.filestoragekey;
        }
    }

    const { deletePhoto, photos, takePhoto } = usePhotoGallery(StoKey);
    const [photoToDelete, setPhotoToDelete] = useState<UserPhoto >();
    const [filepath,setFilepath] = useState();

    const [PostDelete, setPostDelete] = useState(false);
    const [repostnew, setrepostnew] = useState('0');

    const [bonusAlert, setBonusAlert] = useState(false);
    const [msgBonusAlert, setMsgBonusAlert] = useState('');
    const [headBonusAlert, setHeadBonusAlert] = useState('');

    const [locPath, setlocPath] = useState('');
    const [showLoading, setShowLoading] = useState(true);
    const [showAlert, setShowAlert] = useState(false);
    const [msgAlert, setMsgAlert] = useState('');
    const [headAlert, setHeadAlert] = useState('');

    const [UTypeID, setUTypeID] = useState<any>(localStorage.getItem('user_type_id'));
    const [TenantsMaxImg, setTenantsMaxImg] = useState<number>(0);
    const [ManagementMaxImg, setManagementMaxImg] = useState<number>(0);
    const [DelPostID, setDelPostID] = useState<number>(0);

    const [PostsCnt, setPostsCnt] = useState<number>(0);
    const [TenantsMaxPost, setTenantsMaxPost] = useState<number>(0);
    const [PostsAmt, setPostsAmt] = useState<number>(0);

    const [updateFileNotificationSentAlert, setUpdateFileNotificationSentAlert] = useState(false);

    const methods = useForm();
    const { register, handleSubmit, setValue, formState: { errors } } = methods;

    useIonViewWillEnter(() => {
        if (localStorage.getItem('loginState')!='1') {
            history.push(`${Routes.Home}`);
        } else {
            if ((localStorage.getItem('user_type_id')=='1') || (localStorage.getItem('user_type_id')=='2') || ((localStorage.getItem('user_type_id')=='3') && (localStorage.getItem('manage_posts')=='1'))) {
                //loadConfigData();
            } else {
                history.push(`${Routes.UserDashboard}`);
            }
        }
    });

    async function loadConfigData() {
        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {

            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            if (localStorage.getItem('user_type_id')=='1'){
                var current_user = localStorage.getItem('user_id');
            } else {
                var current_user = localStorage.getItem('user_management_id');
            }

            api.get("/postdata/configdata/"+current_user).then((resPData) => {
                const retData = JSON.parse(JSON.stringify(resPData)).data;
                //console.log('Get Config Data --------');
                //console.log(retData.tenants_post_image);

                setPostsCnt(Number(retData.PostsCnt));
                setTenantsMaxPost(Number(retData.tenants_max_post));
                setPostsAmt(retData.tenants_post_cost);
                
                setTenantsMaxImg(Number(retData.tenants_post_image));
                setManagementMaxImg(Number(retData.management_post_image))
            })
            .catch((error) => {
                //error
            })
        })
        .catch((error) => {
          alert("Token not Get!");
        })
    }

    useEffect(() => {
        setlocPath(`${Routes.editPost}`);
        
        
        if (match.params.id>0) {
            loadPostData(match.params.id);
            setShowLoading(false);
        } else {
            showError()
        }
        
    }, [match.params.id]);

    /* useEffect(() => {
        if (locationSHI.state) {
            setShowLoading(false);
            setlocPath(location.state.path);

            if (location.state.postID>0) {
                loadPostData(location.state.postID);
            }
        } else {
            showError()
        }
    }, []); */

    async function loadPostData(postID: any) {
        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {

            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            api.get("/postdata/"+postID+"/"+localStorage.getItem('user_id')).then((resPData) => {
                const retData = JSON.parse(JSON.stringify(resPData)).data;
                //console.log('Get Post Data --------');
                //console.log(retData);

                if (localStorage.getItem('user_type_id')=='1') {
                    if (retData.post_updated_cnt>=retData.tenants_post_max_modify){
                        setMsgAlert(t('POST-Modify-Text1')+ retData.tenants_post_max_modify +t('POST-Modify-Text2'));
                        setHeadAlert(t('POST-Modify-Head'));
                        setShowAlert(true);
                    } else {
                        if (localStorage.getItem('user_id')!=retData.post_user_id) {
                            history.push(`${Routes.UserDashboard}`);
                        }
                    }
                }

                loadConfigData();
    
                const fields = ['post_id', 'post_title', 'post_message', 'post_activation_date', 'post_expiration_date', 'post_price', 'post_pinned_flag', 'post_date'];
                fields.forEach(field => {
                    if (field=='post_activation_date') {
                        setSelectedActivationDate(convert(retData.post_activation_date))
                        setValue(field, convert(retData.post_activation_date))
                    } else if (field=='post_expiration_date') {
                        setSelectedExpirationDate(convert(retData.post_expiration_date))
                        setValue(field, convert(retData.post_expiration_date))
                    } else if (field=='post_date') {
                        setSelectedPostDate(convert(retData.post_date))
                        setValue('postDate', convert(retData.post_date))
                    } else if (field=='post_pinned_flag') {
                        if (retData.post_pinned_flag==1) {
                            setPinPost(true)
                        }
                    } else {
                        if (field=='post_id') {
                            setDelPostID(retData[field])
                        }
                        setValue(field, retData[field])
                    }
                });
            })
            .catch((error) => {
                setMsgAlert(t('POST-Error-Text1'));
                setHeadAlert(t('POST-Error-Head'));
                setShowAlert(true);
            })
        })
        .catch((error) => {
          alert("Token not Get!");
        })
    }

    function convert(str) {
        var date = new Date(str),
          mnth = ("0" + (date.getMonth() + 1)).slice(-2),
          day = ("0" + date.getDate()).slice(-2);
        return [date.getFullYear(), mnth, day].join("-");
    }

    const showError = (): void => {
        setShowLoading(false);
        
        setMsgAlert(t('POST-Error-Text2'));
        setHeadAlert(t('POST-Error-Head'));
        setShowAlert(true);
    }

    const clickWillDismiss = (FlagDismiss) => {
        if (FlagDismiss) {
            if (localStorage.getItem('user_type_id')=='1') {
                history.push(`${Routes.PostList}`);
            } else {
                history.push(`${Routes.managePosts}`);
            }
        }
    };

    const ClickRepost = (valFlag: any) => {
        if (valFlag) {
            setrepostnew('1');
        } else {
            setrepostnew('0');
        }
    }

    const onSubmit = (data: any) => {

        //console.log('Submit');
        //console.log(data.repost_as_new);
        //return false;

        const api = axios.create();
        api.get("/auth/token/refresh").then(res => res)
        .then((response) => {
            
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            var poYear, poMonth, poDate;
            var postDate = data.postDate.substring(0, 10);
            [ poYear, poMonth, poDate ] = postDate.split('-');

            var aYear, aMonth, aDate;
            var activationDate = data.post_activation_date.substring(0, 10);
            [ aYear, aMonth, aDate ] = activationDate.split('-');
  
            var eYear, eMonth, eDate;
            var expirationDate = data.post_expiration_date.substring(0, 10);
            [ eYear, eMonth, eDate ] = expirationDate.split('-');

            const editPostData = {
                "current_user" : localStorage.getItem('user_id'),
                "post_title" : data.post_title,
                "post_message" : data.post_message,
                "post_price" : data.post_price,
                "post_pinned_flag" : PinPost,
                "poYear" : poYear,
                "poMonth" : poMonth,
                "poDate" : poDate,
                "aYear" : aYear,
                "aMonth" : aMonth,
                "aDate" : aDate,
                "eYear" : eYear,
                "eMonth" : eMonth,
                "eDate" : eDate,
                "fileData" : photos,
            };

            console.log(editPostData);

            if (data.repost_as_new=='1'){
                if (PostsCnt>=TenantsMaxPost) {
                    setMsgBonusAlert(t('POST-Bonus-Text5') +  PostsAmt +  t('POST-Bonus-Text7'));
                    setHeadBonusAlert(t('POST-Repost'));
                    setBonusAlert(true);
                }
            } else {
                api.put("/postdata/"+data.post_id+"/"+localStorage.getItem('user_id'), editPostData).then((retData) => {
                    const retDataArr = JSON.parse(JSON.stringify(retData));

                    setMsgAlert(t('POST-Edit-Alert-Msg'));
                    setHeadAlert(t('POST-Edit-Alert-Head'));
                    setShowAlert(true);
            
                }).catch((error) => {
                    alert("Error found in put Data");
                    console.log(error);
                });
            }
        })
        .catch((error) => {
          alert("Token not Get!");
        })
    };

    const contentRef = useRef<HTMLIonContentElement | null>(null);

    const scrollToBottom= () => {
        contentRef.current && contentRef.current.scrollToBottom(500);
    };

    const [selectedPostDate, setSelectedPostDate] = useState<string>('');
    const [selectedActivationDate, setSelectedActivationDate] = useState<string>('');
    const [selectedExpirationDate, setSelectedExpirationDate] = useState<string>('');

    const [PinPost, setPinPost] = useState(false);

    const doNothing = () => {
        history.goBack();
    }

    const onClickPhotoData=(photo:any)=>{
        setPhotoToDelete(photo) 
        setFilepath(photo.filepath)
    }

    const onClickPostDelete=(delPID:any)=>{
        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {
    
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            api.delete("/postdata/"+delPID+"/"+localStorage.getItem('user_id')).then((resUType) => {
                clickWillDismiss(true);
            })
            .catch((error) => {
                alert("Error Delete!");
            })
        })
        .catch((error) => {
          alert("Token not Get!");
        })
    }

    return (
        <IonPage>
  
           <HeaderMain pageTitle={t('POST-Edit-Title')}  logoHide="hide-logo" onBack={doNothing} />
  
           <IonContent className="dashboard-wrapper lease-info-wrapper" ref={contentRef} scrollEvents={true} fullscreen>
                <IonLoading
                    isOpen={showLoading}
                    onDidDismiss={() => setShowLoading(false)}
                    message={'Loading...'}
                />
    
                <IonAlert
                    isOpen={showAlert}
                    onDidDismiss={() => setShowAlert(false)}
                    onWillDismiss={() => clickWillDismiss(true)}
                    cssClass='orange-alert'
                    mode='md'
                    header={headAlert}
                    message={msgAlert}
                    buttons={['Close']}
                />

                <IonAlert
                    isOpen={bonusAlert}
                    onDidDismiss={() => setBonusAlert(false)}
                    cssClass='orange-alert'
                    header={headBonusAlert}
                    message={msgBonusAlert}
                    buttons={[{
                                text: 'Yes',
                                cssClass: 'btn-secondary',
                                handler: () => {
                                    //onClickBonusPointBuy();
                                    history.push(`${Routes.createPostPay}`);

                                    //history.push({
                                    //    pathname: `${Routes.chat}`,
                                    //    state: { path: `${Routes.chat}`, chat_id: retDataArr.InsertID}
                                    //});
                                }
                            },
                            {
                                text: 'No',
                                cssClass: 'btn-primary',//btn-outline
                                handler: () => {
                                    clickWillDismiss(true);
                                }
                            }]}
                />

                <IonAlert
                    isOpen={!!photoToDelete}
                    onDidDismiss={() => setPhotoToDelete(undefined)}
                    cssClass='red-alert'
                    mode='md'
                    header={t('POST-Remove-Img-Alert-Head')}
                    message={t('POST-Remove-Img-Alert-Msg')}
                    buttons={[{
                                text: 'Remove',
                                cssClass: 'btn-secondary',
                                //role: 'destructive',
                                ///icon: trash,
                                handler: () => {
                                if (photoToDelete) {
                                    deletePhoto(photoToDelete);
                                    setPhotoToDelete(undefined);
                                }
                                }
                            }, {
                                text: 'Cancel',
                                cssClass: 'btn-primary',//btn-outline
                                //icon: close,
                                //role: 'cancel'
                            }]}
                />

                <IonAlert
                    isOpen={PostDelete}
                    onDidDismiss={() => setPostDelete(false)}
                    cssClass='red-alert'
                    mode='md'
                    header={t('POST-Delete-Alert-Head')}
                    message={t('POST-Delete-Alert-Msg')}
                    buttons={[{
                                text: 'Remove',
                                cssClass: 'btn-secondary',
                                handler: () => {
                                    onClickPostDelete(DelPostID);
                                }
                            }, {
                                text: 'Cancel',
                                cssClass: 'btn-primary',
                            }]}
                />


                <IonGrid className="dashboard-main-grid">
                    <IonRow className="dashboard-main-row">
                        
                        {/* sidebar start  */}
                        <DashboardSidebar path={locPath} />
                        {/* sidebar end  */}


                        {/* dashboar content start */}
                        <IonCol className="dashboard-content" size="12" sizeMd="12" sizeLg="6" sizeXl="8">
                            <FormProvider {...methods}>
                            <form onSubmit={handleSubmit(onSubmit)}>
                                <div className="dashboard-content-inner create-posts-wrapper">
                                    <IonCard>
                                        <IonCardHeader>
                                            <IonCardTitle>{t('POST-Info')}</IonCardTitle>
                                        </IonCardHeader>

                                        <IonCardContent>
                                        <IonRow>
                                            <IonCol size="12" sizeMd="6" className="form-field">
                                                <IonLabel className="form-lable">{t('POST-Title')}*</IonLabel>

                                                <IonInput
                                                    mode="md"
                                                    type="text"
                                                    className="hide-input"   
                                                    {...register('post_id')}
                                                />

                                                <IonInput
                                                    mode="md"
                                                    type="text"
                                                    className={`form-control ${errors.post_title ? 'is-invalid' : ''}`}   
                                                    {...register('post_title', {
                                                        required: t('POST-Title-msg')
                                                    })}
                                                />
                                                <ErrorMessage
                                                    errors={errors}
                                                    name="post_title"
                                                    as={<div className="error-message" style={{ color: 'red' }} />}
                                                />
                                            </IonCol>
                                            <IonCol size="12" sizeMd="6" className="form-field">
                                                <IonLabel className="form-lable">{t('POST-Description')}*</IonLabel>
                                                <IonTextarea
                                                    mode="md"
                                                    className={`form-control ${errors.post_message ? 'is-invalid' : ''}`}   
                                                    {...register('post_message', {
                                                        required: t('POST-Description-msg')
                                                    })}
                                                />
                                                <ErrorMessage
                                                    errors={errors}
                                                    name="post_message"
                                                    as={<div className="error-message" style={{ color: 'red' }} />}
                                                />
                                            </IonCol>
                                            <IonCol size="12" sizeMd="6" className="form-field">
                                                <IonLabel className="form-lable">{t('POST-Price')}</IonLabel>
                                                <IonInput className="width-20" mode="md" type="number" {...register('post_price')} />
                                                <ErrorMessage
                                                    errors={errors}
                                                    name="post_price"
                                                    as={<div className="error-message" style={{ color: 'red' }} />}
                                                />
                                            </IonCol>

                                            {UTypeID == '1' ? '' : 
                                                <IonCol size="12" sizeMd="6" className="form-field">
                                                    <IonLabel className="form-lable">{t('POST-Pinned')}</IonLabel>
                                                    <IonCheckbox color="primary" checked={PinPost} onIonChange={e => setPinPost(e.detail.checked)} />
                                                </IonCol>
                                            }

                                            <IonCol size="12" sizeMd="6">
                                                <div className="label-with-tooltip">
                                                    <IonLabel className="form-lable">{t('POST-Date')}</IonLabel>
                                                </div>
                                                <div className="date-picker">
                                                    <IonDatetime 
                                                        displayFormat="MMM DD, YYYY"
                                                        placeholder="Select Date" 
                                                        value={selectedPostDate} 
                                                        className={`form-control ${errors.postDate ? 'is-invalid' : ''}`}   
                                                        onIonChange={e => setSelectedPostDate(e.detail.value!)}
                                                        mode="md"  
                                                        {...register('postDate', {
                                                            required: t('POST-Date-msg')
                                                        })}
                                                    >
                                                    </IonDatetime>
                                                    <IonIcon icon="assets/images/calendar-icon.svg" />
                                                </div>
                                                <ErrorMessage
                                                    errors={errors}
                                                    name="postDate"
                                                    as={<div className="error-message" style={{ color: 'red' }} />}
                                                />
                                            </IonCol>



                                            <IonCol size="12" sizeMd="6" className="form-field">
                                                <IonLabel className="form-lable">{t('POST-Attachments')}</IonLabel>
                                            </IonCol>
                                            <IonCol size="12" sizeMd="6" className="uploaded-images-list">   
                                                {photos.map((photo, index) => (
                                                    <div className="uploaded-file">
                                                        <p className="uploaded-file-name read-only">{index + 1}. {photo.filepath}</p>

                                                        {/* <img src={photo.webviewPath} />
                                                        <p>{photo.fileurl}</p> */}
                                                        
                                                        <IonButton fill="clear" onClick={() => onClickPhotoData(photo)} >
                                                            <IonIcon icon={close} />
                                                        </IonButton>
                                                    </div>
                                                ))}
                                            </IonCol>
                                            
                                            {(UTypeID == '1' && photos.length == TenantsMaxImg) || (UTypeID == '2' && photos.length == ManagementMaxImg) ? '' : 
                                                <IonCol size="12" sizeMd="6" className="upload-photo-block ion-text-center">
                                                    <IonButton className="take-photo-btn" fill="solid" shape="round" onClick={() => takePhoto()}>
                                                        <IonIcon icon="assets/images/add-photo-white.svg" />
                                                    </IonButton>
                                                </IonCol>
                                            }
                                        </IonRow>
                                        </IonCardContent>
                                    </IonCard>

                                    <IonCard>
                                        <IonCardHeader>
                                            <IonCardTitle>{t('POST-Settings')}</IonCardTitle>
                                        </IonCardHeader>

                                        <IonCardContent>
                                        <IonRow>
                                            <IonCol size="12" sizeMd="6">
                                                <div className="label-with-tooltip">
                                                    <IonLabel className="form-lable">{t('POST-Activation-Date')}</IonLabel>
                                                    <div className="tooltip" title={t('POST-Activation-Date-tooltip')}>!</div>
                                                </div>
                                                <div className="date-picker">
                                                    <IonDatetime 
                                                        displayFormat="MMM DD, YYYY"
                                                        placeholder="Select Date" 
                                                        value={selectedActivationDate} 
                                                        className={`form-control ${errors.post_activation_date ? 'is-invalid' : ''}`}   
                                                        onIonChange={e => setSelectedActivationDate(e.detail.value!)}
                                                        mode="md"  
                                                        {...register('post_activation_date', {
                                                            required: t('POST-Activation-Date-msg')
                                                        })}
                                                    >
                                                    </IonDatetime>
                                                    <IonIcon icon="assets/images/calendar-icon.svg" />
                                                </div>
                                                <ErrorMessage
                                                    errors={errors}
                                                    name="post_activation_date"
                                                    as={<div className="error-message" style={{ color: 'red' }} />}
                                                />
                                            </IonCol>

                                            <IonCol size="12" sizeMd="6">
                                                <div className="label-with-tooltip">
                                                    <IonLabel className="form-lable">{t('POST-Expiration-Date')}</IonLabel>
                                                    <div className="tooltip" title={t('POST-Expiration-Date-tooltip')}>!</div>
                                                </div>
                                                <div className="date-picker">
                                                    <IonDatetime 
                                                        displayFormat="MMM DD, YYYY"
                                                        placeholder="Select Date"
                                                        className={`form-control ${errors.post_expiration_date ? 'is-invalid' : ''}`}    
                                                        value={selectedExpirationDate} 
                                                        onIonChange={e => setSelectedExpirationDate(e.detail.value!)}
                                                        mode="md"  
                                                        {...register('post_expiration_date', {
                                                            required: t('POST-Expiration-Date-msg')
                                                        })}
                                                    >
                                                    </IonDatetime>
                                                    <IonIcon icon="assets/images/calendar-icon.svg" />
                                                </div>
                                                <ErrorMessage
                                                    errors={errors}
                                                    name="post_expiration_date"
                                                    as={<div className="error-message" style={{ color: 'red' }} />}
                                                />
                                            </IonCol>
                                        </IonRow>
                                        </IonCardContent>
                                    </IonCard>

                                    <IonRow className="ion-justify-content-center">
                                        <IonCol className="ion-text-center">
                                            <IonItem className="checkbox-wrap">
                                                <IonLabel className="form-lable">{t('POST-Repost')}</IonLabel>
                                                <IonCheckbox 
                                                    mode="md" 
                                                    slot="start" 
                                                    value={repostnew}
                                                    onIonChange={(e) => ClickRepost(e.detail.checked)}
                                                    {...register('repost_as_new')} 
                                                />
                                            </IonItem>
                                        </IonCol>
                                    </IonRow>

                                    <IonRow className="ion-justify-content-center">
                                        <IonCol className="ion-text-center">
                                            <IonButton type="submit"  className="secondary-button" fill="outline" shape="round">{t('General-Submit')}</IonButton>
                                            <IonButton className="exit-file-btn" routerLink={Routes.managePosts} fill="solid" shape="round">{t('General-Exit')}</IonButton>
                                        </IonCol>
                                    </IonRow>
                                    <IonRow className="ion-justify-content-center">
                                        <IonCol className="ion-text-center">
                                            <IonButton className="exit-file-btn" fill="solid" onClick={() => setPostDelete(true)} shape="round">{t('POST-Delete')}</IonButton>
                                        </IonCol>
                                    </IonRow>
                                    
                                </div>
                            </form>
                            </FormProvider>

                        </IonCol>
                        {/* dashboar content end */}


                        {/* dashboar posts start */}
                        <IonCol  className="dashboard-posts" size="12" sizeMd="12" sizeLg="3" sizeXl="2">
                            
                        </IonCol>
                        {/* dashboar post end */}
    
                    </IonRow>
                </IonGrid>

                <IonButton className="scroll-to-bottom-btn" onClick={scrollToBottom} fill="clear">
                    <IonIcon icon="assets/images/double-arrow-down.svg" />
                </IonButton>
              
                <Footer />

            </IonContent>

            <FooterMobile />
      </IonPage>
    );
};

export default EditPosts;