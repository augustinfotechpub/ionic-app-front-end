import React, { useRef, useState, useEffect } from "react";
import { useTranslation } from "react-i18next";

import { FormProvider, useForm, Controller, useFieldArray } from "react-hook-form";
import { ErrorMessage } from '@hookform/error-message';

import { useHistory } from "react-router-dom";
import { IonContent, IonPage, IonLoading, IonLabel, IonInput, IonButton, IonGrid, IonRow, IonCol, 
         IonItem, IonList, IonRadioGroup, IonListHeader, IonRadio, IonAlert, 
         useIonViewWillEnter,
         useIonViewDidLeave, 
       } from '@ionic/react';

import Header from '../components/Header';

import { globalConst } from "../constants";

import '../assets/css/Custom.css';
import '../assets/css/Responsive.css';
import { Routes } from "../App";

import axios from "axios";
axios.defaults.baseURL = globalConst.baseURL;
axios.defaults.headers.post['Content-Type'] = globalConst.ContentType;
axios.defaults.headers.post['Access-Control-Allow-Origin'] = globalConst.AllowOrigin;
axios.defaults.headers.post['Accept'] = globalConst.Accept;
axios.defaults.timeout = globalConst.timeout;

const selUtype = "1"
const Signup: React.FC = () => {

  const { t } = useTranslation();
  const [showLoading, setShowLoading] = useState(true);
  const [showAlert, setShowAlert] = useState(false);

  const [UTypeData,setUTypeData]=useState([{"utype_id": '', "utype_name": "", "utype_desc": ""}]);
  const [userTypeSelID, setUserType] = useState<string>();
  
  const [state] = useState({ data: { checked: "2" } });
  const methods = useForm({ mode: "onSubmit"});
  const { register, trigger, reset, watch , handleSubmit, control, setValue, getValues, formState: { errors } } = methods;

  let history = useHistory();

  useIonViewWillEnter(() => {
    if (localStorage.getItem('loginState')=='1') {
      history.push(`${Routes.UserDashboard}`);
    } 
    loadSignupData();
  });

  async function loadSignupData() {
    const api = axios.create();
    api.get("/auth/token/refresh").then((response) => {

      axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
      const api = axios.create();

      api.get("/auth/allusertype").then((resUType) => {
        const retData = JSON.parse(JSON.stringify(resUType)).data;
        setUTypeData(retData);
        setShowLoading(false);
        //console.log(resUType.data)
      })
      .catch((error) => {
        alert("Error Get in User Type!");
      })
      
    })
    .catch((error) => {
      alert("Token not Get!");
    })
  }

  const inputChangeHandler = (event: CustomEvent) => {
    setUserType(event?.detail?.value); 
  };
  
  const onSubmit = (data: any) => {
    //console.log(data);
    //console.log(`${userTypeSelID}`);
    if (`${userTypeSelID}` === "undefined") {
      setShowAlert(true);
    } else {
        history.push({
            pathname: `${Routes.SignupData}`,
            state: { path: `${Routes.SignupData}`, userTypeSelID: `${userTypeSelID}`}
        });
    }
  };

  const doNothing = () => {
    history.goBack();
  }

  return (
    <IonPage>
        <Header class="with-back-arrow" onBack={doNothing} />
        <IonContent fullscreen>
          <FormProvider {...methods}>
            <form onSubmit={handleSubmit(onSubmit)} autoComplete="off">
              <IonLoading
                isOpen={showLoading}
                onDidDismiss={() => setShowLoading(false)}
                message={'Loading...'}
              />
              <IonGrid>
                  <IonRow className="signup-form login-form-row ">
                      <IonCol size="10" sizeMd="6" sizeLg="6">
                          <IonList>
                            <Controller
                              control={control}   
                              name="utype_id"
                              rules={{ required: t('Signup-error-msg1') }}
                              render={({ field: { onChange, value } }) => (
                                <IonRadioGroup 
                                  value={value}
                                  onIonChange={(e:any) => { 
                                    onChange(e); 
                                    inputChangeHandler(e);
                                  }}
                                >
                                  <IonListHeader>
                                    <IonLabel>
                                      {t('Signup-Text')}
                                    </IonLabel>
                                  </IonListHeader>

                                  {UTypeData.map((item, index) => (
                                    <IonItem key={item.utype_id} className="user-select-item">
                                      <IonLabel>
                                        {item.utype_name}
                                        <p>{item.utype_desc}</p>
                                      </IonLabel>
                                      <IonRadio mode="md" value={item.utype_id} />
                                    </IonItem>
                                  ))}

                                </IonRadioGroup>
                              )}
                            />
                          </IonList>
                      </IonCol>

                      <ErrorMessage
                          errors={errors}
                          name="utype_id"
                          as={<div className="error-message" style={{ color: 'red' }} />}
                      />

                      <IonCol size="8" sizeMd="4" sizeLg="4">
                        <IonButton className="secondary-button" type="submit" expand="block" shape="round" fill="outline" >
                            {t('Signup-Continue')}
                        </IonButton>
                      </IonCol>

                      <IonAlert
                        isOpen={showAlert}
                        onDidDismiss={() => setShowAlert(false)}
                        cssClass='red-alert'
                        mode='md'
                        header={t('Signup-error')}
                        message={t('Signup-error-msg2')}
                        buttons={[{
                                    text: 'Ok',
                                    cssClass: 'btn-primary',//btn-outline
                                }]}
                      />
                  </IonRow>
              </IonGrid>
            </form>
          </FormProvider>
        </IonContent>
    </IonPage>
  );
};

export default Signup;