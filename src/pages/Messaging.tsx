import React, {useRef, useState, useEffect, useCallback} from "react";
import { useHistory, useLocation } from 'react-router-dom';
import { useTranslation } from "react-i18next";

import { 
    IonContent, 
    IonPage, 
    IonGrid, 
    IonRow, 
    IonCol, 
    IonSegment,
    IonSegmentButton,
    IonSlides,
    IonSlide,
    IonButton,
    IonIcon,
    createAnimation,
    IonModal,
    useIonViewWillEnter
} from '@ionic/react';

import { addSharp, close } from "ionicons/icons";

import HeaderMain from '../components/Header-main';
import Footer from '../components/Footer';
import FooterMobile from '../components/Footer-mobile';
import DashboardSidebar from '../components/Dahsboard-sidebar';
import MessagingAllTab from "../components/MessagingAllTab";
import MessagingUserTab from "../components/MessagingUserTab";
import MessagingGroupTab from "../components/MessagingGroupTab";
import StartConversationPopup from "../components/StartConversationPopup";

import '../assets/css/Custom.css';
import '../assets/css/Responsive.css';
import { Routes } from '../App';
  
import { globalConst } from "../constants";

import axios from "axios";
axios.defaults.baseURL = globalConst.baseURL;
axios.defaults.headers.post['Content-Type'] = globalConst.ContentType;
axios.defaults.headers.post['Access-Control-Allow-Origin'] = globalConst.AllowOrigin;
axios.defaults.headers.post['Accept'] = globalConst.Accept;
axios.defaults.timeout = globalConst.timeout;

const Messaging: React.FC<{ path: string }> = ({path}) => {

    const { t } = useTranslation();
    // a ref variable to handle the current slider
    // const slider = useRef<HTMLIonSlidesElement>(null);
    const slider = useRef<any>(null);
    // a state value to bind segment value
    const [value, setValue] = useState("0");
    // useEffect(() => {
    //     slider.current.lockSwipes(true);
    // });
    let history = useHistory();

    const [LoadAllMsgData, setLoadAllMsgData] = useState<string[]>([]);
    const [LoadUserMsgData, setLoadUserMsgData] = useState<string[]>([]);
    const [LoadGroupsMsgData, setLoadGroupsMsgData] = useState<string[]>([]);

    const [UserTypeData, setUserTypeData] = useState<any>({'id': '', 'name': '', 'lease_code': ''});

    const slideOpts = {
      initialSlide: 0,
      speed: 400,
      loop: false,
      pagination: {
        el: null
      },
    
    };

    useEffect(() => {
        if ((localStorage.getItem('user_type_id')!==null)) {
            setUserTypeData({'id': localStorage.getItem('user_type_id'), 'name': localStorage.getItem('user_type_name'), 'lease_code': localStorage.getItem('user_code')});
        }
        
    }, []);

    useIonViewWillEnter(() => {
        if (localStorage.getItem('loginState')!='1') {
            history.push(`${Routes.Home}`);
        } else {
            loadMsgData();
        }
    });

    async function loadMsgData() {
        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {

            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            api.get("/messaging/alldatabyuser/"+localStorage.getItem('user_id')+"/"+localStorage.getItem('user_type_id')).then((returnData) => {
                const retData = JSON.parse(JSON.stringify(returnData)).data;

                //console.log(retData.retMsgData);
                setLoadAllMsgData(retData.retAllMsgData);
                setLoadUserMsgData(retData.retUserMsgData);
                setLoadGroupsMsgData(retData.retGroupMsgData);

            })
            .catch((error) => {
                alert("Error Get Property Data!");
            })
        })
        .catch((error) => {
          alert("Token not Get!");
        })
    }

    // a function to handle the segment changes
    const handleSegmentChange = async (e: any) => {
        await slider.current.lockSwipes(false);
        setValue(e.detail.value);
        slider.current!.slideTo(e.detail.value);
        await slider.current.lockSwipes(true);
    };

    // a function to handle the slider changes
    const handleSlideChange = async (event: any) => {
      let index: number = 0;
      await event.target.getActiveIndex().then((value: any) => (index=value));
      setValue(''+index)
    }

    const [showMessageModal, setShowMessageModal] = useState(false);

    const enterAnimation = (baseEl: any) => {
        const backdropAnimation = createAnimation()
          .addElement(baseEl.querySelector('ion-backdrop')!)
          .fromTo('opacity', '0.01', 'var(--backdrop-opacity)');
    
        const wrapperAnimation = createAnimation()
          .addElement(baseEl.querySelector('.modal-wrapper')!)
          .keyframes([
            { offset: 0, opacity: '0', transform: 'scale(0)' },
            { offset: 1, opacity: '0.99', transform: 'scale(1)' }
          ]);
    
        return createAnimation()
          .addElement(baseEl)
          .easing('ease-out')
          .duration(500)
          .addAnimation([backdropAnimation, wrapperAnimation]);
    }
    
    const leaveAnimation = (baseEl: any) => {
        return enterAnimation(baseEl).direction('reverse');
    }

    const doNothing = () => {
        history.goBack();
    }

    const MsgModalClose = useCallback(() => {
        setShowMessageModal(false)
    }, []);

    return (
        <IonPage>
  
           <HeaderMain pageTitle={t('Msg-Title')} logoHide="hide-logo" onBack={doNothing} />
  
           <IonContent className="dashboard-wrapper manage-tenants-wrapper manage-rents-wrapper" fullscreen>
                <IonGrid className="dashboard-main-grid">
                    <IonRow className="dashboard-main-row">
                        
                        {/* sidebar start  */}
                        <DashboardSidebar path={path} />
                        {/* sidebar end  */}

                        {/* dashboar content start */}
                        <IonCol className="dashboard-content messaging-content" size="12" sizeMd="12" sizeLg="6" sizeXl="8">
                            <div className="dashboard-content-inner">
                                <IonSegment scrollable  mode="md" value={value} onIonChange={(e) => handleSegmentChange(e)} >
                                    <IonSegmentButton value="0">
                                        {UserTypeData.id == '1' ? (
                                            t('Msg-All')
                                        ) : (
                                            t('Msg-Tenants')
                                        )}
                                    </IonSegmentButton>
                                    <IonSegmentButton value="1">
                                        {UserTypeData.id == '1' ? (
                                            t('Msg-Management')
                                        ) : (
                                            t('Msg-Staff')
                                        )}
                                    </IonSegmentButton>
                                    <IonSegmentButton value="2">
                                        {t('Msg-Group')}
                                    </IonSegmentButton>
                                </IonSegment>

                                <IonSlides pager={true} options={slideOpts} onIonSlideDidChange={(e) => handleSlideChange(e)} ref={slider}>
                                    <IonSlide>
                                        <MessagingAllTab LoadMsgData={LoadAllMsgData} />
                                    </IonSlide>
                                    {/*-- Package Segment --*/}
                                    <IonSlide>
                                        <MessagingUserTab LoadMsgData={LoadUserMsgData} />
                                    </IonSlide>
                                    <IonSlide>
                                        <MessagingGroupTab LoadMsgData={LoadGroupsMsgData} />
                                    </IonSlide>
                                </IonSlides>

                                <IonButton 
                                    className="property-change-button" 
                                    fill="clear"
                                    onClick={() => setShowMessageModal(true)}
                                >
                                        <IonIcon icon={addSharp}  />
                                </IonButton>
                            </div>

                            <IonModal isOpen={showMessageModal} cssClass="rent-pay-receipt-popup start-conversation-popup">
                                <StartConversationPopup parentCallback={MsgModalClose} />
                            </IonModal>
                        </IonCol>
                        {/* dashboar content end */}

                        {/* dashboar posts start */}
                        <IonCol  className="dashboard-posts" size="12" sizeMd="12" sizeLg="3" sizeXl="2">
                            
                        </IonCol>
                        {/* dashboar post end */}
    
                    </IonRow>
                </IonGrid>
              
                <Footer />

            </IonContent>

            <FooterMobile />
      </IonPage>
    );
  };
  
  export default Messaging;