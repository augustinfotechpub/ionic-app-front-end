import React, {useEffect, useRef, useState} from "react";
import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";

import { 
    IonContent, 
    IonPage,
    IonGrid,
    IonRow,
    IonCol, 
    IonButton, 
    IonIcon,
    IonLabel,
    IonInput,
    IonCard,
    IonCardHeader,
    IonCardTitle,
    IonCardContent,
    IonAlert,
    IonToggle,
    useIonViewWillEnter
} from '@ionic/react';

import { FormProvider, useForm } from "react-hook-form";
import { ErrorMessage } from '@hookform/error-message';

import HeaderMain from '../components/Header-main';
import Footer from '../components/Footer';
import FooterMobile from '../components/Footer-mobile';
import DashboardSidebar from '../components/Dahsboard-sidebar';

import '@ionic/react/css/flex-utils.css';
import '../assets/css/Custom.css';
import '../assets/css/Responsive.css';
import { Routes } from "../App";

import { globalConst } from "../constants";
import axios from "axios";
axios.defaults.baseURL = globalConst.baseURL;
axios.defaults.headers.post['Content-Type'] = globalConst.ContentType;
axios.defaults.headers.post['Access-Control-Allow-Origin'] = globalConst.AllowOrigin;
axios.defaults.headers.post['Accept'] = globalConst.Accept;
axios.defaults.timeout = globalConst.timeout;

const CreateAccesscode: React.FC<{ path: string }> = ({path}) => {

    const { t } = useTranslation();
    const [PropertyToggle, setPropertyToggle] = useState(true);
    const [RentsToggle, setRentsToggle] = useState(true);
    const [PostsToggle, setPostsToggle] = useState(true);

    let history = useHistory();

    const [showAlert, setShowAlert] = useState(false);
    const [msgAlert, setMsgAlert] = useState('');
    const [headAlert, setHeadAlert] = useState('');

    const [isSubmitSuccessful, setisSubmitSuccessful] = useState(false);

    const methods = useForm();
    const { register, reset , handleSubmit, setValue, formState: { errors } } = methods;

    const contentRef = useRef<HTMLIonContentElement | null>(null);

    const scrollToBottom= () => {
        contentRef.current && contentRef.current.scrollToBottom(500);
    };

    useIonViewWillEnter(() => {
        if (localStorage.getItem('loginState')!='1') {
            history.push(`${Routes.Home}`);
        }

        if ((localStorage.getItem('user_type_id')!='2')) {
            history.push(`${Routes.UserDashboard}`);
        }
    });

    useEffect(() => {
        setisSubmitSuccessful(false);

        setValue('access_code', '');
        setPropertyToggle(true);
        setRentsToggle(true);
        setPostsToggle(true);

    }, [isSubmitSuccessful])

    const onSubmit = (data: any) => {
        //console.log(data);
        
        const api = axios.create();
        api.get("/auth/token/refresh").then(res => res)
        .then((response) => {
            
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            var manage_propertys = '0'
            if (PropertyToggle) {
                manage_propertys = '1'
            }

            var manage_rents = '0'
            if (RentsToggle) {
                manage_rents = '1'
            }

            var manage_posts = '0'
            if (PostsToggle) {
                manage_posts = '1'
            }

            const addData = {
                "user_id" : localStorage.getItem('user_id'),
                "access_code" : data.access_code,
                "assign_flag" : '0',
                "assign_user_id" : '0',
                "active_flag" : '1',
                "feature_access" : {"manage_propertys": manage_propertys, "manage_rents": manage_rents, "manage_posts": manage_posts},
            };
  
            //console.log(addData);
            api.post("/auth/accesscode/", addData).then((retData) => {
                const retDataArr = JSON.parse(JSON.stringify(retData)).data;

                if (retDataArr.InsertFlag){
                    setMsgAlert(t('AccessCode-Create-Msg'));
                    setHeadAlert(t('AccessCode-Create-Head'));
                    setShowAlert(true);
                } else {
                    setMsgAlert(retDataArr.msg);
                    setHeadAlert(t('AccessCode-Create-Head'));
                    setShowAlert(true);
                }

                setisSubmitSuccessful(true);

            }).catch((error) => {
                alert("Error found in put Data");
                console.log(error);
            });
        })
        .catch((error) => {
          alert("Token not Get!");
        })
  
    };
  
    const doNothing = () => {
        history.goBack();
    }

    const clickWillDismiss = (FlagDismiss) => {
        if (FlagDismiss) {
            if (localStorage.getItem('user_type_id')!='2') {
                history.push(`${Routes.UserDashboard}`);
            } else {
                history.push({pathname: `${Routes.manageAccesscode}`});
            }
        }
    };

    return (
        <IonPage>
  
           <HeaderMain pageTitle={t('AccessCode-New-Title')}  logoHide="hide-logo" onBack={doNothing} />
  
           <IonContent className="dashboard-wrapper lease-info-wrapper" ref={contentRef} scrollEvents={true} fullscreen>
                <IonAlert
                    isOpen={showAlert}
                    onDidDismiss={() => setShowAlert(false)}
                    onWillDismiss={() => clickWillDismiss(true)}
                    cssClass='orange-alert'
                    header={headAlert}
                    message={msgAlert}
                    buttons={[{text: 'Close', cssClass: 'btn-primary'}]}
                />

                <IonGrid className="dashboard-main-grid">
                    <IonRow className="dashboard-main-row">
                        
                        {/* sidebar start  */}
                        <DashboardSidebar path={path} />
                        {/* sidebar end  */}

                        {/* dashboar content start */}
                        <IonCol className="dashboard-content" size="12" sizeMd="12" sizeLg="6" sizeXl="8">
                            <FormProvider {...methods}>
                            <form onSubmit={handleSubmit(onSubmit)}>
                                <div className="dashboard-content-inner create-posts-wrapper">
                                    <IonCard>
                                        <IonCardHeader>
                                            <IonCardTitle>{t('AccessCode-Info')}</IonCardTitle>
                                        </IonCardHeader>

                                        <IonCardContent>
                                        <IonRow>
                                            <IonCol size="12" sizeMd="6" className="form-field">
                                                <IonLabel className="form-lable">{t('AccessCode-Code')}*</IonLabel>
                                                <IonInput
                                                    mode="md"
                                                    type="text"
                                                    className={`form-control ${errors.access_code ? 'is-invalid' : ''}`}   
                                                    {...register('access_code', {
                                                        required: t('AccessCode-Code-error'), minLength: { value: 4, message: "Must be 4 chars long" }, maxLength: { value: 12, message: "allow only 12 chars" }
                                                    })}
                                                />
                                                <ErrorMessage
                                                    errors={errors}
                                                    name="access_code"
                                                    as={<div className="error-message" style={{ color: 'red' }} />}
                                                />
                                            </IonCol>
                                        </IonRow>
                                        </IonCardContent>
                                    </IonCard>

                                    <IonCard>
                                        <IonCardHeader>
                                            <IonCardTitle>{t('AccessCode-Feature-Access')}</IonCardTitle>
                                        </IonCardHeader>

                                        <IonCardContent>
                                        <IonRow>
                                            <IonCol size="12" sizeMd="6">
                                                <div className="form-field toggle-button-field">
                                                    <IonLabel className="form-lable">{t('AccessCode-Manage-Property')}</IonLabel>
                                                    <IonToggle mode="ios" checked={PropertyToggle} onIonChange={(e) => setPropertyToggle(e.detail.checked)} />
                                                </div>
                                                <div className="form-field toggle-button-field">
                                                    <IonLabel className="form-lable">{t('AccessCode-Manage-Rents')}</IonLabel>
                                                    <IonToggle mode="ios" checked={RentsToggle} onIonChange={(e) => setRentsToggle(e.detail.checked)} />
                                                </div>
                                                <div className="form-field toggle-button-field">
                                                    <IonLabel className="form-lable">{t('AccessCode-Manage-Posts')}</IonLabel>
                                                    <IonToggle mode="ios" checked={PostsToggle} onIonChange={(e) => setPostsToggle(e.detail.checked)} />
                                                </div>
                                            </IonCol>
                                        </IonRow>
                                        </IonCardContent>
                                    </IonCard>

                                    <IonRow className="ion-justify-content-center">
                                        <IonCol className="ion-text-center">
                                            <IonButton type="submit"  className="secondary-button" fill="outline" shape="round">{t('General-Submit')}</IonButton>
                                            <IonButton className="exit-file-btn" routerLink={Routes.manageAccesscode} fill="solid" shape="round">{t('General-Exit')}</IonButton>
                                        </IonCol>
                                    </IonRow>
                                </div>
                            </form>
                            </FormProvider>
                        </IonCol>
                        {/* dashboar content end */}

                        {/* dashboar posts start */}
                        <IonCol  className="dashboard-posts" size="12" sizeMd="12" sizeLg="3" sizeXl="2">
                            
                        </IonCol>
                        {/* dashboar post end */}
    
                    </IonRow>
                </IonGrid>

                <IonButton className="scroll-to-bottom-btn" onClick={scrollToBottom} fill="clear">
                    <IonIcon icon="assets/images/double-arrow-down.svg" />
                </IonButton>
              
                <Footer />
            </IonContent>
            <FooterMobile />
      </IonPage>
    );
};

export default CreateAccesscode;