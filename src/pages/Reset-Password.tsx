import React, {useRef, useState, useEffect} from "react";
import { useTranslation } from "react-i18next";

import { 
  IonContent, 
  IonPage, 
  IonLabel, 
  IonInput, 
  IonButton, 
  IonGrid, 
  IonRow, 
  IonCol,
  IonAlert,
  useIonViewWillEnter
} from '@ionic/react';

import { FormProvider, useForm } from "react-hook-form";
import { ErrorMessage } from '@hookform/error-message';
import { RouteComponentProps, Switch, useLocation } from "react-router";

import Header from '../components/Header';

import '../assets/css/Custom.css';
import '../assets/css/Responsive.css';
import { Routes } from "../App";

import { globalConst } from "../constants";
import axios from "axios";
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';
import { Console } from "console";
axios.defaults.baseURL = globalConst.baseURL;
axios.defaults.headers.post['Content-Type'] = globalConst.ContentType;
axios.defaults.headers.post['Access-Control-Allow-Origin'] = globalConst.AllowOrigin;
axios.defaults.headers.post['Accept'] = globalConst.Accept;
axios.defaults.timeout = globalConst.timeout;

interface PageProps extends RouteComponentProps {path: string; history: any; match: any; location: any;}
const ResetPassword: React.FC<PageProps> = ({ path, history, match, location }) => {

   const { t } = useTranslation();
   const methods = useForm();
   const { register, setValue, trigger, handleSubmit, getValues, formState: { errors } } = methods;

   const [ResetCode, setResetCode] = useState('');
   const [UpdatePwdAlert, setUpdatePwdAlert] = useState(false);
   const [UserNotFoundAlert, setUserNotFoundAlert] = useState(false);

   useIonViewWillEnter(() => {
      if ((localStorage.getItem('loginState')=='1')) {
         history.push(`${Routes.UserDashboard}`);
      } 
   });

  useEffect(() => {
      setResetCode(match.params.code);
      setValue('user_ResetCode', match.params.code)
   }, [match.params.code]);

   const onSubmit = (data: any) => {
      console.log(data);
      
      const api = axios.create();
      api.get("/auth/token/refresh").then((response) => {
   
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            api.put("/auth/updateNewPwd", data).then((resData) => {
               const resReturnData = JSON.parse(JSON.stringify(resData)).data;
               console.log(resReturnData);

               setUpdatePwdAlert(true);
            })
            .catch((error) => {
               setUserNotFoundAlert(true);
            })
      })
      .catch((error) => {
         alert("Token not Get!");
      })
   };

   const doNothing = () => {
      history.goBack();
   }

   return (
      <IonPage>

         <Header class="with-back-arrow"  onBack={doNothing}/>

         <IonContent fullscreen>
            <IonGrid>
               <IonRow className="login-form-row">
                  <IonCol size="10" sizeMd="6" sizeLg="4">
                     <FormProvider {...methods}>
                        <form onSubmit={handleSubmit(onSubmit)}>
                           <h2>{t('Reset-Password-Head')}</h2>
                           <p>{t('Reset-Password-Head-Text')}</p>

                           <IonGrid>
                              <IonRow>
                                 <IonCol size="12" className="ion-margin-top">
                                    <IonLabel className="form-lable">{t('Reset-Password-Password')}</IonLabel>
                                    <IonInput
                                       mode="md" 
                                       type="text" 
                                       class="hide-input"
                                       value={ResetCode}
                                       {...register('user_ResetCode')}
                                    />
                                    <IonInput
                                       mode="md" 
                                       type="password" 
                                       {...register('password1', {
                                          required: t('Login-Error-Password')
                                       })}
                                    />
                                    <ErrorMessage
                                       errors={errors}
                                       name="password1"
                                       as={<div className="error-message" style={{ color: 'red' }} />}
                                    />
                                 </IonCol>

                                 <IonCol size="12" className="ion-margin-top">
                                    <IonLabel className="form-lable">{t('Reset-Password-Confirm-Password')}:</IonLabel>
                                    {/* <IonInput type="password" placeholder="Password" /> */}
                                    <IonInput 
                                       mode="md"
                                       type="password" 
                                       {...register('password2', {
                                          // required: true,
                                          validate: {
                                             noMatch: (value: string) => {
                                                return value !== getValues("password1")
                                                   ? "Passwords do not match"
                                                   : undefined;
                                             },
                                          },
                                       })}
                                    />
                                    <ErrorMessage
                                       errors={errors}
                                       name="password2"
                                       as={<div className="error-message" style={{ color: 'red' }} />}
                                    />
                                 </IonCol>

                                 <IonCol size="12" className="ion-margin-top">
                                    <IonButton className="secondary-button" type="submit" expand="block" shape="round" fill="outline" >
                                       {t('Reset-Password')}
                                    </IonButton>
                                 </IonCol>
                              </IonRow>
                           </IonGrid>

                        </form>
                     </FormProvider>

                     <IonAlert
                        isOpen={UserNotFoundAlert}
                        onDidDismiss={() => setUserNotFoundAlert(false)}
                        cssClass='red-alert'
                        mode='md'
                        header={t('Reset-Password-Error')}
                        message={t('Reset-Password-Usernot-msg')}
                        buttons={[
                           {
                                 text: 'Close',
                                 role: 'cancel',
                                 cssClass: 'btn-primary',
                                 handler: () => {
                                    //setShowMessage(true);
                                 }
                           }
                           
                        ]}
                     />

                     <IonAlert
                        isOpen={UpdatePwdAlert}
                        onDidDismiss={() => setUpdatePwdAlert(false)}
                        cssClass='orange-alert'
                        mode='md'
                        header={t('Reset-Password-Success')}
                        message={t('Reset-Password-Success-msg2')}
                        buttons={[
                           {
                                 text: 'Close',
                                 role: 'cancel',
                                 cssClass: 'btn-primary',
                                 handler: () => {
                                    history.push(`${Routes.Home}`);
                                 }
                           }
                           
                        ]}
                     />
                  </IonCol>

               </IonRow>
            </IonGrid>
        </IonContent>

    </IonPage>
  );
};

export default ResetPassword;