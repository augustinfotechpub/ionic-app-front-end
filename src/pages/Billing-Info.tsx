import React, {useRef, useState, useEffect} from "react";
import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";

import { 
    IonContent, 
    IonPage, 
    IonGrid, 
    IonRow, 
    IonCol, 
    IonLabel,
    IonInput,
    IonButton,
    IonSegment,
    IonSegmentButton,
    IonSlides,
    IonSlide,
    IonImg,
    IonDatetime,
    IonItem,
    IonCheckbox,
    IonAlert,
    IonLoading,
    useIonViewWillEnter
} from '@ionic/react';

import HeaderMain from '../components/Header-main';
import Footer from '../components/Footer';
import FooterMobile from '../components/Footer-mobile';
import DashboardSidebar from '../components/Dahsboard-sidebar';

import '../assets/css/Custom.css';
import '../assets/css/Responsive.css';
import { Routes } from '../App';

import { FormProvider, useForm } from "react-hook-form";
import { ErrorMessage } from '@hookform/error-message';

import { globalConst } from "../constants";
import axios from "axios";
axios.defaults.baseURL = globalConst.baseURL;
axios.defaults.headers.post['Content-Type'] = globalConst.ContentType;
axios.defaults.headers.post['Access-Control-Allow-Origin'] = globalConst.AllowOrigin;
axios.defaults.headers.post['Accept'] = globalConst.Accept;
axios.defaults.timeout = globalConst.timeout;

const BillingInfo: React.FC<{ path: string }> = ({path}) => {

    const { t } = useTranslation();
    const slider = useRef<any>(null);
    let history = useHistory();

    const [PayData, setPayData] = useState<string[]>([]);

    const [showLoading, setShowLoading] = useState(false);
    const [showAlert, setShowAlert] = useState(false);
    const [msgAlert, setMsgAlert] = useState('');
    const [headAlert, setHeadAlert] = useState('');

    const [DataDelete, setDataDelete] = useState(false);
    const [DelID, setDelID] = useState<number>(0);

    const [selectedDateCC, setSelectedDateCC] = useState<string>('');
    const [maxYear, setmaxYear] = useState('2020')

    const [BillingInfoUpdateAlert, setBillingInfoUpdateAlert] = useState(false);
    const [defaultpaymethod, setdefaultpaymethod] = useState('0');

    const methods = useForm();
    const { register, trigger, handleSubmit, getValues, setValue, formState: { errors } } = methods;
 
    const slideOpts = {
        initialSlide: 0,
        speed: 400,
        loop: false,
        pagination: {
          el: null
        },
      
    };
  
    useIonViewWillEnter(() => {
        if (localStorage.getItem('loginState')!='1') {
            history.push(`${Routes.Home}`);
        }
        var ye = new Date().getFullYear();
        ye = ye + 5;
        setmaxYear(ye.toString());

        setValue('CCcardNumber', '');
        setValue('CCcvc', '');
        setValue('CCcardExpiryDate', '');

        if ((localStorage.getItem('user_id')!==null)) {
            //if ((localStorage.getItem('user_type_id')=='2') || (localStorage.getItem('user_code')=='null')) {
            //    history.push(`${Routes.UserDashboard}`);
            //} else if ((localStorage.getItem('user_type_id')=='2') || (localStorage.getItem('user_code')=='')) {
            //    history.push(`${Routes.UserDashboard}`);
            //} else {
            setShowLoading(false);
            loadUserData(localStorage.getItem('user_id'));
            //}
        } else {
            showError()
        }
    });

    useEffect(() => {
    }, []);

    const showError = (): void => {
        setShowLoading(false);
        
        setMsgAlert(t('Act-Msg'));
        setHeadAlert(t('Act-Head'));
        setShowAlert(true);
    }

    async function loadUserData(userID: any) {
        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {

            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            api.get("/auth/getByID/"+userID).then((resPData) => {
                const retData = JSON.parse(JSON.stringify(resPData)).data;
                
                setPayData(retData.paydata);

                console.log(retData);
                const fields = ['CCcardHolderName'];

                fields.forEach(field => {
                    if (field == 'CCcardHolderName') {
                        setValue('CCcardHolderName', retData['firstname'] + " " + retData['lastname'])
                    } /* else if (field == 'CCcvc') {
                        setValue('CCcvc', retData[field])
                    } else if (field == 'cc_cardNumber') {
                        setValue('CCcardNumber', retData[field])
                    } else if (field=='cc_cardExpiryDate') {
                        setValue('CCcardExpiryDate', convert(retData.cc_cardExpiryDate));
                    } else {
                        setValue(field, retData[field])
                    }*/
                });
            })
            .catch((error) => {
                alert("Error Get Payment Data!");
            })
        })
        .catch((error) => {
          alert("Token not Get!");
        })
    }
    function convert(str) {
        var sYear, sMonth;
        [ sMonth, sYear ] = str.split('/');
        var setDate = sYear + '-' + sMonth + '-01';

        var date = new Date(setDate),
          mnth = ("0" + (date.getMonth() + 1)).slice(-2),
          day = ("0" + date.getDate()).slice(-2);
        return [date.getFullYear(), mnth, day].join("-");
    }

    const clickDeleteBtn = async (MID:any) => {
        setDelID(MID);
        setDataDelete(true);
    }

    const clickDetachMethod = async (MID:any) => {
        setShowLoading(true);

        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {
    
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            var editUserData: any;
            editUserData = {
                user_id: localStorage.getItem('user_id'),
                MID: MID
            };

            api.put("/auth/detachMethod", editUserData).then((resData) => {
                setShowLoading(false);
                setBillingInfoUpdateAlert(true)
            })
            .catch((error) => {
                alert("Error Update User Lease Code!");
            })
        })
        .catch((error) => {
          alert("Token not Get!");
        })
    }

    const clickMakeDefault =async (MID:any) => {
        setShowLoading(true);

        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {
    
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            var editUserData: any;
            editUserData = {
                user_id: localStorage.getItem('user_id'),
                MID: MID
            };

            api.put("/auth/MakeDefault", editUserData).then((resData) => {
                setShowLoading(false);
                setBillingInfoUpdateAlert(true)
            })
            .catch((error) => {
                alert("Error Update User Lease Code!");
            })
        })
        .catch((error) => {
          alert("Token not Get!");
        })
    }

    const onSubmit = (data: any) => {
        //console.log(data);
        //return false;

        setShowLoading(true);

        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {
    
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            var editUserData: any;
            var sYear, sMonth, sDate;

            var ExpiryDate = data.CCcardExpiryDate.substring(0, 10);
            [ sYear, sMonth, sDate ] = ExpiryDate.split('-');

            editUserData = {
                pay_type : 'card',
                default_method : defaultpaymethod,
                pay_name : data.CCcardHolderName,
                c_no : data.CCcardNumber,
                pay_exp_month : sMonth,
                pay_exp_year : sYear,
                pay_expirydate : sMonth+"/"+sYear,
                pm_cvc : data.CCcvc,
                user_id: localStorage.getItem('user_id')
            };

            api.put("/auth/updatePaymentDetail", editUserData).then((resData) => {
                const resReturnData = JSON.parse(JSON.stringify(resData)).data;
                console.log(resReturnData);
                
                setShowLoading(false);
                setBillingInfoUpdateAlert(true)
            })
            .catch((error) => {
                alert("Error Update User Lease Code!");
            })
        })
        .catch((error) => {
          alert("Token not Get!");
        })
    };

    const ClickDefaultPay = (valFlag: any) => {
        if (valFlag) {
            setdefaultpaymethod('1');
        } else {
            setdefaultpaymethod('0');
        }
    }

    const doNothing = () => {
		history.goBack();
	}
   
    const clickWillDismiss = (DismissFlag) => {
        if (DismissFlag) {
            history.push(`${Routes.myAccount}`);
        }
    };

    return (
        <IonPage>
            <HeaderMain pageTitle={t('Act-Payment-Title')} logoHide="hide-logo" onBack={doNothing} />
  
            <IonContent className="dashboard-wrapper manage-tenants-wrapper manage-rents-wrapper" fullscreen>
                <IonLoading
                    isOpen={showLoading}
                    onDidDismiss={() => setShowLoading(false)}
                    message={'Loading...'}
                />

                <IonAlert
                    isOpen={DataDelete}
                    onDidDismiss={() => setDataDelete(false)}
                    cssClass='red-alert'
                    mode='md'
                    header={t('Act-Payment-Delete-Head')}
                    message={t('Act-Payment-Delete-Msg')}
                    buttons={[{
                                text: 'Remove',
                                cssClass: 'btn-secondary',
                                handler: () => {
                                    clickDetachMethod(DelID);
                                }
                            }, {
                                text: 'Cancel',
                                cssClass: 'btn-primary',
                            }]}
                />

                <IonAlert
                    isOpen={showAlert}
                    onDidDismiss={() => setShowAlert(false)}
                    onWillDismiss={() => clickWillDismiss(true)}
                    cssClass='orange-alert'
                    mode='md'
                    header={headAlert}
                    message={msgAlert}
                    buttons={['Close']}
                />

                <IonGrid className="dashboard-main-grid">
                    <IonRow className="dashboard-main-row">
                        {/* sidebar start  */}
                        <DashboardSidebar path={path} />
                        {/* sidebar end  */}

                        {/* dashboar content start */}
                        <IonCol className="dashboard-content my-account-content" size="12" sizeMd="12" sizeLg="6" sizeXl="8">
                            <div className="dashboard-content-inner">
                                <FormProvider {...methods}>
                                    <form onSubmit={handleSubmit(onSubmit)}>
                                        <div className="choose-payment-method-block">
                                            <IonSlides pager={true} options={slideOpts} ref={slider}>
                                                <IonSlide>
                                                    <IonGrid className="ion-no-padding">
                                                        <IonRow className="ion-align-items-center credit-card-details">
                                                            <IonCol size="3" sizeMd="2">
                                                                <IonImg className="visa-logo" src="assets/images/visa-logo.png" />
                                                            </IonCol>
                                                            <IonCol size="9" sizeMd="6">
                                                                <div className="form-field">
                                                                    <IonLabel className="form-lable" >{t('Act-Payment-Card')}*</IonLabel>
                                                                    <IonInput
                                                                        mode="md"
                                                                        type="text"
                                                                        {...register('CCcardHolderName', {
                                                                            required: {
                                                                                value: true,
                                                                                message: t('Act-Payment-Card-Error')
                                                                            }
                                                                        })}
                                                                    />
                                                                    <ErrorMessage
                                                                        errors={errors}
                                                                        name="CCcardHolderName"
                                                                        as={<div className="error-message" style={{ color: 'red' }} />}
                                                                    />
                                                                </div>

                                                                
                                                                <div className="form-field">
                                                                    <IonLabel className="form-lable" >{t('Act-Payment-CardNo')}*</IonLabel>
                                                                    <IonInput
                                                                        mode="md"
                                                                        type="text"
                                                                        {...register('CCcardNumber', {
                                                                            required: {
                                                                                value: true,
                                                                                message: t('Act-Payment-CardNo-Error1')
                                                                            },
                                                                            pattern: {
                                                                                value: /^(?:4[0-9]{12}(?:[0-9]{3})?)$/i,
                                                                                message: t('Act-Payment-CardNo-Error2')
                                                                                }
                                                                        })}
                                                                    />
                                                                    <ErrorMessage
                                                                        errors={errors}
                                                                        name="CCcardNumber"
                                                                        as={<div className="error-message" style={{ color: 'red' }} />}
                                                                    />
                                                                </div>
                                                                
                                                                <div className="form-field">
                                                                    <IonLabel className="form-lable">{t('Act-Payment-ExpDate')}*</IonLabel>
                                                                    <IonDatetime 
                                                                        displayFormat="MM/YY" max={maxYear}
                                                                        // name="cardExpiryDate" placeholder="" 
                                                                        {...register('CCcardExpiryDate', {
                                                                            required: {
                                                                                value: true,
                                                                                message: t('Act-Payment-ExpDate-Error')
                                                                            }
                                                                        })}
                                                                        value={selectedDateCC} 
                                                                        onIonChange={e => setSelectedDateCC(e.detail.value!)
                                                                        }>

                                                                    </IonDatetime>
                                                                    <ErrorMessage
                                                                        errors={errors}
                                                                        name="CCcardExpiryDate"
                                                                        as={<div className="error-message" style={{ color: 'red' }} />}
                                                                    />
                                                                </div>

                                                                <div className="form-field">
                                                                    <IonLabel className="form-lable" >{t('Act-Payment-CVCNo')}*</IonLabel>
                                                                    <IonInput
                                                                        mode="md"
                                                                        type="text"
                                                                        {...register('CCcvc', {
                                                                            /*required: {
                                                                                value: true,
                                                                                message: 'Please enter a CVC number.'
                                                                            },*/
                                                                            minLength: {
                                                                                value: 3,
                                                                                message: t('Act-Payment-CVCNo-Error')
                                                                            },
                                                                            maxLength: {
                                                                                value: 3,
                                                                                message: t('Act-Payment-CVCNo-Error')
                                                                            }
                                                                        })}
                                                                    />
                                                                    <ErrorMessage
                                                                        errors={errors}
                                                                        name="CCcvc"
                                                                        as={<div className="error-message" style={{ color: 'red' }} />}
                                                                    />
                                                                </div>

                                                                <div className="form-field">
                                                                    <IonItem className="checkbox-wrap">
                                                                        <IonLabel className="form-lable">{t('Act-Payment-Method')}</IonLabel>
                                                                        <IonCheckbox 
                                                                            mode="md" 
                                                                            slot="start" 
                                                                            value={defaultpaymethod}
                                                                            onIonChange={(e) => ClickDefaultPay(e.detail.checked)}
                                                                            {...register('default_payment_method')} 
                                                                        />
                                                                    </IonItem>
                                                                </div> 

                                                            </IonCol>
                                                        </IonRow>
                                                    </IonGrid>
                                                </IonSlide>
                                            </IonSlides>
                                        </div>

                                        <div className="choose-payment-method-block">
                                            <div className="payment-method-cofirm-method">
                                                <IonButton type="submit"  className="secondary-button ion-margin-top" shape="round" fill="outline" >
                                                    {t('General-Confirm')}
                                                </IonButton>

                                                <IonButton className="exit-file-btn" fill="solid" shape="round" routerLink={Routes.myAccount}>
                                                    {t('General-Cancel')}
                                                </IonButton>
                                            </div>
                                        </div>
                                    </form>
                                </FormProvider>

                                <IonAlert
                                    isOpen={BillingInfoUpdateAlert}
                                    onDidDismiss={() => setBillingInfoUpdateAlert(false)}
                                    onWillDismiss={() => clickWillDismiss(true)}
                                    cssClass='orange-alert rent-pay-alert'
                                    mode='md'
                                    header={t('Act-Billing-Information')}
                                    message={t('Act-Billing-Information-Text')}
                                    buttons={[
                                        {
                                            text: t('Act-Return-Page'),
                                            role: 'cancel',
                                            cssClass: 'btn-primary',
                                            handler: () => {
                                                
                                            }
                                        }    
                                    ]}
                                />

                                <IonGrid className="">
                                    <IonRow className="paid-rents-list">
                                        {PayData.map((PData: any) => (
                                            <IonCol size="12" sizeXl="6">
                                                <div className="tenants-lease-info tenants-lease-payment-info ">
                                                    <p><b>{t('Act-Payment-Card')}: </b>{PData.pay_name}</p>
                                                    <p><b>{t('Act-Payment-CardNo')}: </b>{PData.c_no}</p>
                                                    <p><b>{t('Act-Payment-ExpDate')}: </b>{PData.c_date}</p>
                                                    <p><b>{t('Act-Payment-CVCNo')}: </b>{PData.c_cvc}</p>
                                                    <IonButton className="secondary-button view-receipt-button"  shape="round" fill="outline" onClick={() => clickDeleteBtn(PData.id)} >{t('General-Delete')}</IonButton>
                                                    {PData.default_method==0 ? <IonButton className="primary-button" shape="round" fill="outline" onClick={() => clickMakeDefault(PData.id)}>{t('Act-Make-Default')}</IonButton> : ""}
                                                </div>                                   
                                            </IonCol>
                                        ))}
                                    </IonRow>
                                </IonGrid>
                            </div>
                        </IonCol>
                        {/* dashboar content end */}

                        {/* dashboar posts start */}
                        <IonCol  className="dashboard-posts" size="12" sizeMd="12" sizeLg="3" sizeXl="2">
                            
                        </IonCol>
                        {/* dashboar post end */}
                    </IonRow>
                </IonGrid>
              
                <Footer />
            </IonContent>

            <FooterMobile />
        </IonPage>
    );
  };
  
  export default BillingInfo;