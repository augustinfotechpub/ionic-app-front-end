import React, {useRef, useEffect, useState} from "react";

import { 
    IonContent, 
    IonPage, 
    IonButton, 
    IonGrid, 
    IonRow, 
    IonCol, 
    IonIcon,
    useIonViewWillEnter,
  } from '@ionic/react';
  
import { useHistory } from "react-router-dom";
  
import HeaderMain from '../components/Header-main';
import Footer from '../components/Footer';
import FooterMobile from '../components/Footer-mobile';
import DashboardSidebar from '../components/Dahsboard-sidebar';

import TenantDashboard from '../components/TenantDashboard';
import ManagementDashboard from '../components/ManagementDashboard';
import StaffDashboard from '../components/StaffDashboard';

import '../assets/css/Custom.css';
import '../assets/css/Responsive.css';
import { Routes } from '../App';

const UserDashboard: React.FC<{ path: string }> = ({path}) => {

    let history = useHistory();

    const [UserTypeData, setUserTypeData] = useState<any>({'id': '', 'name': '', 'lease_code': ''});
    const [FeatureAccessData, setFeatureAccessData] = useState<any>({'manage_posts': '', 'manage_propertys': '', 'manage_rents': ''});

    useEffect(() => {
        if ((localStorage.getItem('user_type_id')!==null)) {
            setUserTypeData({'id': localStorage.getItem('user_type_id'), 'name': localStorage.getItem('user_type_name'), 'lease_code': localStorage.getItem('user_code')});
            setFeatureAccessData({'manage_posts': localStorage.getItem('manage_posts'), 'manage_propertys': localStorage.getItem('manage_propertys'), 'manage_rents': localStorage.getItem('manage_rents')});
        }
    }, []);

    useIonViewWillEnter(() => {
        if (localStorage.getItem('loginState')!='1') {
            history.push(`${Routes.Home}`);
        } 
    });

    const contentRef = useRef<HTMLIonContentElement | null>(null);

    const scrollToBottom= () => {
        contentRef.current && contentRef.current.scrollToBottom(500);
    };

    const doNothing = () => {
        history.goBack();
    }

    return (
        <IonPage>
  
           <HeaderMain pageTitle="" logoHide="" onBack={doNothing} />
  
           <IonContent className="dashboard-wrapper" ref={contentRef} scrollEvents={true} fullscreen>
                <IonGrid className="dashboard-main-grid">
                    <IonRow className="dashboard-main-row">
                        
                        {/* sidebar start  */}
                        <DashboardSidebar path={path} />
                        {/* sidebar end  */}


                        {/* Management dashboar content start */}
                        {UserTypeData.id == '2' ? (
                            <ManagementDashboard UserTypeData={UserTypeData} path={path} />
                        ) : ('')}
                        {/* Management dashboar content end */}

                        {/* Staff dashboar content start */}
                        {UserTypeData.id == '3' ? (
                            <StaffDashboard UserTypeData={UserTypeData} FeatureAccessData={FeatureAccessData} path={path} />
                        ) : ('')}
                        {/* Staff dashboar content end */}

                        {/* Tenant dashboar content start */}
                        {UserTypeData.id == '1' ? (
                            <TenantDashboard UserTypeData={UserTypeData} path={path} />
                        ) : ('')}
                        {/* Tenant dashboar content end */}


                        {/* dashboar posts start */}
                        <IonCol  className="dashboard-posts" size="12" sizeMd="12" sizeLg="3" sizeXl="2">
                            
                        </IonCol>
                        {/* dashboar post end */}
    
                    </IonRow>
                </IonGrid>

                <IonButton className="scroll-to-bottom-btn" onClick={scrollToBottom} fill="clear">
                    <IonIcon icon="assets/images/scroll-to-bottom-icon.svg" />
                </IonButton>
              
                <Footer />

            </IonContent>

            <FooterMobile />
      </IonPage>
    );
  };
  
  export default UserDashboard;