import React, {useRef, useState, useEffect} from "react";
import { useTranslation } from "react-i18next";

import { 
    IonContent, 
    IonPage, 
    IonGrid, 
    IonRow, 
    IonCol, 
    IonSlide,
    IonLabel,
    IonInput,
    IonButton,
    IonImg,
    IonDatetime,
    IonLoading,
    IonAlert,
    IonItem,
    IonRadio,
    IonListHeader,
    IonRadioGroup,
    useIonViewWillEnter
} from '@ionic/react';

import { FormProvider, useForm } from "react-hook-form";
import { ErrorMessage } from '@hookform/error-message';
import { RouteComponentProps, Switch, useLocation } from "react-router";

import DashboardSidebar from '../components/Dahsboard-sidebar';
import HeaderMain from '../components/Header-main';
import Footer from '../components/Footer';
import FooterMobile from '../components/Footer-mobile';

import '../assets/css/Custom.css';
import '../assets/css/Responsive.css';
import { Routes } from "../App";

import { globalConst } from "../constants";
import axios from "axios";
axios.defaults.baseURL = globalConst.baseURL;
axios.defaults.headers.post['Content-Type'] = globalConst.ContentType;
axios.defaults.headers.post['Access-Control-Allow-Origin'] = globalConst.AllowOrigin;
axios.defaults.headers.post['Accept'] = globalConst.Accept;
axios.defaults.timeout = globalConst.timeout;

interface PageProps extends RouteComponentProps {path: string; history: any; match: any; location: any;}
const PaymentMethod: React.FC<PageProps> = ({ path, history, match, location }) => {

    const { t } = useTranslation();
    const methods = useForm();
    const { register, trigger, handleSubmit, control, setValue, getValues, formState: { errors } } = methods;
 
    const [SelCard, setSelCard] = useState<number>(0)

    const [locPath, setlocPath] = useState('');
    
    const [showLoading, setShowLoading] = useState(true);
    const [showAlert, setShowAlert] = useState(false);
    const [msgAlert, setMsgAlert] = useState('');
    const [headAlert, setHeadAlert] = useState('');

    const [paymentCompleteAlert, setPaymentCompleteAlert] = useState(false);

    const [LeaseID, setLeaseID] = useState(0);
    const [PayData, setPayData] = useState([{id:'',default_method:'',stripe_pay_method_id:'',pay_name:'',c_no:'',c_date:'',c_cvc:''}]);
    const [QuickPayFlag, setQuickPayFlag] = useState(false);

    const onSubmit = (data: any) => {

        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {
    
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            var now = new Date();

            var selCID = data.selectedCardID;
            if (selCID==''){
                selCID = SelCard;
            }

            const editLeaseData = {
                "lease_price_pay" : data.currentLeasePrice,
                "sYear" : now.getFullYear(),
                "sMonth" : (now.getMonth() + 1),
                "sDate" : now.getDate(),
                "lease_id" : LeaseID,
                "pay_user_id" : localStorage.getItem('user_id'),
                "quick_pay" : false,
                "cvcNo" : data.cvcNo,
                "selectedCardID" : selCID,
            };
    
            api.put("/propertyleases/quickpay/"+LeaseID, editLeaseData).then((retData) => {
                const retDataArr = JSON.parse(JSON.stringify(retData));

                setPaymentCompleteAlert(true)

            }).catch((error) => {
                alert("Error found in put Data");
                console.log(error);
            });
        })
        .catch((error) => {
          alert("Token not Get!");
        })

    };
    
    const doNothing = () => {
        history.goBack();
    }

    useIonViewWillEnter(() => {
        if (localStorage.getItem('loginState')!='1') {
            history.push(`${Routes.Home}`);
        } else {
            if ((localStorage.getItem('user_type_id')=='2') || (localStorage.getItem('user_type_id')=='3')) {
                history.push(`${Routes.UserDashboard}`);
            } else if ((localStorage.getItem('user_code')=='null') || (localStorage.getItem('user_code')=='')) {
                history.push(`${Routes.UserDashboard}`);
            }
        }
    });

    useEffect(() => {
        setlocPath(`${Routes.paymentMethod}`);
        setLeaseID(match.params.id);
        
        if (match.params.id>0) {
            loadData(match.params.id);
            loadUserData(localStorage.getItem('user_id'));
        } else {
            showError()
        }
        
    }, [match.params.id]);

    async function loadUserData(userID: any) {
        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {

            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            api.get("/auth/getPayInfoByID/all/"+userID).then((resPData) => {
                const retData = JSON.parse(JSON.stringify(resPData)).data;

                if (retData['itemFlag']){
                    setSelCard(retData.SelCardID);
                    setPayData(retData.paydata);
                    setQuickPayFlag(true);
                } else {
                    //history.push(`${Routes.payRents}`);
                    //setShowLoading(false);
                    
                    //setMsgAlert("Card information not added.");
                    //setHeadAlert("Payment Method ");
                    //setShowAlert(true);

                    history.push(`${Routes.billingInfo}`);
                }
            })
            .catch((error) => {
                alert("Error Get User Payment Data!");
            })
        })
        .catch((error) => {
          alert("Token not Get!");
        })
    }

    const showError = (): void => {
        setShowLoading(false);
        
        setMsgAlert(t('Rent-Pay-Method-Msg'));
        setHeadAlert(t('Rent-Pay-Method-Head'));
        setShowAlert(true);
    }

    const clickWillDismiss = (DismissFlag) => {
        if (DismissFlag) {
            history.push(`${Routes.payRents}`);
        }
    };

    async function loadData(LID: any) {
        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {

            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            api.get("/propertyleases/"+LID+"/"+localStorage.getItem('user_id')).then((resPData) => {
                const retData = JSON.parse(JSON.stringify(resPData)).data;
                setShowLoading(false);

                console.log(retData);
                const fields = ['RentPeriod', 'RentPay', 'currentLeasePrice'];
                fields.forEach(field => {
                    if (field=='RentPay') {
                        setValue(field, "$" + retData['currentLeasePrice'])
                    } else {
                        setValue(field, retData[field])
                    }
                    //console.log(field);
                    //console.log(retData[field]);
                });
            })
            .catch((error) => {
                alert("Error Get Data!");
            })
        })
        .catch((error) => {
          alert("Token not Get!");
        })
    }

    return (
        <IonPage>
  
           <HeaderMain pageTitle={t('Rent-Pay-Page-Title')} logoHide="hide-logo" onBack={doNothing} />
  
           <IonContent className="dashboard-wrapper manage-tenants-wrapper manage-rents-wrapper" fullscreen>
                <IonLoading
                    isOpen={showLoading}
                    onDidDismiss={() => setShowLoading(false)}
                    message={'Loading...'}
                />

                <IonAlert
                    isOpen={showAlert}
                    onDidDismiss={() => setShowAlert(false)}
                    onWillDismiss={() => clickWillDismiss(true)}
                    cssClass='orange-alert'
                    mode='md'
                    header={headAlert}
                    message={msgAlert}
                    buttons={['Close']}
                />

                <IonAlert
                    isOpen={paymentCompleteAlert}
                    onDidDismiss={() => setPaymentCompleteAlert(false)}
                    onWillDismiss={() => clickWillDismiss(true)}
                    cssClass='orange-alert rent-pay-alert'
                    mode='md'
                    header={t('Rent-Pay-Done-Head')}
                    message={t('Rent-Pay-Done-Msg')}
                    buttons={[
                        {
                            text: t('Rent-Pay-Done-Btn'),
                            role: 'cancel',
                            cssClass: 'btn-primary',
                            handler: () => {
                                
                            }
                        }    
                    ]}
                />

                <IonGrid className="dashboard-main-grid">
                    <IonRow className="dashboard-main-row">
                        
                        {/* sidebar start  */}
                        <DashboardSidebar path={locPath} />
                        {/* sidebar end  */}

                        {/* dashboar content start */}
                        <IonCol className="dashboard-content choose-payment-method-dashboard" size="12" sizeMd="12" sizeLg="6" sizeXl="8">
                            <div className="dashboard-content-inner">
                                <div className="payment-method-wrapper">
                                    <FormProvider {...methods}>
                                        <form onSubmit={handleSubmit(onSubmit)}>
                                            <div className="rent-pay-summary">
                                                <h3><u>{t('Rent-Pay-Summary')}</u></h3>
                                                <IonGrid className="ion-no-padding">
                                                    <IonRow className="rents-pay-details">
                                                        <IonCol size="6">
                                                            <p>{t('Rent-Pay-Period')}</p>
                                                            {/* <h4 className="rent-pay-period">-</h4> */}
                                                            <IonInput
                                                                readonly
                                                                mode="md"
                                                                type="text"
                                                                {...register('RentPeriod')}
                                                            />
                                                        </IonCol>
                                                        <IonCol size="6">
                                                            <p>{t('Rent-Pay-Total')}</p>
                                                            {/* <h4 className="rent-price">{RentPay}</h4> */}
                                                            <IonInput
                                                                readonly
                                                                mode="md"
                                                                type="text"
                                                                {...register('RentPay')}
                                                            />
                                                            <IonInput
                                                                class="hide-input"
                                                                mode="md"
                                                                type="text"
                                                                {...register('currentLeasePrice')}
                                                            />
                                                        </IonCol>
                                                    </IonRow>
                                                </IonGrid>
                                            </div>

                                            <div className="choose-payment-method-block">
                                                <IonSlide>
                                                    <IonGrid className="ion-no-padding">
                                                        <IonRow className="ion-align-items-center credit-card-details">
                                                            <IonCol size="3" sizeMd="2">
                                                                <IonImg className="visa-logo" src="assets/images/visa-logo.png" />
                                                            </IonCol>
                                                            <IonCol size="9" sizeMd="6">
                                                                <div>
                                                                    <IonRadioGroup value={SelCard} onIonChange={e => setSelCard(e.detail.value)}>
                                                                        {PayData.map((payitem, index) => (
                                                                            <div key={index} className="search-result-box">
                                                                                <IonItem>
                                                                                    <IonLabel>{payitem.pay_name} - {payitem.c_no} - {payitem.c_date}</IonLabel>
                                                                                    <IonRadio {...register('selectedCardID')} value={payitem.id} />
                                                                                </IonItem>
                                                                            </div>
                                                                        ))}
                                                                    </IonRadioGroup>
                                                                </div>
                                                            </IonCol>
                                                        </IonRow>
                                                        <IonRow className="ion-align-items-center credit-card-details">
                                                            <IonCol size="3" sizeMd="2">
                                                                <IonLabel className="form-lable" >{t('Act-Payment-CVCNo')}*</IonLabel>
                                                            </IonCol>
                                                            <IonCol size="9" sizeMd="6">
                                                                <IonInput
                                                                    mode="md"
                                                                    type="number"
                                                                    {...register('cvcNo', {
                                                                        required: {
                                                                            value: true,
                                                                            message: t('Act-Payment-CVCNo-Error')
                                                                        },
                                                                        minLength: {
                                                                            value: 3,
                                                                            message: t('Act-Payment-CVCNo-Error')
                                                                        },
                                                                        maxLength: {
                                                                            value: 3,
                                                                            message: t('Act-Payment-CVCNo-Error')
                                                                        }
                                                                    })}
                                                                />
                                                                <ErrorMessage
                                                                    errors={errors}
                                                                    name="cvcNo"
                                                                    as={<div className="error-message" style={{ color: 'red' }} />}
                                                                />
                                                            </IonCol>
                                                        </IonRow>
                                                    </IonGrid>
                                                </IonSlide>
                                            </div>

                                            <div className="choose-payment-method-block">
                                                <div className="payment-method-cofirm-method">
                                                    <IonButton type="submit"  className="secondary-button ion-margin-top" shape="round" fill="outline" >
                                                        {t('General-Confirm')}
                                                    </IonButton>
                                                </div>
                                            </div>
                                        </form>
                                    </FormProvider>
                                </div>
                            </div>
                        </IonCol>
                        {/* dashboar content end */}

                        {/* dashboar posts start */}
                        <IonCol  className="dashboard-posts" size="12" sizeMd="12" sizeLg="3" sizeXl="2">
                            
                        </IonCol>
                        {/* dashboar post end */}
    
                    </IonRow>
                </IonGrid>
              
                <Footer />
            </IonContent>

            <FooterMobile />
        </IonPage>
    );
  };
  
  export default PaymentMethod;