import { 
  IonContent, 
  IonHeader,
  IonTitle,
  IonToolbar,
  IonPage, 
  IonIcon, 
  IonInput, 
  IonButton, 
  IonGrid, 
  IonRow, 
  IonCol,
  useIonViewWillEnter,
  useIonViewDidLeave,
} from '@ionic/react';
import React, {useState, useEffect} from 'react';
import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";

import { FormProvider, useForm, FieldErrors } from "react-hook-form";
import { ErrorMessage } from '@hookform/error-message';

import Header from '../components/Header';

import { globalConst } from "../constants";
import { userVar } from "../global";

import '../assets/css/Custom.css';
import '../assets/css/Responsive.css';

import { Routes } from '../App';

import { ActionCard, UserInfoCard } from '../components';
import { Auth } from '../services/AuthService';
import { AuthActions, AuthActionBuilder } from 'ionic-appauth';
import { RouteComponentProps } from 'react-router';
import { Subscription } from 'rxjs';

import "@codetrix-studio/capacitor-google-auth";
import { Plugins } from '@capacitor/core';

import axios from "axios";
axios.defaults.baseURL = globalConst.baseURL;
axios.defaults.headers.post['Content-Type'] = globalConst.ContentType;
axios.defaults.headers.post['Access-Control-Allow-Origin'] = globalConst.AllowOrigin;
axios.defaults.headers.post['Accept'] = globalConst.Accept;
axios.defaults.timeout = globalConst.timeout;

interface IndexPageProps extends RouteComponentProps {}

const Home : React.FC<IndexPageProps> = (props: IndexPageProps) => {

  const { t } = useTranslation();
  let history = useHistory();
  const methods = useForm({ mode: "all"});
  const { register, handleSubmit, formState: { errors } } = methods;


  const [action, setAction] = useState(AuthActionBuilder.Init);
  const [user, setUser] = useState();
  let subs: Subscription[] = [];

  useIonViewWillEnter(() => {
    console.log('Index useIonViewWillEnter');

    if ((localStorage.getItem('loginState')=='1')) {
      history.push(`${Routes.UserDashboard}`);
    } 

    /* 
      loadIndexData();
        subs.push(
            Auth.Instance.events$.subscribe((action) => {
                setAction(action);
                if (action.action === AuthActions.SignOutSuccess) {
                    props.history.replace('landing');
                }
            }),
            Auth.Instance.user$.subscribe((user) => {
                setUser(user)
            })
        )
    */
  });


    /* useIonViewDidLeave(() => {
      console.log('useIonViewDidLeave');
        subs.forEach(sub => sub.unsubscribe());
    });

    function handleSignOut(e : any) {
        e.preventDefault();
        Auth.Instance.signOut();
    } */

    /* function handleRefresh(e : any) {
        e.preventDefault();
        Auth.Instance.refreshToken();
    }

    function handleGetUserDetails(e : any) {
        e.preventDefault();
        Auth.Instance.loadUserInfo();
    } */

  async function loadIndexData() {
    //console.log(typeof(localStorage.getItem("loginState")));
    //userVar.accessCode = '';
    //userVar.user_id = '';
    //userVar.user_firstname = '';
    //userVar.user_lastname = '';
    //userVar.user_phoneno = '';
    //userVar.user_email = '';
    //userVar.loginState = false;
  }

  const onSubmit = (data: any) => {
    const api = axios.create();
    api.get("/auth/token/refresh").then(res => res)
    .then((response) => {

      axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
      const api = axios.create();

      const loginData = {
        email: data.email,
        password: data.password1,
      };

      api.post("/auth/login", loginData).then((resLogin) => {
        const resLoginData = JSON.parse(JSON.stringify(resLogin));

        if (resLoginData.data.error!=''){
          alert(resLoginData.data.error);
        } else {
          //console.log(resLoginData);
          //return false;

          //console.log(localStorage.getItem('accessCode'));
          localStorage.setItem('accessCode', resLoginData.data.user.access_token);
          localStorage.setItem('loginState', '1');
          localStorage.setItem('user_id', resLoginData.data.user.id);
          localStorage.setItem('user_firstname', resLoginData.data.user.firstname);
          localStorage.setItem('user_lastname', resLoginData.data.user.lastname);
          localStorage.setItem('user_phoneno', resLoginData.data.user.phoneno);
          localStorage.setItem('user_email', resLoginData.data.user.email);
          localStorage.setItem('user_type_id', resLoginData.data.user.usertype_id);
          localStorage.setItem('user_type_name', resLoginData.data.user.usertype_name);
          localStorage.setItem('user_code', resLoginData.data.user.user_code);
          localStorage.setItem('user_management_id', resLoginData.data.user.user_management_id);
          localStorage.setItem('sel_prop_id', resLoginData.data.user.user_prop_id);
          localStorage.setItem('sel_prop_name', resLoginData.data.user.user_prop_name);
          localStorage.setItem('sel_prop_suffix', resLoginData.data.user.user_prop_suffix);

          if (resLoginData.data.user.FeatureAccess.manage_posts=='1') {
            localStorage.setItem('manage_posts', '1');
          } else {
            localStorage.setItem('manage_posts', '0');
          }

          if (resLoginData.data.user.FeatureAccess.manage_propertys=='1') {
            localStorage.setItem('manage_propertys', '1');
          } else {
            localStorage.setItem('manage_propertys', '0');
          }

          if (resLoginData.data.user.FeatureAccess.manage_rents=='1') {
            localStorage.setItem('manage_rents', '1');
          } else {
            localStorage.setItem('manage_rents', '0');
          }


          //userVar.accessCode = resLoginData.data.user.access_token;
          //userVar.loginState = true;
          //userVar.user_id = resLoginData.data.user.id;
          //userVar.user_firstname = resLoginData.data.user.firstname;
          //userVar.user_lastname = resLoginData.data.user.lastname;
          //userVar.user_phoneno = resLoginData.data.user.phoneno;
          //userVar.user_email = resLoginData.data.user.email;
      
          //history.push(`${Routes.UserDashboard}`);
          window.location.assign(`${Routes.UserDashboard}`);
       }
      }).catch((error) => {
        alert("Auth failure! Please create an account");
        console.log(error);
      });
    
    })
    .catch((error) => {
      alert("Token not Get!");
    })

  };

  const doNothing = () => {

  }
  
  type ErrorSummaryProps<T> = {
    errors: FieldErrors<T>;
  };
  function ErrorSummary<T>({ errors }: ErrorSummaryProps<T>) {
    if (Object.keys(errors).length === 0) {
      return null;
    }
    return (
      <div className="error-summary">
        {Object.keys(errors).map((fieldName) => (
          <ErrorMessage errors={errors} name={fieldName as any} as="div" key={fieldName} />
        ))}
      </div>
    );
  }
  
  type ErrorMessageContainerProps = {
    children?: React.ReactNode;
  };
  const ErrorMessageContainer = ({ children }: ErrorMessageContainerProps) => (
    <span className="error">{children}</span>
  );

  async function signInGoogle() {
    //const { history } = this.props;
    const result = await Plugins.GoogleAuth.signIn();
    console.info('result', result);
    if (result) {
      history.push({
        pathname: '/home',
        state: { name: result.name || result.displayName, image: result.imageUrl, email: result.email }
      });
    }

  }

  return (
    <IonPage>

      <Header class="without-back-arrow" onBack={doNothing} />

      <IonContent fullscreen>
{/* 
                <IonButton onClick={handleGetUserDetails}>Get User Details</IonButton>
                <IonButton onClick={handleRefresh}>Refresh Token</IonButton>
                <IonButton onClick={handleSignOut}>Sign Out</IonButton>
                <ActionCard action={action}></ActionCard>
                {user && <UserInfoCard user={user}></UserInfoCard>}       


 */}

        <IonGrid className="full-height-div">
          <IonRow className="login-form-row">
            <IonCol size="10" sizeMd="6" sizeLg="4">
              <FormProvider {...methods}>
                <form className="login-form" onSubmit={handleSubmit(onSubmit)} autoComplete="off">
                  <IonGrid>
                    <IonRow>

                        <IonCol size="12">
                          <ErrorSummary errors={errors} />
                        </IonCol>

                        <IonCol size="12" className="email-field">
                          {/* <IonInput placeholder="Email"/> */}
                          <IonInput
                            mode="md"
                            type="email"
                            className={`form-control ${errors.email ? 'is-invalid' : ''}`}
                            placeholder="Email"
                            {...register('email', {
                              required: t('Login-Error-email'),
                              pattern: {
                              value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                              message: t('Login-Error-email-Invalid')
                              }
                            })}
                          />
                          {/* <ErrorMessage
                            errors={errors}
                            name="email"
                            as={<div className="error-message" style={{ color: 'red' }} />}
                          /> */}
                        </IonCol>

                        <IonCol size="12" className="password-field">
                          {/* <IonInput type="password" placeholder="Password" /> */}
                          <IonInput 
                            mode="md"
                            type="password"
                            className={`form-control ${errors.password1 ? 'is-invalid' : ''}`} 
                            placeholder="Password"
                            {...register('password1', {
                              required: t('Login-Error-Password')
                            })}
                          />
                          {/* <ErrorMessage
                            errors={errors}
                            name="password1"
                            as={<div className="error-message" style={{ color: 'red' }} />}
                          /> */}
                        </IonCol>

                        <IonCol size="12" className="forget-password ion-text-right">
                            <IonButton routerLink={Routes.forgetPassword} fill="clear">
                              {t('Login-Forget-password')}
                            </IonButton>   
                        </IonCol>

                        <IonCol size="6" className="sign-up-btn">
                          <IonButton  className="secondary-button" routerLink={Routes.signup} expand="block" shape="round" fill="outline">
                            {t('Login-Signup')}
                          </IonButton>
                        </IonCol>

                        <IonCol size="6" className="login-btn">
                          <IonButton type="submit" expand="block" shape="round" fill="outline" >
                            {t('Login-Login')}
                          </IonButton>
                        </IonCol>

                        <IonCol size="12" className="gmail-btn">
                          <IonButton href="#" expand="block" shape="round" fill="outline" >
                            <IonIcon slot="start" src="/assets/images/google.svg" />
                            Login with Gmail
                          </IonButton>

                          <IonButton className="login-button" onClick={() => signInGoogle()} expand="block" fill="solid" color="danger">
            Login with Google
        </IonButton>
                        </IonCol>

                    </IonRow>
                  </IonGrid>
                </form>
              </FormProvider>
            </IonCol>

          </IonRow>
        </IonGrid>

      </IonContent>

      

    </IonPage>
  );
};

export default Home;