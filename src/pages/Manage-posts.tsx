import React, {useEffect, useRef, useState} from "react";
import { useHistory, useLocation } from 'react-router-dom';
import { useTranslation } from "react-i18next";

import { 
    IonContent, 
    IonPage,
    IonGrid,
    IonRow,
    IonCol, 
    IonButton, 
    IonIcon,
    IonItem,
    IonPopover,
    IonList,
    IonLoading,
    useIonViewWillEnter
} from '@ionic/react';

import { add } from "ionicons/icons";

import HeaderMain from '../components/Header-main';
import Footer from '../components/Footer';
import FooterMobile from '../components/Footer-mobile';
import DashboardSidebar from '../components/Dahsboard-sidebar';
import { Accordion } from "../components/Accordion";


import '@ionic/react/css/flex-utils.css';
import '../assets/css/Custom.css';
import '../assets/css/Responsive.css';
import { Routes } from "../App";

import { globalConst } from "../constants";
import axios from "axios";
axios.defaults.baseURL = globalConst.baseURL;
axios.defaults.headers.post['Content-Type'] = globalConst.ContentType;
axios.defaults.headers.post['Access-Control-Allow-Origin'] = globalConst.AllowOrigin;
axios.defaults.headers.post['Accept'] = globalConst.Accept;
axios.defaults.timeout = globalConst.timeout;

const ManagePosts: React.FC<{ path: string }> = ({path}) => {

    const { t } = useTranslation();
    let history = useHistory();
    const contentRef = useRef<HTMLIonContentElement | null>(null);

    const [showLoading, setShowLoading] = useState(true);

    const [pinnedPostsData, setPinnedPostsData] = useState<Array<any>>([{"id": '', "filestoragekey": '', "title": "", "content": "", "date": "", "attachFile": [{"file_name": ""}]}]);
    const [generalPostsData, setGeneralPostsData] = useState<Array<any>>([{"id": '', "filestoragekey": '', "title": "", "content": "", "date": "", "attachFile": [{"file_name": ""}]}]);
    
    const [popoverState, setShowPopover] = useState<{showPopover: boolean, event: Event | undefined}>({ showPopover: false, event: undefined });

    const scrollToBottom= () => {
        contentRef.current && contentRef.current.scrollToBottom(500);
    };

    useIonViewWillEnter(() => {
        setShowLoading(true);
        if (localStorage.getItem('loginState')!='1') {
            history.push(`${Routes.Home}`);
        } else {
            if ((localStorage.getItem('user_type_id')=='2') || ((localStorage.getItem('user_type_id')=='3') && (localStorage.getItem('manage_posts')=='1'))) {
                setPinnedPostsData([]);
                setGeneralPostsData([]);
                loadPostData();
            } else {
                history.push(`${Routes.UserDashboard}`);
            }
        }
    });

    async function loadPostData() {
        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {

            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            api.get("/postdata/alldatabyuser/"+localStorage.getItem('user_management_id')).then((resUnitData) => {
                const retData = JSON.parse(JSON.stringify(resUnitData)).data;
                console.log(retData);

                setPinnedPostsData(retData.PinnedPostsData);
                setGeneralPostsData(retData.GeneralPostsData);

                setShowLoading(false);
            })
            .catch((error) => {
                alert("Error Get Data!");
            })
        })
        .catch((error) => {
          alert("Token not Get!");
        })
    }

    const renderPinnedPostsData = () => {
        if (pinnedPostsData.length > 0) {
            return pinnedPostsData.map(item => {
                return (
                    <div key={item.id}>
                        <Accordion
                            id={item.id}
                            filestoragekey={item.filestoragekey}
                            title={item.title}
                            content={item.content}
                            date={item.date}
                            attachFile={item.attachFile}
                        />
                    </div>
                );
            })
        } else {
            return (
                <div><br />{t('AccessCode-No-Data-Found')}<br /><br /></div>
            );
        }
    }

    const renderGeneralPostsData = () => {
        if (generalPostsData.length > 0) {
            return generalPostsData.map(item => {
                return (
                    <div key={item.id}>
                        <Accordion
                            id={item.id}
                            filestoragekey={item.filestoragekey}
                            title={item.title}
                            content={item.content}
                            date={item.date}
                            attachFile={item.attachFile}
                        />
                    </div>
                );
            })
        } else {
            return (
                <div><br />{t('AccessCode-No-Data-Found')}<br /><br /></div>
            );
        }
    }

    const doNothing = () => {
        history.goBack();
    }

    return (
        <IonPage>
  
           <HeaderMain pageTitle={t('POST-Head')}  logoHide="hide-logo" onBack={doNothing} />
  
           <IonContent className="dashboard-wrapper lease-info-wrapper" ref={contentRef} scrollEvents={true} fullscreen>
                <IonLoading
                    isOpen={showLoading}
                    onDidDismiss={() => setShowLoading(false)}
                    message={'Loading...'}
                />

                <IonGrid className="dashboard-main-grid">
                    <IonRow className="dashboard-main-row">
                        
                        {/* sidebar start  */}
                        <DashboardSidebar path={path} />
                        {/* sidebar end  */}

                        {/* dashboar content start */}
                        <IonCol className="dashboard-content" size="12" sizeMd="12" sizeLg="6" sizeXl="8">
                            <div className="dashboard-content-inner">
                                <div className="manage-posts-wrapper">
                                    <p>{t('POST-Head-Text')}</p>
                                    <div className="posts-list pinned-posts">
                                        <h3 className="title-with-line">{t('POST-Pinned-Posts-Text')}:</h3>
                                        {renderPinnedPostsData()}
                                    </div>
                                    
                                    <div className="posts-list general-posts">
                                        <h3 className="title-with-line">{t('POST-General-Posts-Text')}:</h3>
                                        {renderGeneralPostsData()}
                                    </div>

                                    <IonButton 
                                        className="property-change-button" 
                                        fill="clear"
                                        onClick={
                                                (e) => {
                                                setShowPopover({ showPopover: true, event: e.nativeEvent })
                                            }}
                                    >
                                        <IonIcon src="/assets/images/format_list_bulleted.svg"  />
                                    </IonButton>

                                    <IonPopover
                                        cssClass='property-change-menu-popup' 
                                        mode="ios"
                                        event={popoverState.event}
                                        isOpen={popoverState.showPopover}
                                        onDidDismiss={() => setShowPopover({ showPopover: false, event: undefined })}
                                    >
                                        <IonList>
                                            <IonItem>
                                                <IonButton 
                                                    fill="clear" 
                                                    routerLink={Routes.createPosts}
                                                    onClick={
                                                        () => {
                                                        setShowPopover({ showPopover: false, event: undefined })
                                                    }}
                                                >
                                                    <IonIcon icon={add} />  
                                                    <span>{t('POST-Create')}</span>
                                                </IonButton>
                                            </IonItem>
                                        </IonList>
                                    </IonPopover>
                                </div>
                            </div>
                        </IonCol>
                        {/* dashboar content end */}

                        {/* dashboar posts start */}
                        <IonCol  className="dashboard-posts" size="12" sizeMd="12" sizeLg="3" sizeXl="2">
                            
                        </IonCol>
                        {/* dashboar post end */}
    
                    </IonRow>
                </IonGrid>

                <IonButton className="scroll-to-bottom-btn" onClick={scrollToBottom} fill="clear">
                    <IonIcon icon="assets/images/double-arrow-down.svg" />
                </IonButton>
              
                <Footer />

            </IonContent>

            <FooterMobile />
      </IonPage>
    );
};

export default ManagePosts;