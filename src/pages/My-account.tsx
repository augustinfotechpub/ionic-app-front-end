import React, {useRef, useState, useEffect} from "react";
import { useHistory, useLocation } from 'react-router-dom';
import { useTranslation } from "react-i18next";

import { 
    IonContent, 
    IonPage, 
    IonGrid, 
    IonRow, 
    IonCol, 
    IonSegment,
    IonSegmentButton,
    IonSlides,
    IonSlide,
    IonButton,
    IonIcon,
    createAnimation,
    IonModal,
    useIonViewWillEnter
} from '@ionic/react';

import { chevronForwardOutline } from "ionicons/icons";

import HeaderMain from '../components/Header-main';
import Footer from '../components/Footer';
import FooterMobile from '../components/Footer-mobile';
import DashboardSidebar from '../components/Dahsboard-sidebar';

import '../assets/css/Custom.css';
import '../assets/css/Responsive.css';
import { Routes } from '../App';
  
const MyAccount: React.FC<{ path: string }> = ({path}) => {
    const { t } = useTranslation();
    let history = useHistory();

    const [UserTypeData, setUserTypeData] = useState<any>({'id': '', 'name': '', 'lease_code': ''});
    const [FeatureAccessData, setFeatureAccessData] = useState<any>({'manage_posts': '', 'manage_propertys': '', 'manage_rents': ''});

    useEffect(() => {
        if ((localStorage.getItem('user_type_id')!==null)) {
            setUserTypeData({'id': localStorage.getItem('user_type_id'), 'name': localStorage.getItem('user_type_name'), 'lease_code': localStorage.getItem('user_code')});
            setFeatureAccessData({'manage_posts': localStorage.getItem('manage_posts'), 'manage_propertys': localStorage.getItem('manage_propertys'), 'manage_rents': localStorage.getItem('manage_rents')});
        }
    }, []);

    useIonViewWillEnter(() => {
        if (localStorage.getItem('loginState')!='1') {
            history.push(`${Routes.Home}`);
        }    
    });

    const doNothing = () => {
        history.goBack();
    }

    return (
        <IonPage>
  
           <HeaderMain pageTitle={t('Act-MyAccount')} logoHide="hide-logo" onBack={doNothing} />
  
           <IonContent className="dashboard-wrapper manage-tenants-wrapper manage-rents-wrapper" fullscreen>
                <IonGrid className="dashboard-main-grid">
                    <IonRow className="dashboard-main-row">
                        
                        {/* sidebar start  */}
                        <DashboardSidebar path={path} />
                        {/* sidebar end  */}

                        {/* dashboar content start */}
                        <IonCol className="dashboard-content my-account-content" size="12" sizeMd="12" sizeLg="6" sizeXl="8">
                            <div className="dashboard-content-inner">
                                <IonRow className="my-account-links">
                                    <IonCol className="" size="12" sizeMd="12" sizeLg="6" sizeXl="6">
                                        <IonButton className="my-account-button" fill="outline" expand="block"  routerLink={Routes.billingInfo}>
                                            <div className="button-inner">
                                                <span className="">{t('Act-BillingInfo')}</span>
                                                <IonIcon icon={chevronForwardOutline}  />
                                            </div>
                                        </IonButton>
                                        <IonButton className="my-account-button" fill="outline" expand="block"  routerLink={Routes.personalInfo}>
                                            <div className="button-inner">
                                                <span className="">{t('Act-PersonalInfo')}</span>
                                                <IonIcon icon={chevronForwardOutline}  />
                                            </div>
                                        </IonButton>
                                        <IonButton className="my-account-button" fill="outline" expand="block"  routerLink={Routes.passwordChange}>
                                            <div className="button-inner">
                                                <span className="">{t('Act-PasswordChange')}</span>
                                                <IonIcon icon={chevronForwardOutline}  />
                                            </div>
                                        </IonButton>
                                        
                                        {/* Tenant content start */}
                                        {UserTypeData.id == '1' ? (
                                            <IonButton className="my-account-button" fill="outline" expand="block"  routerLink={Routes.notifications}>
                                                <div className="button-inner">
                                                    <span className="">{t('Act-Notification')}</span>
                                                    <IonIcon icon={chevronForwardOutline}  />
                                                </div>
                                            </IonButton>
                                        ) : ('')}
                                        {/* Tenant content end */}

                                    </IonCol>
                                </IonRow>
                            </div>

                        </IonCol>
                        {/* dashboar content end */}


                        {/* dashboar posts start */}
                        <IonCol  className="dashboard-posts" size="12" sizeMd="12" sizeLg="3" sizeXl="2">
                            
                        </IonCol>
                        {/* dashboar post end */}
    
                    </IonRow>
                </IonGrid>
              
                <Footer />

            </IonContent>

            <FooterMobile />
      </IonPage>
    );
  };
  
  export default MyAccount;