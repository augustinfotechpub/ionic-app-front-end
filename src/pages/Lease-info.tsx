import React, {useEffect, useRef, useState} from "react";
import { useHistory } from "react-router-dom";
import { RouteComponentProps, Switch, useLocation } from "react-router";
import { FormProvider, useForm, Controller, useFieldArray } from "react-hook-form";
import { useTranslation } from "react-i18next";

import { 
    IonContent, 
    IonPage,
    IonGrid,
    IonRow,
    IonCol, 
    IonList,
    IonInput, 
    IonLabel, 
    IonButton, 
    IonIcon,
    IonCard,
    IonCardHeader,
    IonCardTitle,
    IonCardContent,
    IonLoading,
    IonAlert,
    IonItem,
    useIonViewWillEnter
} from '@ionic/react';

import { close, attachOutline, chevronDown } from "ionicons/icons";

import HeaderMain from '../components/Header-main';
import Footer from '../components/Footer';
import FooterMobile from '../components/Footer-mobile';
import DashboardSidebar from '../components/Dahsboard-sidebar';

import '@ionic/react/css/flex-utils.css';
import '../assets/css/Custom.css';
import '../assets/css/Responsive.css';
import { Routes } from "../App";

import { globalConst } from "../constants";
import axios from "axios";
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';
axios.defaults.baseURL = globalConst.baseURL;
axios.defaults.headers.post['Content-Type'] = globalConst.ContentType;
axios.defaults.headers.post['Access-Control-Allow-Origin'] = globalConst.AllowOrigin;
axios.defaults.headers.post['Accept'] = globalConst.Accept;
axios.defaults.timeout = globalConst.timeout;

interface PageProps extends RouteComponentProps {path: string; history: any; match: any; location: any;}

const LeaseInfo: React.FC<PageProps> = ({ path, history, match, location }) => {

    const { t } = useTranslation();
    const [UserTypeData, setUserTypeData] = useState<any>({'id': '', 'name': '', 'lease_code': ''});

    const [locPath, setlocPath] = useState('');
    const locationSHI = useLocation();
    const contentRef = useRef<HTMLIonContentElement | null>(null);
    const [showLoading, setShowLoading] = useState(true);
	
    const [showAlert, setShowAlert] = useState(false);
    const [msgAlert, setMsgAlert] = useState('');
    const [headAlert, setHeadAlert] = useState('');
    const [handleDismiss, sethandleDismiss] = useState(false);

    const methods = useForm();
    const { register, trigger, handleSubmit, control, setValue, getValues, formState: { errors } } = methods;

    const [TenantsData, setTenantsData] = useState([{"firstName": "", "lastName": ""}]);
    const [FileData, setFileData] = useState([{"file_name": ""}]);
    const [pid, setpid] = useState('');

    const scrollToBottom= () => {
        contentRef.current && contentRef.current.scrollToBottom(500);
    };

    useIonViewWillEnter(() => {
        if (localStorage.getItem('loginState')!='1') {
            history.push(`${Routes.Home}`);
        } else {
            if ((localStorage.getItem('user_type_id')=='1') || (localStorage.getItem('user_type_id')=='2') || ((localStorage.getItem('user_type_id')=='3') && (localStorage.getItem('manage_propertys')=='1'))) {
                //loadConfigData();
            } else {
                history.push(`${Routes.UserDashboard}`);
            }
        }
    });

    useEffect(() => {
        if ((localStorage.getItem('user_type_id')!==null)) {
            setUserTypeData({'id': localStorage.getItem('user_type_id'), 'name': localStorage.getItem('user_type_name'), 'lease_code': localStorage.getItem('user_code')});
        }

        if (locationSHI.state) {
            setShowLoading(false);
            setlocPath(location.state.path);

            if (location.state.leaseid>0) {
                loadLeaseData(location.state.leaseid);
            }
        } else {
            showError()
        }
    }, []);

    async function loadLeaseData(leaseID: any) {
        //console.log('SHIHISHIHS');
        //console.log(leaseID);

        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {

            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            api.get("/propertyleases/"+leaseID+"/"+localStorage.getItem('user_id')).then((resPData) => {
                const retData = JSON.parse(JSON.stringify(resPData)).data;
                //console.log('Vivan--------');
                //console.log(retData);
                setpid(retData.property_id);

                const fields = ['floorAptNo', 'leaseCode', 'currentLeasePrice', 'leaseStartDate', 'leaseEndDate', 'no_tenants', 'Fdata'];
                fields.forEach(field => {
                    if (field=='no_tenants') {
                        setValue(field, retData['TData'].length)
                        setTenantsData(retData['TData']);
                    } else if (field=='Fdata') {
                        setFileData(retData['FData']);
                    } else {
                        setValue(field, retData[field])
                    }
                });
            })
            .catch((error) => {
                alert("Error Get Data!");
            })
        })
        .catch((error) => {
          alert("Token not Get!");
        })
    }

    const showError = (): void => {
        setShowLoading(false);
        
        setMsgAlert(t('Tenants-Lease-View-Error-Msg'));
        setHeadAlert(t('Tenants-Lease-View-Error-Head'));
        sethandleDismiss(true);
        setShowAlert(true);
    }

    const clickWillDismiss = () => {
        if (handleDismiss) {
            history.push(`${Routes.manageProperties}`);
        }
    };

     const doNothing = () => {
		//history.push(`/signup-data/${userTypeSelID}`);
		history.goBack();
	}

    const ExitPage = async (path) => {
        //alert(path);
        //alert(pid);
        if (pid!='') {
            if (UserTypeData.id == 1) {
                history.push({
                    pathname: `${Routes.manageLeases}`,
                    state: { path: `${Routes.manageLeases}`, prop_id: pid }
                });
            } else {
                history.push({
                    pathname: path,
                    state: { path: path, prop_id: pid }
                });
            }
        } else {
            history.push(`${Routes.manageProperties}`);
        }
    };

    return (
        <IonPage>
  
           <HeaderMain pageTitle={t('Tenants-Lease-Title')} logoHide="hide-logo" onBack={doNothing} />
  
           <IonContent className="dashboard-wrapper lease-info-wrapper" ref={contentRef} scrollEvents={true} fullscreen>
                <IonLoading
                    isOpen={showLoading}
                    onDidDismiss={() => setShowLoading(false)}
                    message={'Loading...'}
                />
    
                <IonAlert
                    isOpen={showAlert}
                    onDidDismiss={() => setShowAlert(false)}
                    onWillDismiss={() => clickWillDismiss()}
                    cssClass='orange-alert'
                    header={headAlert}
                    message={msgAlert}
                    buttons={['Close']}
                />

                <IonGrid className="dashboard-main-grid">
                    <IonRow className="dashboard-main-row">
                        
                        {/* sidebar start  */}
                        <DashboardSidebar path={locPath} />
                        {/* sidebar end  */}


                        {/* dashboar content start */}
                        <IonCol className="dashboard-content" size="12" sizeMd="12" sizeLg="6" sizeXl="8">
                            <IonRow className="apartment-lease-code-info">
                                <IonCol className="apartment-number" size="4" sizeMd="4" sizeLg="4" sizeXl="3">
                                    <IonLabel>{t('Tenants-Lease-Apartment')} #</IonLabel>
                                    <IonInput mode="md" type="text" {...register('floorAptNo')} readonly></IonInput>
                                </IonCol>
                                <IonCol className="lease-code-info" size="8" sizeMd="8" sizeLg="7" sizeXl="6">
                                    <div className="lease-code">
                                        <IonLabel>{t('Tenants-Lease-Lease-Code')}</IonLabel>
                                        <IonInput mode="md" type="text" {...register('leaseCode')} readonly></IonInput>
                                    </div>
                                </IonCol>
                            </IonRow>

                            <IonCard className="tenant-lease-details-card">
                                <IonCardHeader>
                                    <IonCardTitle>{t('Tenants-Lease-Tenant-Details')}</IonCardTitle>
                                </IonCardHeader>

                                <IonCardContent>
                                    {TenantsData.map((Titem, Tindex) => (
                                        <div className="tenant-lease-details-info tenants-info">
                                            <h4>{t('Tenants-Lease-Tenant')} {Tindex + 1}</h4>
                                            <IonRow>
                                                <IonCol size="12" sizeMd="6" sizeLg="6" sizeXl="6">
                                                    <IonLabel>{t('Tenants-Lease-First-Name')}</IonLabel>
                                                    <IonInput mode="md" type="text" value={Titem.firstName} readonly></IonInput>
                                                </IonCol>
                                                <IonCol size="12" sizeMd="6" sizeLg="6" sizeXl="6">
                                                    <IonLabel>{t('Tenants-Lease-Last-Name')}</IonLabel>
                                                    <IonInput mode="md" type="text" value={Titem.lastName} readonly></IonInput>
                                                </IonCol>
                                            </IonRow>
                                        </div>
                                    ))}
                                </IonCardContent>
                            </IonCard>

                            <IonCard className="tenant-lease-details-card">
                                <IonCardHeader>
                                    <IonCardTitle>{t('Tenants-Lease-Details')}</IonCardTitle>
                                </IonCardHeader>

                                <IonCardContent>
                                    <div className="tenant-lease-details-info lease-info">
                                        <div>
                                            <IonLabel>{t('Tenants-Lease-no-Tenants-Apartment')}</IonLabel>
                                            <IonInput mode="md" type="text"  {...register('no_tenants')} readonly></IonInput>
                                        </div>
                                        <div>
                                            <IonLabel>{t('Tenants-Lease-Current-Lease-price')}</IonLabel>
                                            <IonInput mode="md" type="text"  {...register('currentLeasePrice')} readonly></IonInput>
                                        </div>
                                        <div>
                                            <IonLabel>{t('Tenants-Lease-Start-Date')}*</IonLabel>
                                            <IonInput mode="md" type="text" {...register('leaseStartDate')} readonly></IonInput>
                                        </div>
                                        <div>
                                            <IonLabel>{t('Tenants-Lease-End-Date')}*</IonLabel>
                                            <IonInput mode="md" type="text" {...register('leaseEndDate')} readonly></IonInput>
                                        </div>
                                    </div>
                                    <div className="lease-scan-block">
                                        <IonLabel>{t('Tenants-Lease-Scans')}</IonLabel>
                                        {FileData.map((Fitem, Findex) => (
                                            <div className="uploaded-file">
                                                <p className="uploaded-file-name read-only">{Findex + 1}. {Fitem.file_name}</p>
                                            </div>
                                        ))}
                                    </div>
                                </IonCardContent>
                            </IonCard>
                            
                            <IonRow className="ion-justify-content-center">
                                <IonCol className="ion-text-center">
                                    <IonButton className="exit-file-btn" onClick={() => ExitPage(Routes.manageTenants)} fill="solid" shape="round">{t('General-ExitFile')}</IonButton>
                                </IonCol>
                            </IonRow>

                        </IonCol>
                        {/* dashboar content end */}


                        {/* dashboar posts start */}
                        <IonCol  className="dashboard-posts" size="12" sizeMd="12" sizeLg="3" sizeXl="2">
                            
                        </IonCol>
                        {/* dashboar post end */}
    
                    </IonRow>
                </IonGrid>

                <IonButton className="scroll-to-bottom-btn" onClick={scrollToBottom} fill="clear">
                    <IonIcon icon="assets/images/double-arrow-down.svg" />
                </IonButton>
              
                <Footer />

            </IonContent>

            <FooterMobile />
      </IonPage>
    );
};

export default LeaseInfo;