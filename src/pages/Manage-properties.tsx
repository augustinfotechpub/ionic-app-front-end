import React, { useState, useEffect } from 'react';
import { useHistory } from "react-router-dom";
import { Redirect } from 'react-router';
import { Link } from 'react-router-dom';
import { useTranslation } from "react-i18next";

import { 
    IonPage,
    IonContent,
    IonGrid,
    IonRow,
    IonCol,
    IonList,
    IonRadioGroup,
    IonItem,
    IonLabel,
    IonIcon,
    IonFab,
    IonFabButton,
    IonFabList,
    IonButton,
    IonReorder, 
    IonReorderGroup,
    IonPopover,
    useIonViewWillEnter,
    useIonViewWillLeave,
    useIonViewDidLeave,
  } from '@ionic/react';

import { ItemReorderEventDetail } from '@ionic/core';

import HeaderMain from '../components/Header-main';
import Footer from '../components/Footer';
import FooterMobile from '../components/Footer-mobile';
import ManagePropertyMoreMenu from '../components/ManagePropertyMoreMenu';
import DashboardSidebar from '../components/Dahsboard-sidebar';

import { add } from "ionicons/icons";

import '@ionic/react/css/flex-utils.css';
import '../assets/css/Custom.css';
import '../assets/css/Responsive.css';

import { Routes } from '../App';

import { globalConst } from "../constants";

import axios from "axios";
axios.defaults.baseURL = globalConst.baseURL;
axios.defaults.headers.post['Content-Type'] = globalConst.ContentType;
axios.defaults.headers.post['Access-Control-Allow-Origin'] = globalConst.AllowOrigin;
axios.defaults.headers.post['Accept'] = globalConst.Accept;
axios.defaults.timeout = globalConst.timeout;

const ManageProperties: React.FC<{ path: string }> = ({path}) => {

    const { t } = useTranslation();
    const toggleReorder = (event: CustomEvent<ItemReorderEventDetail>) => {
        event.detail.complete();
    };
    const [showLoading, setShowLoading] = useState(true);
    const [PropData,setPropData]=useState([{"id": '', "name": "", "suffix": "", "address": "", "postalcode": "", "RemoveFlag": "", "FavoriteFlag": "", "Country": "", "State": "", "City": "", "Unitformat": ""}]);
    const [PaginationData,setPaginationData]=useState({"has_next": '', "has_prev": "", "next_page": "", "page": "", "pages": "", "prev_page": "", "total_count": ""});

    const [pageList, setpageList] = useState([{"currPage":'', "page_No":''}]);
        const [popoverState, setShowPopover] = useState<{showPopover: boolean, event: Event | undefined}>({ showPopover: false, event: undefined });

    const [archievedFlag, setarchievedFlag] = useState('all');
    const [PageNo, setPageNo] = useState(1);

    const PerPageDataDisplay = 15;
	
	let history = useHistory();

    useIonViewWillLeave(() => {
        //console.log('ionViewWillLeave event fired');
    });

    useIonViewWillEnter(() => {
        if (localStorage.getItem('loginState')!='1') {
            history.push(`${Routes.Home}`);
        } else {
            if ((localStorage.getItem('user_type_id')=='2') || ((localStorage.getItem('user_type_id')=='3') && (localStorage.getItem('manage_propertys')=='1'))) {
                loadListPropData(PageNo, PerPageDataDisplay, archievedFlag);
            } else {
                history.push(`${Routes.UserDashboard}`);
            }
        }
    });

    useEffect(() => {
        loadListPropData(PageNo, PerPageDataDisplay, archievedFlag);
    }, [PageNo, archievedFlag]);

    useIonViewDidLeave(() => {
        //console.log('useIonViewDidLeave');
    });
  
    async function handleClick(tag: any) {
        setPageNo(tag.page_No);
    }

    async function loadListPropData(page_no: any, per_page_data: any, removeFlag: any) {

        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {
    
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            //api.get("/propertys/alldata?current_user="+localStorage.getItem('user_id')).then((resUType) => {

            api.get("/propertys/?page="+page_no+"&per_page="+per_page_data+"&removeFlag="+removeFlag+"&current_user="+localStorage.getItem('user_management_id')).then((resUType) => {
                const retData = JSON.parse(JSON.stringify(resUType)).data;

                setPropData(retData.data);
                setPaginationData(retData.meta);

                console.log(retData.pageList);
                setpageList(retData.pageList);
                
                setShowLoading(false);
            })
            .catch((error) => {
                alert("Error Get in User Type!");
            })
        })
        .catch((error) => {
          alert("Token not Get!");
        })
    }

    const doNothing = () => {
        history.goBack();
    }

    const EditProp = async (path, propid) => {
        //console.log('TEST');
        //console.log(path);
        //console.log(propid);
        
        history.push({
            pathname: path,
            state: { path: path, prop_id: propid }
        });

        //console.log(history);
    };

    const FavoriteClick = async (propid) => {
        console.log('FavoriteClick');
        console.log(propid);

        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {
    
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            api.put("/propertys/toggleFavorite/"+propid+"/"+localStorage.getItem('user_id')).then((resUType) => {
                loadListPropData(PageNo, PerPageDataDisplay, archievedFlag);
            })
            .catch((error) => {
                alert("Error Delete in Property!");
            })
        })
        .catch((error) => {
          alert("Token not Get!");
        })
    };

    const toggleArchived = async (propid) => {
        console.log('toggleArchived');
        console.log(propid);

        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {
    
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            api.put("/propertys/toggleArchived/"+propid+"/"+localStorage.getItem('user_id')).then((resUType) => {
                loadListPropData(PageNo, PerPageDataDisplay, archievedFlag);
            })
            .catch((error) => {
                alert("Error Delete in Property!");
            })
        })
        .catch((error) => {
          alert("Token not Get!");
        })
    };

    const PermentRemove = async (propid) => {
        console.log(propid)
        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {
    
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            api.delete("/propertys/"+propid+"/"+localStorage.getItem('user_id')).then((resUType) => {
                //window.location.reload();
                loadListPropData(PageNo, PerPageDataDisplay, archievedFlag);
            })
            .catch((error) => {
                alert("Error Delete in Property!");
            })
        })
        .catch((error) => {
          alert("Token not Get!");
        })
    }

    const reloadPage = async () => {
        //window.location.reload();
        setarchievedFlag("hide");
    }

    return (

        <IonPage>
  
            <HeaderMain pageTitle={t('Prop-Manage-Title')} logoHide="hide-logo" onBack={doNothing} />
  
            <IonContent className="dashboard-wrapper" fullscreen>

                <IonGrid className="dashboard-main-grid manage-property-wrapper">  
                    <IonRow class="dashboard-main-row">

                        {/* sidebar start  */}
                        <DashboardSidebar path={path} />
                        {/* sidebar end  */}
                        
                        <IonCol className="dashboard-content" size="12" sizeMd="12" sizeLg="6" sizeXl="8">
                            <IonList>
                                <p>{t('Prop-Manage-Text')}</p>
                                <IonReorderGroup>
                                    {PropData.map((item, index) => (
                                        <IonItem key={item.id} className={item.RemoveFlag ? 'user-select-item archieved-propeprty' : 'user-select-item'}>
                                            <IonLabel>
                                                {item.suffix} - {item.name}
                                                <p>{item.address}, {item.City}-{item.postalcode}, {item.State} {item.Country}</p>
                                            </IonLabel>

                                            <ManagePropertyMoreMenu 
                                                FavoriteStr={item.FavoriteFlag ? (t('Prop-Remove-Favorite')) : (t('Prop-Mark-Favorite'))} 
                                                onFavoriteClick={() => FavoriteClick(item.id)} 
                                                ArchivedStr={item.RemoveFlag ? (t('Prop-Restore')) : (t('Prop-Remove'))} 
                                                ontoggleArchived={() => toggleArchived(item.id)}
                                                RemoveStr={item.RemoveFlag ? (t('Prop-Perment-Remove')) : ("")} 
                                                onPermentRemove={() => PermentRemove(item.id)}
                                                onEditProp={() => EditProp(Routes.editProperty, item.id)} prop_id={item.id} />
                                                
                                            <IonReorder slot="end" />
                                        </IonItem>
                                    ))}

                                    {pageList.length > 1 ? (
                                        <IonItem className="user-select-item">
                                            <IonLabel>
                                                {t('Prop-No-Prop')} {PaginationData.total_count}
                                                <br />
                                                {t('Prop-Current-Page')} {PaginationData.page}
                                                <br />
                                                
                                                {pageList.map((item, index) => (
                                                    <IonButton key={index} color="primary" onClick={() => handleClick(item)} >
                                                        {t('Prop-Page')} {item.page_No}
                                                    </IonButton>
                                                ))}
                                            </IonLabel>
                                        </IonItem>
                                    ) 
                                    : ''}

                                </IonReorderGroup>
                            </IonList>
                        </IonCol>
                        
                        {/* dashboar posts start */}
                        <IonCol  className="dashboard-posts" size="12" sizeMd="12" sizeLg="3" sizeXl="2">
                            
                        </IonCol>
                        {/* dashboar post end */}
                    
                    </IonRow>

                    <IonPopover
                        cssClass='property-change-menu-popup' 
                        mode="ios"
                        event={popoverState.event}
                        isOpen={popoverState.showPopover}
                        onDidDismiss={() => setShowPopover({ showPopover: false, event: undefined })}
                    >
                        <IonList>
                            <IonItem>
                                <IonButton 
                                    fill="clear" 
                                    routerLink={Routes.addNewProperty}
                                    onClick={
                                        () => {
                                        setShowPopover({ showPopover: false, event: undefined })
                                    }}
                                >
                                    <IonIcon icon={add} />  
                                    <span>{t('Prop-App')}</span>
                                </IonButton>
                            </IonItem>
                            <IonItem>
                                <IonButton 
                                    fill="clear"
                                    onClick={
                                        () => {
                                        setShowPopover({ showPopover: false, event: undefined })
                                    }}
                                >
                                    <IonIcon icon="/assets/images/reorder.svg"  />
                                    <span>{t('Prop-Rearrange-List')}</span>
                                </IonButton>
                            </IonItem>
                            <IonItem>
                                <IonButton 
                                    fill="clear"
                                    onClick={
                                        () => {
                                        setShowPopover({ showPopover: false, event: undefined })
                                    }}
                                >
                                    <IonIcon />
                                    <span>{t('Prop-Hide-Archieved')}</span>
                                </IonButton>
                            </IonItem>
                        </IonList>
                    </IonPopover>

                    <IonButton 
                        className="property-change-button" 
                        fill="clear"
                        onClick={
                                (e) => {
                                // e.persist();
                                setShowPopover({ showPopover: true, event: e.nativeEvent })
                            }}
                    >
                        <IonIcon src="/assets/images/format_list_bulleted.svg"  />
                    </IonButton>

                </IonGrid>
                 

                {/* <IonFab className="property-fab-button" vertical="bottom" horizontal="end" slot="fixed">
                    <IonFabButton>
                        <IonIcon src="/assets/images/format_list_bulleted.svg"  />
                    </IonFabButton>
                    <IonFabList side="top">
                        <IonList>
                            <IonItem>
                                <IonButton fill="clear" routerLink={Routes.addNewProperty}>
                                    <IonIcon icon={add} />  
                                    <span>Add a Property</span>
                                </IonButton>
                            </IonItem>
                            <IonItem>
                                <IonButton fill="clear">
                                    <IonIcon icon="/assets/images/reorder.svg"  />
                                    <span>Rearrange List</span>
                                </IonButton>
                            </IonItem>
                            <IonItem>
                                <IonButton fill="clear" onClick={(e) => reloadPage()}>
                                    <IonIcon />
                                    <span>Hide Archieved</span>
                                </IonButton>
                            </IonItem>
                        </IonList>
                    </IonFabList>
                </IonFab> */}
            </IonContent>

            <Footer />
            <FooterMobile />

        </IonPage>
        
    );
};

export default ManageProperties;