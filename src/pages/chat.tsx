import React, {useRef, useState, useEffect} from "react";
import { RouteComponentProps, Switch, useLocation } from "react-router";
import { useTranslation } from "react-i18next";

import { 
    IonContent, 
    IonPage, 
    IonGrid, 
    IonRow, 
    IonCol, 
    IonAlert,
    IonTextarea,
    IonIcon,
    IonButton,
    useIonViewWillEnter,
    IonLoading
} from '@ionic/react';

import { sendSharp, happyOutline } from "ionicons/icons";

import HeaderMain from '../components/Header-main';
import Footer from '../components/Footer';
import FooterMobile from '../components/Footer-mobile';
import DashboardSidebar from '../components/Dahsboard-sidebar';

import '../assets/css/Custom.css';
import '../assets/css/Custom-Dev.css';
import '../assets/css/Responsive.css';
import { Routes } from '../App';
  
import { globalConst } from "../constants";

import axios from "axios";
axios.defaults.baseURL = globalConst.baseURL;
axios.defaults.headers.post['Content-Type'] = globalConst.ContentType;
axios.defaults.headers.post['Access-Control-Allow-Origin'] = globalConst.AllowOrigin;
axios.defaults.headers.post['Accept'] = globalConst.Accept;
axios.defaults.timeout = globalConst.timeout;

interface PageProps extends RouteComponentProps {path: string; history: any; match: any; location: any;}
const Chat: React.FC<PageProps> = ({ path, history, match, location }) => {

    const { t } = useTranslation();
    const [LoadUserData, setLoadUserData] = useState<string[]>([]);
    const [LoadChatData, setLoadChatData] = useState<string[]>([]);
    const [call, setCall] = useState(false);
    const [LoadChatSubject, setLoadChatSubject] = useState<string>('');
    //let history = useHistory();

    const val_chat_id = location.state && location.state.chat_id 
    const val_chat_msg = location.state && location.state.chat_msg 

    const [SendMessage, setSendMessage] = useState<string>(val_chat_msg);

    const [showLoading, setShowLoading] = useState(true);
    const [customMessageAlertSentAlert, setCustomMessageAlertSentAlert] = useState(false);

    const [showAlert, setShowAlert] = useState(false);
    const [msgAlert, setMsgAlert] = useState('');
    const [headAlert, setHeadAlert] = useState('');

    useIonViewWillEnter(() => {
        if (localStorage.getItem('loginState')!='1') {
            history.push(`${Routes.Home}`);
        }
    });

    useEffect(() => {
        if ((val_chat_id!='') && (val_chat_id!=null)) {
            setSendMessage(val_chat_msg);
            loadChatData(val_chat_id);
        } else {
            setShowLoading(false);

            /* setMsgAlert("Messaging Chat data not available.");
            setHeadAlert("Messaging Data");
            setShowAlert(true); */
        }
    }, [val_chat_id]);

    async function loadChatData(chatid) {
        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {

            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            const passData = {
                ChatID: chatid,
                CurrUserID: localStorage.getItem('user_id'),
            };

            api.put("/messaging/getchatbyid", passData).then((returnData) => {
                const retData = JSON.parse(JSON.stringify(returnData)).data;
                
                //console.log(retData);
                setLoadChatData(retData.retChatData);
                setLoadUserData(retData.retUserData);
                setLoadChatSubject(retData.retConvSub);

                setShowLoading(false);
            })
            .catch((error) => {
                alert("Error Get Data!");
            })
        })
        .catch((error) => {
            alert("Token not Get!");
        })
    }

    const ClickSendMsg = async (chatid) => {
        const api = axios.create();
        api.get("/auth/token/refresh").then(res => res)
        .then((response) => {
            
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();
            

            const addMsgData = {
                "current_user" : localStorage.getItem('user_id'),
                "chatid" : chatid,
                "custom_message" : SendMessage,
            };
    
            console.log(addMsgData);
    
            api.post("/messaging/addchat", addMsgData).then((retData) => {
                const retDataArr = JSON.parse(JSON.stringify(retData));
                //console.log(retDataArr);

                setCustomMessageAlertSentAlert(true);
    
            }).catch((error) => {
                //alert("Error found in put Data");
                console.log(error);
            }); 

        })
        .catch((error) => {
          alert("Token not Get!");
        })

    };

    const doNothing = () => {
        history.goBack();
    }

    const clickWillDismiss = (FlagDismiss) => {
        if (FlagDismiss) {
            history.push({pathname: `${Routes.messaging}`});
        }
    };

    return (
        <IonPage>
  
           <HeaderMain pageTitle={t('Msg-Title')} logoHide="hide-logo" onBack={doNothing} />
  
           <IonContent className="dashboard-wrapper manage-tenants-wrapper manage-rents-wrapper" fullscreen>
                <IonLoading
                    isOpen={showLoading}
                    onDidDismiss={() => setShowLoading(false)}
                    message={'Loading...'}
                />

                <IonAlert
                    isOpen={showAlert}
                    onDidDismiss={() => setShowAlert(false)}
                    onWillDismiss={() => clickWillDismiss(true)}
                    cssClass='orange-alert'
                    header={headAlert}
                    message={msgAlert}
                    buttons={[{text: 'Close', cssClass: 'btn-primary'}]}
                />

                <IonGrid className="dashboard-main-grid">
                    <IonRow className="dashboard-main-row">
                        
                        {/* sidebar start  */}
                        <DashboardSidebar path={path} />
                        {/* sidebar end  */}

                        <IonAlert
                            isOpen={customMessageAlertSentAlert}
                            onDidDismiss={() => setCustomMessageAlertSentAlert(false)}
                            cssClass='orange-alert'
                            mode='md'
                            header={t('Msg-SendMessage-Head')}
                            message={t('Msg-SendMessage-Msg')}
                            buttons={[
                                {
                                    text: 'Close',
                                    role: 'cancel',
                                    cssClass: 'btn-primary',
                                    handler: () => {
                                        setSendMessage('');
                                        loadChatData(val_chat_id);
                                    }
                                }
                                
                            ]}
                        />

                        {/* dashboar content start */}
                        <IonCol className="dashboard-content chat-content" size="12" sizeMd="12" sizeLg="6" sizeXl="8">
                            <IonRow className="chat-header">
                                <IonCol className="sender-details" size="12" sizeMd="4" sizeLg="4" sizeXl="3">
                                    <h6>{t('Msg-With')}</h6>

                                    {LoadUserData.map((UserData: any, indU: any) => (
                                        <div>
                                            <h5><b>{UserData.firstname} {UserData.lastname}</b></h5>
                                            <p>({UserData.user_type})</p>
                                        </div>
                                    ))}
                                </IonCol>
                                <IonCol className="chat-time ion-text-md-right" size="12" sizeMd="8" sizeLg="8" sizeXl="9">
                                    <h6>{t('Msg-Officehours')}</h6>
                                    <h6>{t('Msg-CallDesk')} <a className="sender-number" onClick={() => setCall(true)}>111111</a></h6>
                                    {LoadChatSubject ? <h6>{t('Msg-Subject')} <b>{LoadChatSubject}</b></h6> : ''}
                                </IonCol>
                                <IonAlert
                                    isOpen={call}
                                    onDidDismiss={() => setCall(false)}
                                    cssClass='orange-alert'
                                    mode='md'
                                    header={t('Msg-CallStaff-Head')}
                                    message={t('Msg-CallStaff-Msg')}
                                    buttons={[
                                        {
                                            text: 'Yes',
                                            cssClass: 'btn-primary',
                                            handler: () => {
                                                console.log('Exit File Okay');
                                            }
                                        },
                                        {
                                            text: 'No',
                                            role: 'cancel',
                                            cssClass: 'btn-outline',
                                            handler: () => {
                                                console.log('Exit File Cancel');
                                            }
                                        }
                                        
                                    ]}
                                />
                            </IonRow>

                            <div className="chat-screens">
                                {LoadChatData.map((item: any, ind1: any) => (
                                    <div className="chat-screens-date">
                                        <div className="chat-date">{item.chat_date}</div>
                                        
                                        {item.chat_data.map((itemchat: any, ind2: any) => (
                                            <div key={ind2} className={localStorage.getItem('user_id')==itemchat.user_id ? "message received" : "message sender"}>
                                                <p>{itemchat.custom_message}</p>
                                                <span className="chat-time">{itemchat.firstname} {itemchat.lastname} - {itemchat.created_date}</span>
                                            </div>
                                        ))}
                                    </div>
                                ))}

                                <div className="type-message">
                                    <IonButton className="emoji-popup-button" fill="clear" >
                                        <IonIcon icon={happyOutline} />
                                    </IonButton>
                                    <IonTextarea
                                        mode="md"
                                        name="sendMessageBox"
                                        className="form-control"
                                        value={SendMessage}
                                        onIonChange={e => setSendMessage(e.detail.value!)}
                                    />
                                    <IonButton className="send-message-button" fill="clear" onClick={() => ClickSendMsg(val_chat_id)} >
                                        <IonIcon icon={sendSharp} />
                                    </IonButton>
                                </div>
                            </div>
                        </IonCol>
                        {/* dashboar content end */}

                        {/* dashboar posts start */}
                        <IonCol  className="dashboard-posts" size="12" sizeMd="12" sizeLg="3" sizeXl="2">
                            
                        </IonCol>
                        {/* dashboar post end */}
    
                    </IonRow>
                </IonGrid>
              
                <Footer />

            </IonContent>

            <FooterMobile />
      </IonPage>
    );
  };
  
  export default Chat;