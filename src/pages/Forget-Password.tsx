import React, {useRef, useState, useEffect} from "react";
import { useTranslation } from "react-i18next";

import { 
   IonContent, 
   IonPage, 
   IonLabel, 
   IonInput, 
   IonButton, 
   IonGrid, 
   IonRow, 
   IonCol,  
   IonAlert,
   useIonViewWillEnter
} from '@ionic/react';

import { useHistory } from "react-router-dom";

import { FormProvider, useForm } from "react-hook-form";
import { ErrorMessage } from '@hookform/error-message';

import Header from '../components/Header';

import '../assets/css/Custom.css';
import '../assets/css/Responsive.css';
import { Routes } from "../App";

import { globalConst } from "../constants";

import axios from "axios";
axios.defaults.baseURL = globalConst.baseURL;
axios.defaults.headers.post['Content-Type'] = globalConst.ContentType;
axios.defaults.headers.post['Access-Control-Allow-Origin'] = globalConst.AllowOrigin;
axios.defaults.headers.post['Accept'] = globalConst.Accept;
axios.defaults.timeout = globalConst.timeout;

const ForgetPassword: React.FC = () => {

   const { t } = useTranslation();
   const methods = useForm();
   const { register, handleSubmit, formState: { errors } } = methods;

   const [UserNotFoundAlert, setUserNotFoundAlert] = useState(false);
   const [SendMailAlert, setSendMailAlert] = useState(false);

   let history = useHistory();

   useIonViewWillEnter(() => {
      if ((localStorage.getItem('loginState')=='1')) {
         history.push(`${Routes.UserDashboard}`);
      } 
   });
   
   const onSubmit = (data: any) => {
      console.log(data.email);

      if (data.email!='') {
         const api = axios.create();
         api.get("/auth/token/refresh").then((response) => {
     
             axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
             const api = axios.create();

             const ResetCodeData = {
                 reset_code: getRandomString(5),
                 email: data.email
             };

             api.put("/auth/updateResetCode", ResetCodeData).then((resData) => {
                 const resReturnData = JSON.parse(JSON.stringify(resData)).data;
                 console.log(resReturnData);

                 setSendMailAlert(true);
             })
             .catch((error) => {
               setUserNotFoundAlert(true);
             })
         })
         .catch((error) => {
            alert("Token not Get!");
         })
      }
 };

   const doNothing = () => {
      history.goBack();
   }

   function getRandomString(length) {
      var randomChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
      var result = '';
      for ( var i = 0; i < length; i++ ) {
          result += randomChars.charAt(Math.floor(Math.random() * randomChars.length));
      }
      return result;
  }

   return (
      <IonPage>

         <Header class="with-back-arrow"  onBack={doNothing} />

         <IonContent fullscreen>
            <IonGrid>
                <IonRow className="login-form-row">
                     <IonCol size="10" sizeMd="6" sizeLg="4">
                        <FormProvider {...methods}>
                           <form onSubmit={handleSubmit(onSubmit)}>
                              <h2>{t('Reset-Password')}</h2>
                              <p>{t('Reset-Password-Text')}</p>

                              <IonLabel className="form-lable ion-margin-top" >{t('Reset-Password-Email')}</IonLabel>
                              <IonInput
                                 mode="md"
                                 type="email"
                                 {...register('email', {
                                    required: t('Login-Error-email'),
                                    pattern: {
                                    value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                                    message: t('Login-Error-email-Invalid')
                                    }
                                 })}
                              />
                              <ErrorMessage
                                 errors={errors}
                                 name="email"
                                 as={<div className="error-message" style={{ color: 'red' }} />}
                              />

                              <IonButton type="submit"  className="secondary-button ion-margin-top" expand="block" shape="round" fill="outline" >
                                 {t('Reset-Password-Send')}
                              </IonButton>

                              <div  className="back-to-login ion-text-right">
                                 <IonButton routerLink={Routes.indexURL} fill="clear">
                                    {t('Reset-Password-Back')}
                                 </IonButton>
                              </div>

                           </form>
                        </FormProvider>

                        <IonAlert
                           isOpen={UserNotFoundAlert}
                           onDidDismiss={() => setUserNotFoundAlert(false)}
                           cssClass='red-alert'
                           mode='md'
                           header={t('Reset-Password-Error')}
                           message={t('Reset-Password-Usernot-msg')}
                           buttons={[
                              {
                                    text: 'Close',
                                    role: 'cancel',
                                    cssClass: 'btn-primary',
                                    handler: () => {
                                       //setShowMessage(true);
                                    }
                              }
                              
                           ]}
                        />

                        <IonAlert
                           isOpen={SendMailAlert}
                           onDidDismiss={() => setSendMailAlert(false)}
                           cssClass='orange-alert'
                           mode='md'
                           header={t('Reset-Password-Success')}
                           message={t('Reset-Password-Success-msg')}
                           buttons={[
                              {
                                    text: 'Close',
                                    role: 'cancel',
                                    cssClass: 'btn-primary',
                                    handler: () => {
                                       //setShowMessage(true);
                                    }
                              }
                              
                           ]}
                        />
                    </IonCol>

                </IonRow>
            </IonGrid>
         </IonContent>

      </IonPage>
   );
};

export default ForgetPassword;