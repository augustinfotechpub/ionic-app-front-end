import React, {useRef, useState, useEffect} from "react";
import { useTranslation } from "react-i18next";

import { 
    IonContent, 
    IonPage, 
    IonGrid, 
    IonRow, 
    IonCol, 
    IonSlide,
    IonLabel,
    IonInput,
    IonButton,
    IonImg,
    IonDatetime,
    IonLoading,
    IonAlert,
    IonItem,
    IonRadio,
    IonListHeader,
    IonRadioGroup,
    useIonViewWillEnter
} from '@ionic/react';

import { FormProvider, useForm } from "react-hook-form";
import { ErrorMessage } from '@hookform/error-message';
import { RouteComponentProps, Switch, useLocation } from "react-router";

import '../assets/css/Custom.css';
import '../assets/css/Responsive.css';
import { Routes } from "../App";

import { globalConst } from "../constants";
import axios from "axios";
import { Console } from "console";
axios.defaults.baseURL = globalConst.baseURL;
axios.defaults.headers.post['Content-Type'] = globalConst.ContentType;
axios.defaults.headers.post['Access-Control-Allow-Origin'] = globalConst.AllowOrigin;
axios.defaults.headers.post['Accept'] = globalConst.Accept;
axios.defaults.timeout = globalConst.timeout;

interface PageProps extends RouteComponentProps {path: string; history: any; match: any; location: any;}
const PrintRentReceipt: React.FC<PageProps> = ({ path, history, match, location }) => {

    const { t } = useTranslation();
    const methods = useForm();
    const { register, trigger, reset, watch , handleSubmit, control, setValue, getValues, formState: { errors } } = methods;

    const [TotalPaid, setTotalPaid] = useState('');
    const [PaidDate, setPaidDate] = useState('');

    const [RetLoadData, setRetLoadData] = useState({'floorAptNo': '', 'RentPeriod': '', 'PaymentMethod': '', 'CardNumber': '', 'RentCost': ''});

    useIonViewWillEnter(() => {
        if (localStorage.getItem('loginState')!='1') {
            history.push(`${Routes.Home}`);
        }

        if (localStorage.getItem('user_type_id')!='1') {
            history.push(`${Routes.UserDashboard}`);
        }
    });

    useEffect(() => {
        
        if (match.params.id>0) {
            loadData(match.params.id);
        } else {
            //showError()
        }
        
    }, [match.params.id]);

    async function loadData(PopID: any) {
        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {

            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            api.get("/propertyleases/"+PopID+"/"+localStorage.getItem('user_id')).then((resPData) => {
                const retData = JSON.parse(JSON.stringify(resPData)).data;

                if (retData) {
                    setRetLoadData(retData);
                    setTotalPaid("$" + retData.RentCost + " CAD");
                    setPaidDate(retData.PaidDate);
                }

                window.print();
            })
            .catch((error) => {
                alert("Error Get Data!");
            })
        })
        .catch((error) => {
          alert("Token not Get!");
        })
    }

    return (
        <IonGrid className="rent-pay-receipt-popup-wrapper">
            <div className="popup-title">
                <h4>{t('Billing-History-Receipt')}</h4>
            </div> 
            <IonRow className="receipt-details">
                <IonCol size="6">
                    <IonLabel>{t('Rent-Apartment')}</IonLabel>
                </IonCol>
                <IonCol size="6">
                    {RetLoadData.floorAptNo}
                </IonCol>

                <IonCol size="6">
                    <IonLabel>{t('Rent-Manage-SearchTool-RentPeriod')}</IonLabel>
                </IonCol>
                <IonCol size="6">
                    {RetLoadData.RentPeriod}
                </IonCol>

                <IonCol size="6">
                    <IonLabel>{t('Billing-History-Method')}</IonLabel>
                </IonCol>
                <IonCol size="6">
                    {RetLoadData.PaymentMethod}
                </IonCol>

                <IonCol size="6">
                    <IonLabel>{t('Billing-History-CardNo')}</IonLabel>
                </IonCol>
                <IonCol size="6">
                    {RetLoadData.CardNumber}
                </IonCol>

                <IonCol size="6">
                    <IonLabel>{t('Rent-Cost')}</IonLabel>
                </IonCol>
                <IonCol size="6">
                    $ {RetLoadData.RentCost}
                </IonCol>
            </IonRow>

            <IonRow  className="receipt-amount">
                <IonCol size="12">
                    <IonLabel>{t('Billing-History-TotalPaid')}</IonLabel>
                </IonCol>
                <IonCol size="12">
                    <p className="total-rent-paid"><b>{TotalPaid}</b></p>
                </IonCol>
                <p className="rent-paid-date">{t('Billing-History-Paid')}: <span className="paid-date">{PaidDate}</span></p>
            </IonRow>
        </IonGrid> 

    );
  };
  
  export default PrintRentReceipt;