import React, {useEffect, useRef, useState} from "react";

import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";

import { 
    IonContent, 
    IonPage,
    IonGrid,
    IonRow,
    IonCol, 
    IonButton, 
    IonIcon,
    IonLoading,
    useIonViewWillEnter
} from '@ionic/react';

import HeaderMain from '../components/Header-main';
import Footer from '../components/Footer';
import FooterMobile from '../components/Footer-mobile';
import DashboardSidebar from '../components/Dahsboard-sidebar';
import { FaqAccordion } from "../components/FaqAccordion";

import '@ionic/react/css/flex-utils.css';
import '../assets/css/Custom.css';
import '../assets/css/Responsive.css';
import { Routes } from "../App";

import { globalConst } from "../constants";
import axios from "axios";
axios.defaults.baseURL = globalConst.baseURL;
axios.defaults.headers.post['Content-Type'] = globalConst.ContentType;
axios.defaults.headers.post['Access-Control-Allow-Origin'] = globalConst.AllowOrigin;
axios.defaults.headers.post['Accept'] = globalConst.Accept;
axios.defaults.timeout = globalConst.timeout;

const FAQ: React.FC<{ path: string }> = ({path}) => {

    const { t } = useTranslation();
    const contentRef = useRef<HTMLIonContentElement | null>(null);
    let history = useHistory();

    const [showLoading, setShowLoading] = useState(true);

    const scrollToBottom= () => {
        contentRef.current && contentRef.current.scrollToBottom(500);
    };

    const [faqsData, setFaqsData] = useState<Array<any>>([{"id": '', "title": "", "content": ""}]);

    useIonViewWillEnter(() => {
        setShowLoading(true);
        if (localStorage.getItem('loginState')!='1') {
            history.push(`${Routes.Home}`);
        } else {
            setFaqsData([]);
            loadFaqData();
        }
    });

    async function loadFaqData() {
        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {

            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            api.get("/faqdata/alldatabyusertype/"+localStorage.getItem('user_type_id')).then((resUnitData) => {
                const retData = JSON.parse(JSON.stringify(resUnitData)).data;
                //console.log(retData);
                setFaqsData(retData.FaqData);
                setShowLoading(false);
            })
            .catch((error) => {
                alert("Error Get Data!");
            })
        })
        .catch((error) => {
          alert("Token not Get!");
        })
    }

    const renderFaqData = () => {
        return faqsData.map((item, i) => {
            return (
                <div key={i}>
                    <FaqAccordion
                        id={item.id}
                        title={item.title}
                        content={item.content}
                    />
                </div>
            );
        })
    }

    const doNothing = () => {
        history.goBack();
    }

    return (
        <IonPage>
  
           <HeaderMain pageTitle={t('FAQ-Title')}  logoHide="hide-logo" onBack={doNothing} />
  
           <IonContent className="dashboard-wrapper lease-info-wrapper" ref={contentRef} scrollEvents={true} fullscreen>
                <IonLoading
                    isOpen={showLoading}
                    onDidDismiss={() => setShowLoading(false)}
                    message={'Loading...'}
                />

                <IonGrid className="dashboard-main-grid">
                    <IonRow className="dashboard-main-row">
                        
                        {/* sidebar start  */}
                        <DashboardSidebar path={path} />
                        {/* sidebar end  */}

                        {/* dashboar content start */}
                        <IonCol className="dashboard-content" size="12" sizeMd="12" sizeLg="6" sizeXl="8">
                            <div className="dashboard-content-inner">
                                <div className="manage-posts-wrapper">                                
                                    <div className="posts-list">
                                        {renderFaqData()}
                                    </div>
                                </div>
                            </div>

                        </IonCol>
                        {/* dashboar content end */}


                        {/* dashboar posts start */}
                        <IonCol  className="dashboard-posts" size="12" sizeMd="12" sizeLg="3" sizeXl="2">
                            
                        </IonCol>
                        {/* dashboar post end */}
    
                    </IonRow>
                </IonGrid>

                <IonButton className="scroll-to-bottom-btn" onClick={scrollToBottom} fill="clear">
                    <IonIcon icon="assets/images/double-arrow-down.svg" />
                </IonButton>
              
                <Footer />

            </IonContent>

            <FooterMobile />
      </IonPage>
    );
};

export default FAQ;