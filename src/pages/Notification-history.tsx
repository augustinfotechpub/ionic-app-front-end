import React, {useRef, useState, useEffect, useCallback} from "react";
import { useHistory, useLocation } from 'react-router-dom';
import { useTranslation } from "react-i18next";

import { 
    IonContent, 
    IonPage,
    IonGrid,
    IonRow,
    IonCol, 
    IonButton, 
    IonIcon,
    IonLoading,
    useIonViewWillEnter
} from '@ionic/react';

import { alertCircleSharp } from "ionicons/icons";

import HeaderMain from '../components/Header-main';
import Footer from '../components/Footer';
import FooterMobile from '../components/Footer-mobile';
import DashboardSidebar from '../components/Dahsboard-sidebar';
import NotificationBlock from '../components/NotificationBlock';

import '@ionic/react/css/flex-utils.css';
import '../assets/css/Custom.css';
import '../assets/css/Responsive.css';
import { Routes } from "../App";

import { globalConst } from "../constants";

import axios from "axios";
axios.defaults.baseURL = globalConst.baseURL;
axios.defaults.headers.post['Content-Type'] = globalConst.ContentType;
axios.defaults.headers.post['Access-Control-Allow-Origin'] = globalConst.AllowOrigin;
axios.defaults.headers.post['Accept'] = globalConst.Accept;
axios.defaults.timeout = globalConst.timeout;

const NotificationHistory: React.FC<{ path: string }> = ({path}) => {

    const { t } = useTranslation();
    const [showLoading, setShowLoading] = useState(true);
    const [SendData, setSendData] = useState([{"Text": "", "CntData": 0, "Data": [{"id": ""}]}]);

    const contentRef = useRef<HTMLIonContentElement | null>(null);
    let history = useHistory();

    const scrollToBottom= () => {
        contentRef.current && contentRef.current.scrollToBottom(500);
    };

    useIonViewWillEnter(() => {
        if (localStorage.getItem('loginState')!='1') {
            history.push(`${Routes.Home}`);
        } else {
            loadNotificationData();
        }
    });

    const doNothing = () => {
        history.goBack();
    }

    async function loadNotificationData() {
        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {

            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            const sendData = {
                UserID: localStorage.getItem('user_id'),
                UserTypeID: localStorage.getItem('user_type_id'),
                LeaseCode: localStorage.getItem('user_code') 
            };

            api.put("/notification/alldatabyuser", sendData).then((getData) => {
                const retData = JSON.parse(JSON.stringify(getData)).data;
                console.log(retData);

                setSendData(retData.SendData);
                

                setShowLoading(false);

            })
            .catch((error) => {
                alert("Error Get Data!");
            })
        })
        .catch((error) => {
          alert("Token not Get!");
        })
    }

    
    return (
        <IonPage>
  
           <HeaderMain pageTitle={t('Notification-History')}  logoHide="hide-logo" onBack={doNothing} />
  
           <IonContent className="dashboard-wrapper lease-info-wrapper" ref={contentRef} scrollEvents={true} fullscreen>
                <IonLoading
                    isOpen={showLoading}
                    onDidDismiss={() => setShowLoading(false)}
                    message={'Loading...'}
                />

                <IonGrid className="dashboard-main-grid">
                    <IonRow className="dashboard-main-row">
                        
                        {/* sidebar start  */}
                        <DashboardSidebar path={path} />
                        {/* sidebar end  */}

                        {/* dashboar content start */}
                        <IonCol className="dashboard-content notification-history" size="12" sizeMd="12" sizeLg="6" sizeXl="8">
                            <div className="dashboard-content-inner">
                                <p>{t('Notification-Text1')}</p>
                                <div className="notification-list">
                                    {SendData.map((item, index) => (
                                        <NotificationBlock SIndex={index} SText={item.Text} SData={item.Data} SCntData={item.CntData} />
                                    ))}
                                </div>
                            </div>
                        </IonCol>
                        {/* dashboar content end */}


                        {/* dashboar posts start */}
                        <IonCol  className="dashboard-posts" size="12" sizeMd="12" sizeLg="3" sizeXl="2">
                            
                        </IonCol>
                        {/* dashboar post end */}
    
                    </IonRow>
                </IonGrid>

                <IonButton className="scroll-to-bottom-btn" onClick={scrollToBottom} fill="clear">
                    <IonIcon icon="assets/images/double-arrow-down.svg" />
                </IonButton>
              
                <Footer />

            </IonContent>

            <FooterMobile />
      </IonPage>
    );
};

export default NotificationHistory;