import { 
  IonPage, 
  useIonViewWillEnter,
  useIonViewDidLeave,
} from '@ionic/react';

import React, {useState, useEffect} from 'react';
import { useHistory } from "react-router-dom";

import '../assets/css/Custom.css';
import '../assets/css/Responsive.css';
import { Routes } from '../App';

const Logout: React.FC = () => {

  let history = useHistory();

  useIonViewWillEnter(() => {
    //console.log('useIonViewWillEnter Logout');
    loadLogoutData();
    window.location.assign(`${Routes.Home}`);
  });

  useIonViewDidLeave(() => {
    //console.log('useIonViewDidLeave Logout');
  });

  async function loadLogoutData() {
    localStorage.setItem('accessCode', '');
    localStorage.setItem('loginState', '0');
    localStorage.setItem('user_id', '');
    localStorage.setItem('user_firstname', '');
    localStorage.setItem('user_lastname', '');
    localStorage.setItem('user_phoneno', '');
    localStorage.setItem('user_email', '');
    localStorage.setItem('sel_prop_id', '');
    localStorage.setItem('sel_prop_name', '');
    localStorage.setItem('sel_prop_suffix', '');
    localStorage.setItem('user_type_id', '');
    localStorage.setItem('user_type_name', '');
    localStorage.setItem('user_code', '');
    localStorage.setItem('user_management_id', '');
    localStorage.setItem('manage_posts', '');
    localStorage.setItem('manage_propertys', '');
    localStorage.setItem('manage_rents', '');
  }

  return (
    <IonPage>
    </IonPage>
  );
};

export default Logout;