import React, {useEffect, useRef, useState} from "react";
import { useLocation, RouteComponentProps } from "react-router";
import { useTranslation } from "react-i18next";

import { 
    IonContent, 
    IonPage,
    IonGrid,
    IonRow,
    IonCol, 
    IonList,
    IonInput, 
    IonDatetime,
    IonLabel, 
    IonButton, 
    IonIcon,
    IonCard,
    IonCardHeader,
    IonCardTitle,
    IonCardContent,
    IonItem,
    IonAlert,
    IonLoading,
    IonImg,
    IonFab,
    IonFabButton,
    IonActionSheet,
    useIonViewWillEnter
} from '@ionic/react';

import { FormProvider, useForm, Controller, useFieldArray } from "react-hook-form";
import { ErrorMessage } from '@hookform/error-message';

import { camera, trash, close, attachOutline, chevronDown } from "ionicons/icons";

import HeaderMain from '../components/Header-main';
import Footer from '../components/Footer';
import FooterMobile from '../components/Footer-mobile';
import DashboardSidebar from '../components/Dahsboard-sidebar';

import { usePhotoGallery, UserPhoto  } from '../hooks/usePhotoGallery';

import '@ionic/react/css/flex-utils.css';
import '../assets/css/Custom.css';
import '../assets/css/Custom-Dev.css'
import '../assets/css/Responsive.css';
import { Routes } from "../App";

import { globalConst } from "../constants";

import axios from "axios";
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';
axios.defaults.baseURL = globalConst.baseURL;
axios.defaults.headers.post['Content-Type'] = globalConst.ContentType;
axios.defaults.headers.post['Access-Control-Allow-Origin'] = globalConst.AllowOrigin;
axios.defaults.headers.post['Accept'] = globalConst.Accept;
axios.defaults.timeout = globalConst.timeout;

interface PageProps extends RouteComponentProps {path: string; history: any; match: any; location: any;}

const AddNewLease: React.FC<PageProps> = ({ path, history, match, location }) => {

    const [StorageKey, setStorageKey] = useState(getRandomString(12));
    const { t } = useTranslation();

    const [locPath, setlocPath] = useState('');
    const contentRef = useRef<HTMLIonContentElement | null>(null);
    const { deletePhoto, photos, takePhoto } = usePhotoGallery(StorageKey);
    const [photoToDelete, setPhotoToDelete] = useState<UserPhoto >();
    const [filepath,setFilepath] = useState();


    const [showLoading, setShowLoading] = useState(true);

    const [showAlert, setShowAlert] = useState(false);
    const [msgAlert, setMsgAlert] = useState('');
    const [headAlert, setHeadAlert] = useState('');

    const locationData = useLocation();

    const [pid, setpid] = useState('');

    const methods = useForm();
    const { register, trigger, reset, watch , handleSubmit, control, setValue, getValues, formState: { errors } } = methods;
    const { fields, append, remove } = useFieldArray({ name: 'tenants', control });

    const defaultList = [
        { firstName: "", lastName: "" },
    ];

    const [inputList, setInputList] = useState(defaultList);

    const scrollToBottom= () => {
        contentRef.current && contentRef.current.scrollToBottom(500);
    };

    const [selectedDate, setSelectedDate] = useState<string>('');

    useIonViewWillEnter(() => {
        if (localStorage.getItem('loginState')!='1') {
            history.push(`${Routes.Home}`);
        } else {
            if ((localStorage.getItem('user_type_id')=='2') || ((localStorage.getItem('user_type_id')=='3') && (localStorage.getItem('manage_propertys')=='1'))) {
                //loadConfigData();
            } else {
                history.push(`${Routes.UserDashboard}`);
            }
        }
    });

    useEffect(() => {
        if (locationData.state) {
            setShowLoading(false);
            setlocPath(location.state.path);

            //var crypto = require("crypto");
            //var id = crypto.randomBytes(12).toString('hex');
            
            //alert(getRandomString(12));

            //console.log(locationData.state);
            //console.log(location.state.SearchPID);
            //console.log(location.state.SearchFloorNo);
            //console.log(location.state.SearchAptNo);
            //console.log(location.state.SearchID);

            setValue('leaseCode', StorageKey)
            //setValue('leaseCode', getRandomString(12))
            
            //getPropFloorData(location.state.SearchID);
            getPropFloorData(location.state.SearchPID, location.state.SearchAptNo);
            setpid(location.state.SearchPID);
        } else {
            showError()
        }
    }, []);

    async function getPropFloorData(PID: any, AptNo: any) {
        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {
    
          axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
          const api = axios.create();
    
            const searchInfo = {
                "PropID" : PID,
                "AptNo" : AptNo,
                "UserID" : localStorage.getItem('user_id'),
            };

            api.put("/propertyfloors/checkExists", searchInfo).then((resPData) => {
                const retData = JSON.parse(JSON.stringify(resPData)).data;
                console.log('Vivan');
                console.log(retData);

                const fields = ['floorAptNo', 'floorNo', 'floorPropID', 'floorID'];
                fields.forEach(field => {
                    setValue(field, retData[field])
                });
            })
            .catch((error) => {
                alert("Error Get Data!");
            })
      

        })
        .catch((error) => {
          alert("Token not Get!");
        })
    }

    const showError = (): void => {
        setShowLoading(false);
        
        setMsgAlert(t('Tenants-Lease-Property-not-selected'));
        setHeadAlert(t('Tenants-Lease-Property-Lease'));
        setShowAlert(true);

        //history.goBack();
    }

    function getRandomString(length) {
        var randomChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        var result = '';
        for ( var i = 0; i < length; i++ ) {
            result += randomChars.charAt(Math.floor(Math.random() * randomChars.length));
        }
        return result;
    }

    const clickWillDismiss = (FlagDismiss) => {
        if (FlagDismiss) {
            if (pid==''){
                history.push(`${Routes.manageProperties}`);
            } else  {
                history.push({
                    pathname: `${Routes.manageTenants}`,
                    state: { path: `${Routes.manageTenants}`, prop_id: pid }
                });
            }
        }
    };

    const doNothing = () => {
        history.goBack();
    }

    // handle input change
    const handleInputChange = (e:any, index:number) => {
        // const { name, value } = e.target;
        // const list = [...inputList];
        // list[index][name] = value;
        // setInputList(list);
    };
    
    // handle click event of the Remove button
    const handleRemoveClick = (index:any) => {
        const list = [...inputList];
        list.splice(index, 1);
        setInputList(list);
    };

    // handle click event of the Add button
    const handleAddClick = () => {
        setInputList([...inputList, { firstName: "", lastName: "" }]);
    };

    const renderList = () => {
        return inputList.map((x, i) => {
            return (
                <div key={i} className="tenant-lease-details-info tenants-info">
                    <div className="tenant-title-wrap">
                        <h4>{t('Tenants-Lease-Tenant')} {i + 1}</h4>
                        <IonButton fill="clear" onClick={() => handleRemoveClick(i)}><IonIcon icon={close} /></IonButton>
                    </div>
                    <IonRow>
                        <IonCol size="12" sizeMd="6" sizeLg="6" sizeXl="6">
                            <IonLabel>{t('Tenants-Lease-First-Name')}*</IonLabel>
                            <IonInput mode="md" type="text" {...register(`tenants.${i}.firstName`, {required: t('Tenants-Lease-First-Name-error')})} ></IonInput>
                        </IonCol>
                        <ErrorMessage
                            errors={errors}
                            name={`tenants.${i}.firstName`}
                            as={<div className="error-message" style={{ color: 'red' }} />}
                        />

                        <IonCol size="12" sizeMd="6" sizeLg="6" sizeXl="6">
                            <IonLabel>{t('Tenants-Lease-Last-Name')}*</IonLabel>
                            <IonInput mode="md" type="text" {...register(`tenants.${i}.lastName`, {required: t('Tenants-Lease-Last-Name-error')})} ></IonInput>
                        </IonCol>
                        <ErrorMessage
                            errors={errors}
                            name={`tenants.${i}.lastName`}
                            as={<div className="error-message" style={{ color: 'red' }} />}
                        />
                    </IonRow>
                </div>
            );
        })
    }

    const testDa = async (fieldsbunch: any) => {
        const result = await trigger(fieldsbunch);
        if (!result) return;
    };

    const onSubmit = (data: any) => {
        //console.log(data);
        //console.log(photos);

        //var test = data.leaseEndDate;

        //console.log(data.leaseEndDate.substring(0, 10));
        //return false;
        
        const api = axios.create();
        api.get("/auth/token/refresh").then(res => res)
        .then((response) => {
            
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            var lease_start_date = data.leaseStartDate.substring(0, 10);
            var lease_end_date = data.leaseEndDate.substring(0, 10);
            var sYear, sMonth, sDate, eYear, eMonth, eDate;

            [ sYear, sMonth, sDate ] = lease_start_date.split('-');
            [ eYear, eMonth, eDate ] = lease_end_date.split('-');

            const addLeaseData = {
                "current_user" : localStorage.getItem('user_id'),
                "property_id" : data.floorPropID,
                "floorsno" : data.floorNo,
                "aptno" : data.floorAptNo,
                "lease_code" : data.leaseCode,
                "oldleaseCode" : '',
                "lease_price" : data.currentLeasePrice,
                "sYear" : sYear,
                "sMonth" : sMonth,
                "sDate" : sDate,
                "eYear" : eYear,
                "eMonth" : eMonth,
                "eDate" : eDate,
                "lease_start_date" : lease_start_date,
                "lease_end_date" : lease_end_date,
                "tenantsData" : data.tenants,
                "fileData" : photos,
            };

            //console.log(addLeaseData);
            api.post("/propertyleases/", addLeaseData).then((retData) => {
                const retDataArr = JSON.parse(JSON.stringify(retData));

                setMsgAlert(t('Tenants-Lease-Property-Lease-Add-Msg'));
                setHeadAlert(t('Tenants-Lease-Property-Lease-Add-Title'));
                setShowAlert(true);
            
                //return false;
            }).catch((error) => {
                alert("Error found in put Data");
                console.log(error);
            });
      
       
        })
        .catch((error) => {
          alert("Token not Get!");
        })
    };

    const ExitPage = async (path) => {
        if (pid!='') {
            history.push({
                pathname: path,
                state: { path: path, prop_id: pid }
            });
        } else {
            history.push(`${Routes.manageProperties}`);
        }
    };

    const onClickPhotoData=(photo:any)=>{
        setPhotoToDelete(photo) 
        setFilepath(photo.filepath)
    }

    return (
        <IonPage>
           <HeaderMain pageTitle={t('Tenants-Lease-Title')}  logoHide="hide-logo" onBack={doNothing} />
  
           <IonContent className="dashboard-wrapper lease-info-wrapper" ref={contentRef} scrollEvents={true} fullscreen>
                <IonLoading
                    isOpen={showLoading}
                    onDidDismiss={() => setShowLoading(false)}
                    message={'Loading...'}
                />

                <IonAlert
                    isOpen={showAlert}
                    onDidDismiss={() => setShowAlert(false)}
                    onWillDismiss={() => clickWillDismiss(true)}
                    cssClass='orange-alert'
                    header={headAlert}
                    message={msgAlert}
                    buttons={[{text: 'Close', cssClass: 'btn-primary'}]}
                />

                <IonGrid className="dashboard-main-grid">
                    <IonRow className="dashboard-main-row">
                        
                        {/* sidebar start  */}
                        <DashboardSidebar path={locPath} />
                        {/* sidebar end  */}

                        {/* dashboar content start */}
                        <IonCol className="dashboard-content" size="12" sizeMd="12" sizeLg="6" sizeXl="8">
                            <FormProvider {...methods}>
                                <form onSubmit={handleSubmit(onSubmit)}>
                                    <IonRow className="apartment-lease-code-info">
                                        <IonCol className="apartment-number" size="4" sizeMd="4" sizeLg="4" sizeXl="3">
                                            <IonLabel>{t('Tenants-Lease-Apartment')} #</IonLabel>
                                            <IonInput mode="md" type="text" readonly {...register('floorAptNo')} />

                                            <IonInput class="hide-input" mode="md" type="text" readonly {...register('floorID')} />
                                            <IonInput class="hide-input" mode="md" type="text" readonly {...register('floorPropID')} />
                                            <IonInput class="hide-input" mode="md" type="text" readonly {...register('floorNo')} />
                                        </IonCol>
                                        <IonCol className="lease-code-info" size="8" sizeMd="8" sizeLg="7" sizeXl="6">
                                            <div className="lease-code">
                                                <IonLabel>{t('Tenants-Lease-Lease-Code')}</IonLabel>
                                                <IonInput mode="md" type="text" readonly {...register('leaseCode')} />
                                            </div>
                                        </IonCol>
                                    </IonRow>

                                    <IonCard className="tenant-lease-details-card">
                                        <IonCardHeader>
                                            <IonCardTitle>{t('Tenants-Lease-Tenant-Details')}</IonCardTitle>
                                        </IonCardHeader>

                                        <IonCardContent>
                                            {renderList()}

                                            <IonButton fill="solid" shape="round" onClick={handleAddClick}>
                                                <IonIcon icon="assets/images/plus-icon-gray.svg" />
                                                <span className="">{t('Tenants-Lease-Add-Tenant')}</span>
                                            </IonButton>
                                        </IonCardContent>
                                    </IonCard>

                                    <IonCard className="tenant-lease-details-card">
                                        <IonCardHeader>
                                            <IonCardTitle>{t('Tenants-Lease-Details')}</IonCardTitle>
                                        </IonCardHeader>

                                        <IonCardContent>
                                            <div className="tenant-lease-details-info lease-info">
                                                <div>
                                                    <IonLabel>{t('Tenants-Lease-no-Tenants-Apartment')}</IonLabel>
                                                    <IonInput className="width-25" mode="md" type="number" min="1" value={inputList.length} {...register('numberOfTenants', {
                                                            required: t('Tenants-Lease-no-Tenants-Apartment-Msg'),
                                                            min: 1
                                                        })} name="numberOfTenants"></IonInput>
                                                    <ErrorMessage
                                                        errors={errors}
                                                        name="numberOfTenants"
                                                        as={<div className="error-message" style={{ color: 'red' }} />}
                                                    />
                                                </div>
                                                <div>
                                                    <IonLabel>{t('Tenants-Lease-Current-Lease-price')}</IonLabel>
                                                    <IonInput className="width-50" mode="md" type="number" {...register('currentLeasePrice', {
                                                            required: t('Tenants-Lease-Current-Lease-price-msg')
                                                        })} />
                                                    <ErrorMessage
                                                        errors={errors}
                                                        name="currentLeasePrice"
                                                        as={<div className="error-message" style={{ color: 'red' }} />}
                                                    />
                                                </div>
                                                <div>
                                                    <IonLabel>{t('Tenants-Lease-Start-Date')}*</IonLabel>
                                                    <div className="date-picker">
                                                        <IonDatetime displayFormat="MMM DD, YYYY" placeholder="Select Date" {...register('leaseStartDate', {
                                                            required: t('Tenants-Lease-Start-Date-msg')
                                                        })} value={selectedDate} ></IonDatetime>
                                                        <IonIcon icon="assets/images/calendar-icon.svg" />
                                                        <ErrorMessage
                                                            errors={errors}
                                                            name="leaseStartDate"
                                                            as={<div className="error-message" style={{ color: 'red' }} />}
                                                        />
                                                    </div>
                                                </div>
                                                <div>
                                                    <IonLabel>{t('Tenants-Lease-End-Date')}*</IonLabel>
                                                    <div className="date-picker">
                                                        <IonDatetime displayFormat="MMM DD, YYYY" placeholder="Select Date" {...register('leaseEndDate', {
                                                            required: t('Tenants-Lease-End-Date-msg')
                                                        })} value={selectedDate} ></IonDatetime>
                                                        <IonIcon icon="assets/images/calendar-icon.svg" />
                                                        <ErrorMessage
                                                            errors={errors}
                                                            name="leaseEndDate"
                                                            as={<div className="error-message" style={{ color: 'red' }} />}
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="lease-scan-block">
                                                <IonLabel>{t('Tenants-Lease-Scans')}</IonLabel>

                                                {photos.map((photo, index) => (
                                                    <div className="uploaded-file">
{/*                                                         <IonGrid>
                                                            <IonRow>
                                                                <IonCol size="3" key={index}>
                                                                    <IonImg onClick={() => onClickPhotoData(photo)} src={photo.webviewPath}  />
                                                                </IonCol>
                                                            </IonRow>
                                                        </IonGrid> */}

{/* <p>{photo.filepath}</p>
<p>{photo.fileurl}</p>
<p>{photo.webviewPath}</p>
 */}

                                                        <p className="uploaded-file-name">{photo.filepath}</p>
                                                        <IonButton fill="clear" onClick={() => onClickPhotoData(photo)} >
                                                            <IonIcon icon={close} />
                                                        </IonButton>
                                                    </div>
                                                ))}

                                                <div className="upload-photo-block">
                                                    {/* <IonButton className="upload-photo-btn" fill="solid" shape="round">
                                                        <IonIcon icon={attachOutline} />
                                                    </IonButton> */}
                                                    <IonButton className="take-photo-btn" fill="solid" shape="round" onClick={() => takePhoto()}>
                                                        <IonIcon icon="assets/images/add-photo.svg" />
                                                    </IonButton>
                                                </div>
                                            </div>
                                        </IonCardContent>
                                    </IonCard>
                                    
                                    <IonRow className="ion-justify-content-center">
                                        <IonCol className="ion-text-center">
                                            <IonButton onClick={() => testDa(['numberOfTenants', 'currentLeasePrice', 'leaseStartDate', 'leaseEndDate'])} className="submit-new-lease-btn" type="submit" fill="outline" shape="round">Submit</IonButton>
                                            <IonButton className="cancle-btn" onClick={() => ExitPage(Routes.manageTenants)} fill="solid" shape="round">Cancle</IonButton>
                                        </IonCol>
                                    </IonRow>
                                </form>
                            </FormProvider>
                        </IonCol>
                        {/* dashboar content end */}

                        {/* dashboar posts start */}
                        <IonCol  className="dashboard-posts" size="12" sizeMd="12" sizeLg="3" sizeXl="2">
                            
                        </IonCol>
                        {/* dashboar post end */}
    
                    </IonRow>
                </IonGrid>

                <IonAlert
                    isOpen={!!photoToDelete}
                    onDidDismiss={() => setPhotoToDelete(undefined)}
                    cssClass='red-alert'
                    mode='md'
                    header={t('Tenants-Lease-Remove-Image')}
                    message={t('Tenants-Lease-Remove-Image-msg')}
                    buttons={[{
                                text: 'Remove',
                                cssClass: 'btn-secondary',
                                //role: 'destructive',
                                ///icon: trash,
                                handler: () => {
                                if (photoToDelete) {
                                    deletePhoto(photoToDelete);
                                    setPhotoToDelete(undefined);
                                }
                                }
                            }, {
                                text: 'Cancel',
                                cssClass: 'btn-primary',//btn-outline
                                //icon: close,
                                //role: 'cancel'
                            }]}
                />

                <IonButton className="scroll-to-bottom-btn" onClick={scrollToBottom} fill="clear">
                    <IonIcon icon="assets/images/double-arrow-down.svg" />
                </IonButton>
              
                <Footer />

            </IonContent>

            <FooterMobile />
      </IonPage>
    );
};

export default AddNewLease;