import React, { useEffect, useRef, useState } from 'react';
import { useLocation, RouteComponentProps } from "react-router";
import { useTranslation } from "react-i18next";

import { 
   IonContent, 
   IonPage, 
   IonGrid, 
   IonRow,
   IonCol, 
   IonLabel, 
   IonInput, 
   IonButton, 
   IonSlides, 
   IonSlide, 
   IonAlert,
   IonLoading,
   useIonViewWillEnter
} from '@ionic/react';

import { FormProvider, useForm } from "react-hook-form";
import { ErrorMessage } from '@hookform/error-message';

import Header from '../components/Header';

import { globalConst } from "../constants";

import '@ionic/react/css/ionic-swiper.css';
import '../assets/css/Custom.css';
import '../assets/css/Custom-Dev.css';
import '../assets/css/Responsive.css';
import { Routes } from "../App";

import axios from "axios";
axios.defaults.baseURL = globalConst.baseURL;
axios.defaults.headers.post['Content-Type'] = globalConst.ContentType;
axios.defaults.headers.post['Access-Control-Allow-Origin'] = globalConst.AllowOrigin;
axios.defaults.headers.post['Accept'] = globalConst.Accept;
axios.defaults.timeout = globalConst.timeout;

const slideOpts = {
   initialSlide: 0,
   speed: 400
};

interface PageProps extends RouteComponentProps {path: string; history: any; match: any; location: any;}

const SignupData: React.FC<PageProps> = ({ path, history, match, location }) => {

   const { t } = useTranslation();
   const locationData = useLocation();
   const [showLoading, setShowLoading] = useState(true);

   const mySlides = useRef<any>(null);

   const [showAlert, setShowAlert] = useState(false);
   const [msgAlert, setMsgAlert] = useState('');
   const [headAlert, setHeadAlert] = useState('');
   const [handleDismiss, sethandleDismiss] = useState(false);

   const methods = useForm();
   const { register, trigger, handleSubmit, getValues, formState: { errors } } = methods;

   const [UTypeID, setUTypeID] = useState<string>();
  
   useEffect(() => {
      mySlides.current.lockSwipes(true);

      if (locationData.state) {
         setShowLoading(false);
         setUTypeID(location.state.userTypeSelID); 
      } else {
            showError()
      }
   });

   const showError = (): void => {
      setShowLoading(false);
      
      setMsgAlert(t('Signup-error-msg3'));
      setHeadAlert(t('Signup-error-head3'));
      setShowAlert(true);

      history.push(`${Routes.signup}`);
   }

   const next = async (fields: any) => {
      const result = await trigger(fields);
      if (!result) return;
      await mySlides.current.lockSwipes(false);
      await mySlides.current.slideNext();
      await mySlides.current.lockSwipes(true);
   };

   const prev = async () => {
      //console.log(mySlides.getActiveIndex());
      //if (!result) return;

      let index: number = 0;
      await mySlides.current.getActiveIndex().then((value: any) => (index=value));

      if (index==0) {
         history.goBack();
      } else {
         await mySlides.current.lockSwipes(false);
         await mySlides.current.slidePrev();
         await mySlides.current.lockSwipes(true);
      }
   };

   const clickWillDismiss = () => {
      if (handleDismiss) {
         //alert("SHILPA");
         //alert(handleDismiss);
         history.push("/");
      }
   };

   const onSubmit = (data: any) => {
      console.log(data);
      //console.log(`${UTypeID}`);
      //return false;

      const api = axios.create();
      api.get("/auth/token/refresh").then(res => res)
      .then((response) => {

        axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
        const api = axios.create();

         var Lcode: any
         if (typeof data.lease_code  === "undefined")
            Lcode = ''
         else 
            Lcode = data.lease_code

         var Acode: any
         if (typeof data.access_code === "undefined")
            Acode = ''
         else 
            Acode = data.access_code
  
        const registerData = {
         "usertype" : `${UTypeID}`,
         "firstname" : data.firstName,
         "lastname" : data.lastName,
         "phoneno" : data.telephone,
         "email" : data.email,
         "password" : data.password1,
         "lease_code" : Lcode,
         "access_code" : Acode
        };

        console.log('registerData');
        console.log(registerData);
  
        api.post("/auth/register", registerData).then((resRegister) => {
          const resRegisterData = JSON.parse(JSON.stringify(resRegister)).data;
          console.log(resRegisterData.returnFlag);

          if (resRegisterData.returnFlag=='error'){
            
            setMsgAlert(resRegisterData.message);
            setHeadAlert(t('Signup-error-head1'));
            sethandleDismiss(false);
            setShowAlert(true);

          } else {
            
            setMsgAlert(t('Signup-success-msg1'));
            setHeadAlert(t('Signup-success-head1'));
            sethandleDismiss(true);
            setShowAlert(true);

          }

         }).catch((error) => {
            setMsgAlert(t('Signup-error-msg4'));
            setHeadAlert(t('Signup-error-head4'));
            sethandleDismiss(false);
            setShowAlert(true);
   
            console.log(error);
        });
      
      })
      .catch((error) => {
        alert("Token not Get!");
      })
      
   };


   return (
      <IonPage>

         <Header class="with-back-arrow with-step-arrow"  onBack={prev} />

         <IonContent fullscreen>
            <IonLoading
               isOpen={showLoading}
               onDidDismiss={() => setShowLoading(false)}
               message={'Loading...'}
            />

            <IonAlert
                isOpen={showAlert}
                onDidDismiss={() => setShowAlert(false)}
                onWillDismiss={() => clickWillDismiss()}
                cssClass='orange-alert'
                header={headAlert}
                message={msgAlert}
                buttons={['Close']}
            />

            <IonGrid>
               <IonRow className="signup-form login-form-row">
                  <IonCol size="10" sizeMd="6" sizeLg="4">
                  <FormProvider {...methods}>
                     <form onSubmit={handleSubmit(onSubmit)}>
                        <IonSlides pager={true} options={slideOpts} ref={mySlides}>
                           <IonSlide>
                                 <IonGrid>
                                    <IonRow>
                                       <IonCol size="12" className="email-field">
                                          <IonLabel className="form-lable">{t('Signup-email')}*</IonLabel>
                                          <IonInput
                                             mode="md"
                                             type="email"
                                             {...register('email', {
                                                required: t('Login-Error-email'),
                                                pattern: {
                                                value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                                                message: t('Login-Error-email-Invalid')
                                                }
                                             })}
                                          />
                                          <ErrorMessage
                                             errors={errors}
                                             name="email"
                                             as={<div className="error-message" style={{ color: 'red' }} />}
                                          />
                                       </IonCol>

                                       <IonCol size="12" className="password-field">
                                          <IonLabel className="form-lable">{t('Signup-Password')}*</IonLabel>
                                          <IonInput 
                                             mode="md"
                                             type="password" 
                                             {...register('password1', {
                                                required: t('Login-Error-Password'),
                                                minLength: { value: 6, message: "Minimum length is 6. " },
                                                maxLength: { value: 12, message: "Maximum length is 12. " }
                                             })}
                                          />
                                          <ErrorMessage
                                             errors={errors}
                                             name="password1"
                                             as={<div className="error-message" style={{ color: 'red' }} />}
                                          />
                                       </IonCol>

                                       <IonCol size="12" className="forget-password">
                                          <IonLabel className="form-lable">{t('Signup-Confirm-Password')}*</IonLabel>
                                          <IonInput 
                                             mode="md"
                                             type="password" 
                                             {...register('password2', {
                                                // required: true,
                                                validate: {
                                                   noMatch: (value: string) => {
                                                      return value !== getValues("password1")
                                                         ? "Passwords do not match"
                                                         : undefined;
                                                   },
                                                },
                                             })}
                                          />
                                          <ErrorMessage
                                             errors={errors}
                                             name="password2"
                                             as={<div className="error-message" style={{ color: 'red' }} />}
                                          />
                                       </IonCol>

                                       <IonCol size="12" className="sign-up-btn">
                                          <IonButton expand="block" shape="round" fill="outline" onClick={() => next(['email','password1', 'password2'])}>
                                             {t('Signup-Continue')}
                                          </IonButton>

                                       </IonCol>

                                    </IonRow>
                                 </IonGrid>
                           </IonSlide>

                           <IonSlide>
                                 <IonGrid>
                                    <IonRow>
                                       <IonCol size="12" className="email-field">
                                          <IonLabel className="form-lable">{t('Signup-First-Name')}*</IonLabel>
                                          <IonInput
                                             mode="md"
                                             type="text"   
                                             {...register('firstName', {
                                                required: t('Signup-First-Name-error')
                                             })}
                                          />
                                          <ErrorMessage
                                             errors={errors}
                                             name="firstName"
                                             as={<div className="error-message" style={{ color: 'red' }} />}
                                          />
                                       </IonCol>

                                       <IonCol size="12" className="email-field">
                                          <IonLabel className="form-lable">{t('Signup-Last-Name')}*</IonLabel>
                                          <IonInput
                                             mode="md"
                                             type="text" 
                                             {...register('lastName', {
                                                required: t('Signup-Last-Name-error')
                                             })}
                                          />
                                          <ErrorMessage
                                             errors={errors}
                                             name="lastName"
                                             as={<div className="error-message" style={{ color: 'red' }} />}
                                          />
                                       </IonCol>

                                       <IonCol size="12" className="email-field">
                                          <IonLabel className="form-lable">{t('Signup-Telephone')}:</IonLabel>
                                          <IonInput
                                             mode="md"
                                             type="number" 
                                             {...register('telephone', {
                                                required: t('Signup-Telephone-error')
                                             })}
                                          />
                                          <ErrorMessage
                                             errors={errors}
                                             name="telephone"
                                             as={<div className="error-message" style={{ color: 'red' }} />}
                                          />
                                       </IonCol>

                                       {UTypeID=='1' ? (
                                          <IonCol size="12" className="email-field">
                                          <IonLabel className="form-lable">{t('Signup-Lease-Code')}</IonLabel>
                                          <IonInput
                                             mode="md"
                                             type="text"   
                                             {...register('lease_code')}
                                          />
                                          <ErrorMessage
                                             errors={errors}
                                             name="lease_code"
                                             as={<div className="error-message" style={{ color: 'red' }} />}
                                          />
                                       </IonCol>
                                       ) : ''}

                                       {UTypeID=='3' ? (
                                          <IonCol size="12" className="email-field">
                                          <IonLabel className="form-lable">{t('Signup-Access-Code')}</IonLabel>
                                          <IonInput
                                             mode="md"
                                             type="text"   
                                             {...register('access_code')}
                                          />
                                          <ErrorMessage
                                             errors={errors}
                                             name="access_code"
                                             as={<div className="error-message" style={{ color: 'red' }} />}
                                          />
                                       </IonCol>
                                       ) : ''}


                                       {/* <IonCol size="12" className="sign-up-btn">
                                          <IonButton expand="block" shape="round" fill="outline" onClick={() => prev()}>
                                             previous
                                          </IonButton>
                                       </IonCol> */}

                                       <IonCol size="12" className="sign-up-btn">
                                          <IonButton type="submit" expand="block" shape="round" fill="outline">
                                             {t('Signup-Finalize')}
                                          </IonButton>
                                       </IonCol>

                                    </IonRow>
                                 </IonGrid>
                           </IonSlide>

                        </IonSlides>
                     </form>
                  </FormProvider>
                  </IonCol>

               </IonRow>
            </IonGrid>
         </IonContent>

      </IonPage>
   );
};

export default SignupData;