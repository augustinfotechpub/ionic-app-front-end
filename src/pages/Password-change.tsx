import React, {useRef, useState, useEffect} from "react";
import { useHistory, useLocation } from 'react-router-dom';
import { useTranslation } from "react-i18next";

import { 
    IonContent, 
    IonPage, 
    IonGrid, 
    IonRow, 
    IonCol, 
    IonLabel,
    IonInput,
    IonButton,
    IonLoading,
    IonAlert,
    useIonViewWillEnter
} from '@ionic/react';

import HeaderMain from '../components/Header-main';
import Footer from '../components/Footer';
import FooterMobile from '../components/Footer-mobile';
import DashboardSidebar from '../components/Dahsboard-sidebar';

import '../assets/css/Custom.css';
import '../assets/css/Responsive.css';
import { Routes } from '../App';

import { FormProvider, useForm } from "react-hook-form";
import { ErrorMessage } from '@hookform/error-message';
  
import { globalConst } from "../constants";
import axios from "axios";
axios.defaults.baseURL = globalConst.baseURL;
axios.defaults.headers.post['Content-Type'] = globalConst.ContentType;
axios.defaults.headers.post['Access-Control-Allow-Origin'] = globalConst.AllowOrigin;
axios.defaults.headers.post['Accept'] = globalConst.Accept;
axios.defaults.timeout = globalConst.timeout;

const PasswordChange: React.FC<{ path: string }> = ({path}) => {

    const { t } = useTranslation();
    let history = useHistory();

    const methods = useForm();
    const { register, trigger, handleSubmit, getValues, setValue, formState: { errors } } = methods;

    const [UpdateUserAlert, setUpdateUserAlert] = useState(false);
    const [UserNotFoundAlert, setUserNotFoundAlert] = useState(false);

    const [showLoading, setShowLoading] = useState(true);
    const [showAlert, setShowAlert] = useState(false);
    const [msgAlert, setMsgAlert] = useState('');
    const [headAlert, setHeadAlert] = useState('');

    useIonViewWillEnter(() => {
        if (localStorage.getItem('loginState')!='1') {
            history.push(`${Routes.Home}`);
        }

        if ((localStorage.getItem('user_id')!==null)) {
            setShowLoading(false);
            //if ((localStorage.getItem('user_type_id')=='2') || (localStorage.getItem('user_code')=='null')) {
            //    history.push(`${Routes.UserDashboard}`);
            //} else if ((localStorage.getItem('user_type_id')=='2') || (localStorage.getItem('user_code')=='')) {
            //    history.push(`${Routes.UserDashboard}`);
            //} else {
            //    setShowLoading(false);
                loadUserData(localStorage.getItem('user_id'));
            //}
        } else {
            showError()
        }
    });

    useEffect(() => {

    }, []);

    const showError = (): void => {
        setShowLoading(false);
        
        setMsgAlert(t('Act-Msg'));
        setHeadAlert(t('Act-Head'));
        setShowAlert(true);
    }

    const clickWillDismiss = (dismissFlag) => {
        if (dismissFlag) {
            history.push(`${Routes.myAccount}`);
        }
    };

    async function loadUserData(userID: any) {
        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {

            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            api.get("/auth/getByID/"+userID).then((resPData) => {
                const retData = JSON.parse(JSON.stringify(resPData)).data;

                console.log(retData);

                const fields = ['email', 'id'];
                fields.forEach(field => {
                    if (field=='id') {
                        setValue('user_id', retData[field])
                    } else {
                        setValue(field, retData[field])
                    }
                });
            })
            .catch((error) => {
                alert("Error Get Data!");
            })
        })
        .catch((error) => {
          alert("Token not Get!");
        })
    }
    
    const onSubmit = (data: any) => {
        //console.log(data);
        //return false;
        
        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {
     
              axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
              const api = axios.create();
  
              api.put("/auth/changePwd", data).then((resData) => {
                 const resReturnData = JSON.parse(JSON.stringify(resData)).data;
                 console.log(resReturnData);
  
                 setUpdateUserAlert(true);
              })
              .catch((error) => {
                 setUserNotFoundAlert(true);
              })
        })
        .catch((error) => {
           alert("Token not Get!");
        })
      };

    const doNothing = () => {
        history.goBack();
      }
   
    return (
        <IonPage>
  
           <HeaderMain pageTitle={t('Act-Password-Title')} logoHide="hide-logo" onBack={doNothing} />
  
           <IonContent className="dashboard-wrapper manage-tenants-wrapper manage-rents-wrapper" fullscreen>
           <IonLoading
                    isOpen={showLoading}
                    onDidDismiss={() => setShowLoading(false)}
                    message={'Loading...'}
                />

                <IonAlert
                    isOpen={showAlert}
                    onDidDismiss={() => setShowAlert(false)}
                    onWillDismiss={() => clickWillDismiss(true)}
                    cssClass='orange-alert'
                    mode='md'
                    header={headAlert}
                    message={msgAlert}
                    buttons={['Close']}
                />

                <IonAlert
                    isOpen={UpdateUserAlert}
                    onDidDismiss={() => setUpdateUserAlert(false)}
                    cssClass='orange-alert'
                    mode='md'
                    header={t('Act-Password-Update-Head')}
                    message={t('Act-Password-Update-Msg')}
                    buttons={[
                        {
                            text: 'Close',
                            role: 'cancel',
                            cssClass: 'btn-primary',
                            handler: () => {
                                window.location.assign(`${Routes.myAccount}`);
                            }
                        }
                        
                    ]}
                />

                <IonAlert
                    isOpen={UserNotFoundAlert}
                    onDidDismiss={() => setUserNotFoundAlert(false)}
                    cssClass='red-alert'
                    mode='md'
                    header={t('Act-Password-Error-Head')}
                    message={t('Act-Password-Error-Msg')}
                    buttons={[
                        {
                                text: 'Close',
                                role: 'cancel',
                                cssClass: 'btn-primary',
                                handler: () => {
                                //setShowMessage(true);
                                }
                        }
                        
                    ]}
                />


                <IonGrid className="dashboard-main-grid">
                    <IonRow className="dashboard-main-row">
                        
                        {/* sidebar start  */}
                        <DashboardSidebar path={path} />
                        {/* sidebar end  */}


                        {/* dashboar content start */}
                        <IonCol className="dashboard-content my-account-content" size="12" sizeMd="12" sizeLg="6" sizeXl="8">
                            <div className="dashboard-content-inner">
                                <FormProvider {...methods}>
                                <form onSubmit={handleSubmit(onSubmit)}>
                                    <IonGrid>
                                        <IonRow>
                                        <IonCol size="12" sizeMd="6" className="email-field">
                                        <div className="form-field">
                                            <IonLabel className="form-lable">{t('Act-email')}*</IonLabel>
                                            <IonInput
                                                mode="md"
                                                type="text"   
                                                class="hide-input"
                                                {...register('user_id')}
                                            />
                                            <IonInput
                                                mode="md"
                                                type="email"
                                                readonly
                                                {...register('email', {
                                                    required: t('Act-Error-email'),
                                                    pattern: {
                                                    value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                                                    message: t('Act-Error-email-Invalid')
                                                    }
                                                })}
                                            />
                                            <ErrorMessage
                                                errors={errors}
                                                name="email"
                                                as={<div className="error-message" style={{ color: 'red' }} />}
                                            />
                                        </div>

                                        <div className="form-field">
                                        <IonLabel className="form-lable">{t('Act-Password')}*</IonLabel>
                                            <IonInput 
                                                mode="md"
                                                type="password" 
                                                {...register('password1', {
                                                    required: t('Act-Error-Password'),
                                                    /* pattern: {
                                                    value: /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*_=+-]).{8,12}$/i,
                                                    message: 'Password should be minimum 8 characters and maximum 12 characters long, with 1 upparcase and 1 special character.'
                                                    } */
                                                })}
                                            />
                                            <ErrorMessage
                                                errors={errors}
                                                name="password1"
                                                as={<div className="error-message" style={{ color: 'red' }} />}
                                            />
                                        </div>

                                        <div className="form-field">
                                            <IonLabel className="form-lable">{t('Act-Confirm-Password')}*</IonLabel>
                                            <IonInput 
                                                mode="md"
                                                type="password" 
                                                {...register('password2', {
                                                    // required: true,
                                                    validate: {
                                                    noMatch: (value: string) => {
                                                        return value !== getValues("password1")
                                                            ? "Passwords do not match"
                                                            : undefined;
                                                    },
                                                    },
                                                })}
                                            />
                                            <ErrorMessage
                                                errors={errors}
                                                name="password2"
                                                as={<div className="error-message" style={{ color: 'red' }} />}
                                            />
                                        </div>

                                        <div className="form-field">
                                            <IonButton className="secondary-button submit-new-lease-btn" type="submit" shape="round" fill="outline">
                                                {t('General-Update')}
                                            </IonButton>
                                            <IonButton className="exit-file-btn" fill="solid" shape="round" routerLink={Routes.myAccount}>
                                                {t('General-Cancel')}
                                            </IonButton>
                                        </div>

                                        </IonCol>
                                        </IonRow>
                                    </IonGrid>
                                </form>
                                </FormProvider>
                            </div>

                        </IonCol>
                        {/* dashboar content end */}


                        {/* dashboar posts start */}
                        <IonCol  className="dashboard-posts" size="12" sizeMd="12" sizeLg="3" sizeXl="2">
                            
                        </IonCol>
                        {/* dashboar post end */}
    
                    </IonRow>
                </IonGrid>
              
                <Footer />

            </IonContent>

            <FooterMobile />
      </IonPage>
    );
  };
  
  export default PasswordChange;