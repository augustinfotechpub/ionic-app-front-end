import React, {useRef, useState, useEffect, useCallback} from "react";
import { RouteComponentProps, Switch, useLocation } from "react-router";
import { useHistory } from 'react-router-dom';
import { useTranslation } from "react-i18next";

import { 
    IonPage,
    IonContent,
    IonGrid, 
    IonRow, 
    IonCol, 
    IonSegment,
    IonSegmentButton,
    IonSlides,
    IonSlide,
    IonButton,
    IonIcon,
    IonCard,
    IonCardHeader,
    IonCardTitle,
    IonCardContent,
    IonInput,
    IonLabel,
    IonAvatar,
    IonAlert,
    IonLoading,
    useIonViewWillEnter
} from '@ionic/react';
  
import HeaderMain from '../components/Header-main';
import Footer from '../components/Footer';
import FooterMobile from '../components/Footer-mobile';
import DashboardSidebar from '../components/Dahsboard-sidebar';

import '../assets/css/Custom.css';
import '../assets/css/Custom-Dev.css';
import '../assets/css/Responsive.css';
import { Routes } from '../App';

import { caretDownOutline, createOutline, eyeOutline, addOutline, arrowBackOutline } from "ionicons/icons";

import { globalConst } from "../constants";

import axios from "axios";
axios.defaults.baseURL = globalConst.baseURL;
axios.defaults.headers.post['Content-Type'] = globalConst.ContentType;
axios.defaults.headers.post['Access-Control-Allow-Origin'] = globalConst.AllowOrigin;
axios.defaults.headers.post['Accept'] = globalConst.Accept;
axios.defaults.timeout = globalConst.timeout;

const PostBillingLising: React.FC<{ path: string }> = ({path}) => {

    const { t } = useTranslation();
    let history = useHistory();

    const [showLoading, setShowLoading] = useState(true);

    const [BonusPostsData, setBonusPostsData] = useState<Array<any>>([{"id": '', "bonuspost_date": '', "bonuspost_date_text": "", "bonuspost_pay": "", "bonuspost_cnt": ""}]);

    useIonViewWillEnter(() => {
        if (localStorage.getItem('loginState')!='1') {
            history.push(`${Routes.Home}`);
        } else {
            if (localStorage.getItem('user_type_id')!='1') {
                history.push(`${Routes.UserDashboard}`);
            }
        }

        setBonusPostsData([]);
        loadBonusPosts();
    });

    async function loadBonusPosts() {
        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {

            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            api.get("/postdata/bonusposts/"+localStorage.getItem('user_id')).then((resUnitData) => {
                const retData = JSON.parse(JSON.stringify(resUnitData)).data;

                setBonusPostsData(retData.BonusPostsData);

                setShowLoading(false);
            })
            .catch((error) => {
                alert("Error Get Data!");
            })
        })
        .catch((error) => {
          alert("Token not Get!");
        })
    }

    const doNothing = () => {
		history.goBack();
	}

    const contentRef = useRef<HTMLIonContentElement | null>(null);

    const scrollToBottom= () => {
        contentRef.current && contentRef.current.scrollToBottom(500);
    };
   
    return (
        <IonPage>
  
           <HeaderMain pageTitle={t('POST-Bonus-Manage-Title')} logoHide="hide-logo" onBack={doNothing} />
  
           <IonContent className="dashboard-wrapper" ref={contentRef} scrollEvents={true} fullscreen>
                <IonGrid className="dashboard-main-grid">
                    <IonRow className="dashboard-main-row">
                        
                        {/* sidebar start  */}
                        <DashboardSidebar path={path} />
                        {/* sidebar end  */}

                        <IonCol className="dashboard-content" size="12" sizeMd="12" sizeLg="6" sizeXl="8">
                            <IonLoading
                                    isOpen={showLoading}
                                    onDidDismiss={() => setShowLoading(false)}
                                    message={'Loading...'}
                            />
                            
                            <div className="dashboard-content-inner">
                                <div className="tab-content tenant-tab-content">
                                    <div className="posts-lists">
                                        {BonusPostsData.map((Pitem, Pindex) => (
                                            <div key={Pitem.id} className="posts-list-block">
                                                <IonGrid className="">
                                                    <IonRow className="post-row">
                                                        <IonCol>
                                                            <p>{Pitem.bonuspost_date}</p>
                                                        </IonCol>
                                                        <IonCol>
                                                            <p>${Pitem.bonuspost_pay} for no of <b>{Pitem.bonuspost_cnt}</b> Post</p>
                                                        </IonCol>
                                                    </IonRow>
                                                </IonGrid>                                    
                                            </div>
                                        ))}
                                    </div>
                                </div>
                            </div>
                        </IonCol>        

                        {/* dashboar posts start */}
                        <IonCol  className="dashboard-posts" size="12" sizeMd="12" sizeLg="3" sizeXl="2">
                            
                        </IonCol>
                        {/* dashboar post end */}
    
                    </IonRow>
                </IonGrid>

                <IonButton className="scroll-to-bottom-btn" onClick={scrollToBottom} fill="clear">
                    <IonIcon icon="assets/images/scroll-to-bottom-icon.svg" />
                </IonButton>

                <Footer />

            </IonContent>

            <FooterMobile />
      </IonPage>
    );
  };
  
  export default PostBillingLising;