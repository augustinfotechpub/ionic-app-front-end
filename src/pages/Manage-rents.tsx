import React, {useRef, useState, useEffect, useCallback} from "react";
import { RouteComponentProps, Switch, useLocation } from "react-router";
import { useTranslation } from "react-i18next";

import { 
    IonContent, 
    IonPage, 
    IonGrid, 
    IonRow, 
    IonCol, 
    IonSegment,
    IonSegmentButton,
    IonSlides,
    IonSlide,
    IonLoading,
    IonAlert,
    useIonViewWillEnter
} from '@ionic/react';
  
import HeaderMain from '../components/Header-main';
import Footer from '../components/Footer';
import FooterMobile from '../components/Footer-mobile';
import DashboardSidebar from '../components/Dahsboard-sidebar';
import ManageRentsGeneralTab from "../components/ManageRentsGeneralTab";
import ManageRentsSearchTab from "../components/ManageRentsSearchTab";

import '../assets/css/Custom.css';
import '../assets/css/Responsive.css';
import { Routes } from '../App';
  
import { globalConst } from "../constants";

import axios from "axios";
axios.defaults.baseURL = globalConst.baseURL;
axios.defaults.headers.post['Content-Type'] = globalConst.ContentType;
axios.defaults.headers.post['Access-Control-Allow-Origin'] = globalConst.AllowOrigin;
axios.defaults.headers.post['Accept'] = globalConst.Accept;
axios.defaults.timeout = globalConst.timeout;

interface PageProps extends RouteComponentProps {path: string; history: any; match: any; location: any;}

const ManageRents: React.FC<PageProps> = ({ path, history, match, location }) => {

    const { t } = useTranslation();
    // a ref variable to handle the current slider
    const slider = useRef<any>(null);
    // a state value to bind segment value
    const [value, setValue] = useState("0");
	
    const locationSHI = useLocation();

    const slideOpts = {
        initialSlide: 0,
        speed: 400,
        loop: false,
        pagination: {
        el: null
        },
    };

    const [showLoading, setShowLoading] = useState(true);
    const [PropID, setPropID] = useState("0");
    const [locPath, setlocPath] = useState('');

    const [showAlert, setShowAlert] = useState(false);
    const [msgAlert, setMsgAlert] = useState('');
    const [headAlert, setHeadAlert] = useState('');
    const [handleDismiss, sethandleDismiss] = useState(false);

    const [RentAptData,setRentAptData]=useState([]);
    const [CntRentAptData, setCntRentAptData] = useState<number>();
    const [LoadRentAptData, setLoadRentAptData] = useState<string[]>([]);
    const [SelRentAptNo, setSelRentAptNo] = useState<number>(0);
    const [LoadRentAptDataMsg, setLoadRentAptDataMsg] = useState<string[]>([]);

    useIonViewWillEnter(() => {
        if (localStorage.getItem('loginState')!='1') {
            history.push(`${Routes.Home}`);
        } else {
            if ((localStorage.getItem('user_type_id')=='2') || ((localStorage.getItem('user_type_id')=='3') && (localStorage.getItem('manage_rents')=='1'))) {
                //loadConfigData();
            } else {
                history.push(`${Routes.UserDashboard}`);
            }
        }
    });

    useEffect(() => {
        slider.current.lockSwipes(true);
        setShowLoading(true);
        if (locationSHI.state) {
            setlocPath(location.state.path);
            
            if (location.state.prop_id>0) {
                setPropID(location.state.prop_id);
                loadPropByIDData(location.state.prop_id);
                setShowLoading(false);
            }
        } else {
            var SPI = localStorage.getItem('sel_prop_id');

            if ((SPI!='') && (SPI!==null)) {
                setlocPath(`${Routes.manageRents}`);

                loadPropByIDData(SPI);
                setPropID(SPI);
                setShowLoading(false);
            } else {
                showError()
            }
        }
    }, []);

    async function loadPropByIDData(EPropID: any) {
        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {

            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            api.get("/propertyleases/aptdataRentUnpaid/"+EPropID+"/"+localStorage.getItem('user_id')).then((resUnitData) => {
                const retData = JSON.parse(JSON.stringify(resUnitData)).data;
                setCntRentAptData(retData.cntRentAptData);
                setLoadRentAptData(retData.RentAptData);
                setLoadRentAptDataMsg(retData.RentAptDataMsg);
            })
            .catch((error) => {
                alert("Error Get Data!");
            })
        })
        .catch((error) => {
          alert("Token not Get!");
        })
    }

    const clickMoreRentAptData = () => {

    }

    const clickAptNo = async () => {
        await slider.current.lockSwipes(false);
        setValue('1');
        slider.current!.slideTo(1);
        await slider.current.lockSwipes(true);
    };

    const callback = useCallback((SearchPID, SearchFloorNo, AptNo) => {
        //setSearchAptNo(AptNo);
        //setSearchData([SearchPID, SearchFloorNo, AptNo, SearchID]);
        history.push({
            pathname: `${Routes.rentPayDetails}`,
            state: { path: `${Routes.rentPayDetails}`, prop_id: SearchPID, floor_no: SearchFloorNo, apt_no: AptNo }
        });
    }, []);

    const showError = (): void => {
        setShowLoading(false);
        
        setMsgAlert(t('Rent-Manage-Error-Msg'));
        setHeadAlert(t('Rent-Manage-Error-Head'));
        sethandleDismiss(true);
        setShowAlert(true);
    }

    const clickWillDismiss = () => {
        if (handleDismiss) {
            let element: HTMLElement = document.getElementsByClassName('change-property')[0] as HTMLElement;
            element.click();

            history.push(`${Routes.manageProperties}`);
        }
    };

    // a function to handle the segment changes
    const handleSegmentChange = async (e: any) => {
        await slider.current.lockSwipes(false);
        setValue(e.detail.value);
        slider.current!.slideTo(e.detail.value);
        await slider.current.lockSwipes(true);
    };

    // a function to handle the slider changes
    const handleSlideChange = async (event: any) => {
        let index: number = 0;
        await event.target.getActiveIndex().then((value: any) => (index=value));
        setValue(''+index)
    }

    const doNothing = () => {
        history.goBack();
    }

    const callbackSearch = useCallback((SearchPID, AptNo) => {
        alert(SearchPID);
        alert(AptNo);
    }, []);

    return (
        <IonPage>
  
           <HeaderMain pageTitle={t('Rent-Manage-Title')} logoHide="hide-logo" onBack={doNothing} />
  
           <IonContent className="dashboard-wrapper manage-tenants-wrapper manage-rents-wrapper" fullscreen>
                <IonLoading
                    isOpen={showLoading}
                    onDidDismiss={() => setShowLoading(false)}
                    message={'Loading...'}
                />

                <IonAlert
                    isOpen={showAlert}
                    onDidDismiss={() => setShowAlert(false)}
                    onWillDismiss={() => clickWillDismiss()}
                    cssClass='orange-alert'
                    header={headAlert}
                    message={msgAlert}
                    buttons={['Close']}
                />

                <IonGrid className="dashboard-main-grid">
                    <IonRow className="dashboard-main-row">
                        
                        {/* sidebar start  */}
                        <DashboardSidebar path={locPath} />
                        {/* sidebar end  */}

                        {/* dashboar content start */}
                        <IonCol className="dashboard-content" size="12" sizeMd="12" sizeLg="6" sizeXl="8">
                            <div className="dashboard-content-inner">
                                <IonSegment scrollable  mode="md" value={value} onIonChange={(e) => handleSegmentChange(e)} >
                                    <IonSegmentButton value="0">
                                        {t('Rent-Manage-General')}
                                    </IonSegmentButton>
                                    <IonSegmentButton value="1">
                                        {t('Rent-Manage-SearchTool')}
                                    </IonSegmentButton>
                                </IonSegment>

                                <IonSlides pager={true} options={slideOpts} onIonSlideDidChange={(e) => handleSlideChange(e)} ref={slider}>
                                    <IonSlide>
                                        <ManageRentsGeneralTab 
                                            LoadRentAptData={LoadRentAptData} 
                                            LoadRentAptDataMsg={LoadRentAptDataMsg}
                                            CntRentAptData={CntRentAptData} 
                                            clickMoreRentAptData={() => clickMoreRentAptData()} 

                                            clickAptNo={() => clickAptNo()} 
                                            
                                            parentCallback={callback}
                                        />
                                    </IonSlide>
                                    {/*-- Package Segment --*/}
                                    <IonSlide>
                                        <ManageRentsSearchTab SearchPropID={PropID} SearchAptNo='TEST' SearchData="TestData" parentCallback={callbackSearch} />
                                    </IonSlide>
                                </IonSlides>
                            </div>
                        </IonCol>
                        {/* dashboar content end */}


                        {/* dashboar posts start */}
                        <IonCol  className="dashboard-posts" size="12" sizeMd="12" sizeLg="3" sizeXl="2">
                            
                        </IonCol>
                        {/* dashboar post end */}
    
                    </IonRow>
                </IonGrid>
              
                <Footer />

            </IonContent>

            <FooterMobile />
      </IonPage>
    );
  };
  
  export default ManageRents;