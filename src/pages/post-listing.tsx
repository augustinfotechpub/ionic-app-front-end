import React, {useRef, useState, useEffect, useCallback} from "react";
import { RouteComponentProps, Switch, useLocation } from "react-router";
import { useTranslation } from "react-i18next";

import { 
    IonPage,
    IonContent,
    IonGrid, 
    IonRow, 
    IonCol, 
    IonSegment,
    IonSegmentButton,
    IonSlides,
    IonSlide,
    IonButton,
    IonIcon,
    IonCard,
    IonCardHeader,
    IonCardTitle,
    IonCardContent,
    IonInput,
    IonLabel,
    IonAvatar,
    IonAlert,
    IonLoading,
    useIonViewWillEnter
} from '@ionic/react';
  
import HeaderMain from '../components/Header-main';
import Footer from '../components/Footer';
import FooterMobile from '../components/Footer-mobile';
import DashboardSidebar from '../components/Dahsboard-sidebar';
import TanatDahsboadPostsTab from "../components/TanantsDashboardPostsTab";

import '../assets/css/Custom.css';
import '../assets/css/Custom-Dev.css';
import '../assets/css/Responsive.css';
import { Routes } from '../App';

import { globalConst } from "../constants";

import axios from "axios";
axios.defaults.baseURL = globalConst.baseURL;
axios.defaults.headers.post['Content-Type'] = globalConst.ContentType;
axios.defaults.headers.post['Access-Control-Allow-Origin'] = globalConst.AllowOrigin;
axios.defaults.headers.post['Accept'] = globalConst.Accept;
axios.defaults.timeout = globalConst.timeout;

interface PageProps extends RouteComponentProps {path: string; history: any; match: any; location: any;}

const PostList: React.FC<PageProps> = ({ path, history, match, location }) => {

    const { t } = useTranslation();
    var sel_val = location.state && location.state.sel_val 

    const [showLoading, setShowLoading] = useState(true);

    const [GeneralPostsData, setGeneralPostsData] = useState<Array<any>>([{"id": '', "filestoragekey": '', "title": "", "content": "", "date": "", "attachFile": [{"file_name": ""}]}]);
    const [TenantPostsData, setTenantPostsData] = useState<Array<any>>([{"id": '', "filestoragekey": '', "title": "", "content": "", "date": "", "attachFile": [{"file_name": ""}]}]);
    const [ManagementPostsData, setManagementPostsData] = useState<Array<any>>([{"id": '', "filestoragekey": '', "title": "", "content": "", "date": "", "attachFile": [{"file_name": ""}]}]);

    const [GeneralPostsCnt, setGeneralPostsCnt] = useState(0);
    const [TenantPostsCnt, setTenantPostsCnt] = useState(0);
    const [ManagementPostsCnt, setManagementPostsCnt] = useState(0);

    const slider = useRef<any>(null);

    //if(sel_val != '1'){
    //    sel_val = 0;
    //}
    
    const [value, setValue] = useState(sel_val);

    const btnref = useRef<HTMLIonButtonElement>(null);
    const [addedLeaseCodeAlert, setAddedLeaseCodeAlert] = useState(false);
    const [leasCode, setLeasecode] = useState(false);
    const [showMessage, setShowMessage] = useState(false);


    useIonViewWillEnter(() => {
        if (localStorage.getItem('loginState')!='1') {
            history.push(`${Routes.Home}`);
        } else {
            if (localStorage.getItem('user_type_id')!='1') {
                history.push(`${Routes.UserDashboard}`);
            }
        }

        setGeneralPostsData([]);
        setTenantPostsData([]);
        setManagementPostsData([]);
        loadPostData();
    });

    const slideOpts = {
        initialSlide: sel_val,
        speed: 400,
        loop: false,
        pagination: {
          el: null
        },
        slidesPerView: 1,
        spaceBetween: 10
    }
      
    useEffect(() => {
        if ((sel_val=='0') || (sel_val=='1')) {
            setValue(sel_val);
            slider.current!.slideTo(sel_val);
        }
    }, [sel_val]);

    // a function to handle the segment changes
    const handleSegmentChange = async (e: any) => {
        await slider.current.lockSwipes(false);
        setValue(e.detail.value);
        slider.current!.slideTo(e.detail.value);
        await slider.current.lockSwipes(true);
    };

    // a function to handle the slider changes
    const handleSlideChange = async (event: any) => {
      let index: number = 0;
      await event.target.getActiveIndex().then((value: any) => (index=value));
      //alert(index);
      setValue(''+index)
    }

    async function loadPostData() {
        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {

            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            api.get("/postdata/datafortenant/"+localStorage.getItem('user_id')+"/0/"+localStorage.getItem('sel_prop_id')).then((resUnitData) => {
                const retData = JSON.parse(JSON.stringify(resUnitData)).data;
                //console.log(retData);

                setGeneralPostsData(retData.GeneralPostsData);
                setTenantPostsData(retData.TenantPostsData);
                setManagementPostsData(retData.ManagementPostsData);

                setGeneralPostsCnt(retData.GeneralPostsCnt);
                setTenantPostsCnt(retData.TenantPostsCnt);
                setManagementPostsCnt(retData.ManagementPostsCnt);
            
                setShowLoading(false);
            })
            .catch((error) => {
                alert("Error Get Data!");
            })
        })
        .catch((error) => {
          alert("Token not Get!");
        })
    }
    

	const doNothing = () => {
		//history.push(`/signup-data/${userTypeSelID}`);
		history.goBack();
	}

   
    const contentRef = useRef<HTMLIonContentElement | null>(null);

    const scrollToBottom= () => {
        contentRef.current && contentRef.current.scrollToBottom(500);
    };
   

    return (
        <IonPage>
  
           <HeaderMain pageTitle={t('POST-Head')} logoHide="hide-logo" onBack={doNothing} />
  
           <IonContent className="dashboard-wrapper" ref={contentRef} scrollEvents={true} fullscreen>
                <IonGrid className="dashboard-main-grid">
                    <IonRow className="dashboard-main-row">
                        
                        {/* sidebar start  */}
                        <DashboardSidebar path={path} />
                        {/* sidebar end  */}

                        <IonCol className="dashboard-content" size="12" sizeMd="12" sizeLg="6" sizeXl="8">
                            <IonLoading
                                    isOpen={showLoading}
                                    onDidDismiss={() => setShowLoading(false)}
                                    message={'Loading...'}
                            />
                            
                            <div className="dashboard-content-inner">
                                <div className="tab-button-wrap">
                                    <IonButton className="post-add-button-besides-tab" fill="clear" routerLink={Routes.createPosts} >
                                        {t('POST-Create-info')}
                                    </IonButton>
                                    <IonSegment scrollable  mode="md" value={value} onIonChange={(e) => handleSegmentChange(e)} >
                                        <IonSegmentButton value="0">
                                            {t('POST-Tenants')}
                                        </IonSegmentButton>
                                        <IonSegmentButton value="1">
                                            {t('POST-Management')}
                                        </IonSegmentButton>
                                    </IonSegment>
                                </div>

                                <IonSlides pager={true} options={slideOpts} onIonSlideDidChange={(e) => handleSlideChange(e)} ref={slider}>
                                    <IonSlide>
                                        <TanatDahsboadPostsTab PostData={TenantPostsData} />
                                    </IonSlide>
                                    <IonSlide>
                                        <TanatDahsboadPostsTab PostData={ManagementPostsData} />
                                    </IonSlide>
                                </IonSlides>

                                <IonButton className="post-add-button" fill="clear" routerLink={Routes.createPosts} >
                                    <IonIcon src="assets/images/post-icon.svg" />
                                </IonButton>
                            </div>
                        </IonCol>        

                        {/* dashboar posts start */}
                        <IonCol  className="dashboard-posts" size="12" sizeMd="12" sizeLg="3" sizeXl="2">
                            
                        </IonCol>
                        {/* dashboar post end */}
    
                    </IonRow>
                </IonGrid>

                <IonButton className="scroll-to-bottom-btn" onClick={scrollToBottom} fill="clear">
                    <IonIcon icon="assets/images/scroll-to-bottom-icon.svg" />
                </IonButton>
                
              
                <Footer />

            </IonContent>

            <FooterMobile />
      </IonPage>
    );
  };
  
  export default PostList;