import React, {useRef, useState, useEffect} from "react";
import { useHistory } from 'react-router-dom';
import { useTranslation } from "react-i18next";

import { 
    IonContent, 
    IonPage, 
    IonGrid, 
    IonRow, 
    IonCol, 
    IonSlide,
    IonLabel,
    IonInput,
    IonButton,
    IonImg,
    IonDatetime,
    IonLoading,
    IonAlert,
    IonItem,
    IonRadio,
    IonListHeader,
    IonRadioGroup,
    useIonViewWillEnter
} from '@ionic/react';

import { FormProvider, useForm } from "react-hook-form";
import { ErrorMessage } from '@hookform/error-message';
import { RouteComponentProps, Switch, useLocation } from "react-router";

import DashboardSidebar from '../components/Dahsboard-sidebar';
import HeaderMain from '../components/Header-main';
import Footer from '../components/Footer';
import FooterMobile from '../components/Footer-mobile';

import '../assets/css/Custom.css';
import '../assets/css/Responsive.css';
import { Routes } from "../App";

import { globalConst } from "../constants";
import axios from "axios";
axios.defaults.baseURL = globalConst.baseURL;
axios.defaults.headers.post['Content-Type'] = globalConst.ContentType;
axios.defaults.headers.post['Access-Control-Allow-Origin'] = globalConst.AllowOrigin;
axios.defaults.headers.post['Accept'] = globalConst.Accept;
axios.defaults.timeout = globalConst.timeout;

const CreatePostPay: React.FC<{ path: string }> = ({path}) => {

    const { t } = useTranslation();
    let history = useHistory();

    const methods = useForm();
    const { register, trigger, handleSubmit, control, setValue, getValues, formState: { errors } } = methods;
 
    const [SelCard, setSelCard] = useState<number>(0)

    const [locPath, setlocPath] = useState('');
    
    const [HeadText, setHeadText] = useState('');
    const [DescText, setDescText] = useState('');
    
    const [showLoading, setShowLoading] = useState(true);
    const [showAlert, setShowAlert] = useState(false);
    const [msgAlert, setMsgAlert] = useState('');
    const [headAlert, setHeadAlert] = useState('');

    const [paymentCompleteAlert, setPaymentCompleteAlert] = useState(false);

    const [PayData, setPayData] = useState([{id:'',default_method:'',stripe_pay_method_id:'',pay_name:'',c_no:'',c_date:'',c_cvc:''}]);

    const onSubmit = (data: any) => {
        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {
    
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            var now = new Date();

            var selCID = data.selectedCardID;
            if (selCID==''){
                selCID = SelCard;
            }

            const addData = {
                "selectedCardID" : selCID,
                "cvcNo" : data.cvcNo,
                "BonusPay" : data.BonusPay,
                "BonusPostNo" : data.BonusPostNo,
                "sYear" : now.getFullYear(),
                "sMonth" : (now.getMonth() + 1),
                "sDate" : now.getDate(),
                "currDate" : now,
                "pay_user_id" : localStorage.getItem('user_id'),
                "prop_id" : localStorage.getItem('sel_prop_id'),
            };

            //console.log(addData);
            //return false;

            api.put("/postdata/addbonusposts", addData).then((retData) => {
                const retDataArr = JSON.parse(JSON.stringify(retData)).data;

                ///console.log(retDataArr);
                //return false;
                if (retDataArr.payment=='success') {
                   setPaymentCompleteAlert(true)
                } else {
                    setShowLoading(false);
                    
                    setMsgAlert(t('POST-Error-Payment-Msg'));
                    setHeadAlert(t('POST-Error-Payment-Head'));
                    setShowAlert(true);
                }

            }).catch((error) => {
                alert("Error found in put Data");
                console.log(error);
            });
        })
        .catch((error) => {
          alert("Token not Get!");
        })

    };
    
    const doNothing = () => {
        history.goBack();
    }

    useIonViewWillEnter(() => {
        setShowLoading(false);
        if (localStorage.getItem('loginState')!='1') {
            history.push(`${Routes.Home}`);
        } else {
            if ((localStorage.getItem('user_type_id')=='2') || (localStorage.getItem('user_type_id')=='3')) {
                history.push(`${Routes.UserDashboard}`);
            } else if ((localStorage.getItem('user_code')=='null') || (localStorage.getItem('user_code')=='')) {
                history.push(`${Routes.UserDashboard}`);
            }
        }
    });

    useEffect(() => {
        if (localStorage.getItem('user_type_id')=='1') {
            loadUserData(localStorage.getItem('user_id'));
            loadConfigData();
        }
    }, []);

    async function loadUserData(userID: any) {
        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {

            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            api.get("/auth/getPayInfoByID/all/"+userID).then((resPData) => {
                const retData = JSON.parse(JSON.stringify(resPData)).data;

                if (retData['itemFlag']){
                    setSelCard(retData.SelCardID);
                    setPayData(retData.paydata);
                }
            })
            .catch((error) => {
                alert("Error Get User Payment Data!");
            })
        })
        .catch((error) => {
          alert("Token not Get!");
        })
    }

    async function loadConfigData() {
        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {

            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            if (localStorage.getItem('user_type_id')=='1'){
                var current_user = localStorage.getItem('user_id');
            } else {
                var current_user = localStorage.getItem('user_management_id');
            }

            api.get("/postdata/configdata/"+current_user).then((resPData) => {
                const retData = JSON.parse(JSON.stringify(resPData)).data;

                if (retData.PostsCnt>=retData.tenants_max_post) {
                    setHeadText(t('POST-Bonus-Title'));
                    setDescText(t('POST-Bonus-Text5')+ retData.tenants_post_cost +t('POST-Bonus-Text3')+ retData.tenants_post_no +t('POST-Bonus-Text6'));

                    setValue('BonusPayText', "$" + retData.tenants_post_cost);
                    setValue('BonusPay', retData.tenants_post_cost);
                    setValue('BonusPostNo', retData.tenants_post_no);
                }
            })
            .catch((error) => {
                //error
            })
        })
        .catch((error) => {
          alert("Token not Get!");
        })
    }

    const clickWillDismiss = (DismissFlag) => {
        if (DismissFlag) {
            history.push(`${Routes.createPosts}`);
        } else {
            history.push(`${Routes.postBillingLising}`);
        }
    };

    return (
        <IonPage>
  
           <HeaderMain pageTitle={t('POST-Create-Payment-Title')} logoHide="hide-logo" onBack={doNothing} />
  
           <IonContent className="dashboard-wrapper manage-tenants-wrapper manage-rents-wrapper" fullscreen>
                <IonLoading
                    isOpen={showLoading}
                    onDidDismiss={() => setShowLoading(false)}
                    message={'Loading...'}
                />

                <IonAlert
                    isOpen={showAlert}
                    onDidDismiss={() => setShowAlert(false)}
                    onWillDismiss={() => clickWillDismiss(false)}
                    cssClass='orange-alert'
                    mode='md'
                    header={headAlert}
                    message={msgAlert}
                    buttons={['Close']}
                />

                <IonAlert
                    isOpen={paymentCompleteAlert}
                    onDidDismiss={() => setPaymentCompleteAlert(false)}
                    onWillDismiss={() => clickWillDismiss(true)}
                    cssClass='orange-alert rent-pay-alert'
                    mode='md'
                    header={t('POST-Create-Payment-Done-Head')}
                    message={t('POST-Create-Payment-Done-Msg')}
                    buttons={[
                        {
                            text: t('POST-Create-Payment-Done-Btn'),
                            role: 'cancel',
                            cssClass: 'btn-primary',
                            handler: () => {
                                
                            }
                        }    
                    ]}
                />

                <IonGrid className="dashboard-main-grid">
                    <IonRow className="dashboard-main-row">
                        
                        {/* sidebar start  */}
                        <DashboardSidebar path={locPath} />
                        {/* sidebar end  */}

                        {/* dashboar content start */}
                        <IonCol className="dashboard-content choose-payment-method-dashboard" size="12" sizeMd="12" sizeLg="6" sizeXl="8">
                            <div className="dashboard-content-inner">
                                <div className="payment-method-wrapper">
                                    <FormProvider {...methods}>
                                        <form onSubmit={handleSubmit(onSubmit)}>
                                            <div className="rent-pay-summary">
                                                <h3><u>{HeadText}</u></h3>
                                                <IonGrid className="ion-no-padding">
                                                    <IonRow className="rents-pay-details">
                                                        <IonCol size="6">
                                                            <h2>{DescText}</h2>
                                                        </IonCol>
                                                        <IonCol size="6">
                                                            <IonInput
                                                                readonly
                                                                mode="md"
                                                                type="text"
                                                                {...register('BonusPayText')}
                                                            />
                                                            <IonInput
                                                                class="hide-input"
                                                                mode="md"
                                                                type="text"
                                                                {...register('BonusPay')}
                                                            />
                                                            <IonInput
                                                                class="hide-input"
                                                                mode="md"
                                                                type="text"
                                                                {...register('BonusPostNo')}
                                                            />
                                                        </IonCol>
                                                    </IonRow>
                                                </IonGrid>
                                            </div>

                                            <div className="choose-payment-method-block">
                                                <IonSlide>
                                                    <IonGrid className="ion-no-padding">
                                                        <IonRow className="ion-align-items-center credit-card-details">
                                                            <IonCol size="3" sizeMd="2">
                                                                <IonImg className="visa-logo" src="assets/images/visa-logo.png" />
                                                            </IonCol>
                                                            <IonCol size="9" sizeMd="6">
                                                                <div>
                                                                    <IonRadioGroup value={SelCard} onIonChange={e => setSelCard(e.detail.value)}>
                                                                        {PayData.map((payitem, index) => (
                                                                            <div key={index} className="search-result-box">
                                                                                <IonItem>
                                                                                    <IonLabel>{payitem.pay_name} - {payitem.c_no} - {payitem.c_date}</IonLabel>
                                                                                    <IonRadio {...register('selectedCardID')} value={payitem.id} />
                                                                                </IonItem>
                                                                            </div>
                                                                        ))}
                                                                    </IonRadioGroup>
                                                                </div>
                                                            </IonCol>
                                                        </IonRow>
                                                        <IonRow className="ion-align-items-center credit-card-details">
                                                            <IonCol size="3" sizeMd="2">
                                                                <IonLabel className="form-lable" >{t('POST-Create-Payment-CVC')}*</IonLabel>
                                                            </IonCol>
                                                            <IonCol size="9" sizeMd="6">
                                                                <IonInput
                                                                    mode="md"
                                                                    type="number"
                                                                    {...register('cvcNo', {
                                                                        required: {
                                                                            value: true,
                                                                            message: t('POST-Create-Payment-CVC-Msg1')
                                                                        },
                                                                        minLength: {
                                                                            value: 3,
                                                                            message: t('POST-Create-Payment-CVC-Msg2')
                                                                        },
                                                                        maxLength: {
                                                                            value: 3,
                                                                            message: t('POST-Create-Payment-CVC-Msg2')
                                                                        }
                                                                    })}
                                                                />
                                                                <ErrorMessage
                                                                    errors={errors}
                                                                    name="cvcNo"
                                                                    as={<div className="error-message" style={{ color: 'red' }} />}
                                                                />
                                                            </IonCol>
                                                        </IonRow>
                                                    </IonGrid>
                                                </IonSlide>
                                            </div>

                                            <div className="choose-payment-method-block">
                                                <div className="payment-method-cofirm-method">
                                                    <IonButton type="submit"  className="secondary-button ion-margin-top" shape="round" fill="outline" >
                                                        {t('General-Confirm')}
                                                    </IonButton>
                                                </div>
                                            </div>
                                        </form>
                                    </FormProvider>
                                </div>
                            </div>
                        </IonCol>
                        {/* dashboar content end */}

                        {/* dashboar posts start */}
                        <IonCol  className="dashboard-posts" size="12" sizeMd="12" sizeLg="3" sizeXl="2">
                            
                        </IonCol>
                        {/* dashboar post end */}
    
                    </IonRow>
                </IonGrid>
              
                <Footer />
            </IonContent>

            <FooterMobile />
        </IonPage>
    );
  };
  
  export default CreatePostPay;