import React, {useEffect, useRef, useState} from "react";
import { useHistory } from "react-router-dom";
import { RouteComponentProps, Switch, useLocation } from "react-router";
import { FormProvider, useForm, Controller, useFieldArray } from "react-hook-form";
import { useTranslation } from "react-i18next";

import { 
    IonContent, 
    IonPage,
    IonGrid,
    IonRow,
    IonCol, 
    IonInput, 
    IonLabel, 
    IonButton, 
    IonIcon,
    IonCard,
    IonCardHeader,
    IonCardTitle,
    IonCardContent,
    IonLoading,
    IonAlert,
    useIonViewWillEnter,
    useIonAlert
} from '@ionic/react';

import HeaderMain from '../components/Header-main';
import Footer from '../components/Footer';
import FooterMobile from '../components/Footer-mobile';
import DashboardSidebar from '../components/Dahsboard-sidebar';

import '@ionic/react/css/flex-utils.css';
import '../assets/css/Custom.css';
import '../assets/css/Responsive.css';
import { Routes } from "../App";

import { globalConst } from "../constants";
import axios from "axios";
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';
axios.defaults.baseURL = globalConst.baseURL;
axios.defaults.headers.post['Content-Type'] = globalConst.ContentType;
axios.defaults.headers.post['Access-Control-Allow-Origin'] = globalConst.AllowOrigin;
axios.defaults.headers.post['Accept'] = globalConst.Accept;
axios.defaults.timeout = globalConst.timeout;

interface PageProps extends RouteComponentProps {path: string; history: any; match: any; location: any;}
const RentPayDetails: React.FC<PageProps> = ({ path, history, match, location }) => {

    const { t } = useTranslation();
    const [locPath, setlocPath] = useState('');
    const locationSHI = useLocation();
    const contentRef = useRef<HTMLIonContentElement | null>(null);
    const [showLoading, setShowLoading] = useState(true);

    const [showAlert, setShowAlert] = useState(false);
    const [msgAlert, setMsgAlert] = useState('');
    const [headAlert, setHeadAlert] = useState('');
    const [handleDismiss, sethandleDismiss] = useState(false);

    const scrollToBottom= () => {
        contentRef.current && contentRef.current.scrollToBottom(500);
    };

    const [updateFileAlert, setUpdateFileAlert] = useState(false);
    const [updateFileNotificationSentAlert, setUpdateFileNotificationSentAlert] = useState(false);
    const [exitFileAlert, setExitFileAlert] = useState(false);
    const [payNotDoneAlert, setPayNotDoneAlert] = useState(false);

    const [GetPID, setGetPID] = useState(0);
    const [GetFloorNo, setGetFloorNo] = useState(0);
    const [GetAptNo, setGetAptNo] = useState('');

    const [GetRentID, setGetRentID] = useState('');

    const [loadPendData, setloadPendData] = useState<string[]>([]);

    const methods = useForm();
    const { register, trigger, reset, watch , handleSubmit, control, setValue, getValues, formState: { errors } } = methods;

    useIonViewWillEnter(() => {
        if (localStorage.getItem('loginState')!='1') {
            history.push(`${Routes.Home}`);
        } else {
            if ((localStorage.getItem('user_type_id')=='2') || ((localStorage.getItem('user_type_id')=='3') && (localStorage.getItem('manage_rents')=='1'))) {
                //loadConfigData();
            } else {
                history.push(`${Routes.UserDashboard}`);
            }
        }
    });

    useEffect(() => {
        if (locationSHI.state) {
            setShowLoading(false);
            setlocPath(location.state.path);

            console.log(location.state);

            if (location.state.SearchPID>0) {
                setGetPID(location.state.SearchPID);
            }

            if (location.state.SearchFloorNo>0) {
                setGetFloorNo(location.state.SearchFloorNo);
            }

            if (location.state.SearchAptNo>0) {
                setGetAptNo(location.state.SearchAptNo);
            }

            if (location.state.SearchPID > 0 && location.state.SearchFloorNo > 0 && location.state.SearchAptNo != ''){
                loadPendLeaseData(location.state.SearchPID, location.state.SearchFloorNo, location.state.SearchAptNo, 0);
            } else if (location.state.RentID) {
                setGetRentID(location.state.RentID);
                loadPendLeaseData(0, 0, 0, location.state.RentID);
            }
        } else {
            showError()
        }
    }, []);

    /* useEffect(() => {
        if (GetPID > 0 && GetFloorNo > 0 && GetAptNo != ''){
            loadPendLeaseData(GetPID, GetFloorNo, GetAptNo);
        }
    }, [GetPID, GetFloorNo, GetAptNo]); */

    async function loadPendLeaseData(GetPID: any, GetFloorNo: any, GetAptNo: any, GetRentID: any) {
        console.log('Load Pend Lease Data');

        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {

            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            const searchInfo = {
                "GetPID" : GetPID,
                "GetFloorNo" : GetFloorNo,
                "GetAptNo" : GetAptNo,
                "GetRentID" : GetRentID,
                "UserID" : localStorage.getItem('user_id'),
            };

            api.put("/propertyleases/PendLeaseData", searchInfo).then((resPData) => {
                const retData = JSON.parse(JSON.stringify(resPData)).data;
                //console.log('Vivan--------');
                //console.log(retData);
                setloadPendData(retData);

                const fields = ['aptno', 'lease_range_date', 'lease_price', 'lease_price_text', 'lease_price_pay', 'lease_id'];
                fields.forEach(field => {
                    if (field=='lease_range_date') {
                        setValue(field, convert(retData.lease_start_date) + " -> " + convert(retData.lease_end_date))
                    } else if (field=='lease_price_text') {
                        setValue(field, "$" + retData['lease_price'])
                    } else if (field=='lease_price_pay') {
                        setValue(field, retData['lease_price'])
                    } else {
                        setValue(field, retData[field])
                    }
                });
            })
            .catch((error) => {
                alert("Error Get Data!");
            })
        })
        .catch((error) => {
          alert("Token not Get!");
        })
    }

    function convert(str) {
        var  months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        var date = new Date(str),
          day = ("0" + date.getDate()).slice(-2);
        return months[date.getMonth()] + " " + day + ", " + date.getFullYear();
    }

    const showError = (): void => {
        setShowLoading(false);
        
        setMsgAlert(t('Rent-Pay-Text'));
        setHeadAlert(t('Rent-Pay-Detail'));
        sethandleDismiss(true);
        setShowAlert(true);
    }

    const clickWillDismiss = (ExitFlag) => {
        if (ExitFlag) {
            history.push(`${Routes.manageRents}`);
        }
    };

    const doNothing = () => {
        history.goBack();
    }

    const onSubmit = (data: any) => {
        if (data.lease_price_pay >= data.lease_price) {
            console.log(data);
    
            const api = axios.create();
            api.get("/auth/token/refresh").then(res => res).then((response) => {
                
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
                const api = axios.create();

                var now = new Date();

                const editLeaseData = {
                    "lease_price_pay" : data.lease_price_pay,
                    "sYear" : now.getFullYear(),
                    "sMonth" : (now.getMonth() + 1),
                    "sDate" : now.getDate(),
                    "lease_id" : data.lease_id
                };

                //console.log(editLeaseData);
                api.put("/propertyleases/updatepay/"+data.lease_id+"/"+localStorage.getItem('user_id'), editLeaseData).then((retData) => {
                    const retDataArr = JSON.parse(JSON.stringify(retData));

                    setUpdateFileNotificationSentAlert(true)

                }).catch((error) => {
                    alert("Error found in put Data");
                    console.log(error);
                });
            })
            .catch((error) => {
                alert("Token not Get!");
            })
        } else {
            //clickWillDismiss(true);
            setPayNotDoneAlert(true);
        }
    }

    return (
        <IonPage>
  
           <HeaderMain pageTitle={t('Rent-Manage-Title')}  logoHide="hide-logo" onBack={doNothing} />
  
           <IonContent className="dashboard-wrapper lease-info-wrapper" ref={contentRef} scrollEvents={true} fullscreen>
                <IonLoading
                    isOpen={showLoading}
                    onDidDismiss={() => setShowLoading(false)}
                    message={'Loading...'}
                />
    
                <IonAlert
                    isOpen={showAlert}
                    onDidDismiss={() => setShowAlert(false)}
                    onWillDismiss={() => clickWillDismiss(true)}
                    cssClass='orange-alert'
                    mode='md'
                    header={headAlert}
                    message={msgAlert}
                    buttons={['Close']}
                />

                <IonAlert
                    isOpen={exitFileAlert}
                    onDidDismiss={() => setExitFileAlert(false)}
                    cssClass='red-alert'
                    mode='md'
                    header={t('General-ExitFile')}
                    message={t('Rent-Pay-Exit-Text')}
                    buttons={[
                        {
                            text: 'Yes',
                            cssClass: 'btn-secondary',
                            handler: () => {
                                clickWillDismiss(true);
                                console.log('Exit File Okay');
                            }
                        },
                        {
                            text: 'No',
                            role: 'cancel',
                            cssClass: 'btn-outline',
                            handler: () => {
                                console.log('Exit File Cancel');
                            }
                        }
                        
                    ]}
                />


                <IonAlert
                    isOpen={payNotDoneAlert}
                    onDidDismiss={() => setPayNotDoneAlert(false)}
                    cssClass='red-alert'
                    mode='md'
                    header={t('Rent-Pay-Error-Head')}
                    message={t('Rent-Pay-Error-Msg')}
                    buttons={[
                        {
                            text: 'Yes',
                            cssClass: 'btn-secondary',
                            handler: () => {
                                clickWillDismiss(true);
                                console.log('Exit File Okay');
                            }
                        },
                        {
                            text: 'No',
                            role: 'cancel',
                            cssClass: 'btn-outline',
                            handler: () => {
                                console.log('Exit File Cancel');
                            }
                        }
                        
                    ]}
                />

                <IonGrid className="dashboard-main-grid">
                    <IonRow className="dashboard-main-row">
                        
                        {/* sidebar start  */}
                        <DashboardSidebar path={locPath} />
                        {/* sidebar end  */}


                        {/* dashboar content start */}
                        <IonCol className="dashboard-content" size="12" sizeMd="12" sizeLg="6" sizeXl="8">

                            <form onSubmit={handleSubmit(onSubmit)}>
                            <IonCard className="tenant-lease-details-card rent-pay-details-card">
                                <IonCardHeader>
                                    <IonCardTitle>{t('Rent-Pay-Title')}</IonCardTitle>
                                </IonCardHeader>

                                <IonCardContent>
                                    <div className="tenant-lease-details-info rent-pay-info">
                                        <div>
                                            <IonLabel>{t('Rent-Apartment')}</IonLabel>
                                            <IonInput mode="md" type="text" {...register('aptno')} readonly></IonInput>
                                        </div>
                                        <div>
                                            <IonLabel>{t('Rent-Manage-SearchTool-RentPeriod')}</IonLabel>
                                            <IonInput mode="md" type="text" {...register('lease_range_date')} readonly></IonInput> 
                                        </div>
                                        <div>
                                            <IonLabel>{t('Rent-Pay-Anticipated')}</IonLabel>
                                            <IonInput mode="md" type="text" {...register('lease_price_text')} readonly></IonInput>
                                        </div>
                                        <div>
                                            <IonLabel>{t('Rent-Pay-NewAnticipated')}</IonLabel>
                                            <div className="paid-btn-wrap">
                                                <IonInput className="width-25" mode="md" type="text" {...register('lease_price_pay')}  ></IonInput>
                                                <IonInput className="hide-input" mode="md" type="text" {...register('lease_id')}  ></IonInput>
                                                <IonInput className="hide-input" mode="md" type="text" {...register('lease_price')}  ></IonInput>
                                                <IonButton fill="outline">{t('Rent-Pay-MarkasPaid')}</IonButton>
                                            </div>
                                        </div>
                                    </div>
                                </IonCardContent>
                            </IonCard>
                            
                            <IonRow className="ion-justify-content-center">
                                <IonCol className="ion-text-center">
                                    <IonButton 
                                        className="confirm-btn"
                                        fill="outline" 
                                        shape="round"
                                        type="submit"
                                    >
                                        {t('General-Confirm')}
                                    </IonButton>
                                    <IonAlert
                                        isOpen={updateFileAlert}
                                        onDidDismiss={() => setUpdateFileAlert(false)}
                                        cssClass='orange-alert'
                                        mode='md'
                                        header={t('General-UpdateFile')}
                                        message={t('General-UpdateFile-Text')}
                                        buttons={[
                                            {
                                                text: 'Yes',
                                                cssClass: 'btn-primary',
                                                handler: () => {
                                                    setUpdateFileNotificationSentAlert(true)
                                                }
                                            },
                                            {
                                                text: 'No',
                                                role: 'cancel',
                                                cssClass: 'btn-outline',
                                                handler: () => {
                                                }
                                            }
                                            
                                        ]}
                                    />
                                    <IonAlert
                                        isOpen={updateFileNotificationSentAlert}
                                        onDidDismiss={() => setUpdateFileNotificationSentAlert(false)}
                                        cssClass='orange-alert'
                                        mode='md'
                                        header={t('General-UpdateFile')}
                                        message={t('Rent-Pay-UpdateFile-Text')}
                                        buttons={[
                                            {
                                                text: 'Close',
                                                role: 'cancel',
                                                cssClass: 'btn-primary',
                                                handler: () => {
                                                    clickWillDismiss(true);
                                                }
                                            }
                                            
                                        ]}
                                    />

                                    <IonButton 
                                        className="exit-file-btn" 
                                        fill="solid" 
                                        shape="round"
                                        onClick={() =>
                                            setExitFileAlert(true)
                                        }
                                    >
                                        {t('General-ExitFile')}
                                    </IonButton>
                                </IonCol>
                            </IonRow>
                            </form>

                        </IonCol>
                        {/* dashboar content end */}


                        {/* dashboar posts start */}
                        <IonCol  className="dashboard-posts" size="12" sizeMd="12" sizeLg="3" sizeXl="2">
                            
                        </IonCol>
                        {/* dashboar post end */}
    
                    </IonRow>
                </IonGrid>

                <IonButton className="scroll-to-bottom-btn" onClick={scrollToBottom} fill="clear">
                    <IonIcon icon="assets/images/double-arrow-down.svg" />
                </IonButton>
              
                <Footer />

            </IonContent>

            <FooterMobile />
      </IonPage>
    );
};

export default RentPayDetails;