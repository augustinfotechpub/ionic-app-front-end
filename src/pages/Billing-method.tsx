import React, {useRef, useState, useEffect} from "react";
import { useTranslation } from "react-i18next";

import { 
    IonContent, 
    IonPage, 
    IonGrid, 
    IonRow, 
    IonCol, 
    IonSlide,
    IonLabel,
    IonInput,
    IonButton,
    IonImg,
    IonDatetime,
    IonLoading,
    IonAlert,
    IonItem,
    IonRadio,
    IonListHeader,
    IonRadioGroup,
    useIonViewWillEnter
} from '@ionic/react';

import { FormProvider, useForm } from "react-hook-form";
import { ErrorMessage } from '@hookform/error-message';
import { RouteComponentProps, Switch, useLocation } from "react-router";

import DashboardSidebar from '../components/Dahsboard-sidebar';
import HeaderMain from '../components/Header-main';
import Footer from '../components/Footer';
import FooterMobile from '../components/Footer-mobile';

import '../assets/css/Custom.css';
import '../assets/css/Responsive.css';
import { Routes } from "../App";

import { globalConst } from "../constants";
import axios from "axios";
import { Console } from "console";
axios.defaults.baseURL = globalConst.baseURL;
axios.defaults.headers.post['Content-Type'] = globalConst.ContentType;
axios.defaults.headers.post['Access-Control-Allow-Origin'] = globalConst.AllowOrigin;
axios.defaults.headers.post['Accept'] = globalConst.Accept;
axios.defaults.timeout = globalConst.timeout;

interface PageProps extends RouteComponentProps {path: string; history: any; match: any; location: any;}
const BillingMethod: React.FC<PageProps> = ({ path, history, match, location }) => {

    const { t } = useTranslation();
    const methods = useForm();
    const { register, trigger, handleSubmit, control, setValue, getValues, formState: { errors } } = methods;
 
    const [SelCard, setSelCard] = useState<number>(0)

    const [locPath, setlocPath] = useState('');
    
    const [showLoading, setShowLoading] = useState(true);
    const [showAlert, setShowAlert] = useState(false);
    const [msgAlert, setMsgAlert] = useState('');
    const [headAlert, setHeadAlert] = useState('');

    const [paymentCompleteAlert, setPaymentCompleteAlert] = useState(false);

    const [BillID, setBillID] = useState(0);
    const [PayData, setPayData] = useState([{id:'',default_method:'',stripe_pay_method_id:'',pay_name:'',c_no:'',c_date:'',c_cvc:''}]);
    const [QuickPayFlag, setQuickPayFlag] = useState(false);

    const onSubmit = (data: any) => {

        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {
    
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            var now = new Date();

            var selCID = data.selectedCardID;
            if (selCID==''){
                selCID = SelCard;
            }

            const editBillData = {
                "amount_pay" : data.billing_cost,
                "sYear" : now.getFullYear(),
                "sMonth" : (now.getMonth() + 1),
                "sDate" : now.getDate(),
                "billing_id" : BillID,
                "pay_user_id" : localStorage.getItem('user_id'),
                "quick_pay" : false,
                "cvcNo" : data.cvcNo,
                "selectedCardID" : selCID,
            };
    
            api.put("/propertyleases/billpay/"+BillID, editBillData).then((retData) => {
                const retDataArr = JSON.parse(JSON.stringify(retData));

                if (retDataArr.payment=='error') {
                    alert(retDataArr.message);
                } else {
                    setPaymentCompleteAlert(true)
                }

            }).catch((error) => {
                alert("Error found in put Data");
                console.log(error);
            });
        })
        .catch((error) => {
          alert("Token not Get!");
        })

    };
    
    const doNothing = () => {
        history.goBack();
    }

    useIonViewWillEnter(() => {
        if (localStorage.getItem('loginState')!='1') {
            history.push(`${Routes.Home}`);
        }

        if (localStorage.getItem('user_type_id')!='2') {
            history.push(`${Routes.UserDashboard}`);
        }
    });

    useEffect(() => {
        setlocPath(`${Routes.billingMethod}`);
        setBillID(match.params.id);
        
        if (match.params.id>0) {
            loadData(match.params.id);
            loadUserData(localStorage.getItem('user_id'));
        } else {
            showError()
        }
        
    }, [match.params.id]);

    async function loadUserData(userID: any) {
        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {

            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            api.get("/auth/getPayInfoByID/all/"+userID).then((resPData) => {
                const retData = JSON.parse(JSON.stringify(resPData)).data;

                if (retData['itemFlag']){
                    setSelCard(retData.SelCardID);
                    setPayData(retData.paydata);
                    setQuickPayFlag(true);
                } else {
                    history.push(`${Routes.billingInfo}`);
                }
            })
            .catch((error) => {
                alert("Error Get User Payment Data!");
            })
        })
        .catch((error) => {
          alert("Token not Get!");
        })
    }

    const showError = (): void => {
        setShowLoading(false);
        
        setMsgAlert("Payment Rent information not selected.");
        setHeadAlert("Payment Method ");
        setShowAlert(true);
    }

    const clickWillDismiss = (DismissFlag) => {
        if (DismissFlag) {
            history.push(`${Routes.billingHistory}`);
        }
    };

    async function loadData(LID: any) {
        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {

            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            api.get("/propertyleases/GetPaymentData/"+LID).then((resPData) => {
                const retData = JSON.parse(JSON.stringify(resPData)).data;
                setShowLoading(false);

                console.log(retData);
                const fields = ['billing_date_text', 'billing_cost', 'billing_cost_text'];
                fields.forEach(field => {
                    if (field=='billing_cost_text') {
                        setValue(field, "$" + retData['billing_cost'])
                    } else {
                        setValue(field, retData[field])
                    }
                });
            })
            .catch((error) => {
                alert("Error Get Property Data!");
            })
        })
        .catch((error) => {
          alert("Token not Get!");
        })
    }

    return (
        <IonPage>
  
           <HeaderMain pageTitle="Rent Pay" logoHide="hide-logo" onBack={doNothing} />
  
           <IonContent className="dashboard-wrapper manage-tenants-wrapper manage-rents-wrapper" fullscreen>
                <IonLoading
                    isOpen={showLoading}
                    onDidDismiss={() => setShowLoading(false)}
                    message={'Loading...'}
                />

                <IonAlert
                    isOpen={showAlert}
                    onDidDismiss={() => setShowAlert(false)}
                    onWillDismiss={() => clickWillDismiss(true)}
                    cssClass='orange-alert'
                    mode='md'
                    header={headAlert}
                    message={msgAlert}
                    buttons={['Close']}
                />

                <IonAlert
                    isOpen={paymentCompleteAlert}
                    onDidDismiss={() => setPaymentCompleteAlert(false)}
                    onWillDismiss={() => clickWillDismiss(true)}
                    cssClass='orange-alert rent-pay-alert'
                    mode='md'
                    header={'Payment Complete'}
                    message={'<p>Your payment has been <b>processed</b>.</p> <p>Thank you!</p> <p>A receipt will be <b>e-mailed</b> to you shortly.</p>'}
                    buttons={[
                        {
                            text: 'Return to Billing Page',
                            role: 'cancel',
                            cssClass: 'btn-primary',
                            handler: () => {
                                
                            }
                        }    
                    ]}
                />

                <IonGrid className="dashboard-main-grid">
                    <IonRow className="dashboard-main-row">
                        
                        {/* sidebar start  */}
                        <DashboardSidebar path={locPath} />
                        {/* sidebar end  */}

                        {/* dashboar content start */}
                        <IonCol className="dashboard-content choose-payment-method-dashboard" size="12" sizeMd="12" sizeLg="6" sizeXl="8">
                            <div className="dashboard-content-inner">
                                <div className="payment-method-wrapper">
                                    <FormProvider {...methods}>
                                        <form onSubmit={handleSubmit(onSubmit)}>
                                            <div className="rent-pay-summary">
                                                <h3><u>Summary</u></h3>
                                                <IonGrid className="ion-no-padding">
                                                    <IonRow className="rents-pay-details">
                                                        <IonCol size="6">
                                                            <p>Period</p>
                                                            <IonInput
                                                                readonly
                                                                mode="md"
                                                                type="text"
                                                                {...register('billing_date_text')}
                                                            />
                                                        </IonCol>
                                                        <IonCol size="6">
                                                            <p>Total</p>
                                                            <IonInput
                                                                readonly
                                                                mode="md"
                                                                type="text"
                                                                {...register('billing_cost_text')}
                                                            />
                                                            <IonInput
                                                                class="hide-input"
                                                                mode="md"
                                                                type="text"
                                                                {...register('billing_cost')}
                                                            />
                                                        </IonCol>
                                                    </IonRow>
                                                </IonGrid>
                                            </div>

                                            <div className="choose-payment-method-block">
                                                <IonSlide>
                                                    <IonGrid className="ion-no-padding">
                                                        <IonRow className="ion-align-items-center credit-card-details">
                                                            <IonCol size="3" sizeMd="2">
                                                                <IonImg className="visa-logo" src="assets/images/visa-logo.png" />
                                                            </IonCol>
                                                            <IonCol size="9" sizeMd="10">
                                                                <div>
                                                                    <IonRadioGroup value={SelCard} onIonChange={e => setSelCard(e.detail.value)}>
                                                                        {PayData.map((payitem, index) => (
                                                                            <div key={index} className="search-result-box">
                                                                                <IonItem>
                                                                                    <IonLabel>{payitem.pay_name} - {payitem.c_no} - {payitem.c_date}</IonLabel>
                                                                                    <IonRadio {...register('selectedCardID')} value={payitem.id} />
                                                                                </IonItem>
                                                                            </div>
                                                                        ))}
                                                                    </IonRadioGroup>
                                                                </div>
                                                            </IonCol>
                                                        </IonRow>
                                                        <IonRow className="ion-align-items-center credit-card-details">
                                                            <IonCol size="3" sizeMd="2">
                                                                <IonLabel className="form-lable" >CVC No*</IonLabel>
                                                            </IonCol>
                                                            <IonCol size="9" sizeMd="6">
                                                                <IonInput
                                                                    mode="md"
                                                                    type="number"
                                                                    {...register('cvcNo', {
                                                                        required: {
                                                                            value: true,
                                                                            message: 'Please enter a CVC number.'
                                                                        },
                                                                        minLength: {
                                                                            value: 3,
                                                                            message: 'Please enter a valid CVC number.'
                                                                        },
                                                                        maxLength: {
                                                                            value: 3,
                                                                            message: 'Please enter a valid CVC number.'
                                                                        }
                                                                    })}
                                                                />
                                                                <ErrorMessage
                                                                    errors={errors}
                                                                    name="cvcNo"
                                                                    as={<div className="error-message" style={{ color: 'red' }} />}
                                                                />
                                                            </IonCol>
                                                        </IonRow>
                                                    </IonGrid>
                                                </IonSlide>
                                            </div>

                                            <div className="choose-payment-method-block">
                                                <div className="payment-method-cofirm-method">
                                                    <IonButton type="submit"  className="secondary-button ion-margin-top" shape="round" fill="outline" >
                                                        Confirm
                                                    </IonButton>
                                                </div>
                                            </div>
                                        </form>
                                    </FormProvider>
                                </div>
                            </div>
                        </IonCol>
                        {/* dashboar content end */}

                        {/* dashboar posts start */}
                        <IonCol  className="dashboard-posts" size="12" sizeMd="12" sizeLg="3" sizeXl="2">
                            
                        </IonCol>
                        {/* dashboar post end */}
    
                    </IonRow>
                </IonGrid>
              
                <Footer />
            </IonContent>

            <FooterMobile />
        </IonPage>
    );
  };
  
  export default BillingMethod;