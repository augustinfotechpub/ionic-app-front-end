import React, {useEffect, useRef, useState} from "react";
import { useHistory } from "react-router-dom";
import { RouteComponentProps, Switch, useLocation } from "react-router";
import { FormProvider, useForm, useFieldArray } from "react-hook-form";
import { useTranslation } from "react-i18next";

import { 
    IonContent, 
    IonPage,
    IonGrid,
    IonRow,
    IonCol, 
    IonButton, 
    IonIcon,
    IonSlides, 
    IonSlide,
    IonSegment,
    IonSegmentButton,
    useIonViewWillEnter,
    IonLoading,
    IonAlert,
    IonImg
} from '@ionic/react';

import { chevronBackSharp, chevronForwardSharp } from "ionicons/icons";

import { Swiper, SwiperSlide } from 'swiper/react/swiper-react.js';
import { Navigation, Controller, Thumbs  } from 'swiper';

import HeaderMain from '../components/Header-main';
import Footer from '../components/Footer';
import FooterMobile from '../components/Footer-mobile';
import DashboardSidebar from '../components/Dahsboard-sidebar';

import { usePhotoGallery, UserPhoto  } from '../hooks/usePhotoGallery';

import '@ionic/react/css/flex-utils.css';
import '../assets/css/Custom.css';
import '../assets/css/Responsive.css';
import 'swiper/swiper.min.css';
import 'swiper/modules/navigation/navigation.min.css';
import { Routes } from "../App";

import { globalConst } from "../constants";
import axios from "axios";
axios.defaults.baseURL = globalConst.baseURL;
axios.defaults.headers.post['Content-Type'] = globalConst.ContentType;
axios.defaults.headers.post['Access-Control-Allow-Origin'] = globalConst.AllowOrigin;
axios.defaults.headers.post['Accept'] = globalConst.Accept;
axios.defaults.timeout = globalConst.timeout;

interface PageProps extends RouteComponentProps {path: string; history: any; match: any; location: any;}
const PostDetails: React.FC<PageProps> = ({ path, history, match, location }) => {

    const { t } = useTranslation();
    const [locPath, setlocPath] = useState('');
    const locationSHI = useLocation();

    let StoKey = '';
    let PID = 0;
    if (locationSHI.state) {
        if (location.state.filestoragekey !== null) {
            StoKey = location.state.filestoragekey;
        }
        if (location.state.post_id>0) {
            PID = location.state.post_id;
        }
    }

    const [PostID, setPostID] = useState<number>(PID);

    const { deletePhoto, photos, takePhoto } = usePhotoGallery(StoKey);
    const [photoToDelete, setPhotoToDelete] = useState<UserPhoto >();
    const [filepath,setFilepath] = useState();

    const contentRef = useRef<HTMLIonContentElement | null>(null);
    const firstSwiper1 = useRef<HTMLIonContentElement | null>(null);
    const [firstSwiper, setFirstSwiper] = useState();
    const [secondSwiper, setSecondSwiper] = useState();
    const [thumbsSwiper, setThumbsSwiper] = useState();

    const [PostData, setPostData] = useState({'post_id': '', 'post_title' : '', 'post_message' : '', 'post_storagekey' : '', 'post_activation_date_text' : '', 'post_expiration_date': '', 'post_date': '', 'FData': ''});

    const [showLoading, setShowLoading] = useState(true);

    const [showAlert, setShowAlert] = useState(false);
    const [msgAlert, setMsgAlert] = useState('');
    const [headAlert, setHeadAlert] = useState('');

    useIonViewWillEnter(() => {
        if (localStorage.getItem('loginState')!='1') {
            history.push(`${Routes.Home}`);
        } else {
            if (localStorage.getItem('user_type_id')!='1') {
                history.push(`${Routes.UserDashboard}`);
            }
        }

        /* if (locationSHI.state) {
            setlocPath(location.state.path);
            if (location.state.post_id>0) {
                setPostID(location.state.post_id);
            }
        } */
    });

    /* useEffect(() => {
        console.log('Use EFF'); // result: '/secondpage'
        console.log(locationSHI.pathname); // result: '/secondpage'
        console.log(locationSHI.search); // result: '?query=abc'
        console.log(match); // result: '?query=abc'
        //console.log(locationSHI.state.post_id); // result: 'some_value'
     }, [locationSHI]); */

    /* useEffect(() => {
        if (locationSHI.state) {
            setShowLoading(false);
            setlocPath(location.state.path);

    
            if (location.state.post_id>0) {
                setPostID(location.state.post_id);
            }
        } else {
            showError()
        }
    }, []); */

    /* useEffect(() => {
        if (PostID>0) {
            loadPostData(PostID);
        }
    }, [PostID]); */

    useEffect(() => {
        setShowLoading(false);
        setlocPath(`${Routes.postDetails}`);
        
        if (match.params.id>0) {
            setPostID(match.params.id);
            loadPostData(match.params.id);
        } else {
            showError()
        }
        
    }, [match.params.id]);





    async function loadPostData(postID: any) {
        //console.log('SHIHISHIHS');
        //console.log(postID);

        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {

            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            api.get("/postdata/"+postID+"/"+localStorage.getItem('user_id')).then((resPData) => {
                const retData = JSON.parse(JSON.stringify(resPData)).data;
                
                //console.log(retData);
                setPostData(retData);

                setFilteredSearch([{
                    post_id: retData.post_id,
                    conv_subject: retData.post_title,
                    property_id: "",
                    floorsno: "",
                    aptno: "",
                    user_code:"",
                    id: retData.post_user_id,
                    name:retData.post_user_name,
                    role:"Tenant",
                    val: "",
                    isChecked: true
                }])
    
            })
            .catch((error) => {
                setMsgAlert(t('POST-Error-Text1'));
                setHeadAlert(t('POST-Error-Head'));
                setShowAlert(true);
            })
        })
        .catch((error) => {
            alert("Token not Get!");
        }) 
    }

    const showError = (): void => {
        setShowLoading(false);
        
        setMsgAlert(t('POST-Error-Text2'));
        setHeadAlert(t('POST-View-Alert-Head'));
        setShowAlert(true);
    }

    const clickWillDismiss = (FlagDismiss) => {
        if (FlagDismiss) {
            history.push(`${Routes.UserDashboard}`);
        }
    };

    const scrollToBottom= () => {
        contentRef.current && contentRef.current.scrollToBottom(500);
    };

    const doNothing = () => {
        history.goBack();
    }
    
    const [filteredSearch, setFilteredSearch] = useState([
        {
            post_id: "",
            conv_subject: "",
            property_id: "",
            floorsno: "",
            aptno: "",
            user_code:"",
            id:"",
            name:"",
            role:"",
            val: "",
            isChecked: false
        }])

    const ClickMessagePoster = async () => {
        const api = axios.create();
        api.get("/auth/token/refresh").then(res => res)
        .then((response) => {
            
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            const ConversationData = {
                "current_user" : localStorage.getItem('user_id'),
                "UserID" : localStorage.getItem('user_id'),
                "UserTypeID" : localStorage.getItem('user_type_id'),
                "SearchSelected" : filteredSearch,
            };
    
            api.post("/messaging/startconversion", ConversationData).then((retData) => {
                const retDataArr = JSON.parse(JSON.stringify(retData)).data;
                console.log(retDataArr);

                history.push({
                    pathname: `${Routes.chat}`,
                    state: { path: `${Routes.chat}`, chat_id: retDataArr.InsertID, chat_msg: ''}
                });
               
            }).catch((error) => {
                //alert("Error found in put Data");
                console.log(error);
            });
        })
        .catch((error) => {
            alert("Token not Get!");
        })
    };

    return (
        <IonPage>
  
           <HeaderMain pageTitle={t('POST-View-Alert-Head')}  logoHide="" onBack={doNothing} />
  
           <IonContent className="dashboard-wrapper lease-info-wrapper" ref={contentRef} scrollEvents={true} fullscreen>
                <IonLoading
                    isOpen={showLoading}
                    onDidDismiss={() => setShowLoading(false)}
                    message={'Loading...'}
                />
    
                <IonAlert
                    isOpen={showAlert}
                    onDidDismiss={() => setShowAlert(false)}
                    onWillDismiss={() => clickWillDismiss(true)}
                    cssClass='orange-alert'
                    header={headAlert}
                    message={msgAlert}
                    buttons={['Close']}
                />

                <IonGrid className="dashboard-main-grid">
                    <IonRow className="dashboard-main-row">
                        {/* sidebar start  */}
                        <DashboardSidebar path={locPath} />
                        {/* sidebar end  */}

                        {/* dashboar content start */}
                        <IonCol className="dashboard-content" size="12" sizeMd="12" sizeLg="6" sizeXl="8">
                            <div className="dashboard-content-inner post-details">
                                <div className="post-images-slider">
                                    <div className="swipe-slider">
                                        <Swiper
                                            className="post-main-image-slider"
                                            modules={[Navigation, Controller, Thumbs]}
                                            navigation
                                            onSwiper={() => setFirstSwiper}
                                            controller={{ control: secondSwiper }}
                                            thumbs={{ swiper: thumbsSwiper }}
                                        >
                                            
                                            {photos.map((photo, index) => (
                                                <SwiperSlide key={index}>  
                                                    <IonImg className="main-post-image" src={photo.webviewPath} />
                                                </SwiperSlide>
                                            ))}
                                        </Swiper>
                                    </div>

                                    <Swiper 
                                        className="post-thumbnail-slider"
                                        modules={[Controller, Thumbs]}
                                        spaceBetween={15}
                                        onSwiper={() => setSecondSwiper}
                                        controller={{ control: firstSwiper }}
                                    >
                                        {photos.map((photo, index) => (
                                            <SwiperSlide>
                                                <IonImg className="thumbnail-post-image" src={photo.webviewPath} />
                                            </SwiperSlide>
                                        ))}
                                    </Swiper>
                                </div>

                                <div className="post-details-content">
                                    <div className="post-details-header">
                                        <div className="post-details-title">
                                            <h3>{PostData.post_title}</h3>
                                            <p className="post-detail-date">{t('POST-View-date')}: {PostData.post_activation_date_text}</p>
                                        </div>
                                        <IonButton className="message-poster-button" onClick={() => ClickMessagePoster()} shape="round" fill="outline">
                                            <div className="button-inner">
                                                <IonIcon src="assets/images/Chat-Bubble.svg" />
                                                <span>{t('POST-Message-Poster')}</span>
                                            </div>
                                        </IonButton>
                                    </div>
                                    <p>{PostData.post_message}</p>
                                </div>
                            </div>
                        </IonCol>
                        {/* dashboar content end */}

                        {/* dashboar posts start */}
                        <IonCol  className="dashboard-posts" size="12" sizeMd="12" sizeLg="3" sizeXl="2">
                            
                        </IonCol>
                        {/* dashboar post end */}
                    </IonRow>
                </IonGrid>

                <IonButton className="scroll-to-bottom-btn" onClick={scrollToBottom} fill="clear">
                    <IonIcon icon="assets/images/double-arrow-down.svg" />
                </IonButton>
              
                <Footer />
            </IonContent>

            <FooterMobile />
      </IonPage>
    );
};

export default PostDetails;