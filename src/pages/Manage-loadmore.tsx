import React, {useState} from "react";
import { useHistory } from 'react-router-dom';

import { 
    IonContent, 
    IonPage, 
    IonGrid, 
    IonRow, 
    IonCol, 
    IonButton,
    IonList,
    IonItem,
    IonLabel,
    useIonViewWillEnter
} from '@ionic/react';

import HeaderMain from '../components/Header-main';
import Footer from '../components/Footer';
import FooterMobile from '../components/Footer-mobile';
import DashboardSidebar from '../components/Dahsboard-sidebar';

import '../assets/css/Custom.css';
import '../assets/css/Responsive.css';
  
const ManageRequests: React.FC<{ path: string }> = ({path}) => {

    let history = useHistory();

    const doNothing = () => {
      history.goBack();
    }

    const [data, setData] = useState<string[]>([]);

    const pushData = () => {
      const max = data.length + 2;
      const min = max - 2;
      const newData : string[] = [];

      for (let i = min; i < max; i++) {
        newData.push("Test " + i);
      }

      setData([
        ...data,
        ...newData
      ]);
    }

    useIonViewWillEnter(() => {
      pushData();
    });

    const clickLoadMore = async () => {
      pushData();
    }

    return (
        <IonPage>
  
           <HeaderMain pageTitle="Manage Requests" logoHide="hide-logo" onBack={doNothing} />
  
           <IonContent className="dashboard-wrapper manage-tenants-wrapper manage-rents-wrapper" fullscreen>
                <IonGrid className="dashboard-main-grid">
                    <IonRow className="dashboard-main-row">
                        
                        {/* sidebar start  */}
                        <DashboardSidebar path={path} />
                        {/* sidebar end  */}

                        <IonButton onClick={() => clickLoadMore()} expand="block">Load More TESTING </IonButton>

                        <IonList>
                          {data.map((item, index) => {
                            return (
                              <IonItem key={index}>
                                <IonLabel>{item}</IonLabel>
                              </IonItem>
                            )
                          })}
                        </IonList>


                        {/* dashboar posts start */}
                        <IonCol  className="dashboard-posts" size="12" sizeMd="12" sizeLg="3" sizeXl="2">
                            
                        </IonCol>
                        {/* dashboar post end */}
    
                    </IonRow>
                </IonGrid>
              
                <Footer />

            </IonContent>

            <FooterMobile />
      </IonPage>
    );
  };
  
  export default ManageRequests;