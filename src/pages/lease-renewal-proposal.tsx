import React, {useEffect, useRef, useState} from "react";
import { useHistory } from "react-router-dom";
import { RouteComponentProps, Switch, useLocation } from "react-router";
import { FormProvider, useForm, Controller, useFieldArray } from "react-hook-form";
import { ErrorMessage } from '@hookform/error-message';
import { useTranslation } from "react-i18next";

import { 
    IonContent, 
    IonPage,
    IonGrid,
    IonRow,
    IonCol, 
    IonList,
    IonInput, 
    IonLabel, 
    IonButton, 
    IonIcon,
    IonCard,
    IonCardHeader,
    IonCardTitle,
    IonCardContent,
    IonDatetime,
    IonLoading,
    useIonAlert,
    IonAlert,
    useIonViewWillEnter
} from '@ionic/react';

import { close, attachOutline, chevronDown } from "ionicons/icons";

import HeaderMain from '../components/Header-main';
import Footer from '../components/Footer';
import FooterMobile from '../components/Footer-mobile';
import DashboardSidebar from '../components/Dahsboard-sidebar';

import { usePhotoGallery, UserPhoto  } from '../hooks/usePhotoGallery';

import '@ionic/react/css/flex-utils.css';
import '../assets/css/Custom.css';
import '../assets/css/Responsive.css';
import { Routes } from "../App";

import { globalConst } from "../constants";
import axios from "axios";
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';
import { Console } from "console";
axios.defaults.baseURL = globalConst.baseURL;
axios.defaults.headers.post['Content-Type'] = globalConst.ContentType;
axios.defaults.headers.post['Access-Control-Allow-Origin'] = globalConst.AllowOrigin;
axios.defaults.headers.post['Accept'] = globalConst.Accept;
axios.defaults.timeout = globalConst.timeout;

interface PageProps extends RouteComponentProps {path: string; history: any; match: any; location: any;}

const LeaseRenewalProposal: React.FC<PageProps> = ({ path, history, match, location }) => {

    const { t } = useTranslation();
    const [StorageKey, setStorageKey] = useState('');
    const [UserTypeData, setUserTypeData] = useState<any>({'id': '', 'name': '', 'lease_code': ''});

    const locationSHI = useLocation();

    let StoKey = 'photos';
    if (locationSHI.state) {
        if (location.state.filestoragekey !== null) {
            StoKey = location.state.filestoragekey;
        }
    }

    const { deletePhoto, photos, takePhoto } = usePhotoGallery(StoKey);
    const [photoToDelete, setPhotoToDelete] = useState<UserPhoto >();
    const [filepath,setFilepath] = useState();

    const [locPath, setlocPath] = useState('');
    const contentRef = useRef<HTMLIonContentElement | null>(null);
    const [showLoading, setShowLoading] = useState(true);

    const [pid, setpid] = useState('');
    const [EditLeaseID, setEditLeaseID] = useState('');

    const [showAlert, setShowAlert] = useState(false);
    const [msgAlert, setMsgAlert] = useState('');
    const [headAlert, setHeadAlert] = useState('');
    const [handleDismiss, sethandleDismiss] = useState(false);
    const [exitFileAlert, setExitFileAlert] = useState(false);

    const [selectedDate, setSelectedDate] = useState<string>('');

    const scrollToBottom= () => {
        contentRef.current && contentRef.current.scrollToBottom(500);
    };

    const [generateNewLeaseCodeAlert, setGenerateNewLeaseCodeAlert] = useState(false);

    const methods = useForm();
    const { register, trigger, reset, watch , handleSubmit, control, setValue, getValues, formState: { errors } } = methods;
    const { fields, append, remove } = useFieldArray({ name: 'tenants', control });

    const defaultList = [
        { firstName: "", lastName: "" },
    ];

    const [inputList, setInputList] = useState(defaultList);
    const [FileData, setFileData] = useState([{"file_name": ""}]);

    useIonViewWillEnter(() => {
        if (localStorage.getItem('loginState')!='1') {
            history.push(`${Routes.Home}`);
        } else {
            if ((localStorage.getItem('user_type_id')=='1') || (localStorage.getItem('user_type_id')=='2') || ((localStorage.getItem('user_type_id')=='3') && (localStorage.getItem('manage_propertys')=='1'))) {
                //loadConfigData();
            } else {
                history.push(`${Routes.UserDashboard}`);
            }
        }
    });

    function getRandomString(length) {
        var randomChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        var result = '';
        for ( var i = 0; i < length; i++ ) {
            result += randomChars.charAt(Math.floor(Math.random() * randomChars.length));
        }
        return result;
    }

    const renderList = () => {
        return inputList.map((x, i) => {
            return (
                <div key={i} className="tenant-lease-details-info tenants-info">
                    <div className="tenant-title-wrap">
                        <h4>{t('Tenants-Lease-Tenant')} {i + 1}</h4>
                    </div>
                    <IonRow>
                        <IonCol size="12" sizeMd="6" sizeLg="6" sizeXl="6">
                            <IonLabel>{t('Tenants-Lease-First-Name')}*</IonLabel>
                            <IonInput readonly mode="md" type="text" value={x.firstName} {...register(`tenants.${i}.firstName`)} ></IonInput>
                            </IonCol>
                        <IonCol size="12" sizeMd="6" sizeLg="6" sizeXl="6">
                            <IonLabel>{t('Tenants-Lease-Last-Name')}*</IonLabel>
                            <IonInput readonly mode="md" type="text"value={x.lastName} {...register(`tenants.${i}.lastName`)} ></IonInput>
                        </IonCol>
                    </IonRow>
                </div>
            );
        })
    }

    useEffect(() => {
        if ((localStorage.getItem('user_type_id')!==null)) {
            setUserTypeData({'id': localStorage.getItem('user_type_id'), 'name': localStorage.getItem('user_type_name'), 'lease_code': localStorage.getItem('user_code')});
        }

        console.log('Vivan edit--------');
        console.log(location.state);

        if (locationSHI.state) {
            setShowLoading(false);
            setlocPath(location.state.path);

            if (location.state.leaseid>0) {
                setEditLeaseID(location.state.leaseid);
                loadLeaseData(location.state.leaseid);
            }
        } else {
            showError()
        }
    }, []);

    async function loadLeaseData(leaseID: any) {
        console.log('SHIHISHIHS');
        console.log(leaseID);

        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {

            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            api.get("/propertyleases/"+leaseID+"/"+localStorage.getItem('user_id')).then((resPData) => {
                const retData = JSON.parse(JSON.stringify(resPData)).data;
                console.log('Vivan--------');
                console.log(retData);
                console.log(convert(retData.leaseStartDate))
                console.log(convert(retData.leaseEndDate)) //MMM DD, YYYY

                setpid(retData.property_id);

                const fields = ['floorAptNo', 'leaseCode', 'file_storagekey', 'currentLeasePrice', 'leaseEndDate', 'leaseNewEndDate', 'no_tenants', 'floorPropID', 'floorNo', 'Fdata'];
                fields.forEach(field => {
                    if (field=='no_tenants') {
                        setValue(field, retData['TData'].length)
                        setValue('tenants', retData['TData'])
                        setInputList(retData['TData']);
                    } else if (field=='leaseEndDate') {
                        setValue('leaseStartDate', convert(retData.leaseNewStartDate))
                    } else if (field=='leaseNewEndDate') {
                        setValue('leaseEndDate', convert(retData.leaseNewEndDate))
                    } else if (field=='floorPropID') {
                        setValue('floorPropID', retData['property_id'])
                    } else if (field=='Fdata') {
                        setFileData(retData['FData']);
                    } else if (field=='file_storagekey') {
                        setStorageKey(retData[field]);
                        setValue("oldleaseCode", retData[field])
                    } else if (field=='floorNo') {
                        setValue('floorNo', retData['floorsno'])
                    } else {
                        setValue(field, retData[field])
                    }
                });
            })
            .catch((error) => {
                alert("Error Get Data!");
            })
        })
        .catch((error) => {
          alert("Token not Get!");
        })
    }

    function convert(str) {
        var date = new Date(str),
          mnth = ("0" + (date.getMonth() + 1)).slice(-2),
          day = ("0" + date.getDate()).slice(-2);
        return [date.getFullYear(), mnth, day].join("-");
    }

    const showError = (): void => {
        setShowLoading(false);
        
        setMsgAlert(t('Tenants-Lease-Renewal-Error-Msg'));
        setHeadAlert(t('Tenants-Lease-Renewal-Error-Head'));
        sethandleDismiss(true);
        setShowAlert(true);
    }

    const clickWillDismiss = (path, loadFlag) => {
        if (loadFlag) {
            if (pid!='') {
                if (UserTypeData.id == 1) {
                    history.push({
                        pathname: `${Routes.manageLeases}`,
                        state: { path: `${Routes.manageLeases}`, prop_id: pid }
                    });
                } else {
                    history.push({
                        pathname: path,
                        state: { path: path, prop_id: pid }
                    });
                }
            } else {
                history.push(`${Routes.manageProperties}`);
            }
        }
    };

    const doNothing = () => {
		history.goBack();
    }

    const onSubmit = (data: any) => {
        //console.log(EditLeaseID);
        //console.log(data);
        //return false;
        //alert('submit FORM');

        const api = axios.create();
        api.get("/auth/token/refresh").then(res => res)
        .then((response) => {
            
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            api.put("/propertyleases/RenewalLeasePropose/"+EditLeaseID+"/0/"+localStorage.getItem('user_id')).then((resUType) => {

                var lease_start_date = data.leaseStartDate.substring(0, 10);
                var lease_end_date = data.leaseEndDate.substring(0, 10);
                var sYear, sMonth, sDate, eYear, eMonth, eDate;

                [ sYear, sMonth, sDate ] = lease_start_date.split('-');
                [ eYear, eMonth, eDate ] = lease_end_date.split('-');

                const addLeaseData = {
                    "current_user" : localStorage.getItem('user_id'),
                    "lease_code" : data.leaseCode,
                    "oldleaseCode" : data.oldleaseCode,
                    "aptno" : data.floorAptNo,
                    "property_id" : data.floorPropID,
                    "floorsno" : data.floorNo,
                    "lease_price" : data.newLeasePrice,
                    "sYear" : sYear,
                    "sMonth" : sMonth,
                    "sDate" : sDate,
                    "eYear" : eYear,
                    "eMonth" : eMonth,
                    "eDate" : eDate,
                    "lease_start_date" : lease_start_date,
                    "lease_end_date" : lease_end_date,
                    "tenantsData" : data.tenants,
                    "fileData" : photos,
                };

                api.post("/propertyleases/", addLeaseData).then((retData) => {
                    const retDataArr = JSON.parse(JSON.stringify(retData));

                    setMsgAlert(t('Tenants-Lease-Renewal-Success-Msg'));
                    setHeadAlert(t('Tenants-Lease-Renewal-Success-Head'));
                    sethandleDismiss(true);
                    setShowAlert(true);

                    //return false;
                }).catch((error) => {
                    alert("Error found in put Data");
                    console.log(error);
                });

            })
            .catch((error) => {
                alert("Error Delete in Property!");
            })
        })
        .catch((error) => {
          alert("Token not Get!");
        })
    }

    const onClickPhotoData=(photo:any)=>{
        setPhotoToDelete(photo) 
        setFilepath(photo.filepath)
    }

    return (
        <IonPage>
  
           <HeaderMain pageTitle={t('Tenants-Lease-Renewal-Title')} logoHide="hide-logo" onBack={doNothing} />
  
           <IonContent className="dashboard-wrapper lease-info-wrapper" ref={contentRef} scrollEvents={true} fullscreen>
           <IonLoading
                    isOpen={showLoading}
                    onDidDismiss={() => setShowLoading(false)}
                    message={'Loading...'}
                />
    
                <IonAlert
                    isOpen={showAlert}
                    onDidDismiss={() => setShowAlert(false)}
                    onWillDismiss={() => clickWillDismiss(Routes.manageTenants, true)}
                    cssClass='orange-alert'
                    mode='md'
                    header={headAlert}
                    message={msgAlert}
                    buttons={['Close']}
                />

                <IonAlert
                    isOpen={exitFileAlert}
                    onDidDismiss={() => setExitFileAlert(false)}
                    cssClass='red-alert'
                    mode='md'
                    header={t('General-ExitFile')}
                    message={t('Tenants-Lease-Exit-Msg')}
                    buttons={[
                        {
                            text: 'Yes',
                            cssClass: 'btn-secondary',
                            handler: () => {
                                clickWillDismiss(Routes.manageTenants, true);
                                console.log('Exit File Okay');
                            }
                        },
                        {
                            text: 'No',
                            role: 'cancel',
                            cssClass: 'btn-outline',
                            handler: () => {
                                console.log('Exit File Cancel');
                            }
                        }
                        
                    ]}
                />

                <IonGrid className="dashboard-main-grid">
                    <IonRow className="dashboard-main-row">
                        
                        {/* sidebar start  */}
                        <DashboardSidebar path={locPath} />
                        {/* sidebar end  */}


                        {/* dashboar content start */}
                        <IonCol className="dashboard-content" size="12" sizeMd="12" sizeLg="6" sizeXl="8">
                            <IonRow className="apartment-lease-code-info">
                                <IonCol className="apartment-number" size="4" sizeMd="4" sizeLg="4" sizeXl="3">
                                    <IonLabel>{t('Tenants-Lease-Apartment')} #</IonLabel>
                                    <IonInput mode="md" type="text" {...register('floorAptNo')} readonly />

                                    <IonInput class="hide-input" mode="md" type="text" readonly {...register('floorPropID')} />
                                    <IonInput class="hide-input" mode="md" type="text" readonly {...register('floorNo')} />
                                </IonCol>
                                <IonCol className="lease-code-info" size="8" sizeMd="8" sizeLg="7" sizeXl="6">
                                    <div className="lease-code">
                                        <IonLabel>{t('Tenants-Lease-Lease-Code')}</IonLabel>
                                        <IonInput mode="md" type="text" readonly {...register('leaseCode')} />

                                        <IonInput class="hide-input" mode="md" type="text" readonly {...register('oldleaseCode')} />
                                    </div>

                                    {UserTypeData.id==1 ? '' :
                                        <IonButton 
                                            fill="solid" 
                                            shape="round"
                                            onClick={() =>
                                                setGenerateNewLeaseCodeAlert(true)
                                            }
                                        >
                                            {t('Tenants-Lease-Generate-Code')}
                                        </IonButton>
                                    }

                                    <IonAlert
                                        isOpen={generateNewLeaseCodeAlert}
                                        onDidDismiss={() => setGenerateNewLeaseCodeAlert(false)}
                                        cssClass='orange-alert'
                                        mode='md'
                                        header={t('Tenants-Lease-New-Code-Head')}
                                        message={t('Tenants-Lease-New-Code-Msg')}
                                        buttons={[
                                            {
                                                text: 'Yes',
                                                cssClass: 'btn-outline',
                                                handler: () => {
                                                    setValue('leaseCode', getRandomString(12));
                                                    console.log('Confirm Okay');
                                                }
                                            },
                                            {
                                                text: 'No',
                                                role: 'cancel',
                                                cssClass: 'btn-outline',
                                                handler: blah => {
                                                    console.log('Confirm Cancel: blah');
                                                }
                                            }
                                            
                                        ]}
                                    />
                                </IonCol>
                            </IonRow>

                            <form id="lease_form" onSubmit={handleSubmit(onSubmit)}>
                            <IonCard className="tenant-lease-details-card">
                                <IonCardHeader>
                                    <IonCardTitle>{t('Tenants-Lease-Tenant-Details')}</IonCardTitle>
                                </IonCardHeader>

                                <IonCardContent>
                                    {renderList()}
                                </IonCardContent>
                            </IonCard>

                            <IonCard className="tenant-lease-details-card">
                                <IonCardHeader>
                                    <IonCardTitle>{t('Tenants-Lease-Details')}</IonCardTitle>
                                </IonCardHeader>

                                <IonCardContent>
                                    <div className="tenant-lease-details-info lease-info">
                                        <div>
                                            <IonLabel>{t('Tenants-Lease-no-Tenants-Apartment')}</IonLabel>
                                            <IonInput readonly className="width-25" mode="md" type="number" value={inputList.length} {...register('numberOfTenants')} ></IonInput>
                                        </div>
                                        <div>
                                            <IonLabel>{t('Tenants-Lease-Current-Lease-price')}</IonLabel>
                                            <IonInput readonly className="width-50" mode="md" type="number" {...register('currentLeasePrice')} />
                                        </div>
                                        <div>
                                            <IonLabel>{t('Tenants-Lease-New-Lease-price')}</IonLabel>
                                            <IonInput className="width-50" mode="md" type="number" {...register('newLeasePrice', {
                                                            required: t('Tenants-Lease-New-Lease-price-msg')
                                                        })} />
                                            <ErrorMessage
                                                errors={errors}
                                                name="newLeasePrice"
                                                as={<div className="error-message" style={{ color: 'red' }} />}
                                            />
                                        </div>
                                        <div>
                                            <IonLabel>{t('Tenants-Lease-Start-Date')}*</IonLabel>
                                            <div className="date-picker width-50">
                                                <IonDatetime displayFormat="MMM DD, YYYY" placeholder="Select Date" {...register('leaseStartDate', {
                                                            required: t('Tenants-Lease-Start-Date-msg')
                                                        })} />
                                                <IonIcon icon="assets/images/calendar-icon.svg" />
                                                <ErrorMessage
                                                    errors={errors}
                                                    name="leaseStartDate"
                                                    as={<div className="error-message" style={{ color: 'red' }} />}
                                                />
                                            </div>
                                        </div>
                                        <div>
                                            <IonLabel>{t('Tenants-Lease-End-Date')}*</IonLabel>
                                            <div className="date-picker width-50">
                                                <IonDatetime displayFormat="MMM DD, YYYY" placeholder="Select Date" {...register('leaseEndDate', {
                                                            required: t('Tenants-Lease-End-Date-msg')
                                                        })} />
                                                <IonIcon icon="assets/images/calendar-icon.svg" />
                                                <ErrorMessage
                                                    errors={errors}
                                                    name="leaseEndDate"
                                                    as={<div className="error-message" style={{ color: 'red' }} />}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="lease-scan-block">
                                        <IonLabel>{t('Tenants-Lease-Scans')}</IonLabel>

                                        {photos.map((photo, index) => (
                                            <div className="uploaded-file">
                                                <p className="uploaded-file-name read-only">{index + 1}. {photo.filepath}</p>
                                                <IonButton fill="clear" onClick={() => onClickPhotoData(photo)} >
                                                    <IonIcon icon={close} />
                                                </IonButton>
                                            </div>
                                        ))}

                                        <div className="upload-photo-block">
                                            <IonButton className="take-photo-btn" fill="solid" shape="round" onClick={() => takePhoto()}>
                                                <IonIcon icon="assets/images/add-photo.svg" />
                                            </IonButton>
                                        </div>
                                    </div>
                                </IonCardContent>
                            </IonCard>

                            <IonRow className="ion-justify-content-center">
                                <IonCol className="ion-text-center">
                                    <IonButton className="secondary-button submit-new-lease-btn" type="submit" fill="outline" shape="round">{t('General-Submit')}</IonButton>
                                    <IonButton className="exit-file-btn" onClick={() => setExitFileAlert(true) } fill="solid" shape="round">{t('General-ExitFile')}</IonButton>
                                </IonCol>
                            </IonRow>
                            </form>

                        </IonCol>
                        {/* dashboar content end */}


                        {/* dashboar posts start */}
                        <IonCol  className="dashboard-posts" size="12" sizeMd="12" sizeLg="3" sizeXl="2">
                            
                        </IonCol>
                        {/* dashboar post end */}
    
                    </IonRow>
                </IonGrid>

                <IonAlert
                    isOpen={!!photoToDelete}
                    onDidDismiss={() => setPhotoToDelete(undefined)}
                    cssClass='red-alert'
                    mode='md'
                    header={t('Tenants-Lease-Remove-Image')}
                    message={t('Tenants-Lease-Remove-Image-msg')}
                    buttons={[{
                                text: 'Remove',
                                cssClass: 'btn-secondary',
                                //role: 'destructive',
                                ///icon: trash,
                                handler: () => {
                                if (photoToDelete) {
                                    deletePhoto(photoToDelete);
                                    setPhotoToDelete(undefined);
                                }
                                }
                            }, {
                                text: 'Cancel',
                                cssClass: 'btn-primary',//btn-outline
                                //icon: close,
                                //role: 'cancel'
                            }]}
                />

                <IonButton className="scroll-to-bottom-btn" onClick={scrollToBottom} fill="clear">
                    <IonIcon icon="assets/images/double-arrow-down.svg" />
                </IonButton>
              
                <Footer />

            </IonContent>

            <FooterMobile />
      </IonPage>
            
        
        
    );
};

export default LeaseRenewalProposal;