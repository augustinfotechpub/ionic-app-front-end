import React, {useRef, useState, useEffect} from "react";
import { useHistory, useLocation } from 'react-router-dom';
import { useTranslation } from "react-i18next";

import { 
    IonContent, 
    IonPage, 
    IonGrid, 
    IonRow, 
    IonCol, 
    IonLabel,
    IonInput,
    IonButton,
    IonLoading,
    IonAlert,
    useIonViewWillEnter
} from '@ionic/react';

import HeaderMain from '../components/Header-main';
import Footer from '../components/Footer';
import FooterMobile from '../components/Footer-mobile';
import DashboardSidebar from '../components/Dahsboard-sidebar';

import '../assets/css/Custom.css';
import '../assets/css/Responsive.css';
import { Routes } from '../App';

import { FormProvider, useForm } from "react-hook-form";
import { ErrorMessage } from '@hookform/error-message';

import { globalConst } from "../constants";
import axios from "axios";
axios.defaults.baseURL = globalConst.baseURL;
axios.defaults.headers.post['Content-Type'] = globalConst.ContentType;
axios.defaults.headers.post['Access-Control-Allow-Origin'] = globalConst.AllowOrigin;
axios.defaults.headers.post['Accept'] = globalConst.Accept;
axios.defaults.timeout = globalConst.timeout;

const PersonalInfo: React.FC<{ path: string }> = ({path}) => {

    const { t } = useTranslation();
    let history = useHistory();

    const [LeaseInfo, setLeaseInfo] = useState<Array<any>>([{'user_code': '', 'user_prop_name': '', 'user_prop_suffix': '', 'disp_name': ''}]);

    const [UserTypeData, setUserTypeData] = useState<any>({'id': '', 'name': '', 'lease_code': ''});

    const methods = useForm();
    const { register, trigger, handleSubmit, getValues, setValue, formState: { errors } } = methods;

    const [UpdateUserAlert, setUpdateUserAlert] = useState(false);

    const [showLoading, setShowLoading] = useState(true);
    const [showAlert, setShowAlert] = useState(false);
    const [msgAlert, setMsgAlert] = useState('');
    const [headAlert, setHeadAlert] = useState('');
    const [GoMyaccountPage, setGoMyaccountPage] = useState(true);

    const [filteredSearch, setFilteredSearch] = useState([
        {
            post_id: "",
            conv_subject: "",
            property_id: "",
            floorsno: "",
            aptno: "",
            user_code:"",
            id:"",
            name:"",
            role:"",
            val: "",
            isChecked: false
        }])

    useIonViewWillEnter(() => {
        if (localStorage.getItem('loginState')!='1') {
            history.push(`${Routes.Home}`);
        }

        if ((localStorage.getItem('user_id')!==null)) {

            //alert(localStorage.getItem('user_management_id'));

            setShowLoading(false);
            if ((localStorage.getItem('user_type_id')=='1') && (localStorage.getItem('user_code')=='null')) {
                history.push(`${Routes.UserDashboard}`);
            } else if ((localStorage.getItem('user_type_id')=='1') && (localStorage.getItem('user_code')=='')) {
                history.push(`${Routes.UserDashboard}`);
            } else {
                if ((localStorage.getItem('user_type_id')!==null)) {
                    setUserTypeData({'id': localStorage.getItem('user_type_id'), 'name': localStorage.getItem('user_type_name'), 'lease_code': localStorage.getItem('user_code')});
                }
                loadUserData(localStorage.getItem('user_id'));
            }
        } else {
            showError()
        }
    });

    useEffect(() => {

    }, []);

    const showError = (): void => {
        setShowLoading(false);
        
        setMsgAlert(t('Act-Msg'));
        setHeadAlert(t('Act-Head'));
        setShowAlert(true);
    }

    const clickWillDismiss = (dismissFlag) => {
        if (GoMyaccountPage) {
            history.push(`${Routes.myAccount}`);
        }
        //if (dismissFlag) {
        //}
    };

    async function loadUserData(userID: any) {
        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {

            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            api.get("/auth/getByID/"+userID).then((resPData) => {
                const retData = JSON.parse(JSON.stringify(resPData)).data;

                //console.log('User Active Lease Information');
                //console.log(retData.user_code);
                //console.log(retData.propManagementID);
                //console.log(retData.propManagementName);

                const fields = ['firstname', 'lastname', 'phoneno', 'id', 'user_code', 'stripe_api_key'];
                fields.forEach(field => {
                    if (field=='id') {
                        setValue('user_id', retData[field])
                    } else {
                        setValue(field, retData[field])
                    }
                });

                setFilteredSearch([{
                    post_id: '',
                    conv_subject: t('Act-RequestLease-Head') + " " + retData.user_code,
                    property_id: "",
                    floorsno: "",
                    aptno: "",
                    user_code:"",
                    id: retData.propManagementID,
                    name:retData.propManagementName,
                    role:"Tenant",
                    val: "",
                    isChecked: true
                }])

                loadUserLeaseInfo(localStorage.getItem('user_id'), localStorage.getItem('user_type_id'));
            })
            .catch((error) => {
                alert("Error Get Data!");
            })
        })
        .catch((error) => {
          alert("Token not Get!");
        })
    }

    async function loadUserLeaseInfo(userID: any, userTypeID: any) {
        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {

            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            api.get("/auth/UserLeaseInfo/"+userID+"/"+userTypeID).then((resPData) => {
                const retData = JSON.parse(JSON.stringify(resPData)).data;

                //console.log('User Lease Information');
                //console.log(retData);
                setLeaseInfo(retData.retData);
                //setAccessInfo(retData.retData);
            })
            .catch((error) => {
                alert("Error Get Data!");
            })
        })
        .catch((error) => {
          alert("Token not Get!");
        })
    }

    const onSubmit = (data: any) => {
        const api = axios.create();
        api.get("/auth/token/refresh").then((response) => {
    
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            var userData: any;
            if ((UserTypeData.id=='1') || (UserTypeData.id=='3')) {
                userData = {
                    user_id: data.user_id,
                    firstname: data.firstname,
                    lastname: data.lastname,
                    phoneno: data.phoneno,
                    user_code: data.user_code,
                    user_type_id: UserTypeData.id,
                };
            } else{
                userData = {
                    user_id: data.user_id,
                    firstname: data.firstname,
                    lastname: data.lastname,
                    phoneno: data.phoneno,
                    stripe_api_key: data.stripe_api_key,
                    user_type_id: UserTypeData.id,
                };
            }

            console.log(userData);

            api.put("/auth/updateUser", userData).then((resData) => {
                const resReturnData = JSON.parse(JSON.stringify(resData)).data;

                if (resReturnData.returnFlag=='error'){
                    setShowLoading(false);
                    setGoMyaccountPage(false);
                    setMsgAlert(resReturnData.message);
                    setHeadAlert(t('Act-Head'));
                    setShowAlert(true);
                } else {
                    localStorage.setItem('user_firstname', resReturnData.firstname);
                    localStorage.setItem('user_lastname', resReturnData.lastname);
                    localStorage.setItem('user_phoneno', resReturnData.phoneno);
                    
                    //Tenant
                    if (UserTypeData.id=='1') {
                        localStorage.setItem('user_code', resReturnData.user_code);
                        localStorage.setItem('sel_prop_id', resReturnData.user_prop_id);
                        localStorage.setItem('sel_prop_name', resReturnData.user_prop_name);
                        localStorage.setItem('sel_prop_suffix', resReturnData.user_prop_suffix);
                    }

                    //Staff
                    if (UserTypeData.id=='3') {
                        localStorage.setItem('user_code', resReturnData.user_code);
                        localStorage.setItem('user_management_id', resReturnData.user_management_id);

                        if (resReturnData.FeatureAccess.manage_posts=='1') {
                            localStorage.setItem('manage_posts', '1');
                        } else {
                            localStorage.setItem('manage_posts', '0');
                        }

                        if (resReturnData.FeatureAccess.manage_propertys=='1') {
                            localStorage.setItem('manage_propertys', '1');
                        } else {
                            localStorage.setItem('manage_propertys', '0');
                        }

                        if (resReturnData.FeatureAccess.manage_rents=='1') {
                            localStorage.setItem('manage_rents', '1');
                        } else {
                            localStorage.setItem('manage_rents', '0');
                        }
                        
                        //console.log(resReturnData.FeatureAccess.manage_posts);
                        //console.log(resReturnData.FeatureAccess);
                    }
            
                    setUpdateUserAlert(true);
                }
            })
            .catch((error) => {
                alert("Error Update User Lease Code!");
            })
        })
        .catch((error) => {
            alert("Token not Get!");
        })
    };

    const doNothing = () => {
        history.goBack();
    }
   
    const ClickRequestLease = async () => {

        const api = axios.create();
        api.get("/auth/token/refresh").then(res => res)
        .then((response) => {
            
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + JSON.parse(JSON.stringify(response)).data.accessCode;
            const api = axios.create();

            const ConversationData = {
                "current_user" : localStorage.getItem('user_id'),
                "UserID" : localStorage.getItem('user_id'),
                "UserTypeID" : localStorage.getItem('user_type_id'),
                "SearchSelected" : filteredSearch,
            };
    
            api.post("/messaging/startconversion", ConversationData).then((retData) => {
                const retDataArr = JSON.parse(JSON.stringify(retData)).data;
                console.log(retDataArr);

                history.push({
                    pathname: `${Routes.chat}`,
                    state: { path: `${Routes.chat}`, chat_id: retDataArr.InsertID, chat_msg: t('Act-RequestLease-Text')}
                });
               
            }).catch((error) => {
                //alert("Error found in put Data");
                console.log(error);
            });
        })
        .catch((error) => {
            alert("Token not Get!");
        })
    };

    return (
        <IonPage>
  
           <HeaderMain pageTitle={t('Act-PersonalInfo-Title')} logoHide="hide-logo" onBack={doNothing} />
  
           <IonContent className="dashboard-wrapper manage-tenants-wrapper manage-rents-wrapper" fullscreen>
                <IonLoading
                    isOpen={showLoading}
                    onDidDismiss={() => setShowLoading(false)}
                    message={'Loading...'}
                />

                <IonAlert
                    isOpen={showAlert}
                    onDidDismiss={() => setShowAlert(false)}
                    onWillDismiss={() => clickWillDismiss(true)}
                    cssClass='orange-alert'
                    mode='md'
                    header={headAlert}
                    message={msgAlert}
                    buttons={['Close']}
                />

                <IonAlert
                    isOpen={UpdateUserAlert}
                    onDidDismiss={() => setUpdateUserAlert(false)}
                    cssClass='orange-alert'
                    mode='md'
                    header={t('Act-PersonalInfo-Update-Head')}
                    message={t('Act-PersonalInfo-Update-Msg')}
                    buttons={[
                        {
                            text: 'Close',
                            role: 'cancel',
                            cssClass: 'btn-primary',
                            handler: () => {
                                window.location.assign(`${Routes.myAccount}`);
                            }
                        }
                        
                    ]}
                />

                <IonGrid className="dashboard-main-grid">
                    <IonRow className="dashboard-main-row">
                        
                        {/* sidebar start  */}
                        <DashboardSidebar path={path} />
                        {/* sidebar end  */}


                        {/* dashboar content start */}
                        <IonCol className="dashboard-content my-account-content" size="12" sizeMd="12" sizeLg="6" sizeXl="8">
                            <div className="dashboard-content-inner">
                                <FormProvider {...methods}>
                                <form onSubmit={handleSubmit(onSubmit)}>
                                    <IonGrid>
                                        <IonRow>
                                        <IonCol size="12" sizeMd="6" className="email-field">
                                        <div className="form-field">
                                            <IonLabel className="form-lable">{t('Act-FirstName')}*</IonLabel>
                                            <IonInput
                                                mode="md"
                                                type="text"   
                                                class="hide-input"
                                                {...register('user_id')}
                                            />
                                            <IonInput
                                                mode="md"
                                                type="text"   
                                                {...register('firstname', {
                                                    required: t('Act-FirstName-Error')
                                                })}
                                            />
                                            <ErrorMessage
                                                errors={errors}
                                                name="firstname"
                                                as={<div className="error-message" style={{ color: 'red' }} />}
                                            />
                                        </div>

                                        <div className="form-field">
                                            <IonLabel className="form-lable">{t('Act-LastName')}*</IonLabel>
                                            <IonInput
                                                mode="md"
                                                type="text" 
                                                {...register('lastname', {
                                                    required: t('Act-LastName-Error')
                                                })}
                                            />
                                            <ErrorMessage
                                                errors={errors}
                                                name="lastname"
                                                as={<div className="error-message" style={{ color: 'red' }} />}
                                            />
                                        </div>

                                        <div className="form-field">
                                            <IonLabel className="form-lable">{t('Act-Telephone')}:</IonLabel>
                                            <IonInput
                                                mode="md"
                                                type="number" 
                                                {...register('phoneno', {
                                                    maxLength: { value: 12, message: t('Act-Telephone-Error') }
                                                })}
                                            />
                                            <ErrorMessage
                                                errors={errors}
                                                name="phoneno"
                                                as={<div className="error-message" style={{ color: 'red' }} />}
                                            />
                                        </div>

                                        <div className={(UserTypeData.id == '1' || UserTypeData.id == '3') ? ('form-field') : ('hide-input')}>
                                            <div className="label-with-tooltip">
                                                <IonLabel className="form-lable">{(UserTypeData.id == '1') ? (t('Act-Lease')) : (t('Act-Access'))} {t('Act-Code')}:</IonLabel>
                                                <div className="tooltip" title={(UserTypeData.id == '1') ? (t('Act-AddLease')) : (t('Act-AddAccess'))}>!</div>
                                            </div>
                                            <IonInput
                                                mode="md"
                                                type="text"
                                                {...register('user_code', {
                                                    maxLength: { value: 12, message: t('Act-Code-Error') }
                                                })}
                                            />
                                            <ErrorMessage
                                                errors={errors}
                                                name="user_code"
                                                as={<div className="error-message" style={{ color: 'red' }} />}
                                            />
                                            <br /><br />
                                            <div className="label-with-tooltip">
                                                <IonLabel className="form-lable"><b>{(UserTypeData.id == '1') ? (t('Act-PreviousLease')) : ('')}</b></IonLabel>
                                            </div>
                                            {LeaseInfo.map((Pitem, Pindex) => (
                                                <div className="label-with-tooltip">
                                                    <IonLabel className="form-lable">{Pitem.user_code} {Pitem.disp_name}</IonLabel>
                                                </div>
                                            ))}
                                        </div>

                                        <div className={UserTypeData.id == '2' ? ('form-field') : ('hide-input')}>
                                            <div className="label-with-tooltip">
                                                <IonLabel className="form-lable">{t('Act-StripeAPIKey')}:*</IonLabel>
                                            </div>
                                            <IonInput
                                                mode="md"
                                                type="text" 
                                                {...register('stripe_api_key')}
                                            />
                                            <ErrorMessage
                                                errors={errors}
                                                name="stripe_api_key"
                                                as={<div className="error-message" style={{ color: 'red' }} />}
                                            />
                                        </div>

                                        {(UserTypeData.id == '1') ? (
                                            <div className="form-field">
                                                <IonButton onClick={() => ClickRequestLease()} className="secondary-button submit-new-lease-btn" shape="round" fill="solid">
                                                    {t('Act-RequestLease')}
                                                </IonButton>
                                            </div>
                                        ) : ('')}

                                        <div className="form-field">
                                            <IonButton className="secondary-button submit-new-lease-btn" type="submit" shape="round" fill="outline">
                                                {t('General-Update')}
                                            </IonButton>
                                            <IonButton className="exit-file-btn" fill="solid" shape="round" routerLink={Routes.myAccount}>
                                                {t('General-Cancel')}
                                            </IonButton>
                                        </div>

                                        </IonCol>
                                        </IonRow>
                                    </IonGrid>
                                </form>
                                </FormProvider>
                            </div>

                        </IonCol>
                        {/* dashboar content end */}


                        {/* dashboar posts start */}
                        <IonCol  className="dashboard-posts" size="12" sizeMd="12" sizeLg="3" sizeXl="2">
                            
                        </IonCol>
                        {/* dashboar post end */}
    
                    </IonRow>
                </IonGrid>
              
                <Footer />

            </IonContent>

            <FooterMobile />
      </IonPage>
    );
  };
  
  export default PersonalInfo;